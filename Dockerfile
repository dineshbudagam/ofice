FROM java:8
EXPOSE 9081
ADD /target/manage-login-service.jar manage-login-service.jar
ENTRYPOINT ["java","-jar","/manage-login-service.jar"]