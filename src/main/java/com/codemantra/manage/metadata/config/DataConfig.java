/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
22-05-2017			v1.0       	   Bharath Prasanna	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@Configuration
@ComponentScan(basePackages = "com.codemantra.manage")
@PropertySource("classpath:manage_resources.properties")

public class DataConfig {

	@Value("${spring.data.mongodb.host}")
	private String mongoDBServer ;
	
	@Value("${spring.data.mongodb.database}")
	private String mongoDBName ;
	
	@Value("${spring.data.mongodb.port}")
	private String mongoDBPort ;
	
	@Value("${spring.data.mongodb.username}")
	private String mongoDBUserName ;
	
	@Value("${spring.data.mongodb.password}")
	private String mongoDBPassword ;
	
	
	public static String cdcmongoDBServer;
	
	public static String cdcmongoDBName;
	
	public static String cdcmongoDBPort;
	
	public static String cdcmongoDBUserName;
	
	public static String cdcmongoDBPassword;

    public static String getCdcmongoDBServer() {
		return cdcmongoDBServer;
	}
    
    @Value("${spring.data.cdcmongodb.host}")
    public void setCdcmongoDBServer(String cdcmongoDBServer) {
		DataConfig.cdcmongoDBServer = cdcmongoDBServer;
	}

	public static String getCdcmongoDBName() {
		return cdcmongoDBName;
	}
	
	@Value("${spring.data.cdcmongodb.database}")
	public void setCdcmongoDBName(String cdcmongoDBName) {
		DataConfig.cdcmongoDBName = cdcmongoDBName;
	}

	public static String getCdcmongoDBPort() {
		return cdcmongoDBPort;
	}
	
	@Value("${spring.data.cdcmongodb.port}")
	public void setCdcmongoDBPort(String cdcmongoDBPort) {
		DataConfig.cdcmongoDBPort = cdcmongoDBPort;
	}

	public static String getCdcmongoDBUserName() {
		return cdcmongoDBUserName;
	}
	
	@Value("${spring.data.cdcmongodb.username}")
	public void setCdcmongoDBUserName(String cdcmongoDBUserName) {
		DataConfig.cdcmongoDBUserName = cdcmongoDBUserName;
	}

	public static String getCdcmongoDBPassword() {
		return cdcmongoDBPassword;
	}
	
	@Value("${spring.data.cdcmongodb.password}")
	public void setCdcmongoDBPassword(String cdcmongoDBPassword) {
		DataConfig.cdcmongoDBPassword = cdcmongoDBPassword;
	}

	@Primary
	public MongoDbFactory mongoDbFactory() throws Exception {
        
       MongoCredential mongoCredential = MongoCredential.createScramSha1Credential(mongoDBUserName, mongoDBName,
    		   mongoDBPassword.toCharArray());
       MongoClient mongoClient = new MongoClient(new ServerAddress(mongoDBServer,Integer.valueOf(mongoDBPort)),Arrays.asList(mongoCredential));
        return new SimpleMongoDbFactory(mongoClient, mongoDBName);
    }
    
    @Primary
    @Bean(autowire = Autowire.BY_NAME, name = "primaryMongoTemplate")
    public MongoTemplate primaryMongoTemplate() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
        return mongoTemplate;
    }
    
    public static  MongoDbFactory cdcmongoDbFactory() throws Exception {
       MongoCredential cdcmongoCredential = MongoCredential.createScramSha1Credential(DataConfig.getCdcmongoDBUserName(), DataConfig.getCdcmongoDBName(),
    		   DataConfig.getCdcmongoDBPassword().toCharArray());
       MongoClient cdcmongoClient = new MongoClient(new ServerAddress(DataConfig.getCdcmongoDBServer(),Integer.valueOf(DataConfig.getCdcmongoDBPort())),Arrays.asList(cdcmongoCredential));
        return new SimpleMongoDbFactory(cdcmongoClient, DataConfig.getCdcmongoDBName());
    }
    
    @Bean(autowire = Autowire.BY_NAME, name = "cdcmongoTemplate")
    public static MongoTemplate cdcmongoTemplate() throws Exception {
        MongoTemplate cdcmongoTemplate = new MongoTemplate(cdcmongoDbFactory());
        return cdcmongoTemplate;
    }
    
    
    @Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}