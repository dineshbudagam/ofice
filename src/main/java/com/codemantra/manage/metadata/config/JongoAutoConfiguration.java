/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.metadata.config;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.ReadConcern;
import org.jongo.Jongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * {@link org.springframework.boot.autoconfigure.EnableAutoConfiguration Auto configuration} for {@code Jongo} support.
 *
 * @author Cyril Schumacher
 * @version 1.0
 * @since 2014-12-23
 */
@Configuration
@ConditionalOnClass({Jongo.class})
public class JongoAutoConfiguration {

    //<editor-fold desc="Fields section.">

    /**
     * Mongo
     */
    @Autowired
    protected MongoDbFactory mongo;

    /**
     * Jongo properties.
     */
    @Autowired
    protected MongoProperties properties;

    //</editor-fold>

    //<editor-fold desc="Methods section.">

    /**
     * Create a instance of the {@code Jongo} class.
     *
     * @return The instance of {@code Jongo} class.
     */
    @Bean
    public Jongo jongo() {
       
        final DB database = mongo.getDb();
        //database.setReadConcern(ReadConcern.MAJORITY);//Updated
        return new Jongo(database);
    }

    //</editor-fold>

}