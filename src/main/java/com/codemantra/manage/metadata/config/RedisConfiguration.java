package com.codemantra.manage.metadata.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration

public class RedisConfiguration extends CachingConfigurerSupport{

	@Value("${spring.redis.host}")
    private String redisServer;

	@Value("${spring.redis.port}")
    private String redisPort;
	
	@Bean
    public JedisConnectionFactory jedisConnectionFactory()

    {

        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();

        jedisConnectionFactory.setHostName(redisServer);

        jedisConnectionFactory.setPort(Integer.valueOf(redisPort));

        return jedisConnectionFactory;

    }

    @Bean

    public RedisTemplate<Object, Object> redisTemplate()

    {

        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<Object, Object>();

        redisTemplate.setConnectionFactory(jedisConnectionFactory());

        redisTemplate.setExposeConnection(true);

        return redisTemplate;

    }

    @Bean

    public RedisCacheManager cacheManager()

    {

        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate());

        redisCacheManager.setTransactionAware(true);

        redisCacheManager.setLoadRemoteCachesOnStartup(true);

        redisCacheManager.setUsePrefix(true);

        return redisCacheManager;

    }

}
