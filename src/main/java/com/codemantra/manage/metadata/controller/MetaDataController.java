/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
08-06-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codemantra.manage.metadata.dto.APIResponse;
import com.codemantra.manage.metadata.dto.APIResponse.ResponseCode;
import com.codemantra.manage.metadata.model.FileObj;
import com.codemantra.manage.metadata.model.MetaDataGroup;
import com.codemantra.manage.metadata.model.MetaDataView;
import com.codemantra.manage.metadata.model.MetaDataXML;
import com.codemantra.manage.metadata.model.Product;
import com.codemantra.manage.metadata.service.MetaDataService;
import com.codemantra.manage.support.tables.request.entities.SupportRequestEntity;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@CrossOrigin
@RestController
@RequestMapping("/manage-metadata-service")
public class MetaDataController<T> {

	private static final Logger logger = LoggerFactory.getLogger(MetaDataController.class);

	@Autowired
	private MetaDataService metaDataService;

	@RequestMapping(value = "/productHierarchies", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getProductHierarchies() {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getAllProductHierarchies();
		} catch (Exception e) {
			logger.error("Error occurred retrieving response getCodeLookUpValues::" + e);
		}

		return response;
	}

	@RequestMapping(value = "/getCreateProductHierarchyFields/{productHierarchyId}", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getCreateProductHierarchyFields(@PathVariable String productHierarchyId) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getCreateProductHierarchyFields(productHierarchyId);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response getProductHierarchyFields::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/getCurrentProductHierarchyFields/{productHierarchyId}", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getCurrentProductHierarchyFields(@PathVariable String productHierarchyId) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getCurrentProductHierarchyFields(productHierarchyId);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response getProductHierarchyFields::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/getCurrentProductHierarchyFields", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> getCurrentProductHierarchyFields(@RequestBody List<String> productHierarchyIds) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getCurrentProductHierarchyFields(productHierarchyIds);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response getProductHierarchyFields::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/getCodeLookUpValues", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getCodeLookUpValues() {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getAllLookUpData();
		} catch (Exception e) {
			logger.error("Error occurred retrieving response getCodeLookUpValues::" + e);
		}

		return response;
	}

	@RequestMapping(value = "getMetadataConfigDetails", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getMetadataConfigDetails() {
		APIResponse<Object> response = new APIResponse<>();

		try {
			response = metaDataService.getMetadataConfigDetails();
		} catch (Exception e) {
			logger.error("Error occurred retrieving response getMetadataConfigDetails::" + e);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveMetaData", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> saveMetaData(@RequestBody Object obj) {
		APIResponse<Object> response = new APIResponse<Object>();
		try {
			Map<String, Object> tempObj = (Map<String, Object>) obj;

			response = metaDataService.saveMetaData(tempObj);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response saveMetaData::" + e);

		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/updateMetaData", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> updateMetaData(@RequestBody Object obj) {
		APIResponse<Object> response = new APIResponse<Object>();

		try {
			Map<String, Object> tempObj = (Map<String, Object>) obj;
			response = metaDataService.updateMetaData(tempObj);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response updateMetaData::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/editMetaData", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> editMetaData(@RequestBody MetaDataView viewMetaData) {
		APIResponse<Object> response = new APIResponse<Object>();

		try {
			response = metaDataService.editMetaData(viewMetaData);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response editMetaData::" + e);
		}
		return response;
	}

	/*
	 * @RequestMapping(value = "/deleteMetaData", method = RequestMethod.POST,
	 * consumes = "application/json")
	 * 
	 * @ResponseBody public APIResponse<Object> deleteMetaData(@RequestBody
	 * MetaDataView viewMetaData) { APIResponse<Object> response = new
	 * APIResponse<Object>();
	 * 
	 * try { response = metaDataService.deleteMetaData(viewMetaData); } catch
	 * (Exception e) { e.printStackTrace(); } return response; }
	 */

	@RequestMapping(value = "/getMetaDataFields", method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	public APIResponse<MetaDataGroup> getMetaDataFields() {
		APIResponse<MetaDataGroup> response = new APIResponse<MetaDataGroup>();
		List<MetaDataGroup> metaDataFields = null;
		try {
			metaDataFields = metaDataService.getMetaDataFields();
		} catch (Exception e) {
			logger.error("Error occurred retrieving response getMetaDataFields::" + e);
		}

		if (metaDataFields != null) {
			response.setCode(ResponseCode.SUCCESS.toString());
			response.setStatus("Success");
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus("No Records found");
		}
		return response;
	}

	@RequestMapping(value = "/customTemplateMData", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> customTemplateMData(@RequestBody MetaDataView custMData) {
		APIResponse<Object> response = new APIResponse<Object>();

		try {
			response = metaDataService.customTemplateMData(custMData.getCustMData());
		} catch (Exception e) {
			logger.error("Error occurred retrieving response customTemplateMData::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/uploadMetaData", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> uploadMetaData(@RequestBody FileObj obj) {
		APIResponse<Object> response = new APIResponse<Object>();
		System.out.println(obj.getPath());
		try {
			response = metaDataService.uploadMetaData(obj);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response uploadMetaData::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/existIsbnCheck/{isbnValue}/", method = RequestMethod.POST)
	@ResponseBody
	public APIResponse<Object> existIsbnCheck(@PathVariable("isbnValue") String isbnValue) {
		APIResponse<Object> response = new APIResponse<Object>();

		try {
			response = metaDataService.existIsbnCheck(isbnValue);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response existIsbnCheck::" + e);
		}

		return response;
	}

	@RequestMapping(value = "/exportMetaDataXml", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> exportMetaDataXml(@RequestBody MetaDataXML metaDataXml) {
		APIResponse<Object> response = new APIResponse<Object>();

		try {
			response = metaDataService.exportMetaDataXml(metaDataXml);
		} catch (Exception e) {
			logger.error("Error occurred retrieving response editMetaData::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/product", method = RequestMethod.POST)
	@ResponseBody
	public APIResponse<Object> saveProduct(@RequestBody Map<Object, Object> productsWithProductHierarchyId,
			@RequestParam("loggedUser") String userId) {
		APIResponse<Object> response = new APIResponse<>();
		try {

			response = metaDataService.saveProduct(productsWithProductHierarchyId, userId);

		} catch (Exception e) {
			logger.error("Error occured while saving product :: saveProduct ::", e);
		}
		return response;
	}

	@RequestMapping(value = "/getFieldsBasedOnAvailabilityCode/{availablilityCode}", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveFieldsBaseOnAC(@PathVariable("availablilityCode") String availCode,
			@RequestParam(value = "metadataId", required = false) String metadataId) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.retrieveFieldsBasedOnAvailCode(availCode, metadataId);
		} catch (Exception e) {
			logger.error("Error occured while getting fields based on Availability Code::", e);
		}
		return response;
	}

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getProducts() {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getProducts();
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getProducts ::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/product/withRelations/{id}/{pHid}/{loggedUser}", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getProducts(@PathVariable("id") String id, @PathVariable("pHid") String pHid,
			@PathVariable("loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getProductWithRelations(id, pHid, loggedUser);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getProducts ::" + e);
		}
		return response;
	}

	@RequestMapping(value = "/product", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> updateProductWithSync(@RequestParam("loggedUser") String userId,
			@RequestBody Map<Object, Product> pro) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			Product product = pro.get(pro.keySet().iterator().next());
			if (product == null) {
				response.setCode(APIResponse.ResponseCode.FAILURE.toString());
				response.setStatusMessage("Product could not be found.");
				throw new NullPointerException("Could not find product in given json");
			}

			response = metaDataService.updateProductWithSync(product, userId);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getProducts ::" + e);
			e.printStackTrace();

		}
		return response;

	}

	// reqFields => parentID, productHierarchyId,SupplementValue,LanguageKey
	@RequestMapping(value = "/getOrderId", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> getOrderId(@RequestBody String reqFields) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getOrderId(reqFields);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getOrderId ::" + e.getMessage());
			e.printStackTrace();

		}
		return response;

	}

	@RequestMapping(value = "/syncProductCreate", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> createProductSync(@RequestBody Map<String, Object> reqFields) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.productSync(reqFields);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response syncProductCreate ::" + e.getMessage());
			e.printStackTrace();
		}
		return response;

	}

	@RequestMapping(value = "/checkForDuplicate", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> checkDuplicate(@RequestBody Map<String, Object> reqFields) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			 response = metaDataService.checkForDuplicate(reqFields);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response syncProductCreate ::" + e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "/retrieve", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> retrieveAll(/*
											 * @RequestParam(required = true, value = "loggedUser") String loggedUser,
											 */
			@RequestBody SupportRequestEntity entity) {
		// logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<Object>();
		Map<String, Object> finalData = null;
		// Status status = null;
		try {
			finalData = metaDataService.getSupportData(entity);
			response.setData(finalData);
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setStatus("Success");
			response.setStatusMessage(("Retrived Successfully..."));

		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(APIResponse.ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage(("Retrive Error..."));
			logger.error("Error occurred retrieving response editMetaData::" + e);

			// logger.error("Exception :: ", e);
		}
		return response;
	}

	@RequestMapping(value = "cascade", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> cascadeFields(// @RequestParam(required = true,
			// value = "loggedUser") String loggedUser,
			@RequestBody SupportRequestEntity entity) {
		// logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<Object>();
		Map<String, Object> finalData = null;
		// Status status = null;
		try {
			finalData = metaDataService.getCascadingData(entity);
			response.setData(finalData);
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setStatus("Success");
			response.setStatusMessage(("Updated Successfully..."));

		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(APIResponse.ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage(("Update Error..."));
			// logger.error("Exception :: ", e);
		}
		return response;
	}

	@RequestMapping(value = "/siblingsData", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> getSiblingsData(@RequestBody Map<String, Object> obj) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getSiblingsData(obj);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSiblingsData ::" + e.getMessage());
			e.printStackTrace();
		}
		return response;

	}

	@RequestMapping(value = "/supplementStatus", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> supplementStatus(@RequestBody Map<String, Object> obj) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getSupplementStatus(obj);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSiblingsData ::" + e.getMessage());
			e.printStackTrace();
		}
		return response;

	}

	@RequestMapping(value = "/manualStageApprove", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> manualStageApprove(@RequestBody Map<String, String> obj) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.updateManualStageApprove(obj);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSiblingsData ::" + e.getMessage());
			e.printStackTrace();
		}
		return response;

	}

	@RequestMapping(value = "/getISBNValue", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getISBNValue() {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getISBNValue();
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getProducts ::", e);
		}
		return response;
	}

	/**
	 * Get List of SupportTables for support table masters
	 */
	@RequestMapping(value = "/supportTables", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getSupportTablesList() {
		APIResponse<Object> response = new APIResponse();
		System.out.println("Request is here supportTables ");
		try {
			response = metaDataService.getSupportTablesList();
		} catch (Exception e) {
			logger.error("Error occured while retrieving support tables ::", e);
		}

		return response;
	}

	@RequestMapping(value = "/getSupportTableMapping", method = RequestMethod.POST)
	@ResponseBody
	public APIResponse<Object> getSupportTableMapping() {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getSupportTableMapping();
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSupportTableMapping ::", e);
		}
		return response;
	}

	// https://nqr9pb.axshare.com/#id=w8vwdh&p=lookup_master_details_view Download
	// Template XLS or XLSX
	@RequestMapping(value = "/generateExcelForSupportTable", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> generateExcelForSupportTable(@RequestBody SupportRequestEntity entity,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			entity.setUserId(loggedUser);
			response = metaDataService.downloadExcelForSupportTable(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSupportTableMapping ::", e);
		}
		return response;
	}

	@RequestMapping(value = "/editRecordForSupportTable", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> editForSupportTable(@RequestBody SupportRequestEntity entity,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			entity.setUserId(loggedUser);
			response = metaDataService.editOneSupportTableRecord(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSupportTableMapping ::", e);
		}
		return response;
	}

	@RequestMapping(value = "/addRecordForSupportTable", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> addForSupportTable(@RequestBody SupportRequestEntity entity,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			entity.setUserId(loggedUser);
			response = metaDataService.addOneSupportTableRecord(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSupportTableMapping ::", e);
		}
		return response;
	}

	// https://nqr9pb.axshare.com/#id=w8vwdh&p=lookup_master_details_view
	@RequestMapping(value = "/viewSearchForSupportTable", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> viewSearchForSupportTable(@RequestBody SupportRequestEntity entity,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			entity.setUserId(loggedUser);
			response = metaDataService.viewSearchForSupportTable(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSupportTableMapping ::", e);
		}
		return response;
	}

	// https://nqr9pb.axshare.com/#id=w8vwdh&p=lookup_master_details_view
	@RequestMapping(value = "/exportSupportTable", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> exportForSupportTable(@RequestBody SupportRequestEntity entity,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			entity.setUserId(loggedUser);
			response = metaDataService.exportSupportTable(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getSupportTableMapping ::", e);
		}
		return response;
	}

	//// https://nqr9pb.axshare.com/#id=oh26xh&p=lookup_master
	@RequestMapping(value = "/getConfigForSupportTable", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> getConfigurationForSupportTable(@RequestBody SupportRequestEntity entity) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.getConfigurationForSupportTable(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getConfigurationForSupportTable ::", e);
		}
		return response;
	}

	@RequestMapping(value = "/activate", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> actiDeactivateSupportConfig(@RequestBody SupportRequestEntity entity,
			@RequestParam("loggedUser") String userId) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			entity.setUserId(userId);
			response = metaDataService.actiDeactivateSupportConfig(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getConfigurationForSupportTable ::", e);
		}
		return response;
	}

	/**
	 * Rest call to lock a Product
	 * 
	 * @param userId
	 * @param metadataId
	 * @author Pavan Kumar Yekabote.
	 */
	@RequestMapping(value = "/lockProduct", method = RequestMethod.GET)
	public APIResponse<Object> lockProduct(@RequestParam("loggedUser") String userId,
			@RequestParam("metadataId") String productId, @RequestParam("productHierarchyId") String pHId) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.lockProduct(userId, pHId, productId);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response lockProduct ::", e);
			response.setCode(APIResponse.ResponseCode.ERROR.toString());
			response.setData(null);
			response.setStatusMessage("Internal server error occured.");
		}
		return response;
	}

	/**
	 * Rest call to check lock on a Product
	 * 
	 * @param userId
	 * @param metadataId
	 * @author Pavan Kumar Yekabote.
	 */

	@RequestMapping(value = "/checkLockOnProduct", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> checkLockOnProduct(@RequestParam("userId") String userId,
			@RequestParam("metadataId") String productId, @RequestParam("productHierarchyId") String pHId) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.checkLockOnProduct(userId, pHId, productId);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response checkLockOnProduct :: ", productId, e);
		}
		return response;
	}

	/**
	 * Rest call to remove lock on a product
	 * 
	 * @param userId
	 * @param metadataId
	 * @author Pavan Kumar Yekabote.
	 */

	@RequestMapping(value = "/removeLockOnProduct", method = RequestMethod.DELETE)
	@ResponseBody
	public APIResponse<Object> removeLockOnProduct(@RequestParam("userId") String userId) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			response = metaDataService.removeLockOnProduct(userId);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response removeLockOnProduct :: ", userId, e);
		}
		return response;
	}

	// @Value(value = "${EXCEL_FOLDER_PATH}")
	// private String excelFolderPath;
	@RequestMapping(value = "/uploadSupportTable", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> handleSupportDataUpload(@RequestBody FileObj fileObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();

		/*
		 * FileObj fileObj = null; String lookupName =
		 * request.getParameter("lookupName"); APIResponse<Object> response = new
		 * APIResponse<>(); String path = excelFolderPath+File.separator+new
		 * SimpleDateFormat("yyyyMMddHHmmss").format(new Date()); FileOutputStream
		 * oStream = null; File f = new File(path); try { MultipartHttpServletRequest
		 * req = (MultipartHttpServletRequest)request; Iterator<String> iterator =
		 * req.getFileNames(); MultipartFile multiFile = req.getFile(iterator.next());
		 * String fileName=multiFile.getOriginalFilename(); f.mkdirs(); f=new
		 * File(f.getAbsolutePath()+File.separator+fileName);
		 * IOUtils.write(IOUtils.toByteArray(multiFile.getInputStream()), new
		 * FileOutputStream(f)); oStream.close(); } catch(Exception e) {
		 * logger.info("Some Exception has happened ... " + e); }
		 * 
		 * fileObj = new FileObj(); fileObj.setLookupName(lookupName);
		 * fileObj.setPath(f.getAbsolutePath());
		 */
		try {
			System.out.println("File path : " + fileObj.getPath() + "::lookupName : : : " + fileObj.getLookupName());
			fileObj.setUserId(loggedUser);
			response = metaDataService.uploadExcelForSupportTable(fileObj);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getConfigurationForSupportTable ::", e);
		} finally {
			try {
				FileUtils.deleteDirectory(new File(fileObj.getPath()));
				// if(oStream != null ) oStream.close();
			} catch (Exception e) {
			}
		}
		return response;

	}

	@RequestMapping(value = "/getInfoForSupportTable", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> getInfoForSupportTable(@RequestBody SupportRequestEntity entity,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = null;
		try {
			entity.setUserId(loggedUser);
			response = metaDataService.getSupportTableInfo(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getConfigurationForSupportTable ::", e);
		}
		return response;
	}

	@RequestMapping(value = "/supportTableCreate", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> createSupportTable(@RequestBody SupportRequestEntity entity,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = null;
		try {
			entity.setUserId(loggedUser);
			response = metaDataService.createSupportTable(entity);
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getConfigurationForSupportTable ::", e);
		}
		return response;
	}
        
        //This API should be deleted
        @RequestMapping(value = "/approveTest", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public APIResponse<Object> testingOnly(@RequestBody SupportRequestEntity entity,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = null;
		try {
			entity.setUserId(loggedUser);
			response = metaDataService.mergeStageRecords(
                                entity.getUserId(),
                                entity.getStageId(),
                                entity.getpHid(),
                                entity.getMetadataIds(),
                                entity.getParentID()
                        
                        );
		} catch (Exception e) {
			logger.error("Error occured while retrieving response getConfigurationForSupportTable ::", e);
		}
		return response;
	}
}
