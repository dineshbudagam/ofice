/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
08-06-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.codemantra.manage.metadata.dto.APIResponse;
import com.codemantra.manage.metadata.entity.CodeListEntity;
import com.codemantra.manage.metadata.entity.GridHeadersEntity;
import com.codemantra.manage.metadata.entity.MetaDataAutoPopulateEntity;
import com.codemantra.manage.metadata.entity.MetaDataFieldsEntity;
import com.codemantra.manage.metadata.entity.MetaDataGroupEntity;
import com.codemantra.manage.metadata.entity.ProductHierarchyEntity;
import com.codemantra.manage.metadata.entity.ProductLockEntity.LockType;
import com.codemantra.manage.metadata.entity.ProductMapEntity;
import com.codemantra.manage.metadata.entity.ProductStagesApprovalEntity;
import com.codemantra.manage.metadata.entity.SequenceId;
import com.codemantra.manage.metadata.model.AwsAPIEntity;
import com.codemantra.manage.metadata.model.CDCMetaData;
import com.codemantra.manage.metadata.model.CDCMetaDataDetails;
import com.codemantra.manage.metadata.model.FileObj;
import com.codemantra.manage.metadata.model.Product;
import com.codemantra.manage.metadata.model.Search;
import com.codemantra.manage.support.tables.entities.SupportTableConfigMain;
import com.codemantra.manage.support.tables.request.entities.SupportRequestEntity;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.File;

public interface MetaDataDao {

	public Map<String, List<? extends Object>> getFieldData(String productHierarchyId);

	public Map<String, List<? extends Object>> getCurrentFieldData(String productHierarchyId);

	public Map<String, List<? extends Object>> getCurrentFieldData(List<String> productHierarchyIds);

	public List<ProductHierarchyEntity> getAllProductHierarchies();

	public List<CodeListEntity> getCodelistData();

	/* public UserRightsEntity getUserRights(String UserId); */

	public List<Search> getSavedMetadata(String userId);

	public boolean saveMetaData(Object obj);

	public List<MetaDataGroupEntity> getGroupData();

	public List<MetaDataFieldsEntity> getFieldData();

	public List<MetaDataFieldsEntity> getFieldDataByGroupId(String groupId);

	public List<MetaDataGroupEntity> getMetadataGroup();

	public List<MetaDataFieldsEntity> getMetadataFields(String groupId);

	public MetaDataFieldsEntity getMetaDataFieldDetail(String displayName);

	public List<MetaDataFieldsEntity> getMetaDataFieldDetails();

	public Map<String, Object> editMetaData(String idValue);

	public List<GridHeadersEntity> getGridHeaders();

	public List<GridHeadersEntity> getUserGridHeaders(String userId);

	public List<GridHeadersEntity> getCustomHeaders();

	public List<MetaDataFieldsEntity> getMetaDataFieldLogicDetails(List<String> displayNames,String pHId);

	public <T> T getEntityObject(T masterObj, Map<String, Object> criteriaMap);

	public MetaDataFieldsEntity getMetaDataFields(String fieldName);

	public MetaDataFieldsEntity getJsonPath(String mDatafieldName);

	public boolean saveCustomMdataHeaders(GridHeadersEntity gridHeadersEntity);

	public boolean existIsbnCheck(String isbn);

	public boolean saveUploadMetaData(Object obj);

	public List<MetaDataAutoPopulateEntity> getAutoPopulateFieldDetails();

	public List<ProductMapEntity> getProducts(List<String> isbns);

	public Map<Object, Object> getProductWithRelations(String id, String pHid, String loggedUser);

	public List<ProductMapEntity> getProducts();
	
	public List<ProductMapEntity> getProducts(Object metadataId);

	public boolean updateProductsWithSync(ProductMapEntity pMap);

	public List<ProductMapEntity> getLastModifiedProducts();

	public boolean updateToSyncProductInChildren(ProductMapEntity product);

	public APIResponse<Object> saveProduct(ProductMapEntity product, String referenceParentID);

	public String getSyncFields(String productHierarchy, String parentId, String referenceParentId);

	public List<ProductHierarchyEntity> getProductHierarchiesBy(String group);

	public Map<String, Object> getISBNValue();

	public boolean validateFields(ProductMapEntity product);

	public List<Map> getSupportTableMapping();

	public String generateExcelForSupportTable(String lookupName, String format, String userId);

	public List<Map> validateExcelForSupportTable(FileObj fileObj);

	public List<Map> uploadExcelForSupportTable(FileObj fileObj);

	public Map<String, Object> exportSupportTable(SupportRequestEntity entity);

	public Map<String, Object> viewSearchForSupportTable(SupportRequestEntity entity);

	public Map<String, Object> getConfigurationForSupportTable(SupportRequestEntity entity);

	public Map<String, Object> addOneSupportTableRecord(SupportRequestEntity entity);

	public Map<String, Object> addMultiSupportTableRecords(SupportRequestEntity entity);

	public Map<String, Object> editOneSupportTableRecord(SupportRequestEntity entity);

	// Is this API Required
	public Map<String, Object> findOneSupportTableRecord(Object key, String lookupName);

	public Integer incrementSequenceNumber(SupportRequestEntity entity);

	List<String> getSupportTablesList();

	public boolean lockProduct(String metadataId, String pHId, String userId);

	LockType isProductLocked(String productId, String pHid, String userId);

	public boolean removeLockOnProduct(String userId, String referenceParentId);
	public boolean removeLockOnProduct(String userId);

	public String saveFile2S3(File file, String userId, String operationName, int expiryDays);

	boolean removeLockOnProductsIn(long timeInMilliSeconds);

	public Boolean isFilterFirst(SupportRequestEntity entity);

	public Map<String, Object> getAvailabilityCodeConfig();

	public ProductMapEntity getProduct(Object metadataId, Object pHid);

	public boolean saveCdcMetadata(CDCMetaData cdcMetadata);

	public Boolean actiDeactivateSupportConfig(SupportRequestEntity entity);
	
	public List<ProductMapEntity> getSiblingsData(Object referenceParentID,Object phId);//,Object syncField);
	
	public ProductMapEntity getMasterSiblingData(Object referenceParentID,Object phId,String syncField);
	
	public boolean checkSFormatsExistForLanguage(Object metadataId,Object pHid);

	public boolean validateMandatoryUniqueFields(ProductMapEntity pMap);

	public boolean validateFieldsV2(ProductMapEntity pMap);
	public List<ProductStagesApprovalEntity> getStagesdataWithStatus(Object metadataId,Object phId);
        public List<Map<String,Object>> createSupportTable(SupportRequestEntity entity);

        public Map<String,Object> getSupportTableInfo(SupportRequestEntity entity);

	public Map<String, Boolean> doesValueExist(String fieldName, Object fieldValue);        
        
        public Map<String, Object> mergeStageRecords( String loggedUser, String stageId, String productHierarchyId, String metadataId,String parentID);

}
