/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
08-06-2017			v1.0        	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.daoImpl;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

//import com.amazonaws.util.json.JSONObject;
import com.codemantra.manage.metadata.dao.MetaDataDao;
import com.codemantra.manage.metadata.dto.APIResponse;
import com.codemantra.manage.metadata.entity.CodeListEntity;
import com.codemantra.manage.metadata.entity.DownloadEntity;
import com.codemantra.manage.metadata.entity.GridHeadersEntity;
import com.codemantra.manage.metadata.entity.JsonDbPathCondition;
import com.codemantra.manage.metadata.entity.JsonDbPathConditionEntity;
import com.codemantra.manage.metadata.entity.ManageConfigEntity;
import com.codemantra.manage.metadata.entity.MetaDataAutoPopulateEntity;
import com.codemantra.manage.metadata.entity.MetaDataFieldsEntity;
import com.codemantra.manage.metadata.entity.MetaDataGroupEntity;
import com.codemantra.manage.metadata.entity.MetaDataProductHierarchyFieldsEntity;
import com.codemantra.manage.metadata.entity.MetaDataProductHierarchyGroupsEntity;
import com.codemantra.manage.metadata.entity.MetaDataProductHierarchyStageEntity;
import com.codemantra.manage.metadata.entity.MetadataFields;
import com.codemantra.manage.metadata.entity.ModuleAccessEntity;
import com.codemantra.manage.metadata.entity.PermissionGroupEntity;
import com.codemantra.manage.metadata.entity.ProductEntity;
import com.codemantra.manage.metadata.entity.ProductHierarchyEntity;
import com.codemantra.manage.metadata.entity.ProductLockEntity;
import com.codemantra.manage.metadata.entity.ProductMapEntity;
import com.codemantra.manage.metadata.entity.ProductStagesApprovalEntity;
import com.codemantra.manage.metadata.entity.RoleEntity;
import com.codemantra.manage.metadata.entity.SubModuleAccessEntity;
import com.codemantra.manage.metadata.entity.UserEntity;
import com.codemantra.manage.metadata.entity.UserRightsEntity;
import com.codemantra.manage.metadata.model.AwsAPIEntity;
import com.codemantra.manage.metadata.model.CDCMetaData;
import com.codemantra.manage.metadata.model.CodeDescription;
import com.codemantra.manage.metadata.model.FileObj;
import com.codemantra.manage.metadata.model.GridHeaders;
import com.codemantra.manage.metadata.model.MetadataStageStatus;
import com.codemantra.manage.metadata.model.Product;
import com.codemantra.manage.metadata.model.Search;
import com.codemantra.manage.metadata.serviceImpl.MetaDataServiceImpl;
import com.codemantra.manage.support.tables.entities.Count;
import com.codemantra.manage.support.tables.entities.HeaderInfo;
import com.codemantra.manage.support.tables.entities.SupportBean;
import com.codemantra.manage.support.tables.entities.SupportTableConfigMain;
import com.codemantra.manage.support.tables.entities.ValidateErrorBean;
import com.codemantra.manage.support.tables.request.entities.FieldInfo;
import com.codemantra.manage.support.tables.request.entities.PageInfo;
import com.codemantra.manage.support.tables.request.entities.SupportRequestEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.mongodb.BasicDBObject;
//import com.codemantra.manage.search.entity.PermissionGroupEntity;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import static org.apache.poi.ss.usermodel.Cell.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jongo.Jongo;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregationOptions;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.skip;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import static org.springframework.data.mongodb.core.aggregation.Fields.fields;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.aggregation.UnwindOperation;
import org.springframework.data.mongodb.core.query.Field;

@Repository
@Qualifier("metaDataDao")
public class MetaDataDaoImpl implements MetaDataDao {

	private static final Logger logger = LoggerFactory.getLogger(MetaDataDaoImpl.class);

	@Autowired
	@Qualifier(value = "primaryMongoTemplate")
	MongoTemplate mongoTemplate;

	@Autowired
	MongoOperations mongoOperations;

	@Qualifier(value = "cdcmongoTemplate")
	static MongoTemplate cdcmongoTemplate;

	DB database = null;
	DBCollection coll = null;

	@Autowired
	private Jongo jongo;

	@Value("${COLLECTION.TRANSACTIONS.MUSERGRIDHEADERS}")
	String metaDataTransctions;

	@Value("${COLLECTION.TRANSACTIONS.MDATARESULTS}")
	String metaDataResults;

	@Value("${COLLECTION.CONFIG.MGRIDHEADERS}")
	String gridConfig;

	@Value("${COLLECTION.TRANSACTIONS.MUSERGRIDHEADERS}")
	String cutomHeaders;

	@Value("${COLLECTION.CONFIG.MDATAFIELDS}")
	String metaDataFieldsDetails;

	@Value("${COLLECTION.CONFIG.MDATAGROUPS}")
	String metaDataGroupsDetails;

	@Value("${COLLECTION.CONFIG.MDATAVIEW}")
	String metaDataFieldView;

	@Value("${COLLECTION.MASTER.CODELISTS}")
	String codeLists;

	@Value("${COLLECTION.CONFIG.AUTOPOPULATE}")
	String autoPopulate;

	@Value("${COLLECTION.SEQUENCE.PRODUCT.PREFIX}")
	String metadataPrefix;

	@Value(value = "${FONT_NAME}")
	private String fontName;

	@Value(value = "${EXCEL_OUTPUT_PATH}")
	private String outputPath;

	@Value(value = "${supportKey}")
	private String supportKey;

	@Value(value = "${separatorSymbol}")
	private String separatorSymbol;

	@Value(value = "${SupportTableCollection}")
	private String supportTableCollection;

	@Value(value = "${DEFAULT_PAGE_SIZE}")
	private Integer defaultPageSize;

	@Value(value = "${SupportTableConfigCollection}")
	private String supportTableConfig;

	@Value(value = "${PATH_TO_MONGOJS}")
	private String path2Js;
	@Value(value = "${MONGO_COMMAND}")
	private String mongoCommand;
	@Value(value = "${OUTPUT_DIR_JS}")

	private String outputDirJs;

	@Value(value = "${FOLDERPATH2CSV}")
	private String folderPath2Csv;

	@Value(value = "${SCRIPT_PATH_TEMP}")
	private String scriptPathTemp;

	@Value(value = "${CSVEXPORT.SERVICE.URL}")
	private String csvexportServiceURL;

	@Value(value = "${CSVEXPORT.SERVICE.API}")
	private String csvexportServiceAPI;

	@Value(value = "${SHELL_PATH}")
	private String shellPath;

	@Value("${spring.data.mongodb.host}")
	private String mongoDBServer;

	@Value(value = "${BATCH_SIZE}")
	private Integer batchLimit;

	@Value(value = "${S3.CSV.ROOT}")
	private String s3CsvRoot;

	@Value(value = "${TDOWNLOAD.COLLECTION.NAME}")
	private String tDownloadCollectionName;

	@Value(value = "${Product.Lock.Interval}")
	private long PRODUCT_LOCK_INTERVAL;

	@Override
	public List<ProductHierarchyEntity> getAllProductHierarchies() {

		List<ProductHierarchyEntity> hierarchiesList = null;
		try {

			Query query = new Query();
			query.addCriteria(Criteria.where("isDeleted").is(false).and("isActive").is(true));
			query.with(new Sort(Sort.Direction.ASC, "parent_id"));
			logger.info("Query [" + query + "]");
			hierarchiesList = mongoOperations.find(query, ProductHierarchyEntity.class);

		} catch (Exception e) {
			logger.error("Error while retrieving product hierarchy IDs in DAO : ", e);
		}
		return hierarchiesList;
	}
	// To get all fields,groups,stages while creating

	/**
	 * added on 27/09/19
	 */
	@Override
	public Map<String, List<? extends Object>> getFieldData(String productHierarchyId) {
		Map<String, List<? extends Object>> fields = new HashMap<String, List<? extends Object>>();
		List<MetaDataProductHierarchyFieldsEntity> metaDataFieldsEntity = null;
		try {
			Query query = new Query();
			/*
			 * MetaDataProductHierarchyFieldsEntity createProductHierarchyId =
			 * getRetriveCreateEditProductHierarchyIds(productHierarchyId); List<String>
			 * createProductHierarchyIdList =
			 * createProductHierarchyId.getCreateProductHierarchyId(); /*
			 * MetaDataProductHierarchyFieldsEntity retriveCreateProductHierarchyIds = new
			 * MetaDataProductHierarchyFieldsEntity(); if(productHierarchyId != null) {
			 * query.addCriteria(Criteria.where("productHierarchyId").is(productHierarchyId)
			 * ); retriveCreateProductHierarchyIds = mongoTemplate.findOne(query,
			 * MetaDataProductHierarchyFieldsEntity.class,"cMetadataFields");
			 * createProductHierarchyId =
			 * retriveCreateProductHierarchyIds.getCreateProductHierarchyId(); query= null;
			 * }
			 */
			// createProductHierarchyId
			// query.addCriteria(Criteria.where(productHierarchyId).in(createProductHierarchyIdList));*/
			if (productHierarchyId.equalsIgnoreCase("all")) {
				query.addCriteria(Criteria.where("isActive").is(true).and("isDeleted").is(false));

				metaDataFieldsEntity = mongoTemplate.find(query, MetaDataProductHierarchyFieldsEntity.class);
				fields.put("Fields", metaDataFieldsEntity);
			} else {
				query.addCriteria(Criteria.where("createProductHierarchyId").in(productHierarchyId).and("isActive")
						.is(true).and("isDeleted").is(false));

				metaDataFieldsEntity = mongoTemplate.find(query, MetaDataProductHierarchyFieldsEntity.class);
				fields.put("Fields", metaDataFieldsEntity);
				List<MetaDataProductHierarchyGroupsEntity> metadataGroupsEntity = null;
				metadataGroupsEntity = mongoTemplate.find(query, MetaDataProductHierarchyGroupsEntity.class,
						"cMetadataGroups");
				fields.put("Groups", metadataGroupsEntity);
				List<MetaDataProductHierarchyStageEntity> metadataStageEntity = mongoTemplate.find(query,
						MetaDataProductHierarchyStageEntity.class, "cMetadataStages");
				fields.put("Stages", metadataStageEntity);
			}

		} catch (Exception e) {
			logger.error("Error Occurred in get fields::" + e.getMessage());
		}

		return fields;
	}

	@Override
	public Map<String, List<? extends Object>> getCurrentFieldData(String productHierarchyId) {
		Map<String, List<? extends Object>> fields = new HashMap<String, List<? extends Object>>();
		List<MetaDataProductHierarchyFieldsEntity> metaDataFieldsEntity = null;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("productHierarchyId").is(productHierarchyId).and("isActive").is(true)
					.and("isDeleted").is(false));

			metaDataFieldsEntity = mongoTemplate.find(query, MetaDataProductHierarchyFieldsEntity.class);
			fields.put("Fields", metaDataFieldsEntity);
			List<MetaDataProductHierarchyGroupsEntity> metadataGroupsEntity = null;
			metadataGroupsEntity = mongoTemplate.find(query, MetaDataProductHierarchyGroupsEntity.class,
					"cMetadataGroups");
			fields.put("Groups", metadataGroupsEntity);
			List<MetaDataProductHierarchyStageEntity> metadataStageEntity = mongoTemplate.find(query,
					MetaDataProductHierarchyStageEntity.class, "cMetadataStages");
			fields.put("Stages", metadataStageEntity);
		} catch (Exception e) {
			logger.error("Error Occurred in get fields::" + e.getMessage());
		}

		return fields;
	}

	@Override
	public Map<String, List<? extends Object>> getCurrentFieldData(List<String> productHierarchyIds) {
		Map<String, List<? extends Object>> fields = new HashMap<String, List<? extends Object>>();
		List<MetaDataProductHierarchyFieldsEntity> metaDataFieldsEntity = new ArrayList<>();

		for (String productHierarchyId : productHierarchyIds) {
			try {
				List<MetaDataProductHierarchyFieldsEntity> mEntity = new ArrayList<>();
				Query query = new Query();
				query.addCriteria(Criteria.where("productHierarchyId").is(productHierarchyId).and("isActive").is(true)
						.and("isDeleted").is(false));

				mEntity = mongoTemplate.find(query, MetaDataProductHierarchyFieldsEntity.class);

				metaDataFieldsEntity.addAll(mEntity);

			} catch (Exception e) {
				logger.error("Error Occurred in get fields::" + e.getMessage());
			}
		}

		fields.put("Fields", metaDataFieldsEntity);

		return fields;
	}

	@Override
	public List<CodeListEntity> getCodelistData() {
		List<CodeListEntity> codeList = null;
		try {
			codeList = mongoTemplate.findAll(CodeListEntity.class, codeLists);
		} catch (Exception e) {
			logger.error("Error Occurred in get codelists::" + e.getMessage());
		}
		return codeList;
	}

	@Override
	public List<MetaDataFieldsEntity> getFieldData() {
		List<MetaDataFieldsEntity> meta = null;
		try {
			meta = mongoTemplate.findAll(MetaDataFieldsEntity.class, metaDataFieldsDetails);
		} catch (Exception e) {
			logger.error("Error occurred while retrieving metadata field config data::" + e.getMessage());
		}
		return meta;
	}

	@Override
	public List<Search> getSavedMetadata(String userId) {
		List<Search> searchNameList = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("userId").is(userId).and("isActive").is(true).and("isDeleted").is(false)
					.and("searchName").ne("").and("pageName").is("Metadata"));
			query2.fields().include("searchName").exclude("_id");
			searchNameList = mongoTemplate.find(query2, Search.class, metaDataTransctions);
		} catch (Exception e) {
			logger.error("Error occurred while retrieving metadata field config data::" + e.getMessage());
		}
		return searchNameList;
	}

	@Override
	public boolean saveMetaData(Object obj) {
		boolean result = false;
		try {
			mongoTemplate.insert(obj, "products");
			result = true;
		} catch (Exception e) {
			logger.error("Error occurred while Saving metadata::" + e.getMessage());
		}
		return result;
	}

	@Override
	public boolean saveUploadMetaData(Object obj) {
		boolean result = false;
		try {
			mongoTemplate.save(obj, "products");
			result = true;

		} catch (Exception e) {
			logger.error("Error occurred while Save Upload metadata::" + e.getMessage());
		}
		return result;
	}

	@Override
	public List<MetaDataGroupEntity> getGroupData() {
		List<MetaDataGroupEntity> meta = null;
		try {
			meta = mongoTemplate.findAll(MetaDataGroupEntity.class, metaDataGroupsDetails);
		} catch (Exception e) {
			logger.error("Error occurred while getting group data::" + e.getMessage());
		}
		return meta;
	}

	@Override
	public List<MetaDataFieldsEntity> getFieldDataByGroupId(String groupId) {
		List<MetaDataFieldsEntity> mfg = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("groupId").is(groupId).and("isActive").is(Boolean.TRUE).and("isDeleted")
					.is(Boolean.FALSE).and("isDisplay").is(Boolean.TRUE));

			mfg = mongoTemplate.find(query2, MetaDataFieldsEntity.class, metaDataFieldsDetails);
		} catch (Exception e) {
			logger.error("Error occurred while getting Field data by GroupId::" + e.getMessage());
		}
		return mfg;
	}

	/*
	 * @Override public boolean updateDeleteMdata(String idValue) { boolean result =
	 * false; Calendar calendar = Calendar.getInstance(); Date currentDate =
	 * calendar.getTime(); ProductEntity metaDataentity = null;
	 * 
	 * try { Query q = new Query();
	 * 
	 * q.addCriteria(Criteria.where("Product.ProductIdentifier.ProductIDType").
	 * is("15") .and("Product.ProductIdentifier.IDValue").is(idValue));
	 * 
	 * metaDataentity = mongoTemplate.findOne(q, ProductEntity.class,"products");
	 * Product metadataUpdate = MetaDataEntityToVo(metaDataentity);
	 * metadataUpdate.getProduct().setDeleted(true);
	 * metadataUpdate.getProduct().setLastModifiedOn(currentDate);
	 * metadataUpdate.setId(metaDataentity.getId());
	 * 
	 * mongoTemplate.save(metadataUpdate, "products");
	 * 
	 * result = true;
	 * 
	 * } catch (Exception e) { throw e; } return result; }
	 */

	@Override
	public Map<String, Object> editMetaData(String idValue) {
		ProductEntity metaDataentity = null;
		Map<String, Object> editMdataResult = null;

		try {
			editMdataResult = new HashMap<>();
			Query query2 = new Query();

			query2.addCriteria(Criteria.where("Product.ProductIdentifier.ProductIDType").is("15")
					.and("Product.ProductIdentifier.IDValue").is(idValue));
			metaDataentity = mongoTemplate.findOne(query2, ProductEntity.class, "products");

			editMdataResult.put("Product", metaDataentity);
		} catch (Exception e) {
			logger.error("Error occurred while Editing Metadata::" + e.getMessage());
		}
		return editMdataResult;
	}

	@Override
	public List<MetaDataGroupEntity> getMetadataGroup() {
		List<MetaDataGroupEntity> metaGroups = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("isDisplay").is(Boolean.TRUE).and("isActive").is(Boolean.TRUE)
					.and("isDelete").is(Boolean.FALSE));
			metaGroups = mongoTemplate.find(query2, MetaDataGroupEntity.class, metaDataGroupsDetails);
		} catch (Exception e) {
			logger.error("Error occurred while Editing Metadata::" + e.getMessage());
		}
		return metaGroups;
	}

	@Override
	public List<MetaDataFieldsEntity> getMetadataFields(String groupId) {
		List<MetaDataFieldsEntity> metaFields = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("isDisplay").is(Boolean.TRUE).and("isActive").is(Boolean.TRUE)
					.and("isDelete").is(Boolean.FALSE).and("groupId").is(groupId));
			metaFields = mongoTemplate.find(query2, MetaDataFieldsEntity.class, metaDataFieldsDetails);
		} catch (Exception e) {
			logger.error("Error occurred while Editing Metadata::" + e.getMessage());
		}
		return metaFields;
	}

	@Override
	public List<GridHeadersEntity> getGridHeaders() {
		List<GridHeadersEntity> gridHeaders = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("isDisplay").is(true).and("isActive").is(true).and("isDeleted").is(false)
					.and("pageName").is("Metadata"));
			query2.with(new Sort(Sort.Direction.ASC, "displayOrder"));

			gridHeaders = mongoTemplate.find(query2, GridHeadersEntity.class, gridConfig);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving GridHeaders::" + e.getMessage());
		}
		return gridHeaders;
	}

	@Override
	public List<GridHeadersEntity> getUserGridHeaders(String userId) {
		List<GridHeadersEntity> gridHeaders = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("userId").is(userId).and("pageName").is("Metadata"));
			query2.fields().exclude(userId);
			query2.with(new Sort(Sort.Direction.ASC, "displayOrder"));

			gridHeaders = mongoTemplate.find(query2, GridHeadersEntity.class, cutomHeaders);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving User GridHeaders::" + e.getMessage());
		}
		return gridHeaders;
	}

	@Override
	public List<GridHeadersEntity> getCustomHeaders() {
		List<GridHeadersEntity> gridHeaders = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(
					Criteria.where("isActive").is(true).and("isDeleted").is(false).and("pageName").is("Metadata"));
			query2.with(new Sort(Sort.Direction.ASC, "displayOrder"));
			gridHeaders = mongoTemplate.find(query2, GridHeadersEntity.class, gridConfig);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving Custom GridHeaders::" + e.getMessage());
		}
		return gridHeaders;
	}

	@Override
	public List<MetaDataFieldsEntity> getMetaDataFieldLogicDetails(List<String> displayNames, String pHId) {
		List<MetaDataFieldsEntity> fieldDetails = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("isDisplay").is(true).and("isActive").is(true).and("isDeleted").is(false)
					.and("metaDataFieldName").in(displayNames).and("productHierarchyId").is(pHId));
			fieldDetails = mongoTemplate.find(query2, MetaDataFieldsEntity.class, metaDataFieldView);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving MetadataField Details::" + e.getMessage());
		}
		return fieldDetails;
	}

	@Override
	public MetaDataFieldsEntity getMetaDataFieldDetail(String mFieldName) {
		MetaDataFieldsEntity fieldDetail = null;
		try {

			Query query2 = new Query();
			query2.addCriteria(Criteria.where("isActive").is(true).and("isDeleted").is(false).and("metaDataFieldName")
					.is(mFieldName));
			query2.fields().exclude("_id");
			fieldDetail = mongoTemplate.findOne(query2, MetaDataFieldsEntity.class, metaDataFieldsDetails);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving MetadataField Detail::" + e.getMessage());
		}
		return fieldDetail;
	}

	@Override
	public List<MetaDataFieldsEntity> getMetaDataFieldDetails() {
		List<MetaDataFieldsEntity> fieldDetails = null;
		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("isActive").is(true).and("isDeleted").is(false));

			fieldDetails = mongoTemplate.find(query2, MetaDataFieldsEntity.class, metaDataFieldsDetails);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving All MetadataField Details::" + e.getMessage());
		}
		return fieldDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getEntityObject(T masterObj, Map<String, Object> criteriaMap) {

		try {
			Query query = new Query();

			if (criteriaMap != null && criteriaMap.size() > 0) {
				Criteria criteria = new Criteria();
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());
				criteriaMap.forEach((k, v) -> {
					if (v instanceof Collection<?>) {
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					} else {
						criterias.add(Criteria.where(k).is(v));
					}
				});
				criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
				query.addCriteria(criteria);
			}

			masterObj = (T) mongoTemplate.findOne(query, masterObj.getClass());
		} catch (Exception e) {
			masterObj = null;
		}
		return masterObj;
	}

	@Override
	public MetaDataFieldsEntity getMetaDataFields(String fieldName) {
		MetaDataFieldsEntity metadataFieldentity = null;

		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("fieldDisplayName").is(fieldName));
			query2.fields().include("referencePath");
			query2.fields().include("jsonPath");
			query2.fields().exclude("_id");

			metadataFieldentity = mongoTemplate.findOne(query2, MetaDataFieldsEntity.class, metaDataFieldsDetails);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving metadata fields by fieldname::" + e.getMessage());
		}
		return metadataFieldentity;
	}

	@Override
	public MetaDataFieldsEntity getJsonPath(String mDatafieldName) {
		MetaDataFieldsEntity metadataFieldentity = null;

		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("metaDataFieldName").is(mDatafieldName));
			query2.fields().include("jsonPath");
			query2.fields().exclude("_id");

			metadataFieldentity = mongoTemplate.findOne(query2, MetaDataFieldsEntity.class, metaDataFieldView);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving JsonPath by fieldname::" + e.getMessage());
		}
		return metadataFieldentity;
	}

	@Override
	public List<MetaDataAutoPopulateEntity> getAutoPopulateFieldDetails() {
		List<MetaDataAutoPopulateEntity> mDataPoulateentity = null;

		try {
			mDataPoulateentity = mongoTemplate.findAll(MetaDataAutoPopulateEntity.class, autoPopulate);
		} catch (Exception e) {
			logger.error("Error occurred while Retrieving JsonPath by fieldname::" + e.getMessage());
		}
		return mDataPoulateentity;
	}

	@Override
	public boolean existIsbnCheck(String isbn) {
		String existingIsbn = "";
		boolean result = false;
		try {
			Query query2 = new Query();

			query2.addCriteria(Criteria.where("Product.ProductIdentifier.ProductIDType").is("15")
					.and("Product.ProductIdentifier.IDValue").is(isbn));

			query2.fields().include("_id");
			existingIsbn = mongoOperations.findOne(query2, String.class, metaDataResults);
			if (existingIsbn != null) {
				result = true;
			}
		} catch (Exception e) {
			logger.error("Error occurred while existIsbnCheck::" + e.getMessage());
		}

		return result;
	}

	@Override
	public boolean saveCustomMdataHeaders(GridHeadersEntity gridHeadersEntity) {
		boolean result = false;
		List<GridHeadersEntity> existGridHeaders = null;
		GridHeadersEntity gridHeaderDetailsEntity = null;
		try {
			boolean check = true;
			existGridHeaders = new ArrayList<>();
			for (GridHeaders gHeaders : gridHeadersEntity.getHeaders()) {
				gridHeaderDetailsEntity = new GridHeadersEntity();
				Query query3 = new Query();
				query3.addCriteria(
						Criteria.where("fieldName").is(gHeaders.getFieldName()).and("pageName").is("Metadata"));
				query3.fields().exclude("_id");
				gridHeaderDetailsEntity = mongoOperations.findOne(query3, GridHeadersEntity.class, gridConfig);
				gridHeaderDetailsEntity.setUserId(gHeaders.getUserId());
				gridHeaderDetailsEntity.setPageName(gHeaders.getPageName());

				if (check) {
					Query query2 = new Query();
					query2.addCriteria(
							Criteria.where("userId").is(gHeaders.getUserId()).and("pageName").is("Metadata"));
					query2.fields().exclude("_id");
					List<GridHeadersEntity> h = mongoTemplate.find(query2, GridHeadersEntity.class, cutomHeaders);
					if (h.size() > 0) {
						mongoTemplate.remove(query2, cutomHeaders);
						check = false;
					}
				}
				existGridHeaders.add(gridHeaderDetailsEntity);
			}
			mongoOperations.insert(existGridHeaders, cutomHeaders);
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
		}
		return result;
	}

	@Override
	public List<ProductMapEntity> getProducts(List<String> isbns) {
		List<ProductMapEntity> productEntity = null;

		try {
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("Product.ProductIdentifier.ProductIDType").is("15")
					.and("Product.ProductIdentifier.IDValue").in(isbns));
			productEntity = mongoOperations.find(query2, ProductMapEntity.class, metaDataResults);
		} catch (Exception ex) {
			logger.error("Error getExcelEntity::" + ex.getMessage());
		}
		return productEntity;
	}

	/**
	 * Saves a product entity into database and increments the metadataSeqId in
	 * Sequence collection if stored successfully
	 * 
	 * @throws IllegalArgumentException
	 * @author Pavan Kumar Yekabote
	 * 
	 */

	@Override
	public APIResponse<Object> saveProduct(ProductMapEntity newProduct, String referenceParentID)
			throws IllegalArgumentException {

		APIResponse<Object> response = new APIResponse<>();
		Query query = new Query();

		// For pH - autoGenField
		Query qry = new Query();

		qry.addCriteria(Criteria.where("_id").is(newProduct.getCustomFields().get("productHierarchyId")));
		ProductHierarchyEntity pHEntity = mongoTemplate.findOne(qry, ProductHierarchyEntity.class);

		newProduct.getCustomFields().put("referenceParentID", referenceParentID);

		// Converting to JSON String again to set Titletext and UID
		String newproJson = new Gson().toJson(newProduct);
		DocumentContext newDoc = JsonPath.parse(newproJson);
		String uid = "", titleText = "";
		try {
			// Unique Identifier from UID path
			uid = newDoc.read(pHEntity.getUidPath()).toString();
			uid = uid.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			// Unique Identifier from UID path
			titleText = newDoc.read(pHEntity.getTitlePath()).toString();
			titleText = titleText.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
		} catch (Exception e) {
			// TODO: handle exception
		}

		newProduct.getCustomFields().put("Titletext", titleText);
		newProduct.getCustomFields().put("UID", uid);
		newProduct.getCustomFields().put("productHierarchyName", pHEntity.getProductHierarchyName());
		newProduct.getProduct().put("LastModifiedOn", new Date());

		try {
			mongoTemplate.insert(newProduct);
			response.setStatus(APIResponse.ResponseCode.SUCCESS.toString());
			// response.setData(mid); Commenting for below changes

			// Changing the below to include Parent record in child product
			response.setData(newProduct);
		} catch (Exception e) {

			response.setStatus(APIResponse.ResponseCode.FAILURE.toString());
			throw new IllegalArgumentException(e.getMessage());
		}

		return response;
	}

	/**
	 * Fetch all products from database
	 * 
	 * @return {@link List}&lt;{@link ProductMapEntity}&gt
	 */
	@Override
	public List<ProductMapEntity> getProducts() {
		List<ProductMapEntity> productEntityList = null;
		try {
			Query query = new Query();
			productEntityList = mongoOperations.find(query, ProductMapEntity.class);
		} catch (Exception e) {
			logger.error("Error getProducts::" + e.getMessage());
		}
		return productEntityList;
	}

	/**
	 * Returns a map of objects of current , parent, siblings, children of given
	 * product id Changes as per embedded parent structure in child
	 * 
	 * @param product
	 *            id
	 * @return Map of(level/ type, product)
	 * @author Pavan.
	 */

	public Map<Object, Object> getProductWithRelations(String id, String productHierarchyId, String loggedUser) {

		LinkedHashMap<Object, Object> pdWithRltns = null;
		List<ProductMapEntity> children = null, siblings = null;
		List<Map<Object, Object>> parentsResult = null;
		List<Map<Object, Object>> breadCrumb = null;
		ObjectMapper oMapper = new ObjectMapper();

		List<Map<Object, Object>> Stages = null;

		List<SubModuleAccessEntity> StagesAccessStatus = null;

		ProductMapEntity parentProduct = null;

		try {

			parentsResult = new ArrayList<>();
			breadCrumb = new ArrayList<>();
			Stages = new ArrayList<>();
			siblings = new ArrayList<>();
			children = new ArrayList<>();
			StagesAccessStatus = new ArrayList<>();

			Query query = new Query();

			query.addCriteria(
					Criteria.where("metadataId").is(id).and("CustomFields.productHierarchyId").is(productHierarchyId));

			ProductMapEntity currentProduct = mongoOperations.findOne(query, ProductMapEntity.class);

			if (currentProduct != null) {
				pdWithRltns = new LinkedHashMap<>();

				// Get Hierarchy Name of a Product
				// String productHierarchyId = (String)
				// currentProduct.getCustomFields().get("productHierarchyId");

				try {
					if (currentProduct.getCustomFields().get("parentID") != null
							&& !currentProduct.getCustomFields().get("parentID").toString().equals("")) {

						// Fetch all metadataHierarchyFields which contains current products
						// productHierarchyId
						query = new Query();
						Collection hierarchyList = new ArrayList<>();
						hierarchyList.add(productHierarchyId);

						query.addCriteria(Criteria.where("editProductHierarchyId").in(hierarchyList));
						List<MetadataFields> cFields = mongoTemplate.find(query, MetadataFields.class);

						// Get Parents of a current product

						String parentId = (String) currentProduct.getCustomFields().get("parentID");

						query = new Query();
						query.addCriteria(
								Criteria.where("_id").is(currentProduct.getCustomFields().get("productHierarchyId")));
						ProductHierarchyEntity parentpHId = mongoOperations.findOne(query,
								ProductHierarchyEntity.class);

						// Getting Immediate Parent
						if (parentpHId != null) {
							query = new Query();
							query.addCriteria(Criteria.where("metadataId").is(parentId)
									.and("CustomFields.productHierarchyId").is(parentpHId.getParent_id()));
							parentProduct = mongoOperations.findOne(query, ProductMapEntity.class);
						}

						while (parentProduct != null) {

							String parentPHID = (String) parentProduct.getCustomFields().get("productHierarchyId");
							List<MetadataFields> tempFields = cFields.stream()
									.filter(mfield -> mfield.getProductHierarchyId().equals(parentPHID))
									.collect(Collectors.toList());

							LinkedHashMap<Object, Object> tMap = new LinkedHashMap<>();

							Map<Object, Object> tempMap = oMapper.convertValue(parentProduct, Map.class);

							/*
							 * for(MetadataFields mfield : tempFields)
							 * tMap.put(mfield.getMetaDataFieldName(),
							 * tempMap.get(mfield.getMetaDataFieldName()) == null ? "" :
							 * tempMap.get(mfield.getMetaDataFieldName()) );
							 */
							for (MetadataFields mfield : tempFields) {
								String obj = "";
								try {
									String jsonPath = mfield.getJsonPath();
									// System.out.println(jsonPath);
									String json = new Gson().toJson(parentProduct);
									DocumentContext inDocCtx = JsonPath.parse(json);
									obj = inDocCtx.read(jsonPath).toString();
									obj = obj.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
								} catch (Exception e) {
									obj = "";
								}
								tMap.put(mfield.getFieldDisplayName(), obj);

							}

							// breadCrumb list
							Map<Object, Object> breadCrumbMap = new HashMap<Object, Object>();
							breadCrumbMap.put("customFieldsTitleTextValue",
									parentProduct.getCustomFields().get("Titletext"));
							breadCrumbMap.put("UID", parentProduct.getCustomFields().get("UID"));
							breadCrumbMap.put("productHierarchyName",
									parentProduct.getCustomFields().get("productHierarchyName"));
							breadCrumbMap.put("productHierarchyId",
									parentProduct.getCustomFields().get("productHierarchyId"));
							breadCrumbMap.put("metadataId", parentProduct.getMetadataId());
							breadCrumb.add(breadCrumbMap);

							parentsResult.add(tMap);

							// Fetch next hierarchal parent

							query = new Query();
							query.addCriteria(Criteria.where("_id")
									.is(parentProduct.getCustomFields().get("productHierarchyId")));
							ProductHierarchyEntity prntpHId = mongoOperations.findOne(query,
									ProductHierarchyEntity.class);

							if (prntpHId != null) {
								query = new Query();
								parentId = (String) parentProduct.getCustomFields().get("parentID");
								query.addCriteria(Criteria.where("metadataId").is(parentId)
										.and("CustomFields.productHierarchyId").is(prntpHId.getParent_id()));
								parentProduct = mongoOperations.findOne(query, ProductMapEntity.class);
							} else {
								parentId = null;
							}

							if (parentId == null || (parentId != null && parentId.trim().equals("")))
								parentProduct = null;
						}

						parentId = (String) currentProduct.getCustomFields().get("parentID");

						// Get Sibling of a current product

						query = new Query();
						Criteria criteria = Criteria.where("CustomFields.parentID").is(parentId)
								.and("CustomFields.productHierarchyId")
								.is(currentProduct.getCustomFields().get("productHierarchyId")).and("metadataId")
								.ne(currentProduct.getMetadataId());
						query.addCriteria(criteria);
						siblings = mongoOperations.find(query, ProductMapEntity.class);

					}

					// Track the status of the stages for the current product

					query = new Query();
					query.addCriteria(Criteria.where("productHierarchyId").is(productHierarchyId).and("isActive")
							.is(true).and("isDeleted").is(false).and("isKeyField").is(true));
					List<MetadataFields> cMFields = mongoTemplate.find(query, MetadataFields.class);

					query = new Query();
					query.addCriteria(Criteria.where("createProductHierarchyId").in(productHierarchyId).and("isActive")
							.is(true).and("isDeleted").is(false));
					List<MetaDataProductHierarchyStageEntity> cMStages = mongoTemplate.find(query,
							MetaDataProductHierarchyStageEntity.class, "cMetadataStages");
					
					query = new Query();
					
					query.addCriteria(Criteria.where("approvalMethod").is("MANUALAPPROVE").and("remarks")
							.is("Awaiting for approval").and("productHierarchyId").is(productHierarchyId)
							.and("metadataId").is(id));
					query.fields().include("stageId").exclude("_id");
					List<ProductStagesApprovalEntity> mApproveStages = mongoOperations.find(query,
							ProductStagesApprovalEntity.class);
					List<String> mUnApproveStageIds = mApproveStages.stream().map(p -> p.getStageId())
							.collect(Collectors.toList());

					for (MetaDataProductHierarchyStageEntity mStages : cMStages) {
						LinkedHashMap<Object, Object> sMap = new LinkedHashMap<>();
                       int stagekeycnt = 0;
						List<MetadataFields> sMetaFields = cMFields.stream()
								.filter(sMFields -> sMFields.getStageId().equals(mStages.getStageId()))
								.collect(Collectors.toList());

						boolean status = true;
						if (sMetaFields.size() > 0) {
							for (MetadataFields metaField : sMetaFields) {
								String jsonPath = metaField.getJsonPath();
								try {
									String json = new Gson().toJson(currentProduct);
									DocumentContext inDocCtx = JsonPath.parse(json);
									Object obj = inDocCtx.read(jsonPath);
									if (obj == null || obj.toString().equals("[]") || obj.toString().equals("")) {
										status = false;
										break;
									}
									stagekeycnt++;
								} catch (Exception e) {
									e.printStackTrace();
									status = false;
									break;
								}

							}
						} else {
							status = true;
						}

						if (status) {
							if (mUnApproveStageIds.contains(mStages.getStageId())) {
								sMap.put("stageId", mStages.getStageId());
								sMap.put("stageDisplayName", mStages.getStageDisplayName());
								sMap.put("status", "Awaiting for approval");
							} else {
								sMap.put("stageId", mStages.getStageId());
								sMap.put("stageDisplayName", mStages.getStageDisplayName());
								sMap.put("status", "Completed");
							}
						} else {
							if(stagekeycnt > 0) {
							sMap.put("stageId", mStages.getStageId());
							sMap.put("stageDisplayName", mStages.getStageDisplayName());
							sMap.put("status", "InProgress");
							}else {
								sMap.put("stageId", mStages.getStageId());
								sMap.put("stageDisplayName", mStages.getStageDisplayName());
								sMap.put("status", "Yet to Start");
							}
						}

						Stages.add(sMap);
					}

					// check the stages access for loggedUser

					Query queryUser = new Query();
					queryUser.addCriteria(Criteria.where("userId").is(loggedUser).and("isActive").is(true)
							.and("isDeleted").is(false));
					UserEntity userEntity = mongoTemplate.findOne(queryUser, UserEntity.class);

					RoleEntity role = null;

					if (userEntity != null) {
						String permissionGroupId = userEntity.getPermissionGroupId();
						if (permissionGroupId != null) {
							Query queryPG = new Query();
							queryPG.addCriteria(Criteria.where("permissionGroupId").is(permissionGroupId)
									.and("isActive").is(true).and("isDeleted").is(false));
							PermissionGroupEntity pg = mongoTemplate.findOne(queryPG, PermissionGroupEntity.class);
							String roleId = pg.getRoleId();
							if (roleId != null) {
								Query queryRole = new Query();
								queryRole.addCriteria(Criteria.where("roleId").is(roleId).and("isActive").is(true)
										.and("isDeleted").is(false));
								role = mongoTemplate.findOne(queryRole, RoleEntity.class);
							}
						}
					}

					ModuleAccessEntity moduleStages = null;
					if (role != null) {
						List<ModuleAccessEntity> maEntity = role.getModule();
						moduleStages = maEntity.stream().filter(p -> p.getModuleId().equals("MOD00014")).findFirst()
								.orElse(null);//MOD00014
					}

					if (moduleStages != null) {
						List<SubModuleAccessEntity> moduleAccessPerStages = moduleStages.getSubmodule();
						List<SubModuleAccessEntity> updateModuleAccessPerStages = null;

						updateModuleAccessPerStages = new ArrayList<>();

						// query all stages with MANUALAPPROVE

						query = new Query();
						query.addCriteria(Criteria.where("approvalMethod").is("MANUALAPPROVE").and("isActive").is(true)
								.and("isDeleted").is(false));

						List<MetaDataProductHierarchyStageEntity> pseEntities = mongoTemplate.find(query,
								MetaDataProductHierarchyStageEntity.class, "cMetadataStages");

						List<String> manualApproveStages = pseEntities.stream().map(p -> p.getStageId())
								.collect(Collectors.toList());

						for (SubModuleAccessEntity moduleAccessPerStage : moduleAccessPerStages) {
							SubModuleAccessEntity modAccessPerStage = new SubModuleAccessEntity();
							modAccessPerStage.setAccessId(moduleAccessPerStage.getAccessId());
							List<String> rights = new ArrayList<>();
							rights.addAll(moduleAccessPerStage.getActivityAccess());

							if (!manualApproveStages.contains(moduleAccessPerStage.getAccessId())) {
								rights.remove("APPROVE");
							}

							modAccessPerStage.setActivityAccess(rights);
							updateModuleAccessPerStages.add(modAccessPerStage);
						}
						StagesAccessStatus.addAll(updateModuleAccessPerStages);
					}

				} catch (Exception e) {
					logger.error("Error while parsing relation", e);
				}

				query = new Query();
				query.addCriteria(
						Criteria.where("parent_id").is(currentProduct.getCustomFields().get("productHierarchyId")));
				ProductHierarchyEntity childpHId = mongoOperations.findOne(query, ProductHierarchyEntity.class);
				Map<String, Object> childValue = new HashMap<String, Object>();
				Map<String, Object> childData = new HashMap<String, Object>();
				List<Map<String, Object>> childList = new ArrayList<>();
				List<String> headerList = new ArrayList<>();
				String pHName = "";

				if (childpHId != null) {
					pHName = childpHId.getProductHierarchyName();
					// Get current product object's children
					query = new Query();
					query.addCriteria(Criteria.where("CustomFields.parentID").is(id)
							.and("CustomFields.productHierarchyId").is(childpHId.getId()));// parentID for Edit--Chella
					children = mongoOperations.find(query, ProductMapEntity.class);
					if (children != null && children.size() > 0) {
						Map<String, Object> criteriaMap = new HashMap<>();
						criteriaMap.put("isDisplay", Boolean.TRUE);
						criteriaMap.put("isActive", Boolean.TRUE);
						criteriaMap.put("isDeleted", Boolean.FALSE);
						criteriaMap.put("pageName", "viewProduct");
						criteriaMap.put("productHierarchyId", childpHId.getId());
						List<String> displayNames = getDistinctFieldValues("cGridHeaders", criteriaMap, "fieldName",
								"displayOrder");
						List<MetaDataFieldsEntity> fieldEntity = getMetaDataFieldLogicDetails(displayNames,
								childpHId.getId());
						headerList = fieldEntity.stream().map(e -> e.getFieldDisplayName())
								.collect(Collectors.toList());
						for (ProductMapEntity cpro : children) {
							childValue = new HashMap<String, Object>();
							String jsonString = new Gson().toJson(cpro);

							DocumentContext docCtx = JsonPath.parse(jsonString);
							for (MetaDataFieldsEntity metaDataFieldsEntity : fieldEntity) {
								JsonPath jsonPath = JsonPath.compile(metaDataFieldsEntity.getJsonPath());
								Object obj = null;
								if (null != jsonPath.getPath()) {
									try {
										obj = docCtx.read(jsonPath);
										childValue.put(metaDataFieldsEntity.getFieldDisplayName(),
												obj.toString().replace("[", "").replace("]", "").replace("\"", "").replace("\\", ""));
										childValue.put("metadataId", cpro.getMetadataId());
										childValue.put("productHierarchyId",
												cpro.getCustomFields().get("productHierarchyId"));
										if (metaDataFieldsEntity.getMetaDataFieldName()
												.equalsIgnoreCase("customFieldsTitleTextValue"))
											childValue.put(metaDataFieldsEntity.getMetaDataFieldName(),
													obj.toString().replace("[", "").replace("]", "").replace("\"", "").replace("\\", ""));
									} catch (Exception e) {
										logger.error("Field Not found in json : "
												+ metaDataFieldsEntity.getFieldDisplayName());
										childValue.put(metaDataFieldsEntity.getFieldDisplayName(), "");
										childValue.put("metadataId", cpro.getMetadataId());
										childValue.put("productHierarchyId",
												cpro.getCustomFields().get("productHierarchyId"));
									}
								}
							}
							childList.add(childValue);

						}

					}

				}

				childData.put("headers", headerList);
				childData.put("data", childList);
				childData.put("name", pHName);

				// Set all values once current product is found
				pdWithRltns.put("Product", currentProduct);
				pdWithRltns.put("parent", parentsResult);
				pdWithRltns.put("breadCrumb", breadCrumb);
				pdWithRltns.put("siblings", siblings);
				pdWithRltns.put("children", childData);
				pdWithRltns.put("stages", Stages);
				pdWithRltns.put("stagesAccess", StagesAccessStatus);
			}
		} catch (Exception e) {
			logger.error("Error occured while fetching from :: getProductWithRelations ", e);
		}
		return pdWithRltns;
	}

	private List<String> approvedAccessStages(String loggedUser) {
		List<String> stages = new ArrayList<>();

		Query queryUser = new Query();
		queryUser.addCriteria(
				Criteria.where("userId").is(loggedUser).and("isActive").is(true).and("isDeleted").is(false));
		UserEntity userEntity = mongoTemplate.findOne(queryUser, UserEntity.class);

		RoleEntity role = null;

		if (userEntity != null) {
			String permissionGroupId = userEntity.getPermissionGroupId();
			if (permissionGroupId != null) {
				Query queryPG = new Query();
				queryPG.addCriteria(Criteria.where("permissionGroupId").is(permissionGroupId).and("isActive").is(true)
						.and("isDeleted").is(false));
				PermissionGroupEntity pg = mongoTemplate.findOne(queryPG, PermissionGroupEntity.class);
				String roleId = pg.getRoleId();
				if (roleId != null) {
					Query queryRole = new Query();
					queryRole.addCriteria(
							Criteria.where("roleId").is(roleId).and("isActive").is(true).and("isDeleted").is(false));
					role = mongoTemplate.findOne(queryRole, RoleEntity.class);
				}
			}
		}

		ModuleAccessEntity moduleStages = null;
		if (role != null) {
			List<ModuleAccessEntity> maEntity = role.getModule();
			moduleStages = maEntity.stream().filter(p -> p.getModuleId().equals("MOD000014")).findFirst().orElse(null);
		}

		if (moduleStages != null) {
			List<SubModuleAccessEntity> moduleAccessPerStages = moduleStages.getSubmodule();

			for (SubModuleAccessEntity moduleAccessPerStage : moduleAccessPerStages) {
				if (moduleAccessPerStage.getActivityAccess().contains("APPROVE")) {
					stages.add(moduleAccessPerStage.getAccessId());
				}
			}

		}

		return stages;
	}

	@Override
	public List<ProductStagesApprovalEntity> getStagesdataWithStatus(Object metadataId, Object phId) {
		List<ProductStagesApprovalEntity> psEntity = null;

		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("metadataId").is(metadataId).and("productHierarchyId").is(phId));
			psEntity = mongoTemplate.find(query, ProductStagesApprovalEntity.class);

		} catch (Exception e) {
			logger.error("error in while retriving stages data with status :" + e.getMessage());
		}
		return psEntity;
	}

	public List<String> getDistinctFieldValues(String collectionName, Map<String, Object> criteriaMap, String fieldName,
			String sortBy) {
		List<String> valueList = new LinkedList<>();

		try {
			Query query = new Query();
			Criteria criteria = new Criteria();

			if (!isNull(criteriaMap)) {
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());
				criteriaMap.forEach((k, v) -> {
					if (v instanceof Collection<?>) {
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					} else
						criterias.add(Criteria.where(k).is(v));
				});
				criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
				query.addCriteria(criteria);
			}

			query.fields().include(fieldName).exclude("_id");
			if (null != sortBy)
				query.with(new Sort(Sort.Direction.ASC, sortBy));

			if (null != sortBy) {
				Aggregation aggregation;
				AggregationResults<CodeDescription> groupResults = null;
				List<CodeDescription> resultList = new ArrayList<>();

				aggregation = newAggregation(match(criteria), group(fieldName, sortBy),
						sort(new Sort(Sort.Direction.ASC, "_id." + sortBy)),
						project(Fields.fields().and("code", "_id." + fieldName)));

				groupResults = mongoTemplate.aggregate(aggregation, collectionName, CodeDescription.class);
				resultList = groupResults.getMappedResults();
				valueList = resultList.stream().map(data -> data.getCode()).collect(Collectors.toList());

			} else {
				valueList = mongoTemplate.getCollection(collectionName).distinct(fieldName, query.getQueryObject());
			}
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Account list :: " + e.getMessage());
			throw e;
		}
		return valueList;
	}

	public <T> boolean isNull(T obj) {

		if (obj instanceof Collection<?>)
			return (obj == null || ((Collection<?>) obj).isEmpty());
		else if (obj instanceof String)
			return (obj == null || ((String) obj).trim().isEmpty());
		else if (obj instanceof Object[])
			return (obj == null || ((Object[]) obj).length == 0);
		else
			return obj == null;

	}

	/**
	 * Returns a map of objects of current , parent, siblings, children of given
	 * product id
	 * 
	 * @param product
	 *            id
	 * @return Map of(level/ type, product)
	 * @author Pavan Kumar Yekabote.
	 */
	/*
	 * @Override public Map<Object, Object> getProductWithRelations(String id) {
	 * 
	 * LinkedHashMap<Object, Object> pdWithRltns = null; ProductMapEntity
	 * parentProduct = null; List<ProductMapEntity> children = null, siblings =
	 * null; List<Map<Object, Object>> parentsResult = null; ObjectMapper oMapper =
	 * new ObjectMapper();
	 * 
	 * try {
	 * 
	 * parentsResult = new ArrayList<>(); siblings = new ArrayList<>(); children =
	 * new ArrayList<>();
	 * 
	 * Query query = new Query();
	 * 
	 * query.addCriteria(Criteria.where("metadataId").is(id)); ProductMapEntity
	 * currentProduct = mongoOperations.findOne(query, ProductMapEntity.class);
	 * 
	 * 
	 * 
	 * // Get parent of current product object if( currentProduct!= null ) {
	 * 
	 * // Get Hierarchy Name of a Product
	 * 
	 * String productHierarchyId =
	 * (String)currentProduct.getCustomFields().get("productHierarchyId"); query =
	 * new Query(); query.addCriteria(Criteria.where("_id").is(productHierarchyId));
	 * ProductHierarchyEntity pHEntity = mongoTemplate.findOne(query,
	 * ProductHierarchyEntity.class); Map<Object,Object> groupMap = new
	 * LinkedHashMap<>(); groupMap.put("group", pHEntity.getGroup());
	 * currentProduct.setCustomFields(groupMap);
	 * 
	 * 
	 * 
	 * pdWithRltns.put("group", pHEntity.getGroup());
	 * 
	 * pdWithRltns = new LinkedHashMap<>(); try { // Get Parent & Siblings of
	 * current product object if( currentProduct.getCustomFields().get("parentID")
	 * != null && !currentProduct.getCustomFields().get("parentID").equals("")) {
	 * 
	 * String parentId = (String)currentProduct.getCustomFields().get("parentID");
	 * 
	 * // Fetch all siblings here query = new Query();
	 * query.addCriteria(Criteria.where("CustomFields.parentID").is(parentId).and(
	 * "metadataId").ne(currentProduct.getMetadataId())); siblings =
	 * mongoOperations.find(query, ProductMapEntity.class);
	 * 
	 * 
	 * // Writing for fetching specific fields from all hierarchal parents query =
	 * new Query(); query.addCriteria(Criteria.where("metadataId").is(parentId));
	 * parentProduct = mongoOperations.findOne(query, ProductMapEntity.class);
	 * 
	 * 
	 * 
	 * // Fetch all metadataHierarchyFields which contains current products
	 * productHierarchyId query = new Query(); Collection hierarchyList = new
	 * ArrayList<>(); hierarchyList.add(productHierarchyId);
	 * 
	 * query.addCriteria(Criteria.where("editProductHierarchyId").in(hierarchyList))
	 * ; List<MetadataFields> cFields = mongoTemplate.find(query,
	 * MetadataFields.class);
	 * 
	 * 
	 * while( parentProduct != null ) {
	 * 
	 * String parentPHID = (String)
	 * parentProduct.getCustomFields().get("productHierarchyId");
	 * List<MetadataFields> tempFields = cFields.stream().filter(mfield ->
	 * mfield.getProductHierarchyId().equals(parentPHID) )
	 * .collect(Collectors.toList());
	 * 
	 * LinkedHashMap<Object, Object> tMap = new LinkedHashMap<>();
	 * 
	 * Map<Object, Object> tempMap = oMapper.convertValue(parentProduct, Map.class);
	 * 
	 * for(MetadataFields mfield : tempFields)
	 * tMap.put(mfield.getMetaDataFieldName(),
	 * tempMap.get(mfield.getMetaDataFieldName()) == null ? "" :
	 * tempMap.get(mfield.getMetaDataFieldName()) );
	 * 
	 * parentsResult.add(tMap);
	 * 
	 * // Fetch next hierarchal parent query = new Query(); parentId = (String)
	 * parentProduct.getCustomFields().get("parentID");
	 * query.addCriteria(Criteria.where("metadataId").is(parentId)); parentProduct =
	 * mongoOperations.findOne(query, ProductMapEntity.class); if(parentId == null
	 * || (parentId !=null && parentId.trim().equals("")) ) parentProduct = null; }
	 * 
	 * 
	 * } } catch(Exception e1) {
	 * logger.error("Error occured while fetching parents and siblings",e1); }
	 * 
	 * 
	 * // Get current product object's children query = new Query();
	 * query.addCriteria(Criteria.where("CustomFields.parentID").is(id));//parentID
	 * for Edit--Chella children = mongoOperations.find(query,
	 * ProductMapEntity.class);
	 * 
	 * // Set all values once current product is found pdWithRltns.put("product",
	 * currentProduct); pdWithRltns.put("parent", parentsResult);
	 * pdWithRltns.put("siblings", siblings); pdWithRltns.put("children", children);
	 * } } catch (Exception e) {
	 * logger.error("Error occured while fetching from :: getProductWithRelations ",
	 * e); } return pdWithRltns; }
	 */

	/**
	 * Updates the current product, as well as all the instances of this product
	 * present in it's children.
	 * 
	 * @author Pavan Yekabote
	 * @return boolean isUpdated or not
	 */
	@Override
	public boolean updateProductsWithSync(ProductMapEntity product) {

		Query query = new Query();
		Update update = new Update();
		try {
			logger.info("PID: " + product.getMetadataId());

			if (product.getMetadataId() == null)
				throw new NullPointerException("Metadata ID Not Found");

			query.addCriteria(Criteria.where("metadataId").is(product.getMetadataId())
					.and("CustomFields.productHierarchyId").is(product.getCustomFields().get("productHierarchyId")));

			update.set("Product", product.getProduct()).set("CustomFields", product.getCustomFields());

			// Update main product first
			mongoOperations.updateFirst(query, update, "products");

			// Commenting SYNC Operation
			query = new Query();
			update = new Update();
			query.addCriteria(Criteria.where("CustomFields.parents.metadataId").is(product.getMetadataId()));
			update.set("CustomFields.parents.$.Product", product.getProduct())
					.set("CustomFields.parents.$.CustomFields", product.getCustomFields());

			// Update this product present as parent in all the instances.
			mongoOperations.updateMulti(query, update, ProductMapEntity.class);

			return true;

		} catch (Exception e) {
			logger.error("Error occured while updating product " + product.getMetadataId()
					+ " .:: updateProductsWithSync . ", e);
		}
		return false;
	}

	@Override
	public List<ProductMapEntity> getLastModifiedProducts() {
		List<ProductMapEntity> productList = null;

		// Get one hour converted to Milliseconds
		long hrsToMillis = TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS);
		long currentTimeStamp = System.currentTimeMillis();
		Date currentTime = new Date(currentTimeStamp);
		// Take time before one hour
		Date oneHourBeforeTime = new Date(currentTimeStamp - hrsToMillis);

		Query query = new Query();
		query.addCriteria(Criteria.where("Product.LastModifiedOn").lte(currentTime).gte(oneHourBeforeTime));
		productList = mongoOperations.find(query, ProductMapEntity.class);

		return productList;
	}

	@Override
	public boolean updateToSyncProductInChildren(ProductMapEntity product) {
		try {

			Query query = new Query();
			Update update = new Update();
			query.addCriteria(Criteria.where("CustomFields.parents.metadataId").is(product.getMetadataId()));
			update.set("CustomFields.parents.$.Product", product.getProduct())
					.set("CustomFields.parents.$.CustomFields", product.getCustomFields());

			// Update this product present as parent in all the instances.
			mongoOperations.updateMulti(query, update, ProductMapEntity.class);
		} catch (Exception e) {
			logger.error("Error occured while updating product in children :: updateToSyncProductInChildren ");
		}
		return false;

	}

	/**
	 * Retrieves all the syncFields from existing product at the time of creating
	 * <br/>
	 * the product at same hierarchy
	 * 
	 * 
	 * @return <b>product json string</b> with all sync fields(if exists)
	 * @author Rupesh Maharjan
	 **/
	@Override
	public String getSyncFields(String productHierarchyID, String parentId, String referenceParentId) {
		Product prod = null;
		ProductHierarchyEntity pH = null;
		String syncFieldPath = "", syncField = "";
		// Get Product Hierarchy and syncField details
		Query pHqry = new Query();
		pHqry.addCriteria(
				Criteria.where("_id").is(productHierarchyID).and("isDeleted").is(false).and("isActive").is(true));
		pH = mongoTemplate.findOne(pHqry, ProductHierarchyEntity.class);
		if (pH != null) {
			syncFieldPath = pH.getSyncField();
			syncField = syncFieldPath.split("\\.")[1];
		}

		if (parentId != null && referenceParentId == null) {
			// query the product in given productHierarchy with given parentId
			Query q = new Query();
			q.addCriteria(Criteria.where("CustomFields.productHierarchyId").is(productHierarchyID)
					.and("CustomFields.parentID").is(parentId).and(syncFieldPath).is("Yes"));
			prod = mongoTemplate.findOne(q, Product.class, "products");
		} else if (referenceParentId != null && parentId == null) {
			// query the product in given productHierarchy with given referenceId
			Query q = new Query();
			q.addCriteria(Criteria.where("CustomFields.productHierarchyId").is(productHierarchyID)
					.and("CustomFields.referenceParentID").is(referenceParentId).and(syncFieldPath).is("Yes"));
			prod = mongoTemplate.findOne(q, Product.class, "products");
		}
		String jsonResult = "{ \"Product\":{}, \"CustomFields\":{\"" + syncField + "\":\"Yes\"} }";
		if (prod != null) {
			Gson gson = new Gson();

			jsonResult = "{ \"Product\":{}, \"CustomFields\":{\"" + syncField + "\":\"No\"} }";

			String prodJson = gson.toJson(prod);
			DocumentContext docCtx = JsonPath.parse(prodJson);
			DocumentContext docCtxResult = JsonPath.parse(jsonResult);

			// query all the fields in given hierarchy with field "syncField: true"
			Query query = new Query();
			query.addCriteria(Criteria.where("productHierarchyId").is(productHierarchyID).and("syncField").is(true)
					.and("isActive").is(true).and("isDeleted").is(false));
			List<MetaDataFieldsEntity> metadatafields = mongoTemplate.find(query, MetaDataFieldsEntity.class,
					"cMetadataFields");
			boolean hasSubjectInJP = false;
			for (MetaDataFieldsEntity mdata : metadatafields) {
				String jsonPath = mdata.getJsonPath();
				if (jsonPath.contains("Subject")) {
					hasSubjectInJP = true;
				}
				try {
					Object value = docCtx.read(jsonPath);
					if (value != null && !jsonPath.contains("Subject")) {
						if (value instanceof Collection<?>) {
							value = ((List<?>) value).stream().map(e -> String.valueOf(e))
									.collect(Collectors.joining(""));
							value = StringEscapeUtils.escapeJava((String) value);
						} else {
							value = StringEscapeUtils.escapeJava((String) value);
						}
						if (!value.equals("")) {
							if (mdata.getJsonType().equalsIgnoreCase("Single"))
								new MetaDataServiceImpl().processForSingleType(mdata, docCtxResult, value.toString());
							else
								new MetaDataServiceImpl().processJsonDbPathCondition(mdata, value.toString(),
										docCtxResult);
						}
					}
				} catch (Exception e) {
					logger.warn(e.getMessage());
				}
			}
			if (hasSubjectInJP) {
				String subjectJsonPath = "$.Product.Subject";
				Object subjectBlock = docCtx.read(subjectJsonPath);
				docCtxResult.put("$.Product", "Subject", subjectBlock);
				query = new Query();
				query.addCriteria(Criteria.where("jsonPath").regex("Subject").and("syncField").exists(false)
						.and("isActive").is(true).and("productHierarchyId").is(productHierarchyID));
				List<MetaDataFieldsEntity> fs = mongoTemplate.find(query, MetaDataFieldsEntity.class,
						"cMetadataFields");
				if (fs.size() > 0) {
					for (MetaDataFieldsEntity f : fs) {
						String removeJsonp = f.getJsonPath();
						docCtxResult.delete(removeJsonp);
					}
				}
			}
			String result = StringEscapeUtils.unescapeJava(docCtxResult.jsonString());
			return result;
		}
		return jsonResult;
	}

	/**
	 * Fetch productHierarchies based on group
	 * 
	 * @return List<ProductHierarchyEntity>
	 * @author Pavan Kumar Yekabote.
	 */
	public List<ProductHierarchyEntity> getProductHierarchiesBy(String groupname) {
		List<ProductHierarchyEntity> pList = null;

		try {
			Query query = new Query();
			Criteria[] c = new Criteria[] { Criteria.where("group").is(groupname),
					Criteria.where("isActive").is(true) };
			query.addCriteria(new Criteria().andOperator(c));
			query.with(new Sort(Sort.Direction.ASC, "parent_id"));
			pList = mongoTemplate.find(query, ProductHierarchyEntity.class);
		} catch (Exception e) {
			logger.error("Exception ", e);
		}
		return pList;
	}

	/**
	 * Getting ISBN from SupportTableData collection
	 * 
	 * @author Chella
	 * @return {@link APIResponse} db.supportTableData.find( {$or:[ {$and :
	 *         [{SupportKey : 'ISBN'},{isActive: true}, {isDeleted : false},
	 *         {'Value.isActive': true}, {'Value.isDeleted':false},
	 *         {'Value.Status':'Y'}]} ,
	 * 
	 * 
	 *         {$and : [{'Value.Status':'T'},{'Value.modifiedOn' : {$gte: new
	 *         Date(new Date() - 1000*24*60*60)}}]}
	 * 
	 *         ] } ).limit(10)
	 */
	@Override
	public Map<String, Object> getISBNValue() {
		APIResponse<Object> response = new APIResponse<>();
		// String ISBN="";
		Map<String, Object> ISBN = null;
		try {
			Query query = new Query();
			Criteria[] c = new Criteria[] { Criteria.where("SupportKey").is("ISBN"),
					Criteria.where("isActive").is(true), Criteria.where("Value.isActive").is(true),
					Criteria.where("Value.isDeleted").is(false), Criteria.where("Value.Status").is("Y") };
			Criteria[] c1 = new Criteria[] { Criteria.where("SupportKey").is("ISBN"),
					Criteria.where("Value.Status").is("T"),
					Criteria.where("Value.modifiedOn").lte(new Date(new Date().getTime() - 1000 * 24 * 60 * 60)) };
			Criteria c11 = new Criteria().andOperator(c);
			Criteria c12 = new Criteria().andOperator(c1);
			Criteria c22[] = { c11, c12 };
			query.addCriteria(new Criteria().orOperator(c22));
			query.with(new Sort(Sort.Direction.ASC, "Key"));
			query.fields().include("Key").exclude("_id");
			query.limit(1);
			logger.info("Query for update findandmodifiy : " + query);
			Update update = new Update();
			update.set("Value.Status", "T");
			update.set("Value.modifiedOn", new Date());

			ISBN = mongoOperations.findAndModify(query, update, Map.class, "supportTableData");
			return ISBN;
			/*
			 * response.setCode("200"); response.setData(ISBN);
			 * response.setStatus("Success");
			 */

		} catch (Exception e) {
			// TODO: handle exception
			/*
			 * response.setCode("500"); response.setData(null);
			 * response.setStatus("Failed");
			 */
		}

		return null;
	}

	@Override
	public List<Map> getSupportTableMapping() {
		Criteria c = Criteria.where("lookupDisplayName").exists(true).and("isActive").is(true).and("isDeleted")
				.is(false);
		Sort sort = new Sort(Sort.Direction.ASC, new String[] { "lookupDisplayName" });
		GroupOperation groupOperation = group("lookupDisplayName", "lookupName");
		SortOperation sortOperation = sort(sort);
		ProjectionOperation projectOperation = project().and("_id.lookupDisplayName").as("lookupDisplayName")
				.and("_id.lookupName").as("lookupName").andExclude("_id");
		MatchOperation matchOperation = match(c);

		Aggregation agg = newAggregation(matchOperation, groupOperation, projectOperation, sortOperation);
		List<Map> results = mongoTemplate.aggregate(agg, supportTableConfig, Map.class).getMappedResults();
		return results;
	}

	@Override
	public String generateExcelForSupportTable(String lookupName, String format, String userId) {
		Criteria c = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("isDisplay").is(true);
		Criteria c1 = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("forExcelDisplay").is(true);
		Criteria[] c2 = { c, c1 };
		Criteria c3 = new Criteria();
		c3.orOperator(c2);
		Query query = new Query(c3);
		System.out.println("Excel Query : " + query);
		Sort sort = new Sort(Sort.Direction.ASC, new String[] { "excelDisplayOrder" });
		query.with(sort);
		SupportTableConfigMain config = null;
		List<SupportTableConfigMain> tableConfigs = mongoTemplate.find(query, SupportTableConfigMain.class,
				this.supportTableConfig);
		String path = null;
		File file = null;
		String url = null;
		try {
			path = generateExcel(tableConfigs, format);
			file = new File(path);
			url = saveFile2S3(file, userId, lookupName, 1);
		} catch (Exception e) {
			logger.info("Exception in excel generation : " + e);
		} finally {
			try {
				FileUtils.deleteDirectory(file);
			} catch (Exception e) {
			}
		}
		return url;

	}

	private String generateExcel(List<SupportTableConfigMain> list, String format) {
		XSSFWorkbook book = new XSSFWorkbook();
		XSSFSheet sheet = book.createSheet();
		sheet.setSelected(true);

		Row row = sheet.createRow(0);
		SupportTableConfigMain config = null;
		Cell cell = null;
		CellStyle style = null;
		Font font = null;
		String path = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("HHMMSSyyyyMMdd");
		String tempDir = dateFormat.format(new Date());
		if (format == null || (format != null && format.equals("XLSX"))) {
			format = "xlsx";
		}
		File outputDir = new File(outputPath + File.separator + tempDir);
		for (int i = 0; i < list.size(); i++) {
			cell = row.createCell(i);
			config = list.get(i);
			style = book.createCellStyle();
			font = book.createFont();
			font.setColor((short) config.getFontcolor());
			style.setFillBackgroundColor((short) config.getBackgroundcolor());
			style.setFillPattern((short) FillPatternType.LEAST_DOTS.ordinal());
			font.setFontName(fontName);
			style.setFont(font);
			cell.setCellStyle(style);
			cell.setCellValue(config.getDisplayName());
			path = config.getLookupDisplayName();
			// System.out.println(config.getDisplayName()+" : " + path + " : " +
			// config.getDisplayOrderNo());
		}
		try {

			outputDir.mkdirs();
			File outputDirFile = new File(
					outputDir.getAbsolutePath() + File.separator + path.replace(" ", "_") + "." + format);

			// File outputDirFile = new File(outputDir.getAbsolutePath()+File.separator+
			// path);
			book.write(new FileOutputStream(outputDirFile));
			return outputDirFile.getAbsolutePath();
		} catch (Exception e) {
			logger.info("Some exception has occurred during excel generation : " + e);
		} finally {
			if (book != null) {
				book = null;
			}
			try {
				// FileUtils.deleteDirectory(outputDir);
			} catch (Exception e) {
			}
		}
		return outputDir.getAbsolutePath();

	}

	private List<Map> readExcelForSupportTable(FileObj fileObj, Map<String, SupportTableConfigMain> configMap) {
		String excelFilePath = fileObj.getPath();
		String lookupName = fileObj.getLookupName();
		Criteria c = new Criteria();
		c.and(supportKey).is(lookupName);
		File excelFile = new File(excelFilePath);
		Workbook workbook = null;
		LinkedList<Map> listOfRecordsStatus = new LinkedList<>();
		Row row = null;
		Sheet sheet = null;
		Row headerRow = null;
		Cell currentCell = null;
		Cell headerCell = null;
		Object currentCellObject = null;
		Object headerCellObject = null;
		Iterator<Cell> cellsInRow = null;
		LinkedHashMap<String, Object> record = null;
		SupportTableConfigMain config = null;
		String dataType = "string";
		int cellCount = 0;
		String keyFieldDataType = null;
		LinkedList<Object> keyObjects = new LinkedList<>();
		List<ValidateErrorBean> errors = new ArrayList<>();
		Map<Object, List> duplicateMap = null;
		Object duplicateRow = "";
		List<Integer> duplicateRows = new LinkedList<>();
		ValidateErrorBean errorBean = null;
		Map<Integer, Map> recordsMap = new LinkedHashMap<>();
		String regex = null;
		if (excelFile.exists()) {
			try {
				if (excelFilePath.endsWith(".xls")) {
					workbook = new HSSFWorkbook(new FileInputStream(excelFile));
				} else if (excelFilePath.endsWith(".xlsx")) {
					workbook = new XSSFWorkbook(new FileInputStream(excelFile));
				}
				sheet = workbook.getSheetAt(0);
				headerRow = sheet.getRow(0); // Hard coded
				Iterator<Cell> rowHeaderCells = headerRow.cellIterator();
				for (int rowNum = 1; rowNum < sheet.getPhysicalNumberOfRows(); rowNum++) {
					row = sheet.getRow(rowNum);
					cellCount = 0;
					cellsInRow = row.cellIterator();
					record = new LinkedHashMap<>();
					errors = new ArrayList<>();
					Criteria c1 = new Criteria();
					c1.and("SupportKey").is(lookupName);
					duplicateRow = "";
					duplicateMap = new HashMap();
					for (int cn = 0, headerCount = 0; cn < headerRow.getLastCellNum()
							&& rowHeaderCells.hasNext(); cn++, headerCount++) {
						currentCell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
						headerCell = headerRow.getCell(headerCount, Row.CREATE_NULL_AS_BLANK);
						headerCellObject = headerCell.getStringCellValue();
						config = configMap.get(headerCellObject.toString());
						if (config == null) {

							// record.put(headerCellObject.toString(), currentCellObject);
							// System.out.println(cellCount+":"+headerCellObject.toString()+" is "+config );
							cellCount++;
							continue;
							// Some thing needs to be done
						}
						dataType = config.getDataType(); // Uncomment when required - M6S
						regex = config.getRegex();
						keyFieldDataType = config.getKeyFieldDataType();
						switch (currentCell.getCellType()) {
						case CELL_TYPE_BLANK:
							if (config.getRequired()) {
								// Mark this object as error
							}
							currentCellObject = null;
							break;
						case CELL_TYPE_BOOLEAN:
							currentCellObject = currentCell.getBooleanCellValue();
							break;
						case CELL_TYPE_NUMERIC:
							currentCellObject = currentCell.getNumericCellValue();
							break;
						case CELL_TYPE_STRING:
							currentCellObject = currentCell.getStringCellValue();
							break;

						case CELL_TYPE_FORMULA:
							DataFormatter formatter = new DataFormatter();
							currentCellObject = formatter.formatCellValue(currentCell);

							if (currentCellObject != null && currentCellObject.toString().trim().equals("")) {
								// Some error needs to be thrown
							}

							break;
						default:
							currentCellObject = currentCell.getStringCellValue();
							break;
						}
						// if(keyFieldDataType!= null)
						{
							if (currentCellObject != null) {
								if (dataType.equalsIgnoreCase("Integer") && currentCellObject instanceof Double) {
									currentCellObject = ((Double) currentCellObject).intValue();
								} else if (dataType.equalsIgnoreCase("string")
										&& currentCellObject instanceof Integer) {
									currentCellObject = currentCellObject.toString();
								} else if (dataType.equalsIgnoreCase("Integer")
										&& currentCellObject instanceof String) {
									errorBean = new ValidateErrorBean(config.getDisplayName(),
											"data type is wrong " + config.getDisplayName());
									errors.add(errorBean);
								}
								// if (dataType.equals("string"))
								{
									if (regex != null && regex.trim().length() > 0) {
										// Apply regex
										boolean isRegexPassed = Pattern.matches(regex, currentCellObject.toString());
										if (!isRegexPassed) {
											errorBean = new ValidateErrorBean(config.getDisplayName(),
													"Invalid data " + config.getDisplayName());
											errors.add(errorBean);
										}
									}
								}
							}
							if (config.getRequired() != null && config.getRequired()
									&& (currentCellObject == null || (currentCellObject != null
											&& currentCellObject.toString().trim().length() == 0))) {
								errorBean = new ValidateErrorBean(config.getDisplayName(),
										"mandatory field value missing in " + config.getDisplayName());
								errors.add(errorBean);
							}

						}
						if (config.getIsKey() != null && config.getIsKey()) {
							keyObjects.add(currentCellObject);
						}
						if (config.getGroup() != null && config.getGroup()) {
							if (currentCellObject != null && dataType.equalsIgnoreCase("string")) {
								c1 = c1.and(config.getReferencePath()).is(currentCellObject.toString());
								duplicateRow += currentCellObject.toString();
							} else if (currentCellObject != null) {
								c1 = c1.and(config.getReferencePath()).is(currentCellObject);
								duplicateRow = currentCellObject;
							} else {
								c1 = c1.and(config.getReferencePath()).is(null);
								duplicateRow += "";
							}

						}

						// System.out.print(cellCount+":"+headerCellObject +":"+ currentCellObject+",");
						record.put(config.getFieldName(), currentCellObject);
						cellCount++;
						if (config != null && config.getIsKey() != null && config.getIsKey()) {
							record.put("configObject", config);
						}
					}
					Query query1 = new Query(c1);
					/*
					 * -- Commented for the time being - M6S * Map record1 =
					 * mongoTemplate.findOne(query1, Map.class, this.supportTableCollection);
					 * if(record1 != null) {
					 * 
					 * }
					 */
					if (duplicateMap.get(duplicateRow) != null) {
						duplicateMap.get(duplicateRow).add(rowNum + 1);
						duplicateMap.put(duplicateRow, duplicateRows);
					} else {
						duplicateRows = new LinkedList<>();
						duplicateRows.add(rowNum + 1);
						duplicateMap.put(duplicateRow, duplicateRows);
					}
					record.put("rowNo", rowNum + 1);
					if (errors.size() > 0) {
						record.put("errors", errors);
					}
					listOfRecordsStatus.add(record);
					recordsMap.put(rowNum, record);
					// System.out.println("");
				}
				for (Object key : duplicateMap.keySet()) {
					List<Integer> dups = duplicateMap.get(key);
					if (dups.size() > 1) {
						String duplicateMsg = "Duplicate Entry in row numbers (";
						Map myRecord = recordsMap.get(dups.get(0));
						for (Integer dupRowNo : dups) {
							duplicateMsg += dupRowNo + ",";
						}
						duplicateMsg += ")";
						if (myRecord.get("errors") != null) {
							errors = (List<ValidateErrorBean>) myRecord.get("errors");
						} else {
							errors = new ArrayList<>();
						}
						errors.add(new ValidateErrorBean("Key", duplicateMsg));
						myRecord.put("errors", errors);
					}
				}
				c.and("Key").in(keyObjects);
				Query query = new Query();
				query.addCriteria(c);
				System.out.println("Query : " + query);
				List<Map> resultMap = mongoTemplate.find(query, Map.class, supportTableCollection);
				String referencePath = null;
				Object temp = null;
				Object key1 = null;
				Object key2 = null;
				String referencePathTemp = null;
				Boolean isFound = false;
				for (Map m1 : listOfRecordsStatus) {
					config = (SupportTableConfigMain) m1.get("configObject");
					isFound = false;
					key1 = null;
					key2 = null;
					if (config != null) {
						referencePath = config.getReferencePath();
						key1 = m1.get(config.getFieldName());
						isFound = false;
						for (int i = 0; i < resultMap.size(); i++) {
							Map m2 = resultMap.get(i);
							if (referencePath.startsWith("Key")) {
								key2 = m2.get("Key");
							} else if (referencePath.startsWith("Value.")) {
								temp = m2.get("Value");
								referencePathTemp = referencePath.replace("Value.", "");
								key2 = ((Map) temp).get(referencePathTemp);
							}

							else {
								temp = m2;
								key2 = m2.get(referencePath);
							}
							if ((key1 != null && key2 != null) && key1.toString().equals(key2.toString())) {
								isFound = true;
								break;// Breaking the inner loop
							}
						} // end inner for

					}
					if (isFound) {
						m1.put("NewObject", false);
					} else {
						m1.put("NewObject", true);
					}
					m1.remove("configObject");
				} // end outer for
			}

			catch (Exception e) {
				e.printStackTrace();
				logger.info("Some exception has occurred while reading excel : " + e);
			} finally {
				if (workbook != null) {
					workbook = null;
				}
			}
		}
		return listOfRecordsStatus;
	}

	@Override
	public List<Map> validateExcelForSupportTable(FileObj fileObj) {
		String lookupName = fileObj.getLookupName();
		Criteria c = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("isDisplay").is(true);
		Criteria c1 = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("forExcelDisplay").is(true);
		Criteria[] c2 = { c, c1 };
		Criteria c3 = new Criteria();
		c3.orOperator(c2);
		Query query = new Query(c3);
		query.with(new Sort(Sort.Direction.ASC, "displayOrderNo"));
		List<SupportTableConfigMain> supportTableConfigObjects = null;
		supportTableConfigObjects = mongoTemplate.find(query, SupportTableConfigMain.class, supportTableConfig);
		Map<String, SupportTableConfigMain> excelConfigMap = new LinkedHashMap<>();
		for (SupportTableConfigMain m : supportTableConfigObjects) {
			if (m.getDisplayName() != null) {
				excelConfigMap.put(m.getDisplayName(), m);
			}
		}
		return readExcelForSupportTable(fileObj, excelConfigMap);
	}

	@Override
	public List<Map> uploadExcelForSupportTable(FileObj fileObj) {

		return validateExcelForSupportTable(fileObj);
	}

	@Override
	public Map<String, Object> exportSupportTable(SupportRequestEntity entity) {
		String format = entity.getExportFormat();
		String lookupName = entity.getLookupName();
		Map<String, Object> queries = this.viewSearchForSupportTable(entity);
		Object queryObj = queries.get("query");
		List<HeaderInfo> headerInfoList = (List<HeaderInfo>) queries.get("headerInfo");
		Boolean isDisplay = false;
		List<String> headers = new LinkedList<>();
		String docpath = null;
		File path2CsvTemp = null;
		Collections.sort(headerInfoList, new HeaderInfo());
		for (HeaderInfo m1 : headerInfoList) {
			isDisplay = m1.getIsDisplay();
			if (isDisplay != null && isDisplay) {
				headers.add(m1.getHeaderName());
			}
		}
		Map<String, Object> status = this.export(entity, entity.getUserId(), headers, batchLimit, queryObj.toString());
		path2CsvTemp = (File) status.get("docpath");
		docpath = path2CsvTemp.toString();
		String url = null;
		try {
			url = saveFile2S3(path2CsvTemp, entity.getUserId(),
					(entity.getLookupDisplayName() != null) ? entity.getLookupDisplayName() : entity.getLookupName(),
					1);
		} catch (Exception e) {
		} finally {
			try {
				FileUtils.deleteDirectory(path2CsvTemp);
			} catch (Exception e) {
			}
		}
		/*
		 * String objectKey = s3CsvRoot + "/" + path2CsvTemp.getName(); AwsAPIEntity
		 * apiEntity = this.getAwsCredentials(); BasicAWSCredentials awsCreds = new
		 * BasicAWSCredentials(apiEntity.getAccessKey(), apiEntity.getSecretKey());
		 * AmazonS3 s3client = new AmazonS3Client(awsCreds);
		 * s3client.setRegion(Region.getRegion(Regions.fromName(apiEntity.getRegion())))
		 * ; s3client.setS3ClientOptions(S3ClientOptions.builder().
		 * setAccelerateModeEnabled(true).build()); PutObjectRequest por = new
		 * PutObjectRequest(apiEntity.getBucketName(), objectKey, path2CsvTemp);
		 * 
		 * s3client.putObject(por); GeneratePresignedUrlRequest preUrl = new
		 * GeneratePresignedUrlRequest(apiEntity.getBucketName(), objectKey);
		 * preUrl.setMethod(HttpMethod.GET);
		 * 
		 * Calendar cal = Calendar.getInstance();
		 * cal.setTimeInMillis(cal.getTimeInMillis() + (24 * 60 * 1000 * 60));
		 * 
		 * preUrl.setExpiration(cal.getTime()); URL url =
		 * s3client.generatePresignedUrl(preUrl); System.out.println(url.toString());
		 * DownloadEntity m = new DownloadEntity(); m.setUserId(entity.getUserId());
		 * m.setDownloadLink(url.toString()); m.setCreatedBy(entity.getUserId());
		 * m.setModifiedBy(entity.getUserId()); m.setCreatedDate(new Date());
		 * m.setModifiedDate(new Date()); m.setDownloadType(lookupName.replace("_", " ")
		 * + " Export"); m.setUniqueId(path2CsvTemp.getName()); m.setExpiryDays(1);
		 * m.setVersionId(null); m.setDownloadStatus("15");
		 * 
		 * m.setExpiryDate(cal.getTime()); mongoTemplate.insert(m,
		 * tDownloadCollectionName);
		 */
		status.put("csvurl", url.toString());
		return status;

	}

	private Boolean getColumnFilterSuggestionsStatus(SupportRequestEntity e) {
		// aMp<String,Object> filterFields = e.getFilterByFields();
		List<FieldInfo> filterFields = e.getFilterByFields();
		String searchText = e.getSearchText();
		String selectedFieldName = e.getSelectedFieldName();
		if (selectedFieldName != null && (searchText != null && searchText.trim().length() >= 0)) {
			return true;
		}
		return false;
	}

	public AwsAPIEntity getAwsCredentials() {
		AwsAPIEntity apiEntity = null;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("company").is("US"));
			apiEntity = (AwsAPIEntity) mongoOperations.findOne(query, AwsAPIEntity.class);

		} catch (Exception e) {
			logger.error("Exception occured while AWS credentials :: " + e.getMessage());
			throw e;
		}
		return apiEntity;
	}

	@Override
	public Map<String, Object> viewSearchForSupportTable(SupportRequestEntity entity) {
		String lookupName = entity.getLookupName();
		Criteria c = new Criteria();
		c = c.and("lookupName").is(lookupName).and("isDisplay").is(true);
		Boolean includeInActiveRecords = entity.getIncludeInActive();
		if (includeInActiveRecords == null || (includeInActiveRecords != null && !includeInActiveRecords)) {
			c = c.and("isDeleted").is(false).and("isActive").is(true);
		}
		Criteria c1 = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("forExcelDisplay").is(true);
                Criteria c11 = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("fieldName").is("Key");
		Criteria[] c2 = { c, c1,c11 };
		Criteria c3 = new Criteria();
		c3.orOperator(c2);
		Query query = new Query(c3);
		List<HeaderInfo> headerList = new LinkedList<HeaderInfo>();
		boolean isColumnFilter = getColumnFilterSuggestionsStatus(entity);
		long count1 = 0;
		Object o = null;
		LinkedHashMap m11 = null;
		Boolean isExportCheck = entity.getExportCheck();
		HashMap gridQuery = new HashMap<>();
		Object obj = null;
		List listObjects = null;
		List<Map> queryMap1 = null;
		HeaderInfo headerInfo = null;
		String searchText = entity.getSearchText();
		String selectedFieldName = entity.getSelectedFieldName();
		Object val = "";
		Boolean isCascade = false;
		List<SupportTableConfigMain> supportTableConfigObjects = null;
		SupportTableConfigMain config = null;
		// Map<String,Object> filterByFields = entity.getFilterByFields();
		List<FieldInfo> filterByFields = entity.getFilterByFields();
		List<String> selectedFieldNames = entity.getSelectedFieldNames();
		List<FieldInfo> filterByNinFields = entity.getNinFilterByFields();
		PageInfo pageInfo = entity.getPageInfo();
		Integer count = defaultPageSize;
		Integer skip = 0;
		Integer pageNo = 1;
		if (pageInfo != null) {
			count = pageInfo.getCount();
			pageNo = pageInfo.getPageNo();
		}

		skip = (pageNo * count) - count;
		String dataType = null;
		Integer limit = count;
		if (searchText == null) {
			searchText = "";
		}
		FieldInfo fieldInfo = null;
		Object filterObjects = null;
		String fieldType = null;
		Boolean isCode = null;
		Map condition = null;
		// List<String> canSortFields = new LinkedList<>();
		String sortByField = entity.getSortByField();
		String sortByDirection = entity.getSortByDirection();
		searchText = Pattern.quote(searchText);
		String referencePath = null;
		String headerName = null;
		FieldInfo info2 = null;
		String fieldName = null;
		Sort sortField = null;
		// c.and("lookupName").is(lookupName);

		// .and("isDisplay").is(true);
		// query.addCriteria(c);
		query.with(new Sort(Sort.Direction.ASC, "excelDisplayOrder"));
		List<String> configuredFields = mongoTemplate.getCollection(supportTableConfig).distinct("fieldName",
				query.getQueryObject());
		supportTableConfigObjects = mongoTemplate.find(query, SupportTableConfigMain.class, supportTableConfig);
		isFilterFirst(entity);
		c = new Criteria();
		Field field1 = new Field();
		Field field2 = new Field();
		// Query query1 = new Query();
		Fields fields = fields();
		// query1.fields().exclude("_id").exclude("SupportKey");
		List<HeaderInfo> headerInfoList = null;
		Query query2 = new Query();
		query2.fields().exclude("SupportKey");
		int maxCascadingField = -1;
		Integer displayOrderNo = -1;
		Integer rowLimit = 1;
		Boolean isDisplay = true;
		Map<String, Object> returnMap = new LinkedHashMap<>();
		// c.and("isActive").is(true).and("isDeleted").is(false); //Commented for the
		// time being for testing
		c.and(supportKey).is(lookupName);
		if (includeInActiveRecords == null || (includeInActiveRecords != null && includeInActiveRecords == false)) {
			c.and("isDeleted").is(false).and("isActive").is(true);
		}
		DBObject cursor = new BasicDBObject("batchSize", defaultPageSize);
		AggregationResults<Map> groupResults = null;
		Aggregation agg = null;
		ProjectionOperation projectOperations = project().andExclude("_id");
		ProjectionOperation projectOperationsTemp = project().andExclude("_id");
		GroupOperation groupOperation = null;
		UnwindOperation unwindOper = null;
		List<AggregationOperation> operList = null;
		// if (isColumnFilter)
		{
			operList = new LinkedList<>();
		}
		List<String> tempList = new ArrayList<>();
		if (filterByFields != null && filterByFields.size() > 0 || selectedFieldName != null) {
			if (filterByFields != null)
				for (FieldInfo f : filterByFields) {
					tempList.add(f.getFieldName());
				}
			if (selectedFieldName != null) {
				tempList.add(selectedFieldName);
			}
			for (String s1 : tempList) {
				if (!configuredFields.contains(s1)) {
					throw new IllegalStateException(
							s1 + " field name is not configured, select a configured field name from headers");
				}
			}
		}

		for (int i = 0; i < supportTableConfigObjects.size(); i++) {
			headerName = supportTableConfigObjects.get(i).getDisplayName();
			fieldName = supportTableConfigObjects.get(i).getFieldName();
			referencePath = supportTableConfigObjects.get(i).getReferencePath();
			dataType = supportTableConfigObjects.get(i).getDataType();
			Boolean canSort = supportTableConfigObjects.get(i).getCanSort();
			Boolean canFilter = supportTableConfigObjects.get(i).getFilter();
			isCascade = supportTableConfigObjects.get(i).getIsCascade();
			headerInfoList = supportTableConfigObjects.get(i).getFieldArray();
			displayOrderNo = supportTableConfigObjects.get(i).getExcelDisplayOrder();
			isDisplay = supportTableConfigObjects.get(i).getIsDisplay();
			Boolean defaultSort = supportTableConfigObjects.get(i).getDefaultSort();
			Boolean required = supportTableConfigObjects.get(i).getRequired();
			Boolean isEditable = supportTableConfigObjects.get(i).getIsEditable();
			String regex = supportTableConfigObjects.get(i).getRegex();

			if (sortByDirection != null && sortByField != null && sortByField.equals(fieldName)) {
				sortField = new Sort(Sort.Direction.fromString(sortByDirection), referencePath);
				// query1.with(sortField);
			} else if (defaultSort != null && defaultSort && sortByField == null) {
				// Code added w.r.t. KK Comments
				if (referencePath.startsWith("Value")) {

					sortField = (new Sort(Sort.Direction.DESC, "Value.modifiedOn"));
				} else {
					sortField = (new Sort(Sort.Direction.DESC, "modifiedOn"));
				}
				// sortField = new Sort(Sort.Direction.ASC, referencePath);
				// query1.with(sortField);
			}
			try {

				fieldInfo = filterByFields.get(filterByFields.indexOf(new FieldInfo(fieldName)));
			} catch (Exception e) {
			}
			if (filterByFields != null && filterByFields.size() > 0
					&& (fieldInfo != null && fieldName.equals(fieldInfo.getFieldName()))) {
				filterObjects = fieldInfo.getFieldValue();
				if (filterObjects instanceof List)
					c.and(referencePath).in(((List) filterObjects).toArray());
				else
					c.and(referencePath).in(filterObjects);
				if (isCascade != null && isCascade && headerInfoList != null && headerInfoList.size() > 0
						&& maxCascadingField <= displayOrderNo) {
					maxCascadingField = displayOrderNo;
					config = supportTableConfigObjects.get(i);
				}
			}
			headerInfo = new HeaderInfo(headerName, canSort, canFilter, dataType, fieldName, isDisplay, displayOrderNo,
					(referencePath != null && referencePath.startsWith("Value.")) ? referencePath.replace("Value.", "")
							: referencePath,
					supportTableConfigObjects.get(i).getFieldInfo(), required);
			headerInfo.setRegex(regex);
			headerInfo.setIsEditable(isEditable);
                        headerInfo.setFieldLength(supportTableConfigObjects.get(i).getFieldLength());
			headerList.add(headerInfo);
			if (filterByNinFields != null && filterByNinFields.size() > 0
					&& (fieldInfo != null && fieldName.equals(fieldInfo.getFieldName()))) {
				filterObjects = fieldInfo.getFieldValue();
				if (filterObjects instanceof List)
					c.and(referencePath).nin(((List) filterObjects).toArray());
				else
					c.and(referencePath).nin(filterObjects);
			}
			fields.and(referencePath);
			if (dataType.equalsIgnoreCase("array")) {
				unwindOper = unwind(referencePath, true);

			}
			if (isColumnFilter) {
				if (selectedFieldName.equals(fieldName)) {
					projectOperations = projectOperations.and(referencePath).as("code");
					projectOperations = projectOperations.and(referencePath).as("description");
					groupOperation = group(fields().and("code").and("code").and("description"))
					// .push("description").as("description")
					;
					if (dataType.equalsIgnoreCase("string") || dataType.equalsIgnoreCase("array"))
						c.and(referencePath).regex(searchText, "i");

				}

			}

		}
		if (isCascade != null && isCascade && headerInfoList != null && config != null) {
			headerInfoList = config.getFieldArray();
			headerList.clear();
			// query1 = new Query();

			// query1.fields().exclude("_id");

			// query1.fields().include(config.getReferencePath());
			operList = new LinkedList<>();
			fields = fields();
			fields = fields.and(config.getReferencePath());
			fields = fields().and(config.getReferencePath());
			projectOperations = project();
			SortOperation sortOperation = null;
			for (HeaderInfo f : headerInfoList) {
				// query1.fields().include(f.getReferencePath());
				if (f.getDefaultSort()) {
					// Hard coded sorting field path
					if (f.getReferencePath().startsWith("Value")) {
						// sortOperation = sort(new Sort(Sort.Direction.ASC, f.getReferencePath()));
						sortOperation = sort(new Sort(Sort.Direction.DESC, "Value.modifiedOn"));
					} else {
						sortOperation = sort(new Sort(Sort.Direction.DESC, "modifiedOn"));
					}
				}
				fields = fields.and(f.getReferencePath());
				headerList.add(f);
				projectOperations = projectOperations.and("_id." + f.getReferencePath()).as(f.getReferencePath());
			}

			MatchOperation match = match(c);
			groupOperation = group(fields);
			operList.add(match);
			operList.add(groupOperation);
			//operList.add(projectOperations);
			operList.add(sortOperation);
			agg = newAggregation(operList);
			// System.out.println("agg1 : : : : " + agg);
			agg = agg.withOptions(newAggregationOptions().cursor(cursor).build());
			queryMap1 = mongoTemplate.aggregate(agg, supportTableCollection, Map.class).getMappedResults();
			returnMap.put("data", queryMap1);
			returnMap.put("count", count1);
			returnMap.put("headerList", headerList);
			return returnMap;
		}
		// query1.skip(skip);
		// query1.limit(limit);
		// query1.addCriteria(c);
		// query1.fields().exclude("_id");
		// System.out.println("------" + query1.getQueryObject());
		if (!isColumnFilter) {
			if (unwindOper != null) {
				operList.add(unwindOper);
			}
			operList.add(match(c));
			operList.add(sort(sortField));
			projectOperationsTemp = projectOperationsTemp.andInclude(fields);
			//operList.add((projectOperationsTemp));
			operList.add(skip(skip));
			operList.add(limit(limit));
			if (isExportCheck != null && !isExportCheck) {
				List<AggregationOperation> countOperList = new LinkedList<>();
				countOperList.add((unwindOper != null) ? unwindOper : match(c));
				countOperList.add((unwindOper != null) ? match(c) : group("1").count().as("n"));
				if (unwindOper != null) {
					countOperList.add(group("1").count().as("n"));
				}

				agg = newAggregation(countOperList);
				logger.info("Aggregation count query : " + agg);
				agg = agg.withOptions(newAggregationOptions().cursor(cursor).build());
				List<Count> counts = mongoTemplate.aggregate(agg, supportTableCollection, Count.class)
						.getMappedResults();
				if (counts.size() > 0)
					count1 = counts.get(0).getN();
				else
					count1 = 0;
				agg = newAggregation(operList);
				agg = agg.withOptions(newAggregationOptions().cursor(cursor).build());
				logger.info("Aggregation query : " + agg);
				queryMap1 = mongoTemplate.aggregate(agg, supportTableCollection, Map.class).getMappedResults();
			} else {
				agg = newAggregation(operList);
				agg = agg.withOptions(newAggregationOptions().cursor(cursor).build());
				logger.info("Aggregation query : " + agg);
				gridQuery.put("query",
						agg.toString().replace("\"__collection__\"", "\"" + supportTableCollection + "\""));
				gridQuery.put("headerInfo", headerList);
				gridQuery.put("supportConfigObjects", supportTableConfigObjects);

				return gridQuery;
			}

			/*
			 * queryMap1 = mongoTemplate.find(query1, Map.class, supportTableCollection);
			 * count1 = mongoTemplate.count(query1, Count.class, supportTableCollection);
			 */
		} else {
			// System.out.println("c : : : : : " + c.getCriteriaObject());
			// System.out.println("groupOperation : : : : : " + groupOperation.getFields());
			// System.out.println("projectOperations : : : : : " +
			// projectOperations.getFields());
			// System.out.println("sort : : : : : " + sort(sortField));
			/* Query needs To be refactored */

			if (unwindOper != null) {
				operList.add(unwindOper);
			}
			operList.add(match(c));
			operList.add(projectOperations);
			operList.add(groupOperation);
			operList.add(sort(new Sort(Sort.Direction.ASC, "code")));
			operList.add(limit(defaultPageSize));
			agg = newAggregation(operList);
			agg = agg.withOptions(newAggregationOptions().cursor(cursor).build());
			// System.out.println("agg1 : : : : " + agg);
			queryMap1 = mongoTemplate.aggregate(agg, supportTableCollection, Map.class).getMappedResults();
			// headerList.clear();
		}
		// System.out.println("query1 : : : : " + query1);
		List<Map> resultMap = new LinkedList<>();
		for (Map m : queryMap1) {
			m.remove("SupportKey");
			LinkedHashMap m12 = new LinkedHashMap();
			for (int i = 0; i < supportTableConfigObjects.size(); i++) {
				referencePath = supportTableConfigObjects.get(i).getReferencePath();
				if (referencePath.equals("Key")) {
					m12.put(referencePath, m.get("Key"));
				} else if (m.get("Value") instanceof Map) {
					m11 = (LinkedHashMap) m.get("Value");
					val = "";

					referencePath = referencePath.replace("Value.", ""); // Have
																			// to be
																			// refactored
					o = m11.get(referencePath);
					if (!supportTableConfigObjects.get(i).getReferencePath().equals("Key")) // Have to be refactored
					{
						if (o instanceof List) {
							listObjects = (List) o;
							for (int i1 = 0; i1 < listObjects.size(); i1++) {
								if (i1 < listObjects.size() - 1) {
									val += (listObjects.get(i1).toString() + separatorSymbol);
								} else {
									val += (listObjects.get(i1).toString());
								}
							}
							o = val;
						}
						m12.put(referencePath, o);
					}
				} else {
					m12 = (LinkedHashMap) m;
				}

			}
                        if(m.get("_id")!= null)
                        {
                            m12.put("_id", m.get("_id").toString());
                        }
			resultMap.add(m12);
			// m.put("Value", m12);
		}

		/*
		 * if(secondaryCollnName != null&&secondaryFieldName != null) { for(Map m :
		 * queryMap1) { if(m.get(secondaryFieldName) != null)
		 * inObjects.add(m.get(secondaryFieldName)); }
		 * c.and(secondaryFieldName).in(inObjects); query2.addCriteria(c);
		 * System.out.println("query2 : : : : " + query2);
		 * query2.fields().include(secondaryFieldName); queryMap2 =
		 * mongoTemplate.find(query2, Map.class, secondaryCollnName); for(Map m1 :
		 * queryMap1) { for(Map m2 : queryMap2) { //obj =
		 * m2.get(m2.get(secondaryFieldName)); //
		 * System.out.println(m2.get(secondaryFieldName).getClass() +
		 * " : : : : : "+m1.get(primaryFieldName).getClass());
		 * if(m2.get(secondaryFieldName).equals(m1.get(primaryFieldName)) ) { for(int
		 * i=0;i<secondaryList.size();i++) { String s1 = secondaryList.get(i);
		 * if(m1.get(s1) != null) { val = m2.get(s1); m1.put(s1, m2.get(s1) +"; "+val);
		 * } else { val = m2.get(s1); if(m2.get(s1) != null) m1.put(s1, m2.get(s1));
		 * else m1.put(s1, ""); }
		 * 
		 * } } else { break;
		 * 
		 * } } m1.remove(secondaryFieldName); } }
		 * 
		 */

		returnMap.put("data", resultMap);
		returnMap.put("count", count1);
		returnMap.put("headerList", headerList);
		returnMap.put("recordCount", ((skip + limit) >= count1) ? count1 : (skip + limit));
		System.out.println(skip + " : : : : : " + limit);
		returnMap.put("prevRecordCount", ((count1) <= 0) ? 0 : (pageNo * count - count + 1));
		// returnMap.put("sortFieldList", canSortFields);
		// returnMap.put("fieldList", fieldList);
		return returnMap;

	}

@Override
	public Map<String, Object> editOneSupportTableRecord(SupportRequestEntity entity) {
		entity.setOperation("Edit");
		Map<String, Object> validatedData = validateSupportTableFields(entity);
		Date date = new Date();
		Criteria c = new Criteria();
		Query q = new Query();
		SupportBean bean = null;
		Object temp1 = null;
		Object temp2 = null;
		Boolean isDone = false;
		ArrayList tempList = null;
		validatedData.remove("NewObject");
		if (validatedData.get("errors") == null) {
			if (validatedData.get("Key") != null) {
				q = q.addCriteria(
						c.and("SupportKey").is(entity.getLookupName()).and("Key").is(validatedData.get("Key")));
				bean = jongo.getCollection(supportTableCollection).findOne(q.getQueryObject().toString())
						.as(SupportBean.class);
				// bean = mongoTemplate.findOne(q, SupportBean.class, supportTableCollection);
				if (bean != null) {
					bean.setKey(validatedData.get("Key"));
					Map tempData = (Map) validatedData.get("Value");
					for (Object property1 : tempData.keySet()) {
						for (Object property2 : bean.getValue().keySet()) {
							if (property2.toString().equals(property1.toString())) {
								temp1 = validatedData.get(property1);
								temp2 = validatedData.get(property2);
								if (temp1 != null && temp2 != null && temp1 instanceof List) {
									// Add temp2 into the list
									tempList = (ArrayList) temp1;
									if (tempList == null) {
										tempList = new ArrayList();
									}
									tempList.addAll((List) temp2);
									validatedData.put(property2.toString(), tempList);
								} else if (temp1 == null && temp2 != null && temp2 instanceof List) {
									// Add a new property
									tempList = (ArrayList) temp2;
									validatedData.put(property2.toString(), tempList);
								} else {
									// Don't do anything
								}
								break;

							}
						}
					}
					bean.setValue((Map) validatedData.get("Value"));
					bean.setModifiedBy(entity.getUserId());
					bean.setModifiedOn(date);
					bean.setSupportKey(entity.getLookupName());
					// These two lines to be refactored later
					bean.setIsActive(entity.getIsActive() == null ? true : entity.getIsActive());
					bean.setIsDeleted(entity.getIsDeleted() == null ? false : entity.getIsDeleted());
					jongo.getCollection(supportTableCollection).save(bean);
					// mongoTemplate.save(bean, this.supportTableCollection);
				} else
					return validatedData;
			} else {
				validatedData.put("SupportKey", entity.getLookupName());
				validatedData.put("createdOn", date);
				validatedData.put("modifiedOn", date);
				validatedData.put("createdBy", entity.getUserId());
				validatedData.put("modifiedBy", entity.getUserId());
                                validatedData.put("isActive", entity.getIsActive() == null ? true : entity.getIsActive());
				validatedData.put("isDeleted", entity.getIsDeleted() == null ? false : entity.getIsDeleted());
				mongoTemplate.save(validatedData, this.supportTableCollection);
			}
		}
		return validatedData;
	}

	@Override
	public Map<String, Object> findOneSupportTableRecord(Object key, String lookupName) {
		throw new UnsupportedOperationException("Is this API Required"); // To change body of generated methods, choose
																			// Tools | Templates.
	}

	@Value(value = "${SEQUENCE_COLLECTION}")
	private String sequenceCollection;

	@Override
	public Map<String, Object> addOneSupportTableRecord(SupportRequestEntity entity) {
		entity.setOperation("insert");
		Map<String, Object> validatedData = validateSupportTableFields(entity);
		Date date = new Date();
		Criteria c = new Criteria();
		Query q = new Query();
		validatedData.remove("NewObject");
		if (validatedData.get("errors") == null) {
			if (validatedData.get("Key") != null) {
				SupportBean bean = new SupportBean();
				bean.setKey(validatedData.get("Key"));
				bean.setValue((Map) validatedData.get("Value"));
                                bean.setModifiedBy(entity.getUserId());
                                bean.setCreatedBy(entity.getUserId());
                                bean.setModifiedOn(date);
                                bean.setCreatedOn(date);
                                bean.setIsActive((Boolean)validatedData.get("isActive"));
                                bean.setIsDeleted(Boolean.FALSE);
				/*
				 * bean.setCreatedBy(entity.getUserId());
				 * bean.setModifiedBy(entity.getUserId()); bean.setModifiedOn(date);
				 * bean.setCreatedOn(date);
				 */
				bean.setIsDeleted(Boolean.FALSE);
				bean.setSupportKey(entity.getLookupName());

				mongoTemplate.save(bean, this.supportTableCollection);
			} else {
				validatedData.put("SupportKey", entity.getLookupName());
				validatedData.put("createdOn", date);
				validatedData.put("modifiedOn", date);
				validatedData.put("createdBy", entity.getUserId());
				validatedData.put("modifiedBy", entity.getUserId());
                                validatedData.put("isActive", (Boolean)validatedData.get("isActive"));
                                validatedData.put("isDeleted", false);
				mongoTemplate.save(validatedData, this.supportTableCollection);
			}
		}
		Map m11 = (Map) validatedData.get("Value");
		if (m11 != null) {
			validatedData.remove("Value");
			validatedData.putAll(m11);
		}
		return validatedData;
	}

	public Map<String, Object> validateSupportTableFields(SupportRequestEntity entity) {
		String lookupName = entity.getLookupName();
		String operation = entity.getOperation();
		Date currentDate = new Date();
		Criteria c = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("isDisplay").is(true);
		Criteria c1 = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("forExcelDisplay").is(true);
		Criteria[] c2 = { c, c1 };
		Criteria c3 = new Criteria();
		List<Criteria> andcriterias = new LinkedList<>();
		List<Criteria> orcriterias = new LinkedList<>();
		c3.orOperator(c2);
		Query query = new Query(c3);
		List<SupportTableConfigMain> configList = entity.getConfigList();
		if (configList == null) {
			configList = mongoTemplate.find(query, SupportTableConfigMain.class, this.supportTableConfig);
		}
		query = new Query();
		c = new Criteria();
		c = c.and(supportKey).is(lookupName).and("isDeleted").is(false).and("isActive").is(true);
		String referencePath = null;
		Object key = null;
		String dataType = null;
		String keyDataType = null;
		String fieldName = null;
		Map<String, Object> dataMap = entity.getUpdateFields();
		Object tempData = null;
		Boolean isRequired = false;
		Map<String, Object> Value = new LinkedHashMap<>();
		Map<String, Object> resultMap = new LinkedHashMap<>();
		List<ValidateErrorBean> listErrors = new ArrayList<>();
		ValidateErrorBean errorBean = null;
		String regex = null;
		String tempReferencePath = null;
		LinkedList<AggregationOperation> operList = new LinkedList<>();
		UnwindOperation unwindOper = null;
		for (SupportTableConfigMain config : configList) {
			referencePath = config.getReferencePath();
			regex = config.getRegex();
			dataType = config.getDataType();
			fieldName = config.getFieldName();
			isRequired = config.getRequired();
			tempData = dataMap.get(fieldName);
			if (tempData == null) {
				tempData = dataMap.get(config.getDisplayName());// Bug fixed for excel upload - M6S
			}
			if (tempData == null) {
				tempData = dataMap.get(
                                (config.getReferencePath().split("\\.").length >1)?
                                        config.getReferencePath().split("\\.")[1]:
                                        config.getReferencePath()
                                );// Bug fixed for excel upload - will work for all support tables except one - Category
			}                        
			if (regex != null && regex.trim().length() > 0 && tempData != null) {
				// Apply regex
				boolean isRegexPassed = Pattern.matches(regex, tempData.toString());
				if (!isRegexPassed) {
					errorBean = new ValidateErrorBean(fieldName, "Invalid data ");
					listErrors.add(errorBean);
					resultMap.put("errors", listErrors);
				}
			}
			if (config.getIsKey() != null && config.getIsKey()) {
				keyDataType = config.getKeyFieldDataType();
				if (tempData == null && operation.equalsIgnoreCase("edit")) {
					errorBean = new ValidateErrorBean(fieldName, "Null or Empty Value in required field: " + tempData);
					listErrors.add(errorBean);
					continue;
				}
				if (keyDataType.equalsIgnoreCase("Integer") && dataType.equals("string")) {
					// Convert string to integer object in dataMap
					try {
						key = Integer.parseInt(tempData.toString());
					} catch (Exception e) {
						logger.info("Exception happened when converting string to integer " + e);
					}
				} else if (dataMap.get("Key") != null) {
					key = dataMap.get("Key");
				}

			}
			if (key != null) {
				resultMap.put("Key", key);
			}
			if (dataType.equalsIgnoreCase("array")) {
				unwindOper = unwind(config.getReferencePath(), true);
			}
			if (isRequired && (tempData == null || (dataType.equalsIgnoreCase("string") && tempData != null
					&& tempData.toString().trim().length() == 0))) {
				errorBean = new ValidateErrorBean(fieldName, "Null or Empty Value in required field: " + tempData);
				listErrors.add(errorBean);
				resultMap.put("errors", listErrors);

			}
			if (tempData != null) {
				if (referencePath.startsWith("Value.")) {
					tempReferencePath = referencePath;
					if (dataType.equalsIgnoreCase("array")) {
						tempReferencePath = referencePath.replace("Value.", "");
						ArrayList objectList = new ArrayList<>();
						objectList.add(tempData);
						Value.put(tempReferencePath, objectList);
					} else {
						tempReferencePath = referencePath.replace("Value.", "");
						Value.put(tempReferencePath, tempData);
					}
				} else {
					resultMap.put(tempReferencePath, tempData);
				}
			}
			if (config.getGroup() != null && config.getGroup()) {
				// c = c.and(config.getReferencePath()).is(tempData);
				andcriterias.add(Criteria.where(config.getReferencePath()).is(tempData));
			} else if (config.getGroup() != null && !config.getGroup()) {
				// c=c.orOperator(Criteria.where(config.getReferencePath()).is(tempData));
				orcriterias.add(Criteria.where(config.getReferencePath()).is(tempData));
			}

		} // end For
		if (andcriterias.size() > 0) {
			c = c.andOperator(andcriterias.toArray(new Criteria[andcriterias.size()]));
		}
		if (orcriterias.size() > 0) {
			c = c.orOperator(orcriterias.toArray(new Criteria[orcriterias.size()]));
		}
		if (c.getCriteriaObject().toMap().size() > 0) {
			// check for the uniqueness
			query.addCriteria(c);
			if (unwindOper != null) {
				operList.add(unwindOper);
			}
			operList.add(match(c));
			Aggregation agg = newAggregation(operList);
			logger.info("Duplicate check query : " + agg);
			List<Map> results = mongoTemplate.aggregate(agg, this.supportTableCollection, Map.class).getMappedResults();
			if (results.size() > 0) {
				errorBean = new ValidateErrorBean("Key", "Duplicate value exists");
				listErrors.add(errorBean);
			}

		}
		if (resultMap.get("Key") == null && resultMap.get("Value") != null && operation.equalsIgnoreCase("edit")) {
			// Raise an error
			errorBean = new ValidateErrorBean(fieldName, "Key does not exists");
			listErrors.add(errorBean);

		}
		if (listErrors.size() > 0) {
			resultMap.put("errors", listErrors);
			resultMap.putAll(dataMap);
		} else if (Value.size() > 0) {
			resultMap.put("Value", Value);
		}

		if (listErrors.size() == 0 && resultMap.get("Key") == null && !operation.equalsIgnoreCase("edit")) {
			// Get the key from sequence collection
			resultMap.put("Key", incrementSequenceNumber(entity));
			Value.put("createdOn", currentDate);
			Value.put("modifiedOn", currentDate);
			Value.put("createdBy", entity.getUserId());
			Value.put("modifiedBy", entity.getUserId());
			resultMap.put("isActive", dataMap.get("isActive") != null && dataMap.get("isActive").equals("Yes") ? true : false);
			resultMap.put("isDeleted", dataMap.get("isDeleted") != null ? dataMap.get("isDeleted") : false);
			resultMap.put("Value", Value);
		}
		if (listErrors.size() == 0 && dataMap.get("Key") != null && operation.equalsIgnoreCase("edit")) {
			// Get the key from sequence collection
			resultMap.put("Key", dataMap.get("Key"));
			Value.put("modifiedOn", currentDate);
			Value.put("modifiedBy", entity.getUserId());
			resultMap.put("isActive", dataMap.get("isActive") != null && dataMap.get("isActive").equals("Yes") ? true : false);
			resultMap.put("isDeleted", dataMap.get("isDeleted") != null ? dataMap.get("isDeleted") : false);
			resultMap.put("Value", Value);
		}

		return resultMap;
	}

	public Integer incrementSequenceNumber(SupportRequestEntity entity) {
		Query query = new Query();
		Criteria c = new Criteria();
		String lookupName = entity.getLookupName();
		c.and("_id").is(lookupName);
		query.addCriteria(c);
		// Map m = mongoTemplate.findOne(query, Map.class,sequenceCollection);
		Map m = jongo.getCollection(sequenceCollection).findOne(query.getQueryObject().toString()).as(Map.class);
		Integer sequence = (Integer) (m.get("seq"));
		sequence++;
		m.put("seq", sequence);
		mongoTemplate.save(m, sequenceCollection);
		return sequence;
	}

	@Override
	public Map<String, Object> getConfigurationForSupportTable(SupportRequestEntity entity) {

		List<Map> queryMap1 = null;
		HashMap configMap = new HashMap();
		String commaSeparatedValues = "";
		List temp = null;
		String supportQuery = "{\n" + "    aggregate : 'supportFieldConfig',\n" + "    pipeline : [\n"
				+ "    {$match:{$and : [{lookupDisplayName : {$exists:true}}"
				+ ",{isDeleted:false},{$or : [{forExcelDisplay : true}, \n" + "{ forExcelDisplay : {$exists : false}}]}"
				// + " ,{isDisplay:true}"
				+ "]}},\n" + "    \n"
				+ "    {$project: {isActive:'$isActive',lookupDisplayName : '$lookupDisplayName', lookupName : '$lookupName', \n"
				+ "        displayName : '$displayName',displayOrderNo : '$displayOrderNo',productHierarchyId:'$productHierarchyId'}},\n"
				+ "{$group : {_id : {lookupDisplayName : '$lookupDisplayName', lookupName : '$lookupName'\n"
				+ "    ,productHierarchyId:'$productHierarchyId',isActive:'$isActive'\n" + "    },  \n"
				+ "bisacTopic:{ $push: {  displayName : '$displayName',displayOrderNo : '$displayOrderNo'  \n"
				+ "  } }}},\n"
				+ "{$project : {isActive : '$_id.isActive',lookupDisplayName : '$_id.lookupDisplayName',lookupName : '$_id.lookupName', \n"
				+ "    productHierarchyId : '$_id.productHierarchyId',\n"
				+ "    displayArray : '$bisacTopic.displayName',\n" + "   _id : 0}} ,\n"
				+ "    {$sort:{'lookupDisplayName':1 }}\n" + "    \n" + "    ],\n" + "    cursor:{batchSize : 1000}\n"
				+ "}";
		queryMap1 = jongo.runCommand(supportQuery).throwOnError().field("cursor").as(Map.class);
		String query = "{find : 'mProductHierarchy', "
				+ "filter: {$and : [{isDeleted:false},{isActive : true}]}, projection:{_id : 1, productHierarchyName:1}}";
		// List<Map> productHierarchyMap =
		// jongo.runCommand(query).throwOnError().field("cursor").as(Map.class);
		List<ProductHierarchyEntity> productHierarchyEntities = this.getAllProductHierarchies();
		Object o = null;
		for (Map m : queryMap1) {
			commaSeparatedValues = "";
			for (Object key : m.keySet()) {
				if (key.equals("productHierarchyId")) {
					o = m.get(key);
					if (o != null) {
						for (ProductHierarchyEntity m1 : productHierarchyEntities) {
							if (m1.getId().equals(o.toString())) {
								m.put(key, m1.getProductHierarchyName());
								break;
							}
						}

					}
				}
				if (m.get(key) instanceof List) {
					temp = (List) m.get(key);
					if (temp != null) {
						Collections.sort(temp);
						for (int i = 0; i < temp.size(); i++) {
							if (i < temp.size() - 1) {
								commaSeparatedValues = commaSeparatedValues + temp.get(i).toString() + ",";
							} else {
								commaSeparatedValues = commaSeparatedValues + temp.get(i).toString();
							}
						}
					}
					m.put(key, commaSeparatedValues);
				}
				;
			}
		}
		configMap.put("SupportConfigurationList", queryMap1);
		return configMap;
	}
        Map<String,Object> fileMap = new HashMap<>();
        @Value(value = "${PATH_TO_APPROVEJS}")
        private String approveJsPath;

        public Map<String, Object> mergeStageRecords( String loggedUser, 
                 String stageId, String productHierarchyId, String metadataId,String parentID) {
 		URL url = null;
		HashMap status = new HashMap<>();
		HttpURLConnection connection = null;
		File path2JsFileTemp = null;
		File path2ScriptTemp = null;
		File path2CsvTemp = null;
		Long fileLastModifiedDate = (Long) fileMap.get("lastModified");
                String excelTemplate = (String)fileMap.get("scriptFile");
                String temp = (String)fileMap.get("mongoScriptFile");
                BufferedReader reader = null;
		try {
                   
                        if(fileLastModifiedDate == null){fileLastModifiedDate=0L;}
                        if((fileMap.size() == 0)||new File(approveJsPath).lastModified()> fileLastModifiedDate)
                        {
                         reader = new BufferedReader(new FileReader(new File(approveJsPath)));
			String s = "";
                        excelTemplate="";
			while ((s = reader.readLine()) != null) {
				excelTemplate += s;
				excelTemplate += "\n";
			} 
                        fileMap.put("scriptFile", excelTemplate);
                        reader.close();
                        reader = new BufferedReader(new FileReader(new File(mongoCommand)));
			temp=""; 
			while ((s = reader.readLine()) != null) {
				temp += s;
				temp += "\n";
			}
			reader.close();
                        fileMap.put("mongoScriptFile", temp);
                        fileMap.put("lastModified", new File(approveJsPath).lastModified());
                        }
			
			
			//Date t = new Date();
                        Calendar c = Calendar.getInstance();
                        
			String date = c.getTimeInMillis()+"";
			String excelQueryJs = outputDirJs + loggedUser + "_"  + date + ".js";
			// String csvPath = folderPath2Csv+reportName+"_"+date+".csv";
			String scriptPath = scriptPathTemp + loggedUser  + "_" + date + ".sh";
			String csvPath = folderPath2Csv + "_" + date + ".csv";

			excelTemplate = excelTemplate.replace("$metadataId", metadataId);
			// excelTemplate =
			// excelTemplate.replace("?permissionGroupId",viewSearchResult.getPermissionGroupId());
			excelTemplate = excelTemplate.replace("$loggedUser", loggedUser);
			excelTemplate = excelTemplate.replace("$productHierarchyId", productHierarchyId + "");
			excelTemplate = excelTemplate.replace("$stageId", stageId);
                        excelTemplate = excelTemplate.replace("$parentID", parentID);
			//excelTemplate = excelTemplate.replace("?lookupName", entity.getLookupName());

			// Check if the productHierarchyId exists...else pHId as ALL
			path2JsFileTemp = new File(excelQueryJs);
			FileOutputStream fos = new FileOutputStream(path2JsFileTemp, false);
			PrintWriter writer = new PrintWriter(fos);
			writer.write(excelTemplate);
			writer.close();

			temp = temp.replace("$1", excelQueryJs);
			temp = temp.replace("$2", csvPath);
			temp = temp.replace("$IPAddress", this.mongoDBServer);
			temp = temp.replace("$isTailableFlag", "false");
			temp = temp.replace("$CSVORXLS", "CSV");
			// File f = new File(scriptPath);
			path2ScriptTemp = new File(scriptPath);
			// System.out.println("Command : : : : : "+temp );
			// fos = new FileOutputStream(f,false);
			fos = new FileOutputStream(path2ScriptTemp, false);

			writer = new PrintWriter(fos);
			writer.println(temp);
			writer.close();

			try {
				// f = service.convert(f);
				path2ScriptTemp = this.convert(path2ScriptTemp);

				try {

					url = new URL(this.csvexportServiceURL);
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("POST");
					connection.setDoOutput(true);
					connection.addRequestProperty("Content-Type", "application/json");
					String requestStr = csvexportServiceAPI.replace("$1", path2ScriptTemp.getName()).replace("$2",
							this.shellPath);
					System.out.println(requestStr);
					OutputStream os = connection.getOutputStream();

					// System.out.println(apiUrl + " L: L L L " + isbnString);
					os.write(requestStr.getBytes());
					os.flush();
					os.close();

					InputStream in = connection.getInputStream();
					reader = new BufferedReader(new InputStreamReader(in));
					String a = "";
					temp = "";
					while ((a = reader.readLine()) != null) {
						temp += a;
						temp += "\n";
					}
				} catch (Exception e) {
					// e.printStackTrace();
					status.put("status", e.getMessage());
					throw new IllegalStateException(e);
				} finally {
					if (connection != null) {
						status.put("status", temp);

						connection.disconnect();
					}
				}

				
				// System.out.println("Renaming file from : " + path2CsvTemp.getAbsolutePath()
				// +" to original file : " + outputFile.getAbsolutePath());
				// boolean renameStatus = path2CsvTemp.renameTo(outputFile);
				// System.out.println("Whether Rename status successful" + renameStatus);

				status.put("docpath", path2CsvTemp);

			} catch (Exception e) {
				status.put("status", e.getMessage());

				throw new IllegalStateException(e);
			}
		} catch (Exception ex) {
			java.util.logging.Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		} finally {
			if (path2JsFileTemp != null) {
				//path2JsFileTemp.delete();
			}

			if (path2ScriptTemp != null) {
				path2ScriptTemp.delete();
			}
			if (path2CsvTemp != null) {
				 path2CsvTemp.delete();
			}
		}

		return status;
	}
	public Map<String, Object> export(SupportRequestEntity entity, String loggedUser, List<String> headerArr,
			int batchLimit, String query

	) {
		String excelTemplate = "";
		URL url = null;
		HashMap status = new HashMap<>();
		HttpURLConnection connection = null;
		File path2JsFileTemp = null;
		File path2ScriptTemp = null;
		File path2CsvTemp = null;
		String regex = "{ \"$date\" : ";
		String limitRegex = "{ \"$limit\" : ";
		String batchSizeRegex = "{ \"batchSize\" : ";
		query = query.replace(regex, "ISODate(").replace("Z\"}", "Z\")").replace("{ \"$skip\" : 0}",
				"{ \"$skip\" : skip}");
		StringBuilder builder = new StringBuilder(query);
		int i = builder.indexOf(limitRegex, 0) + limitRegex.length();
		// System.out.println(builder.replace(i, i+3, "100000"));
		builder = builder.replace(i, i + 3, this.batchLimit.toString());// To be replaced from manageConfig
																		// configuration
		i = builder.indexOf(batchSizeRegex, 0) + batchSizeRegex.length();
		builder = builder.replace(i, i + 3, this.batchLimit.toString());// To be replaced from manageConfig
																		// configuration
		query = builder.toString();
		System.out.println("With query : " + query);
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(path2Js)));
			String s = "";
			while ((s = reader.readLine()) != null) {
				excelTemplate += s;
				// excelTemplate += "\n";
			}
			reader.close();
			Date t = new Date();
			String date = new SimpleDateFormat("yyyyMMddhhmmss").format(t);
			String excelQueryJs = outputDirJs + loggedUser + "_" + entity.getLookupName() + "_" + date + ".js";
			String csvPath = folderPath2Csv + entity.getLookupName() + "_" + date + ".csv";
			// String csvPath = folderPath2Csv+reportName+"_"+date+".csv";
			String scriptPath = scriptPathTemp + loggedUser + "_" + entity.getLookupName() + "_" + date + ".sh";

			excelTemplate = excelTemplate.replace("?headerArr", new Gson().toJson(headerArr));
			// excelTemplate =
			// excelTemplate.replace("?permissionGroupId",viewSearchResult.getPermissionGroupId());
			excelTemplate = excelTemplate.replace("?query", query);
			excelTemplate = excelTemplate.replace("?batchLimit", batchLimit + "");
			excelTemplate = excelTemplate.replace("?CSVORXLS", entity.getExportFormat());
			excelTemplate = excelTemplate.replace("?lookupName", entity.getLookupName());

			// Check if the productHierarchyId exists...else pHId as ALL
			path2JsFileTemp = new File(excelQueryJs);
			FileOutputStream fos = new FileOutputStream(path2JsFileTemp, false);
			PrintWriter writer = new PrintWriter(fos);
			writer.write(excelTemplate);
			writer.close();
			reader = new BufferedReader(new FileReader(new File(mongoCommand)));
			String temp = "";
			while ((s = reader.readLine()) != null) {
				temp += s;
				temp += "\n";
			}
			reader.close();
			temp = temp.replace("$1", excelQueryJs);
			temp = temp.replace("$2", csvPath);
			temp = temp.replace("$IPAddress", this.mongoDBServer);
			temp = temp.replace("$isTailableFlag", "false");
			temp = temp.replace("$CSVORXLS", entity.getExportFormat());
			// File f = new File(scriptPath);
			path2ScriptTemp = new File(scriptPath);
			// System.out.println("Command : : : : : "+temp );
			// fos = new FileOutputStream(f,false);
			fos = new FileOutputStream(path2ScriptTemp, false);

			writer = new PrintWriter(fos);
			writer.println(temp);
			writer.close();

			try {
				// f = service.convert(f);
				path2ScriptTemp = this.convert(path2ScriptTemp);

				try {

					url = new URL(this.csvexportServiceURL);
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("POST");
					connection.setDoOutput(true);
					connection.addRequestProperty("Content-Type", "application/json");
					String requestStr = csvexportServiceAPI.replace("$1", path2ScriptTemp.getName()).replace("$2",
							this.shellPath);
					System.out.println(requestStr);
					OutputStream os = connection.getOutputStream();

					// System.out.println(apiUrl + " L: L L L " + isbnString);
					os.write(requestStr.getBytes());
					os.flush();
					os.close();

					InputStream in = connection.getInputStream();
					reader = new BufferedReader(new InputStreamReader(in));
					String a = "";
					temp = "";
					while ((a = reader.readLine()) != null) {
						temp += a;
						temp += "\n";
					}
				} catch (Exception e) {
					// e.printStackTrace();
					status.put("status", e.getMessage());
					throw new IllegalStateException(e);
				} finally {
					if (connection != null) {
						status.put("status", temp);

						connection.disconnect();
					}
				}

				if (entity.getExportFormat() != null && entity.getExportFormat().toLowerCase().endsWith("xlsx")) {

					csvPath = csvPath.replace(".csv", ".xlsx");
				} else {
					entity.setExportFormat("csv");
				}
				path2CsvTemp = new File(csvPath);
				// System.out.println("Renaming file from : " + path2CsvTemp.getAbsolutePath()
				// +" to original file : " + outputFile.getAbsolutePath());
				// boolean renameStatus = path2CsvTemp.renameTo(outputFile);
				// System.out.println("Whether Rename status successful" + renameStatus);

				status.put("docpath", path2CsvTemp);

			} catch (Exception e) {
				status.put("status", e.getMessage());

				throw new IllegalStateException(e);
			}
		} catch (Exception ex) {
			java.util.logging.Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		} finally {
			if (path2JsFileTemp != null) {
				path2JsFileTemp.delete();
			}

			if (path2ScriptTemp != null) {
				path2ScriptTemp.delete();
			}
			if (path2CsvTemp != null) {
				// path2CsvTemp.delete();
			}
		}

		return status;
	}

	public File convert(File epub) {
		String[] cmds = new String[3];
		try {
			cmds[0] = "chmod";
			cmds[1] = "777";
			cmds[2] = epub.getAbsolutePath();
			execProcess(cmds);
			int exitCode = 0;// execJar(exportjar,cmd[0]);// execProcess(cmd);
			// int exitCode = execProcess(cmd);;

			if (exitCode != 0 && exitCode != 1) { // ok or warning
				throw new IllegalStateException("CSV generation failed with error code " + exitCode);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalStateException("CSV generation failed: " + e.getMessage(), e);
		}

		String name = FilenameUtils.getBaseName(epub.getName());

		return epub;
	}

	private int execProcess(String[] cmd) throws IOException {
		final Process process = Runtime.getRuntime().exec(cmd);
		StreamConsumer stdout = new StreamConsumer(process.getInputStream());
		Thread inputReader = new Thread(stdout);
		inputReader.start();
		StreamConsumer stderr = new StreamConsumer(process.getErrorStream());
		Thread stdErrReader = new Thread(stderr);
		stdErrReader.start();
		int exitCode = -1;
		try {
			exitCode = process.waitFor();
			// List<String> csvLines = IOUtils.readLines(stdout.stream,"UTF-8");
			// System.out.println(csvLines.size());
		} catch (InterruptedException e) {
			process.destroy();
			inputReader.interrupt();
			stdErrReader.interrupt();
		} finally {
			try {
				inputReader.join();
			} catch (InterruptedException e) {
				// exit
			}
			try {
				stdErrReader.join();
			} catch (InterruptedException e) {
				// exit
			}
		}

		logger.debug("mongo stdout: " + stdout.getResult());
		String stderrResult = stderr.getResult();
		if (stderrResult.length() > 0) {
			logger.warn("mongo stderr: " + stderrResult);
		}

		return exitCode;
	}

	/*
	 * @Override public void setApplicationContext(ApplicationContext
	 * applicationContext) throws BeansException { // Resource resource =
	 * applicationContext.getResource(mongoCommand); File file = null; try { file =
	 * new PathResource(mongoCommand).getFile(); } catch (IOException ex) {
	 * java.util.logging.Logger.getLogger(this.getClass().getName()).log(Level.
	 * SEVERE, null, ex); }
	 * 
	 * if (!file.exists()) { throw new
	 * FatalBeanException("Required conversion utility not found: mongo"); } try {
	 * 
	 * String[] cmd = new String[] {"chmod", "a+x", this.path2kindlegen}; int
	 * exitCode = execProcess(cmd); if (exitCode != 0) { throw new
	 * FatalBeanException("Can not set executable bin on conversion utility " +
	 * this.path2kindlegen); } } catch ( Exception e) { throw new
	 * FatalBeanException("Required mongo utility not found: " + e.getMessage()); }
	 * }
	 * 
	 */

	@Override
	public Map<String, Object> addMultiSupportTableRecords(SupportRequestEntity entity) {
		List<Map> recordList = new LinkedList<>();
		List<Map<String, Object>> updateFields = entity.getMultiUpdateFields();
		// Criteria c = new Criteria();
		String lookupName = entity.getLookupName();
		Map<String, Object> resultMap = new HashMap<>();
		String operation = entity.getOperation();
		Criteria c = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("isDisplay").is(true);
		Criteria c1 = Criteria.where("lookupName").is(lookupName).and("isActive").is(true).and("isDeleted").is(false)
				.and("forExcelDisplay").is(true);
		Criteria[] c2 = { c, c1 };
		Criteria c3 = new Criteria();
		c3.orOperator(c2);
		Query query = new Query(c3);
		// Query query = new Query(c);
		List<SupportTableConfigMain> configList = null;
		configList = mongoTemplate.find(query, SupportTableConfigMain.class, this.supportTableConfig);
		if (updateFields != null)
			for (Map<String, Object> update : updateFields) {
				entity.setUpdateFields(update);
				entity.setConfigList(configList);
				recordList.add(addOneSupportTableRecord(entity));
			}
		int errorCounter = 0;
		int successCounter = 0;
		for (Map validatedData : recordList) {
			if (validatedData.get("errors") != null) {
				errorCounter++;
			} else {
				successCounter++;
			}
		}
		resultMap.put("successRecords", successCounter + " records inserted succcessfully");
		resultMap.put("failureRecords", errorCounter + " records were failures");
		resultMap.put("recordStatus", recordList);
		return resultMap;
	}

	@Override
	public String saveFile2S3(File file, String userId, String operationName, int expiryDays) {
		String returnurl = null;
		try {
			String objectKey = s3CsvRoot + "/" + file.getName();
			AwsAPIEntity apiEntity = this.getAwsCredentials();
			BasicAWSCredentials awsCreds = new BasicAWSCredentials(apiEntity.getAccessKey(), apiEntity.getSecretKey());
			AmazonS3 s3client = new AmazonS3Client(awsCreds);
			s3client.setRegion(Region.getRegion(Regions.fromName(apiEntity.getRegion())));
			s3client.setS3ClientOptions(S3ClientOptions.builder().setAccelerateModeEnabled(true).build());
			PutObjectRequest por = new PutObjectRequest(apiEntity.getBucketName(), objectKey, file);

			s3client.putObject(por);
			GeneratePresignedUrlRequest preUrl = new GeneratePresignedUrlRequest(apiEntity.getBucketName(), objectKey);
			preUrl.setMethod(HttpMethod.GET);

			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(cal.getTimeInMillis() + (expiryDays * 24 * 60 * 1000 * 60));

			preUrl.setExpiration(cal.getTime());
			URL url = s3client.generatePresignedUrl(preUrl);
			System.out.println(url.toString());
			returnurl = url.toString();
			DownloadEntity m = new DownloadEntity();
			m.setUserId(userId);
			m.setDownloadLink(url.toString());
			m.setCreatedBy(userId);
			m.setModifiedBy(userId);
			m.setCreatedDate(new Date());
			m.setModifiedDate(new Date());
			m.setDownloadType(operationName.replace("_", " ") + " Export");
			m.setUniqueId(file.getName());
			m.setExpiryDays(1);
			m.setVersionId(null);
			m.setDownloadStatus("15");

			m.setExpiryDate(cal.getTime());
			mongoTemplate.insert(m, tDownloadCollectionName);
		} catch (Exception e) {
			logger.info("error caused in saveFile2S3 : " + e);
			return null;
		}
		return returnurl;
	}

	private static class StreamConsumer implements Runnable {
		private final InputStream stream;
		private StringBuilder logMessage = new StringBuilder(4096);

		public StreamConsumer(InputStream stream) {
			this.stream = stream;
		}

		@Override
		public void run() {
			byte[] buffer = new byte[4096];
			int count;
			try {
				while ((count = stream.read(buffer)) > 0) {
					logMessage.append(new String(buffer, 0, count));
					if (Thread.interrupted()) {
						return;
					}
				}
			} catch (IOException e) {
				throw new RuntimeException("Unknown IO error");
			} finally {
				IOUtils.closeQuietly(stream);
			}

		}

		public String getResult() {
			return logMessage.toString();
		}
	}

	@Override
	public List<String> getSupportTablesList() {

		List<String> supportTables = null;

		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("isActive").is(true).andOperator(Criteria.where("isDeleted").is(false)));
			query.fields().include("lookupName");
			supportTables = mongoOperations.getCollection("supportFieldConfig").distinct("lookupName",
					query.getQueryObject());

		} catch (Exception e) {
			logger.error("Could not retrive support tables list", e);
		}
		return supportTables;
	}

	// Method to check all unique field validation. If any field is found to be
	// duplicated within siblings, will return false
	public boolean validateMandatoryUniqueFields(ProductMapEntity currentProduct) {
		List<String> uniqueValidators = null;
		boolean isUnique = true;
		try {
			// Get ParentId and ProductHierarchyId from Product
			String productHierarchyId = (String) currentProduct.getCustomFields().get("productHierarchyId");
			Query query = new Query();
			// Fetch validations field containing validationType : unique,
			// of current productHierarchy from mProductHierarchy Collection
			query.addCriteria(Criteria.where("_id").is(productHierarchyId));
			query.fields().elemMatch("validations", Criteria.where("validationType").is("all_unique"));
			query.fields().include("validations");
			Map<String, Object> mFields = mongoTemplate.findOne(query, Map.class, "mProductHierarchy");
			// From the result Map, get fields field as List
			try {
				uniqueValidators = (List<String>) (((List<Map>) mFields.get("validations")).stream()
						.filter(validator -> validator.get("validationType").equals("all_unique"))
						.map(field -> field.get("fields"))).collect(Collectors.toList()).get(0);
			} catch (Exception ee) {
				// If no validations of type unique found, then say no validators found
				if (ee instanceof NullPointerException)
					logger.info("No mandatory unique field validators found for ProductHierarchyId : "
							+ productHierarchyId);
			}
			int dupFieldCntr = 0, dupFldProdCntr = 0;
			// If uniqueValidators are found
			if (uniqueValidators != null && uniqueValidators.size() > 0) {
				Gson gson = new Gson();
				String parentId = (String) currentProduct.getCustomFields().get("parentID");
				query = new Query();
				// Get all siblings of current product
				query.addCriteria(
						Criteria.where("CustomFields.parentID").is(parentId).and("CustomFields.productHierarchyId")
								.is(currentProduct.getCustomFields().get("productHierarchyId"))
								.andOperator(Criteria.where("metadataId").ne(currentProduct.getMetadataId())));
				List<ProductMapEntity> products = mongoOperations.find(query, ProductMapEntity.class);
				for (String jsonPath : uniqueValidators) {
					dupFldProdCntr = 0;
					// For each uniqueValidator (jsonPath)
					// Read the currentProduct's value using JsonPath
					Object curVal = null;
					try {
						curVal = JsonPath.read(gson.toJson(currentProduct), jsonPath);
					} catch (Exception jsonPathException) {
						if (jsonPathException instanceof PathNotFoundException)
							logger.error("JsonPath " + jsonPath + " not found in MetadataId ::"
									+ currentProduct.getMetadataId());
						continue;
					}
					// Check if incoming Object from JsonPath is list
					// Since every field's value is atomic ( not an Object/Array ), if list just get
					// first element
					try {
						curVal = ((List) curVal).size() > 0 ? ((List) curVal).get(0) : "";
					} catch (Exception strExp) {
					}
					for (ProductMapEntity product : products) {
						// For each sibling
						// Read the currentSibling's value using jsonPath
						Object dbVal = null;
						try {
							dbVal = JsonPath.read(gson.toJson(product), jsonPath);
						} catch (Exception jsonPathException) {
							if (jsonPathException instanceof PathNotFoundException)
								logger.error("JsonPath " + jsonPath + " not found in MetadataId ::"
										+ product.getMetadataId());
							continue;
						}
						// Check if incoming Object from JsonPath is list
						// Since every field's value is atomic ( not an Object/Array ), if list just get
						// first element
						try {
							dbVal = ((List) curVal).size() > 0 ? ((List) dbVal).get(0) : "";
						} catch (Exception strExp) {
						}
						// if dbVal and curVal are same, then increase the duplicateFieldProductCounter
						// value by 1
						if (curVal.equals(dbVal))
							return false;
					}
				}
			}
		} catch (Exception e) {
			logger.error(
					"Error occured while validating product fields of MetadataId " + currentProduct.getMetadataId(), e);
			isUnique = false;
		}
		return isUnique;
	}

	// Validation method validateFieldsV2 goes through all sibling products and
	// finds out whether any duplicate sibling
	// is present for current product based on jsonPaths configured in
	// validationType : unique for that hierarchy
	// If present, returns false, else true
	public boolean validateFieldsV2(ProductMapEntity currentProduct) {
		System.out.println("$$ Inside validateFieldsV2 $$");
		List<String> uniqueValidators = null;
		boolean isUnique = true;
		try {
			// Get ParentId and ProductHierarchyId from Product
			String productHierarchyId = (String) currentProduct.getCustomFields().get("productHierarchyId");
			String currentProdJson = new Gson().toJson(currentProduct);
			Query query = new Query();
			// Fetch validations field containing validationType : unique,
			// of current productHierarchy from mProductHierarchy Collection
			query.addCriteria(Criteria.where("_id").is(productHierarchyId));
			query.fields().elemMatch("validations", Criteria.where("validationType").is("unique"));
			query.fields().include("validations");
			Map<String, Object> mFields = mongoTemplate.findOne(query, Map.class, "mProductHierarchy");
			// From the result Map, get fields field as List
			try {
				uniqueValidators = (List<String>) (((List<Map>) mFields.get("validations")).stream()
						.filter(validator -> validator.get("validationType").equals("unique"))
						.map(field -> field.get("fields"))).collect(Collectors.toList()).get(0);
			} catch (Exception ee) {
				// If no validations of type unique found, then say no validators found
				if (ee instanceof NullPointerException)
					logger.info("No validators found for ProductHierarchyId : " + productHierarchyId);
			}
			// If uniqueValidators are found
			if (uniqueValidators != null && uniqueValidators.size() > 0) {
				String parentId = (String) currentProduct.getCustomFields().get("parentID");
				query = new Query();
				// Get all siblings of current product
				query.addCriteria(
						Criteria.where("CustomFields.parentID").is(parentId).and("CustomFields.productHierarchyId")
								.is(currentProduct.getCustomFields().get("productHierarchyId"))
								.andOperator(Criteria.where("metadataId").ne(currentProduct.getMetadataId())));
				List<ProductMapEntity> products = mongoOperations.find(query, ProductMapEntity.class);
				for (ProductMapEntity product : products) {
					int dupFieldCounter = 0;
					// For each sibling
					// Read the currentSibling's value using jsonPath
					String siblingProdJson = new Gson().toJson(product);
					for (String jsonPath : uniqueValidators) {
						Object currentProdVal = "";
						Object siblingProdVal = "";
						try {
							currentProdVal = JsonPath.read(currentProdJson, jsonPath);
						} catch (Exception jsonPathException) {
							if (jsonPathException instanceof PathNotFoundException) {
								logger.error("JsonPath " + jsonPath + " for current prod not found in MetadataId ::"
										+ currentProduct.getMetadataId());
								currentProdVal = "";
							}
						}
						try {
							siblingProdVal = JsonPath.read(siblingProdJson, jsonPath);
						} catch (Exception jsonPathException) {
							if (jsonPathException instanceof PathNotFoundException) {
								logger.error("JsonPath " + jsonPath + " for sibling not found in MetadataId ::"
										+ product.getMetadataId());
								siblingProdVal = "";
							}
						}
						if (currentProdVal.toString().replace("[", "").replace("]", "").replace("\"", "").replace("\\", "").equals(
								siblingProdVal.toString().replace("[", "").replace("]", "").replace("\"", "").replace("\\", ""))) {
							// System.out.println("Current Prod : " + jsonPath + " : " +
							// currentProdVal.toString().replace("[", "").replace("]", "").replace("\"",
							// ""));
							// System.out.println("Sibling Prod : " + jsonPath + " : " +
							// siblingProdVal.toString().replace("[", "").replace("]", "").replace("\"",
							// ""));
							dupFieldCounter++;
						} else {
							System.out.println("Breaking loop on json path " + jsonPath);
							System.out.println(
									"Current Prod value : " + currentProduct.getMetadataId() + " : " + currentProdVal);
							System.out.println(
									"Sibling Prod value : " + product.getMetadataId() + " : " + siblingProdVal);
							break;
						}
					}
					if (dupFieldCounter == uniqueValidators.size()) {
						System.out.println("All json path value pairs found identical for current id : "
								+ currentProduct.getMetadataId() + " and sibling id : " + product.getMetadataId());
						System.out.println("$$ Exiting validateFieldsV2 with false $$");
						return false;
					}
				}
			}
		} catch (Exception e) {
			logger.error(
					"Error occured while validating product fields of MetadataId " + currentProduct.getMetadataId(), e);
			System.out.println("$$ Exiting validateFieldsV2 with " + isUnique + "$$");
			isUnique = false;
		}
		System.out.println("$$ Exiting validateFieldsV2 with " + isUnique + "$$");
		return isUnique;
	}

	/**
	 * Validates {@link ProductMapEntity}'s fields present in database based on
	 * ProductHierarchyId <br />
	 * <strong>Database Configuration Sample</strong> <code><br/>
	 * {<br />
	 * 	"productHierarchyId" : "PH000X", <br/>
	 *  "validations" : [ <br/>
	 *   &nbsp;{<br/>
	 *   &nbsp;&nbsp;   "validationType" : "unique",<br/>
	 *   &nbsp;&nbsp;   "fields" : [ "$.Product.ProductForm"]<br/>
	 *   &nbsp;}]<br/>
	 *  }<br/>
	 * </code>
	 * 
	 * @author Pavan Kumar Yekabote
	 * @return boolean
	 */
	public boolean validateFields(ProductMapEntity currentProduct) {

		List<String> uniqueValidators = null;
		boolean isUnique = true;
		try {
			// Get ParentId and ProductHierarchyId from Product
			String productHierarchyId = (String) currentProduct.getCustomFields().get("productHierarchyId");

			Query query = new Query();
			// Fetch validations field containing validationType : unique,
			// of current productHierarchy from mProductHierarchy Collection
			query.addCriteria(Criteria.where("_id").is(productHierarchyId));
			query.fields().elemMatch("validations", Criteria.where("validationType").is("unique"));
			query.fields().include("validations");
			Map<String, Object> mFields = mongoTemplate.findOne(query, Map.class, "mProductHierarchy");

			// From the result Map, get fields field as List
			try {
				uniqueValidators = (List<String>) (((List<Map>) mFields.get("validations")).stream()
						.filter(validator -> validator.get("validationType").equals("unique"))
						.map(field -> field.get("fields"))).collect(Collectors.toList()).get(0);
			} catch (Exception ee) {
				// If no validations of type unique found, then say no validators found
				if (ee instanceof NullPointerException)
					logger.info("No validators found for ProductHierarchyId : " + productHierarchyId);
			}

			int dupFieldCntr = 0, dupFldProdCntr = 0;
			// If uniqueValidators are found
			if (uniqueValidators != null && uniqueValidators.size() > 0) {
				Gson gson = new Gson();
				String parentId = (String) currentProduct.getCustomFields().get("parentID");
				query = new Query();
				// Get all siblings of current product
				query.addCriteria(
						Criteria.where("CustomFields.parentID").is(parentId).and("CustomFields.productHierarchyId")
								.is(currentProduct.getCustomFields().get("productHierarchyId"))
								.andOperator(Criteria.where("metadataId").ne(currentProduct.getMetadataId())));
				List<ProductMapEntity> products = mongoOperations.find(query, ProductMapEntity.class);

				for (String jsonPath : uniqueValidators) {
					dupFldProdCntr = 0;
					// For each uniqueValidator (jsonPath)
					// Read the currentProduct's value using JsonPath
					Object curVal = null;
					try {
						curVal = JsonPath.read(gson.toJson(currentProduct), jsonPath);
					} catch (Exception jsonPathException) {
						if (jsonPathException instanceof PathNotFoundException)
							logger.error("JsonPath " + jsonPath + " not found in MetadataId ::"
									+ currentProduct.getMetadataId());
						continue;
					}
					// Check if incoming Object from JsonPath is list
					// Since every field's value is atomic ( not an Object/Array ), if list just get
					// first element
					try {
						curVal = ((List) curVal).size() > 0 ? ((List) curVal).get(0) : "";
					} catch (Exception strExp) {
					}
					for (ProductMapEntity product : products) {
						// For each sibling
						// Read the currentSibling's value using jsonPath
						Object dbVal = null;
						try {
							dbVal = JsonPath.read(gson.toJson(product), jsonPath);
						} catch (Exception jsonPathException) {
							if (jsonPathException instanceof PathNotFoundException)
								logger.error("JsonPath " + jsonPath + " not found in MetadataId ::"
										+ product.getMetadataId());
							continue;
						}
						// Check if incoming Object from JsonPath is list
						// Since every field's value is atomic ( not an Object/Array ), if list just get
						// first element
						try {
							dbVal = ((List) curVal).size() > 0 ? ((List) dbVal).get(0) : "";
						} catch (Exception strExp) {
						}

						// if dbVal and curVal are same, then increase the duplicateFieldProductCounter
						// value by 1
						if (curVal.equals(dbVal))
							dupFldProdCntr++;
					}
					// If duplicateFieldProductCounter value > 0,
					// then it means, this field is duplicated
					// Therefore increase the duplicateFieldCounter value by 1
					if (dupFldProdCntr > 0)
						dupFieldCntr++;
				}
				// If dupFieldCntr value equals to uniqueFields,
				// it means that all fields are duplicated.
				// Therefore assign false to isUnique
				if (dupFieldCntr == uniqueValidators.size())
					isUnique = false;
			}
		} catch (Exception e) {
			logger.error(
					"Error occured while validating product fields of MetadataId " + currentProduct.getMetadataId(), e);
			isUnique = false;
		}

		return isUnique;
	}

	/**
	 * Lock a whole product, with all its hierarchal parents and children
	 * 
	 * @author Pavan Kumar Yekabote.
	 */
	@Override
	public boolean lockProduct(String productId, String pHId, String userId) {
		boolean isLocked = false;
		try {
			List<String> productIds = new ArrayList<>();

			Query query = new Query();
			query.addCriteria(
					Criteria.where("metadataId").is(productId).and("CustomFields.productHierarchyId").is(pHId));

			// Fetch all parent hierarchies -> parents are present in CustomFields.parents
			// field
			ProductMapEntity product = mongoOperations.findOne(query, ProductMapEntity.class);

			String referenceParentId = (String) product.getCustomFields().get("referenceParentID");

			if (referenceParentId == null)
				return false;

			// Add current product's referenceParentId to productsIds
			productIds.add(referenceParentId);

			// Get the user's details who is updating the current product
			query = new Query();
			query.addCriteria(Criteria.where("userId").is(userId).and("isActive").is(true).and("isDeleted").is(false));

			UserRightsEntity user = mongoOperations.findOne(query, UserRightsEntity.class);

			// Find all products which belong to the family of referenceParentId's product
			query = new Query();
			query.addCriteria(Criteria.where("CustomFields.referenceParentID").is(referenceParentId));

			Date lockedDate = new Date(System.currentTimeMillis());

			// Update following fields in all family members of referenceParentId's product
			Update update = new Update();
			update.set("CustomFields.isLocked", true);
			update.set("CustomFields.lockedByUsername", user.getFirstName());
			update.set("CustomFields.lockedByUserId", user.getUserId());
			update.set("CustomFields.lockedOn", lockedDate);

			mongoOperations.updateMulti(query, update, ProductMapEntity.class);

			if (productIds.size() > 0) {
				ProductLockEntity lock = new ProductLockEntity();
				lock.setUserId(userId);
				lock.setProductIds(productIds);
				lock.setLastLockedOn(lockedDate);
				mongoOperations.save(lock);
				isLocked = true;
			}
		} catch (Exception e) {
			logger.error("Error occured while applying lock for user :: " + userId + " metadataId :: " + productId
					+ " :: lockProduct() ", e);
			isLocked = false;
		}

		return isLocked;
	}

	@Override
	public boolean removeLockOnProductsIn(long hrsInMilliSeconds) {

		try {
			Date nHoursBackTime = new Date(System.currentTimeMillis() - hrsInMilliSeconds);

			Query query = new Query();
			query.addCriteria(Criteria.where("lastLockedOn").lt(nHoursBackTime).and("isActive").is(true));

			List<ProductLockEntity> locks = mongoTemplate.find(query, ProductLockEntity.class);

			Update update = new Update();
			update.set("isActive", false);
			mongoTemplate.updateMulti(query, update, ProductLockEntity.class);

			query = new Query();
			List<String> refIds = new ArrayList<>();
			locks.stream().map(e -> e.getProductIds()).forEach(refIds::addAll);

			if (refIds.size() > 0) {

				query.addCriteria(Criteria.where("CustomFields.referenceParentID").in(refIds));
				update = new Update();
				update.set("CustomFields.isLocked", false);
				update.set("CustomFields.lockedByUsername", null);
				update.set("CustomFields.lockedByUserId", null);
				update.set("CustomFields.lockedOn", null);
				// List<ProductMapEntity> pent = mongoTemplate.find(query,
				// ProductMapEntity.class);

				WriteResult res = mongoOperations.updateMulti(query, update, ProductMapEntity.class);
				logger.info("Unlocked Products: " + res.getN());
			}
			return true;
		} catch (Exception e) {
			logger.error("Error occured while updating old product locks", e);
		}
		return false;
	}

	/**
	 * 
	 */
	@Override
	public ProductLockEntity.LockType isProductLocked(String productId, String pHid, String userId) {

		ProductLockEntity.LockType lockType = ProductLockEntity.LockType.UNLOCKED;
		List<String> productIds = new ArrayList<>();

		// Check if any active lock exists for these productIds
		Query query = new Query();

		// Get current product's ancestor's productId -> that is referenceParentId;
		query.addCriteria(Criteria.where("metadataId").is(productId).and("CustomFields.productHierarchyId").is(pHid));
		ProductMapEntity product = mongoOperations.findOne(query, ProductMapEntity.class);
		String referenceParentID = (String) product.getCustomFields().get("referenceParentID");

		productIds.add(referenceParentID);

		// Find any ProductLock existing with current referenceParentID and userId;
		query = new Query();
		query.addCriteria(Criteria.where("productIds").in(productIds).and("isActive").is(true));

		ProductLockEntity lock = mongoOperations.findOne(query, ProductLockEntity.class);

		if (lock != null) {
			// Check the time difference between currentTime and lockedTime
			Date currentDate = new Date(System.currentTimeMillis());
			Date lockedDate = lock.getLastLockedOn();
			long diffTime = currentDate.getTime() - lockedDate.getTime();

			// Convert the difference from Millseconds to Hours time
			long hrs = TimeUnit.HOURS.convert(diffTime, TimeUnit.MILLISECONDS);

			// If time diff is greater than specified time period
			// Deactivate the current lock
			if (hrs >= PRODUCT_LOCK_INTERVAL) {
				// Remove lock on the productids/metadataIds associated with userId
				boolean isLockRemoved = this.removeLockOnProduct(userId, referenceParentID);
				lockType = isLockRemoved ? ProductLockEntity.LockType.UNLOCKED : ProductLockEntity.LockType.LOCKED;
			} else if (userId.equals(lock.getUserId()))
				lockType = ProductLockEntity.LockType.SELF_LOCK;
			else
				lockType = ProductLockEntity.LockType.LOCKED;
		}
		return lockType;

	}

	public boolean removeLockOnProduct(String userId) {
		boolean isLockRemoved = false;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("userId").is(userId).and("isActive").is(true));
			// query.with(new Sort(Sort.Direction.DESC, "lastLockedOn"));
			Update update = new Update();
			update.set("isActive", false);
			mongoOperations.updateMulti(query, update, ProductLockEntity.class);

			query = new Query();

			query.addCriteria(Criteria.where("CustomFields.lockedByUserId").is(userId));

			update = new Update();
			update.set("CustomFields.isLocked", false);
			update.set("CustomFields.lockedByUsername", null);
			update.set("CustomFields.lockedByUserId", null);
			update.set("CustomFields.lockedOn", null);
			mongoOperations.updateMulti(query, update, ProductMapEntity.class);
			isLockRemoved = true;
		} catch (Exception e) {
			logger.error("Error occured while removing lock on product : " + userId, e);
		}
		return isLockRemoved;
	}

	public boolean removeLockOnProduct(String userId, String referenceParentId) {
		boolean isLockRemoved = false;

		try {

			ArrayList<String> productIds = new ArrayList<>();
			Query query = new Query();

			if (referenceParentId == null)
				throw new NullPointerException("Error.. Could not find referenceParentID ");

			productIds.add(referenceParentId);

			// Make the lock inActive
			query = new Query();
			query.addCriteria(
					Criteria.where("productIds").in(productIds).and("userId").is(userId).and("isActive").is(true));
			Update update = new Update();
			update.set("isActive", false);
			mongoOperations.updateFirst(query, update, ProductLockEntity.class);

			// Update lock in products collection
			query = new Query();

			query.addCriteria(Criteria.where("CustomFields.referenceParentID").is(referenceParentId));

			update = new Update();
			update.set("CustomFields.isLocked", false);
			update.set("CustomFields.lockedByUsername", null);
			update.set("CustomFields.lockedByUserId", null);
			update.set("CustomFields.lockedOn", null);
			mongoOperations.updateMulti(query, update, ProductMapEntity.class);
			isLockRemoved = true;

		} catch (Exception e) {
			logger.error("Error occured while removing lock on product : " + referenceParentId, e);
		}

		return isLockRemoved;
	}

	@Override
	public ProductMapEntity getProduct(Object metadataId, Object pHid) {
		ProductMapEntity pMap = null;
		try {

			Query query = new Query();
			query.addCriteria(Criteria.where("metadataId").is(metadataId));
			query.addCriteria(Criteria.where("CustomFields.productHierarchyId").is(pHid));

			pMap = mongoOperations.findOne(query, ProductMapEntity.class);

		} catch (Exception e) {
			logger.error("Error occured while retriving product based on metadataId & pHid : ", e);

		}
		return pMap;
	}

	@Override
	public List<ProductMapEntity> getProducts(Object metadataId) {
		List<ProductMapEntity> products = null;
		try {
			Query query = new Query();
			List<Criteria> criterias = new ArrayList<>();
			criterias.add(Criteria.where("metadataId").is(metadataId));
			criterias.add(Criteria.where("CustomFields.parentID").is(metadataId));
			Criteria c = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			query.addCriteria(c);
			products = mongoTemplate.find(query, ProductMapEntity.class);
		} catch (Exception e) {
			logger.error("Error occured in getProducts : ", e);
		}
		return products;
	}

	@Override
	public boolean saveCdcMetadata(CDCMetaData cdcMetaData) {
		boolean result = false;
		try {
			mongoTemplate.insert(cdcMetaData, "tCDCMetaData");
			result = true;
		} catch (Exception e) {
			logger.error("Error occured while saving cdc data : ", e);
		}
		return result;
	}

	public Boolean isFilterFirst(SupportRequestEntity entity) {
		Boolean isColumnFilter = this.getColumnFilterSuggestionsStatus(entity);
		if (isColumnFilter) {
			if (entity.getFilterByFields() != null && entity.getFilterByFields().size() > 0) {
				entity.getFilterByFields().remove(0);// For testing purposes
				return true;
			}
		}
		return false;
	}

	@Override
	public Map<String, Object> getAvailabilityCodeConfig() {
		Query q = new Query();
		q.addCriteria(Criteria.where("_id").is("AvailabilityCode"));
		Map<String, Object> acConfig = mongoTemplate.findOne(q, Map.class, "manageConfig");
		return acConfig;
	}

	@Override
	public Boolean actiDeactivateSupportConfig(SupportRequestEntity entity) {
		Boolean isActive = entity.getIsActive();
		String lookupName = entity.getLookupName();
		Criteria c = new Criteria();
		c.and("lookupName").is(lookupName);
		Query query = new Query(c);
		Update update = new Update();
		Integer noOfRecords = null;
		if (isActive != null) {
			update.set("isActive", isActive);
			update.set("modifiedBy", entity.getUserId());
			update.set("modifiedOn", new Date());
			try {
				noOfRecords = mongoTemplate.updateMulti(query, update, this.supportTableConfig).getN();
			} catch (Exception e) {
				noOfRecords = -1;
				logger.info("Some error happenned when update : " + e.getMessage());
			}
		}
		return (noOfRecords < 0) ? false : true;
	}

	@Override
	public List<ProductMapEntity> getSiblingsData(Object referenceParentID, Object phId) {
		List<ProductMapEntity> product = null;

		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("CustomFields.referenceParentID").is(referenceParentID)
					.and("CustomFields.productHierarchyId").is(phId));
			query.fields().exclude("asset");

			product = mongoTemplate.find(query, ProductMapEntity.class);

		} catch (Exception e) {
			logger.error("error in getSiblingsData " + e.getMessage());
		}
		return product;
	}

	@Override
	public ProductMapEntity getMasterSiblingData(Object referenceParentID, Object phId, String syncField) {
		ProductMapEntity product = null;

		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("CustomFields.referenceParentID").is(referenceParentID)
					.and("CustomFields.productHierarchyId").is(phId).and("CustomFields." + syncField).is("Yes"));
			query.fields().exclude("asset");

			product = mongoTemplate.findOne(query, ProductMapEntity.class);

		} catch (Exception e) {
			logger.error("error in getSiblingsData " + e.getMessage());
		}
		return product;
	}

	@Override
	public boolean checkSFormatsExistForLanguage(Object metaDataID, Object pHid) {
		boolean result = false;
		try {
			Query query = new Query();
			/*
			 * if(isFormatLevel)
			 * query.addCriteria(Criteria.where("metadataId").is(metaDataID).
			 * and("CustomFields.productHierarchyId").is(pHid)); else
			 */
			query.addCriteria(Criteria.where("CustomFields.parents.metadataId").is(metaDataID));
			// and("CustomFields.parents.CustomFields.productHierarchyId").is(pHid));
			result = mongoTemplate.count(query, ProductMapEntity.class) > 0;
		} catch (Exception e) {
			logger.error("error in checkSupplementExist " + e.getMessage());
		}
		return result;
	}
         @Override
    public List<Map<String,Object>> createSupportTable(SupportRequestEntity entity) {
        String lookupName = entity.getLookupName();
        String lookupDisplayName = entity.getLookupDisplayName();
        List<Map<String,Object>> multiUpdateFields = entity.getMultiUpdateFields();
        List<Map<String,Object>> customUpdateFields = entity.getCustomUpdateFields();
        String productHierarchyId = entity.getpHid();
        //List<String> objectIds = new ArrayList<>();
        Object metadataFieldId = null;
        Map<String,Map> objectMap = new HashMap<>();
        Map<String,Object> typeMap = null;
        List<MetaDataProductHierarchyFieldsEntity> metadataList = null;
        List<SupportTableConfigMain> supportConfigs = new ArrayList<>();
        Query query = new Query();
        Criteria c = new Criteria();
        c = c.and("isActive").is(true).and("isDeleted").is(false);
        SupportTableConfigMain config = null;
        Date date = new Date();
        int i = 0;
        /**
         * Note : the cMetadataFields should be assigned supportFieldName which is not documented now
         * will do some time later
         */
        if(multiUpdateFields != null)
        {
            for(Map<String,Object> m : multiUpdateFields)
            {

                metadataFieldId = m.get("metaDataFieldName");
                if(metadataFieldId != null)
                {
                     //objectIds.add(metadataFieldId.toString());
                    
                    objectMap.put(metadataFieldId.toString(), m);
                }
            }
            c=c.and("metaDataFieldName").in(objectMap.keySet());
            query.addCriteria(c);
            String fieldName = null;
//            System.out.println("my query : : : : " + query);
            metadataList = mongoTemplate.find(query, MetaDataProductHierarchyFieldsEntity.class,"cMetadataFields");
            for(MetaDataProductHierarchyFieldsEntity e : metadataList)
            {
                config = new SupportTableConfigMain();
                config.setCreatedBy(entity.getUserId());
                config.setModifiedBy(entity.getUserId());
                config.setCreatedOn(date);
                config.setModifiedOn(date);
                config.setIsActive(Boolean.TRUE);
                config.setIsDeleted(Boolean.FALSE);
                config.setIsDisplay(Boolean.TRUE);
                config.setIsEditable(Boolean.TRUE);
                config.setDefaultSort(Boolean.TRUE);
                config.setDisplayOrderNo(++i);
                config.setExcelDisplayOrder(i);
                config.setBackgroundcolor(10);
                config.setFontcolor(8);
                config.setRequired(Boolean.FALSE);
                config.setCanSort(Boolean.FALSE);
                config.setFilter(Boolean.TRUE);
                config.setReferencePath("Value.varName"+i);
                config.setFieldName(e.getMetaDataFieldName());
                config.setDisplayName(e.getFieldDisplayName());
                config.setProductHierarchyId(productHierarchyId);
                try{
                    if(e.getMaxLength() != null)
                config.setFieldLength(Integer.parseInt(e.getMaxLength()));
                    else
                    {
                    config.setFieldLength(null);    
                    }
                }
                catch(Exception e1){
                    config.setFieldLength(null);
                }
                if(lookupDisplayName != null)
                {
                    config.setLookupDisplayName(lookupDisplayName);
                }
                else
                {
                    config.setLookupDisplayName(lookupName);
                }
                config.setLookupName(lookupName);
                typeMap = (Map)e.getFieldDataType();
                String dataType = null;
                if(typeMap != null)
                {
                    Map validationMap = (Map)typeMap.get("validation");
                    dataType = (String)typeMap.get("dataType");
                    if(dataType != null)
                    {
                        if(dataType.equalsIgnoreCase("text"))
                        {
                            dataType = "string";
                        }
                        else if(dataType.equalsIgnoreCase("numeric"))
                        {
                            dataType = "integer";
                        }
                        else if (dataType.contains("date"))
                        {
                            dataType = "date";
                        }
                        config.setDataType(dataType);
                    }
                    if(validationMap != null)
                    {
                        String regex = (String)validationMap.get("regex");
                        if(regex != null && regex.trim().length() > 0)
                        {
                            config.setRegex(regex);
                        }
                    }
                }
                config.setIsCascade(Boolean.TRUE);
                if(objectMap.get(e.getMetaDataFieldName()) != null)
                {
                   Map temp = objectMap.get(e.getMetaDataFieldName()); 
                   Boolean isKey = (Boolean) temp.get("isKey");
                   if(isKey != null && isKey)
                   {
                       config.setIsKey(Boolean.TRUE);
                       config.setKeyFieldDataType(dataType);
                   }
                }
                

                //e.setSupportLookupName(lookupName);
                e.supportFieldName="varName"+i;//the prototype is as shown 
                e.supportUIFieldName="varName"+i;//the prototype is as shown 
                mongoTemplate.save(e,"cMetadataFields");
                supportConfigs.add(config);
            }
            if(customUpdateFields != null)
            {
            for(Map m : customUpdateFields)
            {
                
                config = new SupportTableConfigMain();
                config.setCreatedBy(entity.getUserId());
                config.setModifiedBy(entity.getUserId());
                config.setCreatedOn(date);
                config.setModifiedOn(date);
                config.setIsActive(Boolean.TRUE);
                config.setIsDeleted(Boolean.FALSE);
                config.setIsDisplay(Boolean.TRUE);
                config.setIsEditable(Boolean.TRUE);
                config.setDefaultSort(Boolean.TRUE);
                config.setDisplayOrderNo(++i);
                config.setExcelDisplayOrder(i);
                config.setBackgroundcolor(10);
                config.setFontcolor(8);
                config.setRequired(Boolean.FALSE);
                config.setCanSort(Boolean.FALSE);
                config.setFilter(Boolean.TRUE);                
                config.setReferencePath("Value.varName"+i);
                config.setProductHierarchyId(productHierarchyId);
                config.setIsCascade(Boolean.FALSE);                
                config.setLookupName(lookupName);
                 try{
                    if(m.get("maxLength") != null) 
                    {
                        config.setFieldLength((Integer)m.get("maxLength"));
                    }
                    else
                    {
                    config.setFieldLength(null);    
                    }
                }
                catch(Exception e1){
                    config.setFieldLength(null);
                }
                if(lookupDisplayName != null)
                {
                    config.setLookupDisplayName(lookupDisplayName);
                }
                else
                {
                    config.setLookupDisplayName(lookupName);
                }
                String dataType = (String) m.get("dataType"); 
                if(dataType != null)
                {
                        if(dataType.equalsIgnoreCase("text"))
                        {
                            dataType = "string";
                        }
                        else if(dataType.equalsIgnoreCase("numeric"))
                        {
                            dataType = "integer";
                        }
                        else if (dataType.contains("date"))
                        {
                            dataType = "date";
                        }
                        config.setDataType(dataType);
                }
                metadataFieldId = m.get("metaDataFieldName");
                if(metadataFieldId != null)
                {
                    config.setFieldName(metadataFieldId.toString());
                    config.setDisplayName(metadataFieldId.toString());
                }
                if(objectMap.get(m.get("metaDataFieldName")) != null)
                {
                   Map temp = objectMap.get(m.get("metaDataFieldName")); 
                   Boolean isKey = (Boolean) temp.get("isKey");
                   if(isKey != null && isKey)
                   {
                       config.setIsKey(Boolean.TRUE);
                       config.setKeyFieldDataType(dataType);                       
                   }
                }
                
                //e.setSupportLookupName(lookupName);
                supportConfigs.add(config);
             
            }
            }
            Update update = new Update();
            update.set("supportLookupName", lookupName);
            //mongoTemplate.updateMulti(query, update, "cMetadataFields");
            mongoTemplate.insert(supportConfigs, this.supportTableConfig);
        }
        
        return multiUpdateFields;
    }

    @Override
    public Map<String, Object> getSupportTableInfo(SupportRequestEntity entity) {
        Map<String,Object> supportTableObjs = new HashMap<>();
        List<CodeDescription> codeDescriptionList = new LinkedList<>();
        CodeDescription codeDesc = null;
        Criteria c = new Criteria();
        c.and("isActive").is(true).and("isDeleted").is(false)
                .and("supportLookupName").exists(false);
        Query query = new Query();
        query.addCriteria(c);
        query.fields().exclude("_id").include("metaDataFieldName")
                .include("fieldDisplayName").include("fieldDataType")
        .include("productHierarchyId");
        query.with(new Sort("fieldDisplayName"));
        List<MetaDataProductHierarchyFieldsEntity> list = mongoTemplate.find(query,MetaDataProductHierarchyFieldsEntity.class,"cMetadataFields" );
        List<ProductHierarchyEntity> phEntity =  null;
        HashSet<String> hierarchySet = new HashSet<String>();
        HashSet<String> dataTypes = new HashSet<String>();
        Map m = null;
        String s = null;
        for(MetaDataProductHierarchyFieldsEntity e : list)
        {
            hierarchySet.add(e.getProductHierarchyId());
            m = (Map)e.getFieldDataType();
            if(m != null)
            {
                s = (String)m.get("dataType");
                dataTypes.add(s);
            }
            
        }
        query = new Query();
        c = new Criteria();
        c.and("isActive").is(true).and("isDeleted").is(false).and("_id").in(hierarchySet);
        query.addCriteria(c);
        query.fields().include("_id").include("productHierarchyName")//.exclude("_id")
                ;
        //System.out.println("query : " + query);
        phEntity = mongoTemplate.find(query, ProductHierarchyEntity.class,"mProductHierarchy");
        supportTableObjs.put("metadataInfo", list);
        supportTableObjs.put("dataTypeInfo", dataTypes);
        supportTableObjs.put("productHierarchyInfo", phEntity);
        return supportTableObjs;
    }
    
    @Override
	public Map<String, Boolean> doesValueExist(String fieldName, Object fieldValue) {
		Map<String, Boolean> result = new HashMap<>();
		Query q = new Query();
		q.addCriteria(Criteria.where("metaDataFieldName").is(fieldName));
		MetaDataFieldsEntity metaField = mongoTemplate.findOne(q, MetaDataFieldsEntity.class, "metaDataFieldConfig");
		if (metaField == null) {
			return null;
		}
		q = new Query();
		if(metaField.getJsonDbPathCondition() == null) {
			q.addCriteria(Criteria.where(metaField.getReferencePath()).is(fieldValue));
		}else {
			List<Criteria> cs = new ArrayList<>();
			for(JsonDbPathConditionEntity condition: metaField.getJsonDbPathCondition()) {				
				cs.add(Criteria.where(condition.getField()).is(condition.getValue()));
			}
			cs.add(Criteria.where(metaField.getJsonDbPathElement()).is(fieldValue));
			Criteria criteria = new Criteria().andOperator(cs.toArray(new Criteria[cs.size()]));
			q.addCriteria(Criteria.where(metaField.getJsonDbPathList()).elemMatch(criteria));
		}
		Object prod = mongoTemplate.findOne(q, Object.class, "products");
		if(prod == null) {
			result.put("isUnique", true);
		}else {
			result.put("isUnique", false);
		}
		return result;
	}
}