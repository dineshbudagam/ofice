/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

public class AccountAccessEntity{
	
	private String accountCode;
	private String accountName;
	
	private List<ImprintAccess> imprints;
	private List<MetaDataTypeAccessEntity> metadataType;
	private List<MetaDataAccessEntity> metadata;	
	
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public List<ImprintAccess> getImprints() {
		return imprints;
	}
	public void setImprints(List<ImprintAccess> imprints) {
		this.imprints = imprints;
	}
	public List<MetaDataTypeAccessEntity> getMetadataType() {
		return metadataType;
	}
	public void setMetadataType(List<MetaDataTypeAccessEntity> metadataType) {
		this.metadataType = metadataType;
	}
	public List<MetaDataAccessEntity> getMetadata() {
		return metadata;
	}
	public void setMetadata(List<MetaDataAccessEntity> metadata) {
		this.metadata = metadata;
	}

}
