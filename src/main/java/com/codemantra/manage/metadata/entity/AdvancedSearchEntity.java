/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class AdvancedSearchEntity {
	
	@Field("isbns")
	private String[] isbns;
	
	@Field("titles")
	private String[] titles;
	
	@Field("authors")
	private String[] authors;

	@Field("accounts")
	private String[] accounts;

	@Field("formats")
	private String[] formats;

	@Field("status")
	private String[] status;

	@Field("imprints")
	private String[] imprints;

	@Field("pCategories")
	private String[] pCategories;

	@Field("others")
	private String[] others;
	
	public String[] getIsbns() {
		return isbns;
	}

	public void setIsbns(String[] isbns) {
		this.isbns = isbns;
	}

	public String[] getAccounts() {
		return accounts;
	}

	public void setAccounts(String[] accounts) {
		this.accounts = accounts;
	}

	public String[] getFormats() {
		return formats;
	}

	public void setFormats(String[] formats) {
		this.formats = formats;
	}

	public String[] getStatus() {
		return status;
	}

	public void setStatus(String[] status) {
		this.status = status;
	}

	public String[] getImprints() {
		return imprints;
	}

	public void setImprints(String[] imprints) {
		this.imprints = imprints;
	}

	public String[] getProductCategories() {
		return pCategories;
	}

	public void setProductCategories(String[] productCategories) {
		this.pCategories = productCategories;
	}

	public String[] getOthers() {
		return others;
	}

	public void setOthers(String[] others) {
		this.others = others;
	}

	public String[] getTitles() {
		return titles;
	}

	public void setTitles(String[] titles) {
		this.titles = titles;
	}

	public String[] getAuthors() {
		return authors;
	}

	public void setAuthors(String[] authors) {
		this.authors = authors;
	}

	public String[] getpCategories() {
		return pCategories;
	}

	public void setpCategories(String[] pCategories) {
		this.pCategories = pCategories;
	}
}
