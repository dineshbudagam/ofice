/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;


public class AssetEntity {
	
	@Field("asset")
	private String productIDValue;
	@Field("assetId")
	private String assetId;
	@Field("formatId")
	private String formatId;
	@Field("bucket")
	private String bucket;
	@Field("folder")
	private String folder;
	@Field("size")
	private long size;
	@Field("filedetails")
	private List<FileDetailEntity> filedetails;
	@Field("uploadedOn")
	private Date uploadedOn;
	@Field("uploadedBy")
	private String uploadedBy;
	@Field("isDeleted")
	private boolean isDeleted;
	@Field("modifiedOn")
    private Date modifiedOn;
	
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	@Field("formatStatus")
	private String formatStatus;

	
	public String getProductIDValue() {
		return productIDValue;
	}
	public void setProductIDValue(String productIDValue) {
		this.productIDValue = productIDValue;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public List<FileDetailEntity> getFiledetails() {
		return filedetails;
	}
	public void setFiledetails(List<FileDetailEntity> filedetails) {
		this.filedetails = filedetails;
	}
	public Date getUploadedOn() {
		return uploadedOn;
	}
	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}
	public String getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}
	
	public String getFormatStatus() {
		return formatStatus;
	}
	public void setFormatStatus(String formatStatus) {
		this.formatStatus = formatStatus;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	

	
}
