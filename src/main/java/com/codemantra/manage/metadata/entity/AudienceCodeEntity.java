package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class AudienceCodeEntity {
	
	@Field("AudienceCode")
	public String AudienceCode;

	public String getAudienceCode() {
		return AudienceCode;
	}

	public void setAudienceCode(String audienceCode) {
		AudienceCode = audienceCode;
	}
	

}
