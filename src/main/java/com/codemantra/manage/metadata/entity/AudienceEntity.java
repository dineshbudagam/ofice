/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class AudienceEntity
{
	@Field("AudienceCodeType")
	public String audienceCodeType;

	@Field("AudienceCodeTypeName")
	public String audienceCodeTypeName;

	@Field("AudienceCodeValue")
	public String audienceCodeValue;

	public String getAudienceCodeType() {
		return audienceCodeType;
	}

	public void setAudienceCodeType(String audienceCodeType) {
		this.audienceCodeType = audienceCodeType;
	}

	public String getAudienceCodeTypeName() {
		return audienceCodeTypeName;
	}

	public void setAudienceCodeTypeName(String audienceCodeTypeName) {
		this.audienceCodeTypeName = audienceCodeTypeName;
	}

	public String getAudienceCodeValue() {
		return audienceCodeValue;
	}

	public void setAudienceCodeValue(String audienceCodeValue) {
		this.audienceCodeValue = audienceCodeValue;
	}
}
