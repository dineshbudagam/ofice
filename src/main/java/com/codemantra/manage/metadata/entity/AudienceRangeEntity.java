/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class AudienceRangeEntity
{
	@Field("AudienceRangePrecision")
	public String audienceRangePrecision;

	@Field("AudienceRangeQualifier")
	public String audienceRangeQualifier;

	@Field("AudienceRangeValue")
	public String audienceRangeValue;

	public String getAudienceRangePrecision() {
		return audienceRangePrecision;
	}

	public void setAudienceRangePrecision(String audienceRangePrecision) {
		this.audienceRangePrecision = audienceRangePrecision;
	}

	public String getAudienceRangeQualifier() {
		return audienceRangeQualifier;
	}

	public void setAudienceRangeQualifier(String audienceRangeQualifier) {
		this.audienceRangeQualifier = audienceRangeQualifier;
	}

	public String getAudienceRangeValue() {
		return audienceRangeValue;
	}

	public void setAudienceRangeValue(String audienceRangeValue) {
		this.audienceRangeValue = audienceRangeValue;
	}
	
}
