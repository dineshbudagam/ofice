/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

import com.codemantra.manage.metadata.model.CDCMetaDataDetails;

public class CDCMetaDataEntity {
	
	@Field("referenceId")
	public String referenceId;
	
	@Field("cdcMetaDataDetails")
	public List<CDCMetaDataDetails> cdcMetaDataDetails;
	
	@Field("lastModifiedOn")
	public Date lastModifiedOn;
	
	@Field("lastModifiedBy")
	public String lastModifiedBy;
	
	@Field("updatedSource")
	public String updatedSource;

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public List<CDCMetaDataDetails> getCdcMetaDataDetails() {
		return cdcMetaDataDetails;
	}

	public void setCdcMetaDataDetails(List<CDCMetaDataDetails> cdcMetaDataDetails) {
		this.cdcMetaDataDetails = cdcMetaDataDetails;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getUpdatedSource() {
		return updatedSource;
	}

	public void setUpdatedSource(String updatedSource) {
		this.updatedSource = updatedSource;
	}
}
