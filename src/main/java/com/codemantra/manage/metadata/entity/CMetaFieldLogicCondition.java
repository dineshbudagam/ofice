package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class CMetaFieldLogicCondition {

	@Field("metaDataFieldName")
	private String metaDataFieldName;
	
	@Field("operator")
	private String operator;
	
	@Field("value")
	private Object value;

	public CMetaFieldLogicCondition() {
	}

	public CMetaFieldLogicCondition(String metaDataFieldName, String operator, String value) {
		super();
		this.metaDataFieldName = metaDataFieldName;
		this.operator = operator;
		this.value = value;
	}

	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}

	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
