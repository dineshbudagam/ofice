package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class CMetaFieldLogicCustomFieldsString {
	@Field("metaDataFieldName")
    public String metaDataFieldName;
    
    @Field("operation")
    public String operation;
    
    @Field("startIndex")
    public Integer startIndex;

    @Field("endIndex")
    public Integer endIndex;
    
    @Field("characters")
    public Integer characters;
    
    @Field("substituteText")
    public String substituteText;
    
    

    public String getSubstituteText() {
		return substituteText;
	}

	public void setSubstituteText(String substituteText) {
		this.substituteText = substituteText;
	}

	public String getMetaDataFieldName() {
            return metaDataFieldName;
    }

    public String getOperation() {
            return operation;
    }

    public Integer getStartIndex() {
            return startIndex;
    }

    public Integer getEndIndex() {
            return endIndex;
    }

    public Integer getCharacters() {
            return characters;
    }

    public void setMetaDataFieldName(String metaDataFieldName) {
            this.metaDataFieldName = metaDataFieldName;
    }

    public void setOperation(String operation) {
            this.operation = operation;
    }

    public void setStartIndex(Integer startIndex) {
            this.startIndex = startIndex;
    }

    public void setEndIndex(Integer endIndex) {
            this.endIndex = endIndex;
    }

    public void setCharacters(Integer characters) {
            this.characters = characters;
    }
}
