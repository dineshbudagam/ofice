package com.codemantra.manage.metadata.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class CMetaFieldLogicEntity {
	
	 @Field("metaDataFieldName")
     public String metaDataFieldName;
     
     @Field("operation")
     public String operation;
     
     @Field("fieldsRequired")
     public List<String> fieldsRequired;
     
     @Field("staticValue")
     public String staticValue;

     @Field("condition")
     public List<CMetaFieldLogicCondition> condition;
     
     @Field("customFieldsString")
     public List<CMetaFieldLogicCustomFieldsString> customFieldsString;
     //dbQuery
     @Field("dbQuery")
     public String dbQuery;
     
     @Field("checkOnEdit")
     public Boolean checkOnEdit;
     
     @Field("isActive")
     public String isActive;
     
     @Field("isDeleted")
     public String isDeleted;

     @Field("createdBy")
     public String createdBy;
     
     @Field("modifiedBy")
     public String modifiedBy;

     @Field("createdOn")
     public Date createdOn;
     
     @Field("modifiedOn")
     public Date modifiedOn;

     
     
     public String getDbQuery() {
             return dbQuery;
     }

     public void setDbQuery(String dbQuery) {
             this.dbQuery = dbQuery;
     }

     public Boolean getCheckOnEdit() {
             return checkOnEdit;
     }

     public void setCheckOnEdit(Boolean checkOnEdit) {
             this.checkOnEdit = checkOnEdit;
     }

     public String getMetaDataFieldName() {
             return metaDataFieldName;
     }

     public String getOperation() {
             return operation;
     }

     public List<String> getFieldsRequired() {
             return fieldsRequired;
     }

     public String getStaticValue() {
             return staticValue;
     }

     public List<CMetaFieldLogicCondition> getCondition() {
             return condition;
     }

     public List<CMetaFieldLogicCustomFieldsString> getCustomFieldsString() {
             return customFieldsString;
     }

     public String getIsActive() {
             return isActive;
     }

     public String getIsDeleted() {
             return isDeleted;
     }

     public String getCreatedBy() {
             return createdBy;
     }

     public String getModifiedBy() {
             return modifiedBy;
     }

     public Date getCreatedOn() {
             return createdOn;
     }

     public Date getModifiedOn() {
             return modifiedOn;
     }

     public void setMetaDataFieldName(String metaDataFieldName) {
             this.metaDataFieldName = metaDataFieldName;
     }

     public void setOperation(String operation) {
             this.operation = operation;
     }

     public void setFieldsRequired(List<String> fieldsRequired) {
             this.fieldsRequired = fieldsRequired;
     }

     public void setStaticValue(String staticValue) {
             this.staticValue = staticValue;
     }

     public void setCondition(List<CMetaFieldLogicCondition> condition) {
             this.condition = condition;
     }

     public void setCustomFieldsString(List<CMetaFieldLogicCustomFieldsString> customFieldsString) {
             this.customFieldsString = customFieldsString;
     }

     public void setIsActive(String isActive) {
             this.isActive = isActive;
     }

     public void setIsDeleted(String isDeleted) {
             this.isDeleted = isDeleted;
     }

     public void setCreatedBy(String createdBy) {
             this.createdBy = createdBy;
     }

     public void setModifiedBy(String modifiedBy) {
             this.modifiedBy = modifiedBy;
     }

     public void setCreatedOn(Date createdOn) {
             this.createdOn = createdOn;
     }

     public void setModifiedOn(Date modifiedOn) {
             this.modifiedOn = modifiedOn;
     }
	
}
