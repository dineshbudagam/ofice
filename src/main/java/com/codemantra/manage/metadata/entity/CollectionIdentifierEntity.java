/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class CollectionIdentifierEntity
{
	@Field("CollectionIDType")
	public String collectionIDType;

	@Field("IdTypeName")
	public String idTypeName;

	@Field("IdValue")
	public String idValue;

	public String getCollectionIDType() {
		return collectionIDType;
	}

	public void setCollectionIDType(String collectionIDType) {
		this.collectionIDType = collectionIDType;
	}

	public String getIdTypeName() {
		return idTypeName;
	}

	public void setIdTypeName(String idTypeName) {
		this.idTypeName = idTypeName;
	}

	public String getIdValue() {
		return idValue;
	}

	public void setIdValue(String idValue) {
		this.idValue = idValue;
	}

		
}
