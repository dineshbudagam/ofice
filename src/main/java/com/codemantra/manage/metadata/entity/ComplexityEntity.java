/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class ComplexityEntity
{
	@Field("ComplexityCode")
	public String complexityCode;

	@Field("ComplexitySchemeIdentifiers")
	public String complexitySchemeIdentifiers;

	public String getComplexityCode() {
		return complexityCode;
	}

	public void setComplexityCode(String complexityCode) {
		this.complexityCode = complexityCode;
	}

	public String getComplexitySchemeIdentifiers() {
		return complexitySchemeIdentifiers;
	}

	public void setComplexitySchemeIdentifiers(String complexitySchemeIdentifiers) {
		this.complexitySchemeIdentifiers = complexitySchemeIdentifiers;
	}
		
}
