/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class ConferenceEntity
{
	@Field("ConferenceRole")
	public String conferenceRole;

	@Field("ConferenceName")
	public String conferenceName;

	@Field("ConferenceAcronym")
	public String conferenceAcronym;

	@Field("ConferenceNumber")
	public String conferenceNumber;

	@Field("ConferenceTheme")
	public String conferenceTheme;

	@Field("ConferenceDate")
	public String conferenceDate;

	@Field("ConferencePlace")
	public String conferencePlace;

	public List<ConferenceSponsorEntity> conferenceSponsor;

	public List<WebsiteEntity> website;

	public String getConferenceRole() {
		return conferenceRole;
	}

	public void setConferenceRole(String conferenceRole) {
		this.conferenceRole = conferenceRole;
	}

	public String getConferenceName() {
		return conferenceName;
	}

	public void setConferenceName(String conferenceName) {
		this.conferenceName = conferenceName;
	}

	public String getConferenceAcronym() {
		return conferenceAcronym;
	}

	public void setConferenceAcronym(String conferenceAcronym) {
		this.conferenceAcronym = conferenceAcronym;
	}

	public String getConferenceNumber() {
		return conferenceNumber;
	}

	public void setConferenceNumber(String conferenceNumber) {
		this.conferenceNumber = conferenceNumber;
	}

	public String getConferenceTheme() {
		return conferenceTheme;
	}

	public void setConferenceTheme(String conferenceTheme) {
		this.conferenceTheme = conferenceTheme;
	}

	public String getConferenceDate() {
		return conferenceDate;
	}

	public void setConferenceDate(String conferenceDate) {
		this.conferenceDate = conferenceDate;
	}

	public String getConferencePlace() {
		return conferencePlace;
	}

	public void setConferencePlace(String conferencePlace) {
		this.conferencePlace = conferencePlace;
	}

	public List<ConferenceSponsorEntity> getConferenceSponsor() {
		return conferenceSponsor;
	}

	public void setConferenceSponsor(List<ConferenceSponsorEntity> conferenceSponsor) {
		this.conferenceSponsor = conferenceSponsor;
	}

	public List<WebsiteEntity> getWebsite() {
		return website;
	}

	public void setWebsite(List<WebsiteEntity> website) {
		this.website = website;
	}

}
