/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
21-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class ConferenceSponsorEntity
{
	public ConferenceSponsorIdentifierEntity ConferenceSponsorIdentifier;

	@Field("PersonName")
	public String PersonName;

	@Field("CorporateName")
	public String CorporateName;

	public ConferenceSponsorIdentifierEntity getConferenceSponsorIdentifier() {
		return ConferenceSponsorIdentifier;
	}

	public void setConferenceSponsorIdentifier(ConferenceSponsorIdentifierEntity conferenceSponsorIdentifier) {
		ConferenceSponsorIdentifier = conferenceSponsorIdentifier;
	}

	public String getPersonName() {
		return PersonName;
	}

	public void setPersonName(String personName) {
		PersonName = personName;
	}

	public String getCorporateName() {
		return CorporateName;
	}

	public void setCorporateName(String corporateName) {
		CorporateName = corporateName;
	}

}
