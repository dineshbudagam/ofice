/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
16-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class ContentItemEntity
{
	@Field("LevelSequenceNumber")
	public String levelSequenceNumber;

	@Field("TextItem")
	public String textItem;

	public List<WebsiteEntity> website;

	@Field("ComponentTypeName")
	public String componentTypeName;

	@Field("ComponentNumber")
	public String componentNumber;

	@Field("DistinctiveTitle")
	public String distinctiveTitle;

	public List<TitleEntity> title;

	public List<WorkIdentifierEntity> workIdentifier;

	public List<ContributorEntity> contributor;

	@Field("ContributorStatement")
	public String contributorStatement;

	public List<SubjectEntity> subject;

	public List<PersonAsSubjectEntity> personAsSubject;

	@Field("CorporateBodyAsSubject")
	public List<String> corporateBodyAsSubject;

	@Field("PlaceAsSubject")
	public List<String> placeAsSubject;

	public List<OtherTextEntity> otherText;

	public List<MediaFileEntity> mediaFile;

	public String getLevelSequenceNumber() {
		return levelSequenceNumber;
	}

	public void setLevelSequenceNumber(String levelSequenceNumber) {
		this.levelSequenceNumber = levelSequenceNumber;
	}

	public String getTextItem() {
		return textItem;
	}

	public void setTextItem(String textItem) {
		this.textItem = textItem;
	}

	public List<WebsiteEntity> getWebsite() {
		return website;
	}

	public void setWebsite(List<WebsiteEntity> website) {
		this.website = website;
	}

	public String getComponentTypeName() {
		return componentTypeName;
	}

	public void setComponentTypeName(String componentTypeName) {
		this.componentTypeName = componentTypeName;
	}

	public String getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(String componentNumber) {
		this.componentNumber = componentNumber;
	}

	public String getDistinctiveTitle() {
		return distinctiveTitle;
	}

	public void setDistinctiveTitle(String distinctiveTitle) {
		this.distinctiveTitle = distinctiveTitle;
	}

	public List<TitleEntity> getTitle() {
		return title;
	}

	public void setTitle(List<TitleEntity> title) {
		this.title = title;
	}

	public List<WorkIdentifierEntity> getWorkIdentifier() {
		return workIdentifier;
	}

	public void setWorkIdentifier(List<WorkIdentifierEntity> workIdentifier) {
		this.workIdentifier = workIdentifier;
	}

	public List<ContributorEntity> getContributor() {
		return contributor;
	}

	public void setContributor(List<ContributorEntity> contributor) {
		this.contributor = contributor;
	}

	public String getContributorStatement() {
		return contributorStatement;
	}

	public void setContributorStatement(String contributorStatement) {
		this.contributorStatement = contributorStatement;
	}

	public List<SubjectEntity> getSubject() {
		return subject;
	}

	public void setSubject(List<SubjectEntity> subject) {
		this.subject = subject;
	}

	public List<PersonAsSubjectEntity> getPersonAsSubject() {
		return personAsSubject;
	}

	public void setPersonAsSubject(List<PersonAsSubjectEntity> personAsSubject) {
		this.personAsSubject = personAsSubject;
	}

	public List<String> getCorporateBodyAsSubject() {
		return corporateBodyAsSubject;
	}

	public void setCorporateBodyAsSubject(List<String> corporateBodyAsSubject) {
		this.corporateBodyAsSubject = corporateBodyAsSubject;
	}

	public List<String> getPlaceAsSubject() {
		return placeAsSubject;
	}

	public void setPlaceAsSubject(List<String> placeAsSubject) {
		this.placeAsSubject = placeAsSubject;
	}

	public List<OtherTextEntity> getOtherText() {
		return otherText;
	}

	public void setOtherText(List<OtherTextEntity> otherText) {
		this.otherText = otherText;
	}

	public List<MediaFileEntity> getMediaFile() {
		return mediaFile;
	}

	public void setMediaFile(List<MediaFileEntity> mediaFile) {
		this.mediaFile = mediaFile;
	}
	
}
