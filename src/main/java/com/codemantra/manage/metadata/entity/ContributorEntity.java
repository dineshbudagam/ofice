/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class ContributorEntity
{
	@Field("SequenceNumber")
	public String sequenceNumber;

	@Field("ContributorRole")
	public String contributorRole;
	
	@Field("PersonNameKey")
	public String personNameKey;
	
	@Field("PersonNameBeforeKey")
	public String personNameBeforeKey;

	@Field("LanguageCode")
	public List<String> languageCode;

	@Field("SequenceNumberWithinRole")
	public String sequenceNumberWithinRole;

	@Field("PersonName")
	public String personName;
	
	@Field("PersonNameInverted")
	public String personNameInverted;
	
	@Field("TitlesBeforeNames")
	public String titlesBeforeNames;
	
	@Field("NamesBeforeKey")
	public String namesBeforeKey;
	
	@Field("PrefixToKey")
	public String prefixToKey;
	
	@Field("KeyNames")
	public String keyNames;
	
	@Field("NamesAfterKey")
	public String namesAfterKey;
	
	@Field("SuffixToKey")
	public String suffixToKey;
	
	@Field("LettersAfterNames")
	public String lettersAfterNames;
	
	@Field("TitlesAfterNames")
	public String titlesAfterNames;
	
	@Field("Name")
	public List<String> name;
	
	@Field("PersonNameIdentifier")
	public List<PersonNameIdentifierEntity> personNameIdentifier;
	
	@Field("PersonDate")
	public List<String> personDate;
	
	@Field("ProfessionalAffiliation")
	public List<ProfessionalAffiliationEntity> professionalAffiliation;

	@Field("CorporateName")
	public String corporateName;

	@Field("BiographicalNote")
	public String biographicalNote;

	@Field("Website")
	public List<WebsiteEntity> website;

	@Field("ContributorDescription")
	public String contributorDescription;
	
	@Field("UnnamedPersons")
	public String unnamedPersons;
	
	@Field("CountryCode")
	public List<String> countryCode;
	
	@Field("RegionCode")
	public List<String> regionCode;

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getContributorRole() {
		return contributorRole;
	}

	public void setContributorRole(String contributorRole) {
		this.contributorRole = contributorRole;
	}

	public String getPersonNameKey() {
		return personNameKey;
	}

	public void setPersonNameKey(String personNameKey) {
		this.personNameKey = personNameKey;
	}

	public String getPersonNameBeforeKey() {
		return personNameBeforeKey;
	}

	public void setPersonNameBeforeKey(String personNameBeforeKey) {
		this.personNameBeforeKey = personNameBeforeKey;
	}

	public List<String> getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(List<String> languageCode) {
		this.languageCode = languageCode;
	}

	public String getSequenceNumberWithinRole() {
		return sequenceNumberWithinRole;
	}

	public void setSequenceNumberWithinRole(String sequenceNumberWithinRole) {
		this.sequenceNumberWithinRole = sequenceNumberWithinRole;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPersonNameInverted() {
		return personNameInverted;
	}

	public void setPersonNameInverted(String personNameInverted) {
		this.personNameInverted = personNameInverted;
	}

	public String getTitlesBeforeNames() {
		return titlesBeforeNames;
	}

	public void setTitlesBeforeNames(String titlesBeforeNames) {
		this.titlesBeforeNames = titlesBeforeNames;
	}

	public String getNamesBeforeKey() {
		return namesBeforeKey;
	}

	public void setNamesBeforeKey(String namesBeforeKey) {
		this.namesBeforeKey = namesBeforeKey;
	}

	public String getPrefixToKey() {
		return prefixToKey;
	}

	public void setPrefixToKey(String prefixToKey) {
		this.prefixToKey = prefixToKey;
	}

	public String getKeyNames() {
		return keyNames;
	}

	public void setKeyNames(String keyNames) {
		this.keyNames = keyNames;
	}

	public String getNamesAfterKey() {
		return namesAfterKey;
	}

	public void setNamesAfterKey(String namesAfterKey) {
		this.namesAfterKey = namesAfterKey;
	}

	public String getSuffixToKey() {
		return suffixToKey;
	}

	public void setSuffixToKey(String suffixToKey) {
		this.suffixToKey = suffixToKey;
	}

	public String getLettersAfterNames() {
		return lettersAfterNames;
	}

	public void setLettersAfterNames(String lettersAfterNames) {
		this.lettersAfterNames = lettersAfterNames;
	}

	public String getTitlesAfterNames() {
		return titlesAfterNames;
	}

	public void setTitlesAfterNames(String titlesAfterNames) {
		this.titlesAfterNames = titlesAfterNames;
	}

	public List<String> getName() {
		return name;
	}

	public void setName(List<String> name) {
		this.name = name;
	}

	public List<PersonNameIdentifierEntity> getPersonNameIdentifier() {
		return personNameIdentifier;
	}

	public void setPersonNameIdentifier(List<PersonNameIdentifierEntity> personNameIdentifier) {
		this.personNameIdentifier = personNameIdentifier;
	}

	public List<String> getPersonDate() {
		return personDate;
	}

	public void setPersonDate(List<String> personDate) {
		this.personDate = personDate;
	}

	public List<ProfessionalAffiliationEntity> getProfessionalAffiliation() {
		return professionalAffiliation;
	}

	public void setProfessionalAffiliation(List<ProfessionalAffiliationEntity> professionalAffiliation) {
		this.professionalAffiliation = professionalAffiliation;
	}

	public String getCorporateName() {
		return corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	public String getBiographicalNote() {
		return biographicalNote;
	}

	public void setBiographicalNote(String biographicalNote) {
		this.biographicalNote = biographicalNote;
	}

	public List<WebsiteEntity> getWebsite() {
		return website;
	}

	public void setWebsite(List<WebsiteEntity> website) {
		this.website = website;
	}

	public String getContributorDescription() {
		return contributorDescription;
	}

	public void setContributorDescription(String contributorDescription) {
		this.contributorDescription = contributorDescription;
	}

	public String getUnnamedPersons() {
		return unnamedPersons;
	}

	public void setUnnamedPersons(String unnamedPersons) {
		this.unnamedPersons = unnamedPersons;
	}

	public List<String> getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(List<String> countryCode) {
		this.countryCode = countryCode;
	}

	public List<String> getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(List<String> regionCode) {
		this.regionCode = regionCode;
	}
}