/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class CopyrightOwnerEntity
{
	public CopyrightOwnerIdentifierEntity copyrightOwnerIdentifier;

	@Field("PersonName")
	public String personName;

	@Field("CorporateName")
	public String corporateName;

	public CopyrightOwnerIdentifierEntity getCopyrightOwnerIdentifier() {
		return copyrightOwnerIdentifier;
	}

	public void setCopyrightOwnerIdentifier(CopyrightOwnerIdentifierEntity copyrightOwnerIdentifier) {
		this.copyrightOwnerIdentifier = copyrightOwnerIdentifier;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getCorporateName() {
		return corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}
	
}
