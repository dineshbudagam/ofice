/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;


public class CopyrightStatementEntity
{
	@Field("CopyrightYear")
	public List<String> copyrightYear;
	
	@Field("CopyrightOwner")
	public List<CopyrightOwnerEntity> copyrightOwner;

	public List<String> getCopyrightYear() {
		return copyrightYear;
	}

	public void setCopyrightYear(List<String> copyrightYear) {
		this.copyrightYear = copyrightYear;
	}

	public List<CopyrightOwnerEntity> getCopyrightOwner() {
		return copyrightOwner;
	}

	public void setCopyrightOwner(List<CopyrightOwnerEntity> copyrightOwner) {
		this.copyrightOwner = copyrightOwner;
	}

			
}
