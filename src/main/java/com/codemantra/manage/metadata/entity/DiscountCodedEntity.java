/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class DiscountCodedEntity
{
	@Field("DiscountCodeType")
	public String discountCodeType;

	@Field("DiscountCode")
	public String discountCode;

	@Field("DiscountCodeTypeName")
	public String discountCodeTypeName;

	public String getDiscountCodeType() {
		return discountCodeType;
	}

	public void setDiscountCodeType(String discountCodeType) {
		this.discountCodeType = discountCodeType;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public String getDiscountCodeTypeName() {
		return discountCodeTypeName;
	}

	public void setDiscountCodeTypeName(String discountCodeTypeName) {
		this.discountCodeTypeName = discountCodeTypeName;
	}

}
