/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class ExtentEntity
{
	@Field("ExtentType")
	public String extentType;

	@Field("ExtentUnit")
	public String extentUnit;

	@Field("ExtentValue")
	public Double extentValue;

	public String getExtentType() {
		return extentType;
	}

	public void setExtentType(String extentType) {
		this.extentType = extentType;
	}

	public String getExtentUnit() {
		return extentUnit;
	}

	public void setExtentUnit(String extentUnit) {
		this.extentUnit = extentUnit;
	}

	public Double getExtentValue() {
		return extentValue;
	}

	public void setExtentValue(Double extentValue) {
		this.extentValue = extentValue;
	}

}
