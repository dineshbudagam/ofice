package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class FieldDataType{
	
	@Field("fieldType")
	private String fieldType;
	
	@Field("dataTypeName")
	private String dataTypeName;
	
	@Field("dataType")
	private String dataType;
	
	@Field("validationType")
	private String validationType;
	
	@Field("validation")
	private Validation validation;
	

	
	public String getFieldType() {
	return fieldType;
	}
	
	
	public void setFieldType(String fieldType) {
	this.fieldType = fieldType;
	}
	
	
	public String getDataTypeName() {
	return dataTypeName;
	}
	
	
	public void setDataTypeName(String dataTypeName) {
	this.dataTypeName = dataTypeName;
	}
	
	
	public String getDataType() {
	return dataType;
	}
	
	
	public void setDataType(String dataType) {
	this.dataType = dataType;
	}
	
	
	public String getValidationType() {
	return validationType;
	}
	
	
	public void setValidationType(String validationType) {
	this.validationType = validationType;
	}
	
	
	public Validation getValidation() {
	return validation;
	}
	
	
	public void setValidation(Validation validation) {
	this.validation = validation;
	}	

}