/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class IllustrationsEntity
{
	@Field("IllustrationType")
	public String illustrationType;

	@Field("IllustrationTypeDescription")
	public String illustrationTypeDescription;

	@Field("Number")
	public String number;

	public String getIllustrationType() {
		return illustrationType;
	}

	public void setIllustrationType(String illustrationType) {
		this.illustrationType = illustrationType;
	}

	public String getIllustrationTypeDescription() {
		return illustrationTypeDescription;
	}

	public void setIllustrationTypeDescription(String illustrationTypeDescription) {
		this.illustrationTypeDescription = illustrationTypeDescription;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
		
}
