/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

public class ImprintAccess extends AccessEntity {
	
	private String imprintCode;
	private String imprintName;
	
	public String getImprintCode() {
		return imprintCode;
	}
	public void setImprintCode(String imprintCode) {
		this.imprintCode = imprintCode;
	}
	public String getImprintName() {
		return imprintName;
	}
	public void setImprintName(String imprintName) {
		this.imprintName = imprintName;
	}
}
