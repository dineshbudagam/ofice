/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class ImprintEntity
{
	@Field("NameCodeType")
	public String nameCodeType;
	
	@Field("NameCodeTypeName")
	public String nameCodeTypeName;
	
	@Field("NameCodeValue")
	public String nameCodeValue;
	
	@Field("ImprintName")
	public String imprintName;
	
	public String getNameCodeType() {
		return nameCodeType;
	}
	public void setNameCodeType(String nameCodeType) {
		this.nameCodeType = nameCodeType;
	}
	public String getNameCodeTypeName() {
		return nameCodeTypeName;
	}
	public void setNameCodeTypeName(String nameCodeTypeName) {
		this.nameCodeTypeName = nameCodeTypeName;
	}
	public String getNameCodeValue() {
		return nameCodeValue;
	}
	public void setNameCodeValue(String nameCodeValue) {
		this.nameCodeValue = nameCodeValue;
	}
	public String getImprintName() {
		return imprintName;
	}
	public void setImprintName(String imprintName) {
		this.imprintName = imprintName;
	}
	
	
}