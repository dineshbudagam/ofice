/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

//@Document(collection = "products")
public class ImprintsEntity {

	@Field("Imprint")
	private ImprintEntity ImprintName;

	public ImprintEntity getImprintName() {
		return ImprintName;
	}

	public void setImprintName(ImprintEntity imprintName) {
		ImprintName = imprintName;
	}
	

	
}
