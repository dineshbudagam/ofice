/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
22-05-2017			v1.0       	   Bharath Prasanna	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class InactiveTitlesEntity {
	
	@Field("rules")
	private List<RulesEntity> rules;

	public List<RulesEntity> getRules() {
		return rules;
	}

	public void setRules(List<RulesEntity> rules) {
		this.rules = rules;
	}
}
