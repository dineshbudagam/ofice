package com.codemantra.manage.metadata.entity;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"field",
"condition",
"value"
})
public class JsonDbPathCondition {

	@Field("field")
	private String field;
	@Field("condition")
	private String condition;
	@Field("value")
	private String value;
	
	
	public String getField() {
	return field;
	}
	
	
	public void setField(String field) {
	this.field = field;
	}
	
	
	public String getCondition() {
	return condition;
	}
	
	
	public void setCondition(String condition) {
	this.condition = condition;
	}
	
	
	public String getValue() {
	return value;
	}
	
	
	public void setValue(String value) {
	this.value = value;
	}
	

}