package com.codemantra.manage.metadata.entity;
import org.springframework.data.mongodb.core.mapping.Field;

public class JsonDbPathConditionEntity {
	@Field("condition")
	private String Condition;
	
	@Field("value")
	private String Value;
	
	@Field("field")
	private String Field;

	public String getCondition() {
		return Condition;
	}

	public void setCondition(String condition) {
		Condition = condition;
	}

	public String getValue() {
		return Value;
	}

	public void setValue(String value) {
		Value = value;
	}

	public String getField() {
		return Field;
	}

	public void setField(String field) {
		Field = field;
	}
	
	
}
