/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class LanguageEntity {
	
	@Field("LanguageRole")
	public String languageRole;

	@Field("CountryCode")
	public String countryCode;

	@Field("LanguageCode")
	public String languageCode;

	public String getLanguageRole() {
		return languageRole;
	}

	public void setLanguageRole(String languageRole) {
		this.languageRole = languageRole;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
}
