/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class MainSubjectEntity
{
	@Field("MainSubjectSchemeIdentifier")
	public String mainSubjectSchemeIdentifier;

	@Field("SubjectSchemeVersion")
	public String subjectSchemeVersion;

	@Field("SubjectCode")
	public String subjectCode;

	@Field("SubjectHeadingText")
	public String subjectHeadingText;

	public String getMainSubjectSchemeIdentifier() {
		return mainSubjectSchemeIdentifier;
	}

	public void setMainSubjectSchemeIdentifier(String mainSubjectSchemeIdentifier) {
		this.mainSubjectSchemeIdentifier = mainSubjectSchemeIdentifier;
	}

	public String getSubjectSchemeVersion() {
		return subjectSchemeVersion;
	}

	public void setSubjectSchemeVersion(String subjectSchemeVersion) {
		this.subjectSchemeVersion = subjectSchemeVersion;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectHeadingText() {
		return subjectHeadingText;
	}

	public void setSubjectHeadingText(String subjectHeadingText) {
		this.subjectHeadingText = subjectHeadingText;
	}
			
}
