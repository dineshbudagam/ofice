/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class MediaFileEntity
{
	@Field("MediaFileTypeCode")
	public String mediaFileTypeCode;

	@Field("MediaFileFormatCode")
	public String mediaFileFormatCode;

	@Field("ImageResolution")
	public String imageResolution;

	@Field("MediaFileLinkTypeCode")
	public String mediaFileLinkTypeCode;

	@Field("MediaFileLink")
	public String mediaFileLink;

	@Field("TextWithDownload")
	public String textWithDownload;

	@Field("DownloadCaption")
	public String downloadCaption;

	@Field("DownloadCredit")
	public String downloadCredit;

	@Field("DownloadCopyrightNotice")
	public String downloadCopyrightNotice;

	@Field("DownloadTerms")
	public String downloadTerms;

	@Field("MediaFileDate")
	public String mediaFileDate;

	public String getMediaFileTypeCode() {
		return mediaFileTypeCode;
	}

	public void setMediaFileTypeCode(String mediaFileTypeCode) {
		this.mediaFileTypeCode = mediaFileTypeCode;
	}

	public String getMediaFileFormatCode() {
		return mediaFileFormatCode;
	}

	public void setMediaFileFormatCode(String mediaFileFormatCode) {
		this.mediaFileFormatCode = mediaFileFormatCode;
	}

	public String getImageResolution() {
		return imageResolution;
	}

	public void setImageResolution(String imageResolution) {
		this.imageResolution = imageResolution;
	}

	public String getMediaFileLinkTypeCode() {
		return mediaFileLinkTypeCode;
	}

	public void setMediaFileLinkTypeCode(String mediaFileLinkTypeCode) {
		this.mediaFileLinkTypeCode = mediaFileLinkTypeCode;
	}

	public String getMediaFileLink() {
		return mediaFileLink;
	}

	public void setMediaFileLink(String mediaFileLink) {
		this.mediaFileLink = mediaFileLink;
	}

	public String getTextWithDownload() {
		return textWithDownload;
	}

	public void setTextWithDownload(String textWithDownload) {
		this.textWithDownload = textWithDownload;
	}

	public String getDownloadCaption() {
		return downloadCaption;
	}

	public void setDownloadCaption(String downloadCaption) {
		this.downloadCaption = downloadCaption;
	}

	public String getDownloadCredit() {
		return downloadCredit;
	}

	public void setDownloadCredit(String downloadCredit) {
		this.downloadCredit = downloadCredit;
	}

	public String getDownloadCopyrightNotice() {
		return downloadCopyrightNotice;
	}

	public void setDownloadCopyrightNotice(String downloadCopyrightNotice) {
		this.downloadCopyrightNotice = downloadCopyrightNotice;
	}

	public String getDownloadTerms() {
		return downloadTerms;
	}

	public void setDownloadTerms(String downloadTerms) {
		this.downloadTerms = downloadTerms;
	}

	public String getMediaFileDate() {
		return mediaFileDate;
	}

	public void setMediaFileDate(String mediaFileDate) {
		this.mediaFileDate = mediaFileDate;
	}

			
}
