/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

public class MetaDataAutoPopulateEntity {
	
	@Field("SourceField")
	public String sourceField;
	
	@Field("PopulateLookup")
	public List<Map<String,String>> populateLookup;
	
	@Field("PopulateField")
	public String populateField;
	
	@Field("PopulateCondition")
	public String populateCondition;

	public String getSourceField() {
		return sourceField;
	}

	public void setSourceField(String sourceField) {
		this.sourceField = sourceField;
	}

	public List<Map<String, String>> getPopulateLookup() {
		return populateLookup;
	}

	public void setPopulateLookup(List<Map<String, String>> populateLookup) {
		this.populateLookup = populateLookup;
	}

	public String getPopulateField() {
		return populateField;
	}

	public void setPopulateField(String populateField) {
		this.populateField = populateField;
	}

	public String getPopulateCondition() {
		return populateCondition;
	}

	public void setPopulateCondition(String populateCondition) {
		this.populateCondition = populateCondition;
	}
}
