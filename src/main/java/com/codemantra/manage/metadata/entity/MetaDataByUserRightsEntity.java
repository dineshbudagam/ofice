/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-07-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

public class MetaDataByUserRightsEntity {

	private String userId;
	
	private List<AccountAccessEntity> accounts;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<AccountAccessEntity> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AccountAccessEntity> accounts) {
		this.accounts = accounts;
	}

}
