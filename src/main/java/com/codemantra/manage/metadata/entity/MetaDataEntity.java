/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MetaDataEntity
{
	@Field("RecordReference")
	public String recordReference;
	
	@Field("NotificationType")
	public String notificationType;

	@Field("DeletionCode")
	public String deletionCode;
	
	@Field("DeletionText")
	public String deletionText;
	
	@Field("RecordSourceType")
	public String recordSourceType;
	
	@Field("RecordSourceIdentifierType")
	public String recordSourceIdentifierType;
	
	@Field("RecordSourceIdentifier")
	public String recordSourceIdentifier;
	
	@Field("RecordSourceName")
	public String recordSourceName;
	
	@Field("ISBN")
	public String iSBN;
	
	@Field("EAN13")
	public String eAN13;
	
	@Field("UPC")
	public String uPC;

	@Field("PublisherProductNo")
	public String publisherProductNo;

	@Field("ISMN")
	public String iSMN;
	
	@Field("DOI")
	public String dOI;
	
	@Field("ProductIdentifier")
	public List<ProductIdentifierEntity> productIdentifier;
	
	@Field("ReplacesISBN")
	public String replacesISBN;

	@Field("ReplacesEAN13")
	public String replacesEAN13;
	
	@Field("ProductFormDetail")
	public List<String> productFormDetail;

	@Field("ProductFormFeature")
	public List<ProductFormFeatureEntity> ProductFormFeature;

	@Field("BookFormDetail")
	public List<String> BookFormDetail;

	@Field("ProductPackaging")
	public String ProductPackaging;
	
	@Field("ProductFormDescription")
	public String productFormDescription;

	@Field("NumberOfPieces")
	public String numberOfPieces;
	
	@Field("TradeCategory")
	public String tradeCategory;

	@Field("ProductContentType")
	public List<String> productContentType;

	/*public List<ContainedItem> ContainedItem;*/

	@Field("ProductClassification")
	public List<ProductClassificationEntity> productClassification;

	@Field("EpubType")
	public String ePubType;

	@Field("EpubTypeVersion")
	public String ePubTypeVersion;
	
	@Field("EpubTypeDescription")
	public String ePubTypeDescription;
	
	@Field("EpubFormat")
	public String ePubFormat;
	
	@Field("EpubFormatVersion")
	public String ePubFormatVersion;

	@Field("EpubFormatDescription")
	public String ePubFormatDescription;
	
	@Field("EpubSource")
	public String ePubSource;
	
	@Field("EpubSourceVersion")
	public String ePubSourceVersion;
	
	@Field("EpubSourceDescription")
	public String ePubSourceDescription;
	
	@Field("EpubTypeNote")
	public String ePubTypeNote;

	@Field("Series")
	public List<SeriesEntity> series;

	@Field("NoSeries")
	public String noSeries;

	@Field("Set")
	public List<SetEntity> set;

	@Field("TextCaseFlag")
	public String textCaseFlag;

	@Field("DistinctiveTitle")
	public String distinctiveTitle;
	
	@Field("TitlePrefix")
	public String titlePrefix;
	
	@Field("TitleWithoutPrefix")
	public String titleWithoutPrefix;
	
	@Field("Subtitle")
	public String subtitle;
	
	@Field("TranslationOfTitle")
	public String translationOfTitle;

	@Field("FormerTitle")
	public List<String> formerTitle;

	@Field("Title")
	public List<TitleEntity> title;
	
	@Field("WorkIdentifier")
	public List<WorkIdentifierEntity> workIdentifier;

	@Field("Website")
	public List<WebsiteEntity> website;
	
	@Field("ThesisType")
	public String thesisType;
	
	@Field("ThesisPresentedTo")
	public String thesisPresentedTo;
	
	@Field("ThesisYear")
	public String thesisYear;

	@Field("Contributor")
	public List<ContributorEntity> contributor;
	
	@Field("ContributorStatement")
	public String contributorStatement;
	
	@Field("NoContributor")
	public String noContributor;
	
	@Field("ConferenceDescription")
	public String conferenceDescription;
	
	@Field("ConferenceRole")
	public String conferenceRole;
	
	@Field("ConferenceName")
	public String conferenceName;
	
	@Field("ConferenceNumber")
	public String conferenceNumber;
	
	@Field("ConferenceDate")
	public String conferenceDate;
	
	@Field("ConferencePlace")
	public String conferencePlace;

	@Field("Conference")
	public List<ConferenceEntity> conference;

	/*public List<EditionTypeCode> EditionTypeCode;*/
	
	@Field("EditionNumber")
	public String editionNumber;
	
	@Field("EditionVersionNumber")
	public String editionVersionNumber;
	
	@Field("EditionStatement")
	public String editionStatement;
	
	@Field("NoEdition")
	public String noEdition;
	
	@Field("ReligiousText")
	public String religiousText;

	/*public List<LanguageOfText> LanguageOfText;*/
	
	@Field("OriginalLanguage")
	public String originalLanguage;

	@Field("Language")
	public List<LanguageEntity> language;
	
	@Field("NumberOfPages")
	public String numberOfPages;
	
	@Field("PagesRoman")
	public String pagesRoman;
	
	@Field("PagesArabic")
	public String pagesArabic;

	@Field("Extent")
	public List<ExtentEntity> extent;
	
	@Field("NumberOfIllustrations")
	public String numberOfIllustrations;
	
	@Field("IllustrationsNote")
	public String illustrationsNote;

	@Field("Illustrations")
	public List<IllustrationsEntity> illustrations;

	/*public List<MapScale> MapScale;*/
	
	@Field("BASICMainSubject")
	public String basicMainSubject;
	
	@Field("BASICVersion")
	public String basicVersion;
	
	@Field("BICMainSubject")
	public String bicMainSubject;
	
	@Field("BICVersion")
	public String bicVersion;

	@Field("MainSubject")
	public List<MainSubjectEntity> mainSubject;

	@Field("Subject")
	public List<SubjectEntity> subject;

	@Field("PersonAsSubject")
	public List<PersonAsSubjectEntity> personAsSubject;

	/*public List<CorporateBodyAsSubject> CorporateBodyAsSubject;

	public List<PlaceAsSubject> PlaceAsSubject;*/

	@Field("Audience")
	public List<AudienceEntity> audience;
	
	@Field("USSchoolGrade")
	public String usSchoolGrade;
	
	@Field("InterestAge")
	public String interestAge;

	@Field("AudienceRange")
	public List<AudienceRangeEntity> audienceRange;
	
	@Field("AudienceDescription")
	public String audienceDescription;

	@Field("Complexity")
	public List<ComplexityEntity> complexity;
	
	@Field("Annotation")
	public String annotation;
	
	@Field("MainDescription")
	public String mainDescription;

	@Field("OtherText")
	public List<OtherTextEntity> otherText;

	@Field("ReviewQuote")
	public List<String> reviewQuote;
	
	@Field("CoverImageFormatCode")
	public String coverImageFormatCode;
	
	@Field("CoverImageLinkTypeCode")
	public String coverImageLinkTypeCode;
	
	@Field("CoverImageLink")
	public String coverImageLink;

	@Field("MediaFile")
	public List<MediaFileEntity> mediaFile;
	
	@Field("ProductWebsite")
	public List<ProductWebsiteEntity> productWebsite;
	
	@Field("PrizesDescription")
	public String prizesDescription;

	@Field("Prize")
	public List<PrizeEntity> prize;
	
	@Field("ContentItem")
	public List<ContentItemEntity> contentItem;
	
	@Field("Imprint")
	public List<ImprintEntity> imprint;
	
	@Field("Publisher")
	public List<PublisherEntity> publisher;

	@Field("CityOfPublication")
	public String cityOfPublication;

	@Field("CountryOfPublication")
	public String countryOfPublication;

	@Field("CopublisherName")
	public List<String> copublisherName;

	@Field("SponsorName")
	public List<String> sponsorName;
	
	@Field("OriginalPublisher")
	public String originalPublisher;

	@Field("PublishingStatus")
	public String publishingStatus;
	
	@Field("PublishingStatusNote")
	public String publishingStatusNote;
	
	@Field("AnnouncementDate")
	public String announcementDate;
	
	@Field("TradeAnnouncementDate")
	public String tradeAnnouncementDate;
	
	@Field("PublicationDate")
	public String publicationDate;

	@Field("CopyrightStatement")
	public List<CopyrightStatementEntity> copyrightStatement;
	
	@Field("CopyrightYear")
	public String copyrightYear;
	
	@Field("YearFirstPublished")
	public String yearFirstPublished;
	
	@Field("SalesRights")
	public List<SalesRightsEntity> salesRights;

	@Field("NotForSale")
	public List<NotForSaleEntity> notForSale;

	@Field("SalesRestriction")
	public List<SalesRestrictionEntity> salesRestriction;
	
	@Field("Height")
	public String height;
	
	@Field("Width")
	public String width;
	
	@Field("Thickness")
	public String thickness;
	
	@Field("Weight")
	public String weight;
	
	@Field("Measure")
	public List<MeasureEntity> measure;
	
	@Field("Dimensions")
	public String dimensions;
	
	@Field("ReplacedByISBN")
	public String replacedByISBN;
	
	@Field("ReplacedByEAN13")
	public String replacedByEAN13;
	
	@Field("AlternativeFormatISBN")
	public String alternativeFormatISBN;
	
	@Field("AlternativeFormatEAN13")
	public String alternativeFormatEAN13;
	
	@Field("AlternativeProductISBN")
	public String alternativeProductISBN;
	
	@Field("AlternativeProductEAN13")
	public String alternativeProductEAN13;

	@Field("RelatedProduct")
	public List<RelatedProductEntity> relatedProduct;
	
	@Field("OutOfPrintDate")
	public String outOfPrintDate;

	@Field("SupplyDetail")
	public List<SupplyDetailEntity> supplyDetail;

	/*public List<MarketRepresentation> MarketRepresentation;*/
	
	@Field("PromotionCampaign")
	public String promotionCampaign;
	
	@Field("PromotionContact")
	public String promotionContact;
	
	@Field("InitialPrintRun")
	public String initialPrintRun;

	/*public List<ReprintDetail> ReprintDetail;*/
	
	@Field("CopiesSold")
	public String copiesSold;
	
	@Field("BookClubAdoption")
	public String bookClubAdoption;

	@Field("EditionType")
	public String editionType;

	@Field("ProductForm")
	public String productForm;
	
	@Field("AudienceCode")
	public List<String> audienceCode;
	
	@Field("MainTitle")
	public String mainTitle;
	
	@Field("NumberWithinSeries")
	public String numberWithinSeries;
	
	@Field("SeriesIdentifier")
	public List<SeriesIdentifierEntity> seriesIdentifier;
	
	@Field("BarcodeIndicator")
	public String barcodeIndicator;
	
	@Field("UsageType")
	public String usageType;
	
	@Field("BisacReturnableIndicator")
	public String bisacReturnableIndicator;
	
	@Field("LSIPublisherID")
	public String lsiPublisherID;
	
	/*public String preflightStatus;*/
	
	@Field("LastModifiedOn")
	public Date lastModifiedOn;
	
	@Field("LastModifiedBy")
	public String lastModifiedBy;
	
	@Field("isDeleted")
	public boolean isDeleted;

	public String getRecordReference() {
		return recordReference;
	}

	public void setRecordReference(String recordReference) {
		this.recordReference = recordReference;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getDeletionCode() {
		return deletionCode;
	}

	public void setDeletionCode(String deletionCode) {
		this.deletionCode = deletionCode;
	}

	public String getDeletionText() {
		return deletionText;
	}

	public void setDeletionText(String deletionText) {
		this.deletionText = deletionText;
	}

	public String getRecordSourceType() {
		return recordSourceType;
	}

	public void setRecordSourceType(String recordSourceType) {
		this.recordSourceType = recordSourceType;
	}

	public String getRecordSourceIdentifierType() {
		return recordSourceIdentifierType;
	}

	public void setRecordSourceIdentifierType(String recordSourceIdentifierType) {
		this.recordSourceIdentifierType = recordSourceIdentifierType;
	}

	public String getRecordSourceIdentifier() {
		return recordSourceIdentifier;
	}

	public void setRecordSourceIdentifier(String recordSourceIdentifier) {
		this.recordSourceIdentifier = recordSourceIdentifier;
	}

	public String getRecordSourceName() {
		return recordSourceName;
	}

	public void setRecordSourceName(String recordSourceName) {
		this.recordSourceName = recordSourceName;
	}

	public String getiSBN() {
		return iSBN;
	}

	public void setiSBN(String iSBN) {
		this.iSBN = iSBN;
	}

	public String geteAN13() {
		return eAN13;
	}

	public void seteAN13(String eAN13) {
		this.eAN13 = eAN13;
	}

	public String getuPC() {
		return uPC;
	}

	public void setuPC(String uPC) {
		this.uPC = uPC;
	}

	public String getPublisherProductNo() {
		return publisherProductNo;
	}

	public void setPublisherProductNo(String publisherProductNo) {
		this.publisherProductNo = publisherProductNo;
	}

	public String getiSMN() {
		return iSMN;
	}

	public void setiSMN(String iSMN) {
		this.iSMN = iSMN;
	}

	public String getdOI() {
		return dOI;
	}

	public void setdOI(String dOI) {
		this.dOI = dOI;
	}

	public List<ProductIdentifierEntity> getProductIdentifier() {
		return productIdentifier;
	}

	public void setProductIdentifier(List<ProductIdentifierEntity> productIdentifier) {
		this.productIdentifier = productIdentifier;
	}

	public String getReplacesISBN() {
		return replacesISBN;
	}

	public void setReplacesISBN(String replacesISBN) {
		this.replacesISBN = replacesISBN;
	}

	public String getReplacesEAN13() {
		return replacesEAN13;
	}

	public void setReplacesEAN13(String replacesEAN13) {
		this.replacesEAN13 = replacesEAN13;
	}

	public List<String> getProductFormDetail() {
		return productFormDetail;
	}

	public void setProductFormDetail(List<String> productFormDetail) {
		this.productFormDetail = productFormDetail;
	}

	public List<ProductFormFeatureEntity> getProductFormFeature() {
		return ProductFormFeature;
	}

	public void setProductFormFeature(List<ProductFormFeatureEntity> productFormFeature) {
		ProductFormFeature = productFormFeature;
	}

	public List<String> getBookFormDetail() {
		return BookFormDetail;
	}

	public void setBookFormDetail(List<String> bookFormDetail) {
		BookFormDetail = bookFormDetail;
	}

	public String getProductPackaging() {
		return ProductPackaging;
	}

	public void setProductPackaging(String productPackaging) {
		ProductPackaging = productPackaging;
	}

	public String getProductFormDescription() {
		return productFormDescription;
	}

	public void setProductFormDescription(String productFormDescription) {
		this.productFormDescription = productFormDescription;
	}

	public String getNumberOfPieces() {
		return numberOfPieces;
	}

	public void setNumberOfPieces(String numberOfPieces) {
		this.numberOfPieces = numberOfPieces;
	}

	public String getTradeCategory() {
		return tradeCategory;
	}

	public void setTradeCategory(String tradeCategory) {
		this.tradeCategory = tradeCategory;
	}

	public List<String> getProductContentType() {
		return productContentType;
	}

	public void setProductContentType(List<String> productContentType) {
		this.productContentType = productContentType;
	}

	public List<ProductClassificationEntity> getProductClassification() {
		return productClassification;
	}

	public void setProductClassification(List<ProductClassificationEntity> productClassification) {
		this.productClassification = productClassification;
	}

	public String getePubType() {
		return ePubType;
	}

	public void setePubType(String ePubType) {
		this.ePubType = ePubType;
	}

	public String getePubTypeVersion() {
		return ePubTypeVersion;
	}

	public void setePubTypeVersion(String ePubTypeVersion) {
		this.ePubTypeVersion = ePubTypeVersion;
	}

	public String getePubTypeDescription() {
		return ePubTypeDescription;
	}

	public void setePubTypeDescription(String ePubTypeDescription) {
		this.ePubTypeDescription = ePubTypeDescription;
	}

	public String getePubFormat() {
		return ePubFormat;
	}

	public void setePubFormat(String ePubFormat) {
		this.ePubFormat = ePubFormat;
	}

	public String getePubFormatVersion() {
		return ePubFormatVersion;
	}

	public void setePubFormatVersion(String ePubFormatVersion) {
		this.ePubFormatVersion = ePubFormatVersion;
	}

	public String getePubFormatDescription() {
		return ePubFormatDescription;
	}

	public void setePubFormatDescription(String ePubFormatDescription) {
		this.ePubFormatDescription = ePubFormatDescription;
	}

	public String getePubSource() {
		return ePubSource;
	}

	public void setePubSource(String ePubSource) {
		this.ePubSource = ePubSource;
	}

	public String getePubSourceVersion() {
		return ePubSourceVersion;
	}

	public void setePubSourceVersion(String ePubSourceVersion) {
		this.ePubSourceVersion = ePubSourceVersion;
	}

	public String getePubSourceDescription() {
		return ePubSourceDescription;
	}

	public void setePubSourceDescription(String ePubSourceDescription) {
		this.ePubSourceDescription = ePubSourceDescription;
	}

	public String getePubTypeNote() {
		return ePubTypeNote;
	}

	public void setePubTypeNote(String ePubTypeNote) {
		this.ePubTypeNote = ePubTypeNote;
	}

	public List<SeriesEntity> getSeries() {
		return series;
	}

	public void setSeries(List<SeriesEntity> series) {
		this.series = series;
	}

	public String getNoSeries() {
		return noSeries;
	}

	public void setNoSeries(String noSeries) {
		this.noSeries = noSeries;
	}

	public List<SetEntity> getSet() {
		return set;
	}

	public void setSet(List<SetEntity> set) {
		this.set = set;
	}

	public String getTextCaseFlag() {
		return textCaseFlag;
	}

	public void setTextCaseFlag(String textCaseFlag) {
		this.textCaseFlag = textCaseFlag;
	}

	public String getDistinctiveTitle() {
		return distinctiveTitle;
	}

	public void setDistinctiveTitle(String distinctiveTitle) {
		this.distinctiveTitle = distinctiveTitle;
	}

	public String getTitlePrefix() {
		return titlePrefix;
	}

	public void setTitlePrefix(String titlePrefix) {
		this.titlePrefix = titlePrefix;
	}

	public String getTitleWithoutPrefix() {
		return titleWithoutPrefix;
	}

	public void setTitleWithoutPrefix(String titleWithoutPrefix) {
		this.titleWithoutPrefix = titleWithoutPrefix;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getTranslationOfTitle() {
		return translationOfTitle;
	}

	public void setTranslationOfTitle(String translationOfTitle) {
		this.translationOfTitle = translationOfTitle;
	}

	public List<String> getFormerTitle() {
		return formerTitle;
	}

	public void setFormerTitle(List<String> formerTitle) {
		this.formerTitle = formerTitle;
	}

	public List<TitleEntity> getTitle() {
		return title;
	}

	public void setTitle(List<TitleEntity> title) {
		this.title = title;
	}

	public List<WorkIdentifierEntity> getWorkIdentifier() {
		return workIdentifier;
	}

	public void setWorkIdentifier(List<WorkIdentifierEntity> workIdentifier) {
		this.workIdentifier = workIdentifier;
	}

	public List<WebsiteEntity> getWebsite() {
		return website;
	}

	public void setWebsite(List<WebsiteEntity> website) {
		this.website = website;
	}

	public String getThesisType() {
		return thesisType;
	}

	public void setThesisType(String thesisType) {
		this.thesisType = thesisType;
	}

	public String getThesisPresentedTo() {
		return thesisPresentedTo;
	}

	public void setThesisPresentedTo(String thesisPresentedTo) {
		this.thesisPresentedTo = thesisPresentedTo;
	}

	public String getThesisYear() {
		return thesisYear;
	}

	public void setThesisYear(String thesisYear) {
		this.thesisYear = thesisYear;
	}

	public List<ContributorEntity> getContributor() {
		return contributor;
	}

	public void setContributor(List<ContributorEntity> contributor) {
		this.contributor = contributor;
	}

	public String getContributorStatement() {
		return contributorStatement;
	}

	public void setContributorStatement(String contributorStatement) {
		this.contributorStatement = contributorStatement;
	}

	public String getNoContributor() {
		return noContributor;
	}

	public void setNoContributor(String noContributor) {
		this.noContributor = noContributor;
	}

	public String getConferenceDescription() {
		return conferenceDescription;
	}

	public void setConferenceDescription(String conferenceDescription) {
		this.conferenceDescription = conferenceDescription;
	}

	public String getConferenceRole() {
		return conferenceRole;
	}

	public void setConferenceRole(String conferenceRole) {
		this.conferenceRole = conferenceRole;
	}

	public String getConferenceName() {
		return conferenceName;
	}

	public void setConferenceName(String conferenceName) {
		this.conferenceName = conferenceName;
	}

	public String getConferenceNumber() {
		return conferenceNumber;
	}

	public void setConferenceNumber(String conferenceNumber) {
		this.conferenceNumber = conferenceNumber;
	}

	public String getConferenceDate() {
		return conferenceDate;
	}

	public void setConferenceDate(String conferenceDate) {
		this.conferenceDate = conferenceDate;
	}

	public String getConferencePlace() {
		return conferencePlace;
	}

	public void setConferencePlace(String conferencePlace) {
		this.conferencePlace = conferencePlace;
	}

	public List<ConferenceEntity> getConference() {
		return conference;
	}

	public void setConference(List<ConferenceEntity> conference) {
		this.conference = conference;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getEditionVersionNumber() {
		return editionVersionNumber;
	}

	public void setEditionVersionNumber(String editionVersionNumber) {
		this.editionVersionNumber = editionVersionNumber;
	}

	public String getEditionStatement() {
		return editionStatement;
	}

	public void setEditionStatement(String editionStatement) {
		this.editionStatement = editionStatement;
	}

	public String getNoEdition() {
		return noEdition;
	}

	public void setNoEdition(String noEdition) {
		this.noEdition = noEdition;
	}

	public String getReligiousText() {
		return religiousText;
	}

	public void setReligiousText(String religiousText) {
		this.religiousText = religiousText;
	}

	public String getOriginalLanguage() {
		return originalLanguage;
	}

	public void setOriginalLanguage(String originalLanguage) {
		this.originalLanguage = originalLanguage;
	}

	public List<LanguageEntity> getLanguage() {
		return language;
	}

	public void setLanguage(List<LanguageEntity> language) {
		this.language = language;
	}

	public String getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(String numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public String getPagesRoman() {
		return pagesRoman;
	}

	public void setPagesRoman(String pagesRoman) {
		this.pagesRoman = pagesRoman;
	}

	public String getPagesArabic() {
		return pagesArabic;
	}

	public void setPagesArabic(String pagesArabic) {
		this.pagesArabic = pagesArabic;
	}

	public List<ExtentEntity> getExtent() {
		return extent;
	}

	public void setExtent(List<ExtentEntity> extent) {
		this.extent = extent;
	}

	public String getNumberOfIllustrations() {
		return numberOfIllustrations;
	}

	public void setNumberOfIllustrations(String numberOfIllustrations) {
		this.numberOfIllustrations = numberOfIllustrations;
	}

	public String getIllustrationsNote() {
		return illustrationsNote;
	}

	public void setIllustrationsNote(String illustrationsNote) {
		this.illustrationsNote = illustrationsNote;
	}

	public List<IllustrationsEntity> getIllustrations() {
		return illustrations;
	}

	public void setIllustrations(List<IllustrationsEntity> illustrations) {
		this.illustrations = illustrations;
	}
	
	public String getBasicMainSubject() {
		return basicMainSubject;
	}

	public void setBasicMainSubject(String basicMainSubject) {
		this.basicMainSubject = basicMainSubject;
	}

	public String getBasicVersion() {
		return basicVersion;
	}

	public void setBasicVersion(String basicVersion) {
		this.basicVersion = basicVersion;
	}

	public String getBicMainSubject() {
		return bicMainSubject;
	}

	public void setBicMainSubject(String bicMainSubject) {
		this.bicMainSubject = bicMainSubject;
	}

	public String getBicVersion() {
		return bicVersion;
	}

	public void setBicVersion(String bicVersion) {
		this.bicVersion = bicVersion;
	}

	public List<MainSubjectEntity> getMainSubject() {
		return mainSubject;
	}

	public void setMainSubject(List<MainSubjectEntity> mainSubject) {
		this.mainSubject = mainSubject;
	}

	public List<SubjectEntity> getSubject() {
		return subject;
	}

	public void setSubject(List<SubjectEntity> subject) {
		this.subject = subject;
	}

	public List<PersonAsSubjectEntity> getPersonAsSubject() {
		return personAsSubject;
	}

	public void setPersonAsSubject(List<PersonAsSubjectEntity> personAsSubject) {
		this.personAsSubject = personAsSubject;
	}

	public List<AudienceEntity> getAudience() {
		return audience;
	}

	public void setAudience(List<AudienceEntity> audience) {
		this.audience = audience;
	}

	public String getuSSchoolGrade() {
		return usSchoolGrade;
	}

	public void setuSSchoolGrade(String uSSchoolGrade) {
		this.usSchoolGrade = uSSchoolGrade;
	}

	public String getInterestAge() {
		return interestAge;
	}

	public void setInterestAge(String interestAge) {
		this.interestAge = interestAge;
	}

	public List<AudienceRangeEntity> getAudienceRange() {
		return audienceRange;
	}

	public void setAudienceRange(List<AudienceRangeEntity> audienceRange) {
		this.audienceRange = audienceRange;
	}

	public String getAudienceDescription() {
		return audienceDescription;
	}

	public void setAudienceDescription(String audienceDescription) {
		this.audienceDescription = audienceDescription;
	}

	public List<ComplexityEntity> getComplexity() {
		return complexity;
	}

	public void setComplexity(List<ComplexityEntity> complexity) {
		this.complexity = complexity;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getMainDescription() {
		return mainDescription;
	}

	public void setMainDescription(String mainDescription) {
		this.mainDescription = mainDescription;
	}

	public List<OtherTextEntity> getOtherText() {
		return otherText;
	}

	public void setOtherText(List<OtherTextEntity> otherText) {
		this.otherText = otherText;
	}

	public List<String> getReviewQuote() {
		return reviewQuote;
	}

	public void setReviewQuote(List<String> reviewQuote) {
		this.reviewQuote = reviewQuote;
	}

	public String getCoverImageFormatCode() {
		return coverImageFormatCode;
	}

	public void setCoverImageFormatCode(String coverImageFormatCode) {
		this.coverImageFormatCode = coverImageFormatCode;
	}

	public String getCoverImageLinkTypeCode() {
		return coverImageLinkTypeCode;
	}

	public void setCoverImageLinkTypeCode(String coverImageLinkTypeCode) {
		this.coverImageLinkTypeCode = coverImageLinkTypeCode;
	}

	public String getCoverImageLink() {
		return coverImageLink;
	}

	public void setCoverImageLink(String coverImageLink) {
		this.coverImageLink = coverImageLink;
	}

	public List<MediaFileEntity> getMediaFile() {
		return mediaFile;
	}

	public void setMediaFile(List<MediaFileEntity> mediaFile) {
		this.mediaFile = mediaFile;
	}

	public List<ProductWebsiteEntity> getProductWebsite() {
		return productWebsite;
	}

	public void setProductWebsite(List<ProductWebsiteEntity> productWebsite) {
		this.productWebsite = productWebsite;
	}

	public String getPrizesDescription() {
		return prizesDescription;
	}

	public void setPrizesDescription(String prizesDescription) {
		this.prizesDescription = prizesDescription;
	}

	public List<PrizeEntity> getPrize() {
		return prize;
	}

	public void setPrize(List<PrizeEntity> prize) {
		this.prize = prize;
	}

	public List<ContentItemEntity> getContentItem() {
		return contentItem;
	}

	public void setContentItem(List<ContentItemEntity> contentItem) {
		this.contentItem = contentItem;
	}

	public List<ImprintEntity> getImprint() {
		return imprint;
	}

	public void setImprint(List<ImprintEntity> imprint) {
		this.imprint = imprint;
	}

	public List<PublisherEntity> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<PublisherEntity> publisher) {
		this.publisher = publisher;
	}

	public String getCityOfPublication() {
		return cityOfPublication;
	}

	public void setCityOfPublication(String cityOfPublication) {
		this.cityOfPublication = cityOfPublication;
	}

	public String getCountryOfPublication() {
		return countryOfPublication;
	}

	public void setCountryOfPublication(String countryOfPublication) {
		this.countryOfPublication = countryOfPublication;
	}

	public List<String> getCopublisherName() {
		return copublisherName;
	}

	public void setCopublisherName(List<String> copublisherName) {
		this.copublisherName = copublisherName;
	}

	public List<String> getSponsorName() {
		return sponsorName;
	}

	public void setSponsorName(List<String> sponsorName) {
		this.sponsorName = sponsorName;
	}

	public String getOriginalPublisher() {
		return originalPublisher;
	}

	public void setOriginalPublisher(String originalPublisher) {
		this.originalPublisher = originalPublisher;
	}

	public String getPublishingStatus() {
		return publishingStatus;
	}

	public void setPublishingStatus(String publishingStatus) {
		this.publishingStatus = publishingStatus;
	}

	public String getPublishingStatusNote() {
		return publishingStatusNote;
	}

	public void setPublishingStatusNote(String publishingStatusNote) {
		this.publishingStatusNote = publishingStatusNote;
	}

	public String getAnnouncementDate() {
		return announcementDate;
	}

	public void setAnnouncementDate(String announcementDate) {
		this.announcementDate = announcementDate;
	}

	public String getTradeAnnouncementDate() {
		return tradeAnnouncementDate;
	}

	public void setTradeAnnouncementDate(String tradeAnnouncementDate) {
		this.tradeAnnouncementDate = tradeAnnouncementDate;
	}

	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	public List<CopyrightStatementEntity> getCopyrightStatement() {
		return copyrightStatement;
	}

	public void setCopyrightStatement(List<CopyrightStatementEntity> copyrightStatement) {
		this.copyrightStatement = copyrightStatement;
	}

	public String getCopyrightYear() {
		return copyrightYear;
	}

	public void setCopyrightYear(String copyrightYear) {
		this.copyrightYear = copyrightYear;
	}

	public String getYearFirstPublished() {
		return yearFirstPublished;
	}

	public void setYearFirstPublished(String yearFirstPublished) {
		this.yearFirstPublished = yearFirstPublished;
	}

	public List<SalesRightsEntity> getSalesRights() {
		return salesRights;
	}

	public void setSalesRights(List<SalesRightsEntity> salesRights) {
		this.salesRights = salesRights;
	}

	public List<NotForSaleEntity> getNotForSale() {
		return notForSale;
	}

	public void setNotForSale(List<NotForSaleEntity> notForSale) {
		this.notForSale = notForSale;
	}

	public List<SalesRestrictionEntity> getSalesRestriction() {
		return salesRestriction;
	}

	public void setSalesRestriction(List<SalesRestrictionEntity> salesRestriction) {
		this.salesRestriction = salesRestriction;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getThickness() {
		return thickness;
	}

	public void setThickness(String thickness) {
		this.thickness = thickness;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public List<MeasureEntity> getMeasure() {
		return measure;
	}

	public void setMeasure(List<MeasureEntity> measure) {
		this.measure = measure;
	}

	public String getDimensions() {
		return dimensions;
	}

	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}

	public String getReplacedByISBN() {
		return replacedByISBN;
	}

	public void setReplacedByISBN(String replacedByISBN) {
		this.replacedByISBN = replacedByISBN;
	}

	public String getReplacedByEAN13() {
		return replacedByEAN13;
	}

	public void setReplacedByEAN13(String replacedByEAN13) {
		this.replacedByEAN13 = replacedByEAN13;
	}

	public String getAlternativeFormatISBN() {
		return alternativeFormatISBN;
	}

	public void setAlternativeFormatISBN(String alternativeFormatISBN) {
		this.alternativeFormatISBN = alternativeFormatISBN;
	}

	public String getAlternativeFormatEAN13() {
		return alternativeFormatEAN13;
	}

	public void setAlternativeFormatEAN13(String alternativeFormatEAN13) {
		this.alternativeFormatEAN13 = alternativeFormatEAN13;
	}

	public String getAlternativeProductISBN() {
		return alternativeProductISBN;
	}

	public void setAlternativeProductISBN(String alternativeProductISBN) {
		this.alternativeProductISBN = alternativeProductISBN;
	}

	public String getAlternativeProductEAN13() {
		return alternativeProductEAN13;
	}

	public void setAlternativeProductEAN13(String alternativeProductEAN13) {
		this.alternativeProductEAN13 = alternativeProductEAN13;
	}

	public List<RelatedProductEntity> getRelatedProduct() {
		return relatedProduct;
	}

	public void setRelatedProduct(List<RelatedProductEntity> relatedProduct) {
		this.relatedProduct = relatedProduct;
	}

	public String getOutOfPrintDate() {
		return outOfPrintDate;
	}

	public void setOutOfPrintDate(String outOfPrintDate) {
		this.outOfPrintDate = outOfPrintDate;
	}

	public List<SupplyDetailEntity> getSupplyDetail() {
		return supplyDetail;
	}

	public void setSupplyDetail(List<SupplyDetailEntity> supplyDetail) {
		this.supplyDetail = supplyDetail;
	}

	public String getPromotionCampaign() {
		return promotionCampaign;
	}

	public void setPromotionCampaign(String promotionCampaign) {
		this.promotionCampaign = promotionCampaign;
	}

	public String getPromotionContact() {
		return promotionContact;
	}

	public void setPromotionContact(String promotionContact) {
		this.promotionContact = promotionContact;
	}

	public String getInitialPrintRun() {
		return initialPrintRun;
	}

	public void setInitialPrintRun(String initialPrintRun) {
		this.initialPrintRun = initialPrintRun;
	}

	public String getCopiesSold() {
		return copiesSold;
	}

	public void setCopiesSold(String copiesSold) {
		this.copiesSold = copiesSold;
	}

	public String getBookClubAdoption() {
		return bookClubAdoption;
	}

	public void setBookClubAdoption(String bookClubAdoption) {
		this.bookClubAdoption = bookClubAdoption;
	}

	public String getEditionType() {
		return editionType;
	}

	public void setEditionType(String editionType) {
		this.editionType = editionType;
	}

	public String getProductForm() {
		return productForm;
	}

	public void setProductForm(String productForm) {
		this.productForm = productForm;
	}


	public List<String> getAudienceCode() {
		return audienceCode;
	}

	public void setAudienceCode(List<String> audienceCode) {
		this.audienceCode = audienceCode;
	}

	public String getMainTitle() {
		return mainTitle;
	}

	public void setMainTitle(String mainTitle) {
		this.mainTitle = mainTitle;
	}

	public String getNumberWithinSeries() {
		return numberWithinSeries;
	}

	public void setNumberWithinSeries(String numberWithinSeries) {
		this.numberWithinSeries = numberWithinSeries;
	}

	public List<SeriesIdentifierEntity> getSeriesIdentifier() {
		return seriesIdentifier;
	}

	public void setSeriesIdentifier(List<SeriesIdentifierEntity> seriesIdentifier) {
		this.seriesIdentifier = seriesIdentifier;
	}

	public String getBarcodeIndicator() {
		return barcodeIndicator;
	}

	public void setBarcodeIndicator(String barcodeIndicator) {
		this.barcodeIndicator = barcodeIndicator;
	}

	public String getUsageType() {
		return usageType;
	}

	public void setUsageType(String usageType) {
		this.usageType = usageType;
	}

	public String getBisacReturnableIndicator() {
		return bisacReturnableIndicator;
	}

	public void setBisacReturnableIndicator(String bisacReturnableIndicator) {
		this.bisacReturnableIndicator = bisacReturnableIndicator;
	}
	
	public String getUsSchoolGrade() {
		return usSchoolGrade;
	}

	public void setUsSchoolGrade(String usSchoolGrade) {
		this.usSchoolGrade = usSchoolGrade;
	}

	public String getLsiPublisherID() {
		return lsiPublisherID;
	}

	public void setLsiPublisherID(String lsiPublisherID) {
		this.lsiPublisherID = lsiPublisherID;
	}

	/*public String getPreflightStatus() {
		return preflightStatus;
	}

	public void setPreflightStatus(String preflightStatus) {
		this.preflightStatus = preflightStatus;
	}*/

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
}
