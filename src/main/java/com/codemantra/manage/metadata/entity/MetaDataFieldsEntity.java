/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
25-07-2017			v1.0       	   	Bharath Prasanna Y V	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class MetaDataFieldsEntity {

	@Field("metaDataFieldId")
	public String metaDataFieldId;

	@Field("metaDataFieldName")
	public String metaDataFieldName;

	@Field("displayOrderNo")
	public String displayOrderNo;
	
	@Field(value="editDisplayOrderNo")
    private int editDisplayOrderNo;

	@Field("groupId")
	public String groupId;
	
	@Field("jsonDbPathList")
	public String jsonDbPathList;
	
	
	@Field("subGroupId")
	public String subGroupId;

	@Field("fieldDisplayName")
	public String fieldDisplayName;

	@Field("fieldDataType")
	public String fieldDataType;
	
	@Field("jsonPath")
	public String jsonPath;
	
	@Field("jsonPathResultIndex")
	public Integer jsonPathResultIndex;

	@Field("lookUp")
	public String lookUp;

	@Field("jsonType")
	public String jsonType;
	
	@Field("permissionGroupField")
	public Boolean permissionGroupField;
	@Field("permissionGroupValue")
	public String permissionGroupValue;
	@Field("referencePath")
	public String referencePath;
	
	@Field("referenceValue")
	public String referenceValue;
	
	@Field("jsonDbPathTransform")
	public String jsonDbPathTransform;
	@Field("jsonDbPathElement")
	public String jsonDbPathElement;
	
	@Field("referenceValuePath")
	public String referenceValuePath;
	
	@Field("minlength")
	public String minlength;
	
	@Field("maxlength")
	public String maxlength;
	
	@Field("isDisplay")
	public Boolean isDisplay;

	@Field("isMandatory")
	public Boolean isMandatory;

	@Field("isActive")
	public Boolean isActive;

	@Field("isDeleted")
	public Boolean isDeleted;
	
	@Field("jsonDbPathCondition")
	private List<JsonDbPathConditionEntity> jsonDbPathCondition;
	
	@Field("productHierarchyId")
	public String productHierarchyId;
	
	@Field("isDefault")
	private String isDefault;
	

	@Field("defaultValue")
	private String defaultValue;

	public String getJsonDbPathTransform() {
		return jsonDbPathTransform;
	}

	public void setJsonDbPathTransform(String jsonDbPathTransform) {
		this.jsonDbPathTransform = jsonDbPathTransform;
	}

	public String getJsonDbPathElement() {
		return jsonDbPathElement;
	}

	public void setJsonDbPathElement(String jsonDbPathElement) {
		this.jsonDbPathElement = jsonDbPathElement;
	}

	public String getProductHierarchyId() {
		return productHierarchyId;
	}

	public void setProductHierarchyId(String productHierarchyId) {
		this.productHierarchyId = productHierarchyId;
	}

	public List<JsonDbPathConditionEntity> getJsonDbPathCondition() {
		return jsonDbPathCondition;
	}

	public void setJsonDbPathCondition(List<JsonDbPathConditionEntity> jsonDbPathCondition) {
		this.jsonDbPathCondition = jsonDbPathCondition;
	}

	public String getMetaDataFieldId() {
		return metaDataFieldId;
	}

	public void setMetaDataFieldId(String metaDataFieldId) {
		this.metaDataFieldId = metaDataFieldId;
	}

	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}

	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}

	public String getJsonDbPathList() {
		return jsonDbPathList;
	}

	public void setJsonDbPathList(String jsonDbPathList) {
		this.jsonDbPathList = jsonDbPathList;
	}

	public String getDisplayOrderNo() {
		return displayOrderNo;
	}

	public void setDisplayOrderNo(String displayOrderNo) {
		this.displayOrderNo = displayOrderNo;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}

	public String getFieldDataType() {
		return fieldDataType;
	}

	public void setFieldDataType(String fieldDataType) {
		this.fieldDataType = fieldDataType;
	}

	public String getJsonPath() {
		return jsonPath;
	}

	public void setJsonPath(String jsonPath) {
		this.jsonPath = jsonPath;
	}

	public String getLookUp() {
		return lookUp;
	}

	public void setLookUp(String lookUp) {
		this.lookUp = lookUp;
	}

	public String getReferencePath() {
		return referencePath;
	}

	public void setReferencePath(String referencePath) {
		this.referencePath = referencePath;
	}

	public String getReferenceValue() {
		return referenceValue;
	}

	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}

	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	public Boolean getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getJsonType() {
		return jsonType;
	}

	public void setJsonType(String jsonType) {
		this.jsonType = jsonType;
	}

	public String getSubGroupId() {
		return subGroupId;
	}

	public void setSubGroupId(String subGroupId) {
		this.subGroupId = subGroupId;
	}

	public String getReferenceValuePath() {
		return referenceValuePath;
	}

	public void setReferenceValuePath(String referenceValuePath) {
		this.referenceValuePath = referenceValuePath;
	}

	public String getMinlength() {
		return minlength;
	}

	public void setMinlength(String minlength) {
		this.minlength = minlength;
	}

	public String getMaxlength() {
		return maxlength;
	}

	public void setMaxlength(String maxlength) {
		this.maxlength = maxlength;
	}
	public Boolean getPermissionGroupField() {
		return permissionGroupField;
	}
	public void setPermissionGroupField(Boolean permissionGroupField) {
		this.permissionGroupField = permissionGroupField;
	}
	public String getPermissionGroupValue() {
		return permissionGroupValue;
	}
	public void setPermissionGroupValue(String permissionGroupValue) {
		this.permissionGroupValue = permissionGroupValue;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Integer getJsonPathResultIndex() {
		return jsonPathResultIndex;
	}

	public void setJsonPathResultIndex(Integer jsonPathResultIndex) {
		this.jsonPathResultIndex = jsonPathResultIndex;
	}

	public int getEditDisplayOrderNo() {
		return editDisplayOrderNo;
	}

	public void setEditDisplayOrderNo(int editDisplayOrderNo) {
		this.editDisplayOrderNo = editDisplayOrderNo;
	}


}
