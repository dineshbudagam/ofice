/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
25-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class MetaDataGroupEntity {
	
	@Field("groupId")
	public String groupId;
	
	@Field("subGroupId")
	public String subGroupId;
		
	@Field("groupDisplayName")
	public String groupDisplayName;
	
	@Field("groupName")
	public String groupName;
	
	@Field("groupDisplayOrder")
	public String groupDisplayOrder;
	
	@Field("isDisplay")
	public Boolean isDisplay;
	
	@Field("isActive")
	public Boolean isActive;
	
	@Field("isDeleted")
	public Boolean isDeleted;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getSubGroupId() {
		return subGroupId;
	}

	public void setSubGroupId(String subGroupId) {
		this.subGroupId = subGroupId;
	}

	public String getGroupDisplayName() {
		return groupDisplayName;
	}

	public void setGroupDisplayName(String groupDisplayName) {
		this.groupDisplayName = groupDisplayName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDisplayOrder() {
		return groupDisplayOrder;
	}

	public void setGroupDisplayOrder(String groupDisplayOrder) {
		this.groupDisplayOrder = groupDisplayOrder;
	}

	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
