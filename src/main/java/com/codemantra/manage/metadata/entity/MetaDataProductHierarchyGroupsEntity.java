package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class MetaDataProductHierarchyGroupsEntity {
	
	@Field(value="groupType")
    private String groupType;
    @Field(value="productHierarchyId")
    private String productHirearchyId;
    @Field(value="groupId")
    private String groupId;
    @Field(value="isActive")
    private Boolean isActive;
    @Field(value="createdOn")
    private String createdOn;
    @Field(value="isDisplay")
    private Boolean isDisplay;
    @Field(value="groupDisplayName")
    private String groupDisplayName;
    @Field(value="groupName")
    private String groupName;
    @Field(value="modifiedOn")
    private String modifiedOn;
    @Field(value="isDeleted")
    private Boolean isDeleted;
    @Field(value="createdBy")
    private String createdBy;
    @Field(value="subGroupId")
    private String subGroupId;
    @Field(value="modifiedBy")
    private String modifiedBy;
    
    @Field(value="groupDisplayOrder")
    private String groupDisplayOrder;
    @Field(value="stageId")
    private String stageId;

    public String getGroupType ()
    {
        return groupType;
    }

    public void setGroupType (String groupType)
    {
        this.groupType = groupType;
    }

    public String getProductHirearchyId ()
    {
        return productHirearchyId;
    }

    public void setProductHirearchyId (String productHirearchyId)
    {
        this.productHirearchyId = productHirearchyId;
    }

    public String getGroupId ()
    {
        return groupId;
    }

    public void setGroupId (String groupId)
    {
        this.groupId = groupId;
    }

    public Boolean getIsActive ()
    {
        return isActive;
    }

    public void setIsActive (Boolean isActive)
    {
        this.isActive = isActive;
    }

    public String getCreatedOn ()
    {
        return createdOn;
    }

    public void setCreatedOn (String createdOn)
    {
        this.createdOn = createdOn;
    }

    public Boolean getIsDisplay ()
    {
        return isDisplay;
    }

    public void setIsDisplay (Boolean isDisplay)
    {
        this.isDisplay = isDisplay;
    }

    public String getGroupDisplayName ()
    {
        return groupDisplayName;
    }

    public void setGroupDisplayName (String groupDisplayName)
    {
        this.groupDisplayName = groupDisplayName;
    }

    public String getGroupName ()
    {
        return groupName;
    }

    public void setGroupName (String groupName)
    {
        this.groupName = groupName;
    }

    public String getModifiedOn ()
    {
        return modifiedOn;
    }

    public void setModifiedOn (String modifiedOn)
    {
        this.modifiedOn = modifiedOn;
    }

    public Boolean getIsDeleted ()
    {
        return isDeleted;
    }

    public void setIsDeleted (Boolean isDeleted)
    {
        this.isDeleted = isDeleted;
    }

    public String getCreatedBy ()
    {
        return createdBy;
    }

    public void setCreatedBy (String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getSubGroupId ()
    {
        return subGroupId;
    }

    public void setSubGroupId (String subGroupId)
    {
        this.subGroupId = subGroupId;
    }

    public String getModifiedBy ()
    {
        return modifiedBy;
    }

    public void setModifiedBy (String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }


    public String getGroupDisplayOrder ()
    {
        return groupDisplayOrder;
    }

    public void setGroupDisplayOrder (String groupDisplayOrder)
    {
        this.groupDisplayOrder = groupDisplayOrder;
    }

    public String getStageId ()
    {
        return stageId;
    }

    public void setStageId (String stageId)
    {
        this.stageId = stageId;
    }


}
