package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;
public class MetaDataProductHierarchyStageEntity {
	
	@Field(value="stageId")
	private String stageId;
	
	@Field(value="stageDisplayName")
    private String stageDisplayName;
	
	@Field(value="productHierarchyId")
    private String productHierarchyId;
	
	@Field(value="stageName")
    private String stageName;
	
	@Field(value="stageDisplayOrder")
	private String stageDisplayOrder;
	
	@Field(value="approvalMethod")
	private String approvalMethod;
	
	@Field(value="isDisplay")
    private Boolean isDisplay;
	
	@Field(value="isActive")
    private Boolean isActive;
	
	@Field(value="modifiedBy")
    private String modifiedBy;

	@Field(value="createdBy")
    private String createdBy;
	
	@Field(value="createdOn")
    private String createdOn;
	
	
	@Field(value="modifiedOn")
    private String modifiedOn;
	
	@Field(value="isDeleted")
    private Boolean isDeleted;
	    

    public String getStageDisplayOrder ()
    {
        return stageDisplayOrder;
    }

    public void setStageDisplayOrder (String stageDisplayOrder)
    {
        this.stageDisplayOrder = stageDisplayOrder;
    }


    public Boolean getIsActive ()
    {
        return isActive;
    }

    public void setIsActive (Boolean isActive)
    {
        this.isActive = isActive;
    }

    public String getCreatedOn ()
    {
        return createdOn;
    }

    public void setCreatedOn (String createdOn)
    {
        this.createdOn = createdOn;
    }

    public Boolean getIsDisplay ()
    {
        return isDisplay;
    }

    public void setIsDisplay (Boolean isDisplay)
    {
        this.isDisplay = isDisplay;
    }

    public String getStageDisplayName ()
    {
        return stageDisplayName;
    }

    public void setStageDisplayName (String stageDisplayName)
    {
        this.stageDisplayName = stageDisplayName;
    }

    public String getModifiedOn ()
    {
        return modifiedOn;
    }

    public void setModifiedOn (String modifiedOn)
    {
        this.modifiedOn = modifiedOn;
    }

    public String getStageName ()
    {
        return stageName;
    }

    public void setStageName (String stageName)
    {
        this.stageName = stageName;
    }

    public Boolean getIsDeleted ()
    {
        return isDeleted;
    }

    public void setIsDeleted (Boolean isDeleted)
    {
        this.isDeleted = isDeleted;
    }

    public String getCreatedBy ()
    {
        return createdBy;
    }

    public void setCreatedBy (String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getModifiedBy ()
    {
        return modifiedBy;
    }

    public void setModifiedBy (String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    public String getStageId ()
    {
        return stageId;
    }

    public void setStageId (String stageId)
    {
        this.stageId = stageId;
    }

	public String getProductHierarchyId() {
		return productHierarchyId;
	}

	public void setProductHierarchyId(String productHierarchyId) {
		this.productHierarchyId = productHierarchyId;
	}

	public String getApprovalMethod() {
		return approvalMethod;
	}

	public void setApprovalMethod(String approvalMethod) {
		this.approvalMethod = approvalMethod;
	}

}
