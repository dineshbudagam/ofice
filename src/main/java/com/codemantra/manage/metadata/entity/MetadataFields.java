package com.codemantra.manage.metadata.entity;

import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="cMetadataFields")
public class MetadataFields {


	@Field("_id")
	
	private String id;
	
	@Field("metaDataFieldId")
	
	private String metaDataFieldId;
	
	@Field("fieldDisplayName")
	
	private String fieldDisplayName;
	
	@Field("metaDataFieldName")
	
	private String metaDataFieldName;
	
	@Field("jsonPath")
	
	private String jsonPath;
	
	@Field("displayOrderNo")
	
	private String displayOrderNo;
	
	@Field("groupId")
	
	private String groupId;
	
	@Field("jsonDbPathCondition")
	
	private List<JsonDbPathCondition> jsonDbPathCondition = null;
	
	@Field("stageId")
	
	private String stageId;
	
	@Field("subGroupId")
	
	private String subGroupId;
	
	@Field("fieldDataType")
	
	private Object fieldDataType;
	
	@Field("jsonType")
	
	private String jsonType;
	
	@Field("referencePath")
	
	private String referencePath;
	
	@Field("permissionGroupField")
	
	private Boolean permissionGroupField;
	
	@Field("permissionGroupValue")
	
	private Object permissionGroupValue;
	
	@Field("isMandatory")
	
	private Boolean isMandatory;
	
	@Field("isDisplay")
	
	private Boolean isDisplay;
	
	@Field("isActive")
	
	private Boolean isActive;
	
	@Field("isDeleted")
	
	private Boolean isDeleted;
	
	
	@Field("createdBy")
	
	private String createdBy;
	
	@Field("createdOn")
	
	private Object createdOn;
	
	@Field("modifiedBy")
	
	private Object modifiedBy;
	
	@Field("modifiedOn")
	
	private Object modifiedOn;
	
	@Field("lookUp")
	
	private Object lookUp;
	
	@Field("referenceValuePath")
	
	private Object referenceValuePath;
	
	@Field("referenceValue")
	
	private Object referenceValue;
	
	@Field("minlength")
	
	private Object minlength;
	
	@Field("maxlength")
	
	private Object maxlength;
	
	@Field("productHierarchyId")
	
	private String productHierarchyId;
	
	@Field("createProductHierarchyId")
	
	private List<String> createProductHierarchyId = null;
	
	@Field("editProductHierarchyId")
	
	private List<String> editProductHierarchyId = null;
	
	@Field("isDefault")
	private String isDefault;
	

	@Field("defaultValue")
	private String defaultValue;
	
	@Field("placeHolder")
	private String placeHolder;
	
	public String getPlaceHolder() {
		return placeHolder;
	}

	public void setPlaceHolder(String placeHolder) {
		this.placeHolder = placeHolder;
	}

	public String getId() {
	return id;
	}
	
	public void setId(String id) {
	this.id = id;
	}
	
	public String getMetaDataFieldId() {
	return metaDataFieldId;
	}
	
	public void setMetaDataFieldId(String metaDataFieldId) {
	this.metaDataFieldId = metaDataFieldId;
	}
	
	public String getFieldDisplayName() {
	return fieldDisplayName;
	}
	
	public void setFieldDisplayName(String fieldDisplayName) {
	this.fieldDisplayName = fieldDisplayName;
	}
	
	public String getMetaDataFieldName() {
	return metaDataFieldName;
	}
	
	public void setMetaDataFieldName(String metaDataFieldName) {
	this.metaDataFieldName = metaDataFieldName;
	}
	
	public String getJsonPath() {
	return jsonPath;
	}
	
	public void setJsonPath(String jsonPath) {
	this.jsonPath = jsonPath;
	}
	
	public String getDisplayOrderNo() {
	return displayOrderNo;
	}
	
	public void setDisplayOrderNo(String displayOrderNo) {
	this.displayOrderNo = displayOrderNo;
	}
	
	public String getGroupId() {
	return groupId;
	}
	
	public void setGroupId(String groupId) {
	this.groupId = groupId;
	}
	
	public List<JsonDbPathCondition> getJsonDbPathCondition() {
	return jsonDbPathCondition;
	}
	
	public void setJsonDbPathCondition(List<JsonDbPathCondition> jsonDbPathCondition) {
	this.jsonDbPathCondition = jsonDbPathCondition;
	}
	
	public String getStageId() {
	return stageId;
	}
	
	public void setStageId(String stageId) {
	this.stageId = stageId;
	}
	
	public String getSubGroupId() {
	return subGroupId;
	}
	
	public void setSubGroupId(String subGroupId) {
	this.subGroupId = subGroupId;
	}
	
	public Object getFieldDataType() {
	return fieldDataType;
	}
	
	public void setFieldDataType(Object fieldDataType) {
	this.fieldDataType = fieldDataType;
	}
	
	public String getJsonType() {
	return jsonType;
	}
	
	public void setJsonType(String jsonType) {
	this.jsonType = jsonType;
	}
	
	public String getReferencePath() {
	return referencePath;
	}
	
	public void setReferencePath(String referencePath) {
	this.referencePath = referencePath;
	}
	
	public Boolean getPermissionGroupField() {
	return permissionGroupField;
	}
	
	public void setPermissionGroupField(Boolean permissionGroupField) {
	this.permissionGroupField = permissionGroupField;
	}
	
	public Object getPermissionGroupValue() {
	return permissionGroupValue;
	}
	
	public void setPermissionGroupValue(Object permissionGroupValue) {
	this.permissionGroupValue = permissionGroupValue;
	}
	
	public Boolean getIsMandatory() {
	return isMandatory;
	}
	
	public void setIsMandatory(Boolean isMandatory) {
	this.isMandatory = isMandatory;
	}
	
	public Boolean getIsDisplay() {
	return isDisplay;
	}
	
	public void setIsDisplay(Boolean isDisplay) {
	this.isDisplay = isDisplay;
	}
	
	public Boolean getIsActive() {
	return isActive;
	}
	
	public void setIsActive(Boolean isActive) {
	this.isActive = isActive;
	}
	
	public Boolean getIsDeleted() {
	return isDeleted;
	}
	
	public void setIsDeleted(Boolean isDeleted) {
	this.isDeleted = isDeleted;
	}
	
	public String getCreatedBy() {
	return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
	}
	
	public Object getCreatedOn() {
	return createdOn;
	}
	
	public void setCreatedOn(Object createdOn) {
	this.createdOn = createdOn;
	}
	
	public Object getModifiedBy() {
	return modifiedBy;
	}
	
	public void setModifiedBy(Object modifiedBy) {
	this.modifiedBy = modifiedBy;
	}
	
	public Object getModifiedOn() {
	return modifiedOn;
	}
	
	public void setModifiedOn(Object modifiedOn) {
	this.modifiedOn = modifiedOn;
	}
	
	public Object getLookUp() {
	return lookUp;
	}
	
	public void setLookUp(Object lookUp) {
	this.lookUp = lookUp;
	}
	
	public Object getReferenceValuePath() {
	return referenceValuePath;
	}
	
	public void setReferenceValuePath(Object referenceValuePath) {
	this.referenceValuePath = referenceValuePath;
	}
	
	public Object getReferenceValue() {
	return referenceValue;
	}
	
	public void setReferenceValue(Object referenceValue) {
	this.referenceValue = referenceValue;
	}
	
	public Object getMinlength() {
	return minlength;
	}
	
	public void setMinlength(Object minlength) {
	this.minlength = minlength;
	}
	
	public Object getMaxlength() {
	return maxlength;
	}
	
	public void setMaxlength(Object maxlength) {
	this.maxlength = maxlength;
	}
	
	public String getProductHierarchyId() {
	return productHierarchyId;
	}
	
	public void setProductHierarchyId(String productHierarchyId) {
	this.productHierarchyId = productHierarchyId;
	}
	
	public List<String> getCreateProductHierarchyId() {
	return createProductHierarchyId;
	}
	
	public void setCreateProductHierarchyId(List<String> createProductHierarchyId) {
	this.createProductHierarchyId = createProductHierarchyId;
	}
	
	public List<String> getEditProductHierarchyId() {
	return editProductHierarchyId;
	}
	
	public void setEditProductHierarchyId(List<String> editProductHierarchyId) {
	this.editProductHierarchyId = editProductHierarchyId;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

}