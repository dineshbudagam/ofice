/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;


public class NotForSaleEntity
{
	@Field("RightsCountry")
	public List<String> rightsCountry;

	@Field("RightsTerritory")
	public List<String> rightsTerritory;

	@Field("ISBN")
	public String isbn;

	@Field("EAN13")
	public String ean13;
	
	@Field("ProductIdentifier")
	public List<ProductIdentifierEntity> productIdentifier;

	@Field("PublisherName")
	public String publisherName;

	public List<String> getRightsCountry() {
		return rightsCountry;
	}

	public void setRightsCountry(List<String> rightsCountry) {
		this.rightsCountry = rightsCountry;
	}

	public List<String> getRightsTerritory() {
		return rightsTerritory;
	}

	public void setRightsTerritory(List<String> rightsTerritory) {
		this.rightsTerritory = rightsTerritory;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getEan13() {
		return ean13;
	}

	public void setEan13(String ean13) {
		this.ean13 = ean13;
	}

	public List<ProductIdentifierEntity> getProductIdentifier() {
		return productIdentifier;
	}

	public void setProductIdentifier(List<ProductIdentifierEntity> productIdentifier) {
		this.productIdentifier = productIdentifier;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

}
