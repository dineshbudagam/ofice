/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class OtherTextEntity
{
	@Field("TextTypeCode")
	public String textTypeCode;

	@Field("TextFormat")
	public String textFormat;

	@Field("Text")
	public String text;

	@Field("TextLinkType")
	public String textLinkType;

	@Field("TextLink")
	public String textLink;

	@Field("TextAuthor")
	public String textAuthor;

	@Field("TextSourceCorporate")
	public String textSourceCorporate;

	@Field("TextSourceTitle")
	public String textSourceTitle;

	@Field("TextPublicationDate")
	public String textPublicationDate;

	@Field("StartDate")
	public String startDate;

	@Field("EndDate")
	public String endDate;

	public String getTextTypeCode() {
		return textTypeCode;
	}

	public void setTextTypeCode(String textTypeCode) {
		this.textTypeCode = textTypeCode;
	}

	public String getTextFormat() {
		return textFormat;
	}

	public void setTextFormat(String textFormat) {
		this.textFormat = textFormat;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTextLinkType() {
		return textLinkType;
	}

	public void setTextLinkType(String textLinkType) {
		this.textLinkType = textLinkType;
	}

	public String getTextLink() {
		return textLink;
	}

	public void setTextLink(String textLink) {
		this.textLink = textLink;
	}

	public String getTextAuthor() {
		return textAuthor;
	}

	public void setTextAuthor(String textAuthor) {
		this.textAuthor = textAuthor;
	}

	public String getTextSourceCorporate() {
		return textSourceCorporate;
	}

	public void setTextSourceCorporate(String textSourceCorporate) {
		this.textSourceCorporate = textSourceCorporate;
	}

	public String getTextSourceTitle() {
		return textSourceTitle;
	}

	public void setTextSourceTitle(String textSourceTitle) {
		this.textSourceTitle = textSourceTitle;
	}

	public String getTextPublicationDate() {
		return textPublicationDate;
	}

	public void setTextPublicationDate(String textPublicationDate) {
		this.textPublicationDate = textPublicationDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
