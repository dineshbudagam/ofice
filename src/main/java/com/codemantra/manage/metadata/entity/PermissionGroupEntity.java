/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mPermissionGroup")
public class PermissionGroupEntity {

	@Id
	private String id;
	
	private String permissionGroupId;
	private String permissionGroupName;
	
	private String roleId;
	
	private List<String> account;
	private List<String> metadataType;
	private List<String> imprint;
	private List<String> productCategory;
	private List<String> metadataGroup;
	private List<String> partnerType;
	private List<String> vendor;
	private List<String> application;
	
	private List<String> report;
	private List<String> administrator;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPermissionGroupId() {
		return permissionGroupId;
	}
	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}
	public String getPermissionGroupName() {
		return permissionGroupName;
	}
	public void setPermissionGroupName(String permissionGroupName) {
		this.permissionGroupName = permissionGroupName;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public List<String> getAccount() {
		return account;
	}
	public void setAccount(List<String> account) {
		this.account = account;
	}
	public List<String> getMetadataType() {
		return metadataType;
	}
	public void setMetadataType(List<String> metadataType) {
		this.metadataType = metadataType;
	}
	public List<String> getImprint() {
		return imprint;
	}
	public void setImprint(List<String> imprint) {
		this.imprint = imprint;
	}
	public List<String> getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(List<String> productCategory) {
		this.productCategory = productCategory;
	}
	public List<String> getMetadataGroup() {
		return metadataGroup;
	}
	public void setMetadataGroup(List<String> metadataGroup) {
		this.metadataGroup = metadataGroup;
	}
	public List<String> getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(List<String> partnerType) {
		this.partnerType = partnerType;
	}
	public List<String> getVendor() {
		return vendor;
	}
	public void setVendor(List<String> vendor) {
		this.vendor = vendor;
	}
	public List<String> getApplication() {
		return application;
	}
	public void setApplication(List<String> application) {
		this.application = application;
	}
	public List<String> getReport() {
		return report;
	}
	public void setReport(List<String> report) {
		this.report = report;
	}
	public List<String> getAdministrator() {
		return administrator;
	}
	public void setAdministrator(List<String> administrator) {
		this.administrator = administrator;
	}
}

