/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;


public class PersonAsSubjectEntity
{
	@Field("PersonName")
	public String personName;

	@Field("PersonNameInverted")
	public String personNameInverted;

	@Field("TitlesBeforeNames")
	public String titlesBeforeNames;

	@Field("NamesBeforeKey")
	public String namesBeforeKey;

	@Field("PrefixToKey")
	public String prefixToKey;

	@Field("KeyNames")
	public String keyNames;

	@Field("NamesAfterKey")
	public String namesAfterKey;

	@Field("SuffixToKey")
	public String suffixToKey;

	@Field("LettersAfterNames")
	public String lettersAfterNames;

	@Field("TitlesAfterNames")
	public String titlesAfterNames;

	@Field("Name")
	public List<String> name;

	@Field("PersonNameIdentifier")
	public List<PersonNameIdentifierEntity> personNameIdentifier;

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPersonNameInverted() {
		return personNameInverted;
	}

	public void setPersonNameInverted(String personNameInverted) {
		this.personNameInverted = personNameInverted;
	}

	public String getTitlesBeforeNames() {
		return titlesBeforeNames;
	}

	public void setTitlesBeforeNames(String titlesBeforeNames) {
		this.titlesBeforeNames = titlesBeforeNames;
	}

	public String getNamesBeforeKey() {
		return namesBeforeKey;
	}

	public void setNamesBeforeKey(String namesBeforeKey) {
		this.namesBeforeKey = namesBeforeKey;
	}

	public String getPrefixToKey() {
		return prefixToKey;
	}

	public void setPrefixToKey(String prefixToKey) {
		this.prefixToKey = prefixToKey;
	}

	public String getKeyNames() {
		return keyNames;
	}

	public void setKeyNames(String keyNames) {
		this.keyNames = keyNames;
	}

	public String getNamesAfterKey() {
		return namesAfterKey;
	}

	public void setNamesAfterKey(String namesAfterKey) {
		this.namesAfterKey = namesAfterKey;
	}

	public String getSuffixToKey() {
		return suffixToKey;
	}

	public void setSuffixToKey(String suffixToKey) {
		this.suffixToKey = suffixToKey;
	}

	public String getLettersAfterNames() {
		return lettersAfterNames;
	}

	public void setLettersAfterNames(String lettersAfterNames) {
		this.lettersAfterNames = lettersAfterNames;
	}

	public String getTitlesAfterNames() {
		return titlesAfterNames;
	}

	public void setTitlesAfterNames(String titlesAfterNames) {
		this.titlesAfterNames = titlesAfterNames;
	}

	public List<String> getName() {
		return name;
	}

	public void setName(List<String> name) {
		this.name = name;
	}

	public List<PersonNameIdentifierEntity> getPersonNameIdentifier() {
		return personNameIdentifier;
	}

	public void setPersonNameIdentifier(List<PersonNameIdentifierEntity> personNameIdentifier) {
		this.personNameIdentifier = personNameIdentifier;
	}
}
