/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class PersonNameIdentifierEntity
{
	@Field("PersonNameIDType")
	public String personNameIDType;
	
	@Field("IDTypeName")
	public String iDTypeName;
	
	@Field("IDValue")
	public String iDValue;

	public String getPersonNameIDType() {
		return personNameIDType;
	}

	public void setPersonNameIDType(String personNameIDType) {
		this.personNameIDType = personNameIDType;
	}

	public String getiDTypeName() {
		return iDTypeName;
	}

	public void setiDTypeName(String iDTypeName) {
		this.iDTypeName = iDTypeName;
	}

	public String getiDValue() {
		return iDValue;
	}

	public void setiDValue(String iDValue) {
		this.iDValue = iDValue;
	}
}
