/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class PriceEntity
{
	@Field("PriceTypeCode")
	public String priceTypeCode;
	
	@Field("PriceAmount")
	public Double priceAmount;
	
	@Field("CurrencyCode")
	public String currencyCode;
	
	@Field("PriceQualifier")
	public String priceQualifier;

	@Field("PriceTypeDescription")
	public String priceTypeDescription;

	@Field("PricePercent")
	public String pricePercent;

	@Field("MinimumOrderQuantity")
	public String minimumOrderQuantity;

	/*public List<BatchBonus> BatchBonus;*/

	@Field("ClassOfTrade")
	public String classOfTrade;

	@Field("BicDiscountGroupCode")
	public String bicDiscountGroupCode;

	@Field("DiscountPercent")
	public Double discountPercent;

	@Field("PriceStatus")
	public String priceStatus;

	@Field("CountryCode")
	public String countryCode;

	@Field("Territory")
	public String territory;

	@Field("CountryExcluded")
	public String countryExcluded;

	@Field("TerritoryExcluded")
	public String territoryExcluded;

	@Field("TaxRateCode1")
	public String taxRateCode1;

	@Field("TaxRatePercent1")
	public Double taxRatePercent1;

	@Field("TaxableAmount1")
	public Double taxableAmount1;

	@Field("TaxAmount1")
	public Double taxAmount1;

	@Field("TaxRateCode2")
	public String taxRateCode2;

	@Field("TaxRatePercent2")
	public Double taxRatePercent2;

	@Field("TaxableAmount2")
	public Double taxableAmount2;

	@Field("TaxAmount2")
	public Double taxAmount2;

	@Field("PriceEffectiveFrom")
	public String priceEffectiveFrom;

	@Field("PriceEffectiveUntil")
	public String priceEffectiveUntil;

	@Field("DiscountCoded")
	public List<DiscountCodedEntity> discountCoded;

	public String getPriceTypeCode() {
		return priceTypeCode;
	}

	public void setPriceTypeCode(String priceTypeCode) {
		this.priceTypeCode = priceTypeCode;
	}

	public Double getPriceAmount() {
		return priceAmount;
	}

	public void setPriceAmount(Double priceAmount) {
		this.priceAmount = priceAmount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPriceQualifier() {
		return priceQualifier;
	}

	public void setPriceQualifier(String priceQualifier) {
		this.priceQualifier = priceQualifier;
	}

	public String getPriceTypeDescription() {
		return priceTypeDescription;
	}

	public void setPriceTypeDescription(String priceTypeDescription) {
		this.priceTypeDescription = priceTypeDescription;
	}

	public String getPricePercent() {
		return pricePercent;
	}

	public void setPricePercent(String pricePercent) {
		this.pricePercent = pricePercent;
	}

	public String getMinimumOrderQuantity() {
		return minimumOrderQuantity;
	}

	public void setMinimumOrderQuantity(String minimumOrderQuantity) {
		this.minimumOrderQuantity = minimumOrderQuantity;
	}

	public String getClassOfTrade() {
		return classOfTrade;
	}

	public void setClassOfTrade(String classOfTrade) {
		this.classOfTrade = classOfTrade;
	}

	public String getBicDiscountGroupCode() {
		return bicDiscountGroupCode;
	}

	public void setBicDiscountGroupCode(String bicDiscountGroupCode) {
		this.bicDiscountGroupCode = bicDiscountGroupCode;
	}

	public Double getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}

	public String getPriceStatus() {
		return priceStatus;
	}

	public void setPriceStatus(String priceStatus) {
		this.priceStatus = priceStatus;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getTerritory() {
		return territory;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public String getCountryExcluded() {
		return countryExcluded;
	}

	public void setCountryExcluded(String countryExcluded) {
		this.countryExcluded = countryExcluded;
	}

	public String getTerritoryExcluded() {
		return territoryExcluded;
	}

	public void setTerritoryExcluded(String territoryExcluded) {
		this.territoryExcluded = territoryExcluded;
	}

	public String getTaxRateCode1() {
		return taxRateCode1;
	}

	public void setTaxRateCode1(String taxRateCode1) {
		this.taxRateCode1 = taxRateCode1;
	}

	public Double getTaxRatePercent1() {
		return taxRatePercent1;
	}

	public void setTaxRatePercent1(Double taxRatePercent1) {
		this.taxRatePercent1 = taxRatePercent1;
	}

	public Double getTaxableAmount1() {
		return taxableAmount1;
	}

	public void setTaxableAmount1(Double taxableAmount1) {
		this.taxableAmount1 = taxableAmount1;
	}

	public Double getTaxAmount1() {
		return taxAmount1;
	}

	public void setTaxAmount1(Double taxAmount1) {
		this.taxAmount1 = taxAmount1;
	}

	public String getTaxRateCode2() {
		return taxRateCode2;
	}

	public void setTaxRateCode2(String taxRateCode2) {
		this.taxRateCode2 = taxRateCode2;
	}

	public Double getTaxRatePercent2() {
		return taxRatePercent2;
	}

	public void setTaxRatePercent2(Double taxRatePercent2) {
		this.taxRatePercent2 = taxRatePercent2;
	}

	public Double getTaxableAmount2() {
		return taxableAmount2;
	}

	public void setTaxableAmount2(Double taxableAmount2) {
		this.taxableAmount2 = taxableAmount2;
	}

	public Double getTaxAmount2() {
		return taxAmount2;
	}

	public void setTaxAmount2(Double taxAmount2) {
		this.taxAmount2 = taxAmount2;
	}

	public String getPriceEffectiveFrom() {
		return priceEffectiveFrom;
	}

	public void setPriceEffectiveFrom(String priceEffectiveFrom) {
		this.priceEffectiveFrom = priceEffectiveFrom;
	}

	public String getPriceEffectiveUntil() {
		return priceEffectiveUntil;
	}

	public void setPriceEffectiveUntil(String priceEffectiveUntil) {
		this.priceEffectiveUntil = priceEffectiveUntil;
	}

	public List<DiscountCodedEntity> getDiscountCoded() {
		return discountCoded;
	}

	public void setDiscountCoded(List<DiscountCodedEntity> discountCoded) {
		this.discountCoded = discountCoded;
	}
				
}