/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class PrizeEntity
{
	@Field("PrizeCode")
	public String prizeCode;

	@Field("PrizeCountry")
	public String prizeCountry;

	@Field("PrizeJurys")
	public List<String> prizeJurys;

	@Field("PrizeNames")
	public List<String> prizeNames;

	@Field("PrizeYear")
	public String prizeYear;

	public String getPrizeCode() {
		return prizeCode;
	}

	public void setPrizeCode(String prizeCode) {
		this.prizeCode = prizeCode;
	}

	public String getPrizeCountry() {
		return prizeCountry;
	}

	public void setPrizeCountry(String prizeCountry) {
		this.prizeCountry = prizeCountry;
	}

	public List<String> getPrizeJurys() {
		return prizeJurys;
	}

	public void setPrizeJurys(List<String> prizeJurys) {
		this.prizeJurys = prizeJurys;
	}

	public List<String> getPrizeNames() {
		return prizeNames;
	}

	public void setPrizeNames(List<String> prizeNames) {
		this.prizeNames = prizeNames;
	}

	public String getPrizeYear() {
		return prizeYear;
	}

	public void setPrizeYear(String prizeYear) {
		this.prizeYear = prizeYear;
	}
			
}
