/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "mProductCategory")
public class ProductCategoryEntity {

	@Field("metadataTypeId")
	private String metadataTypeId;
	
	@Field("productCategoryId")
	private String productCategoryId;
	
	@Field("productCategoryName")
	private String productCategoryName;
	
	@Field("productCategoryCode")
	private String productCategoryCode;
	
	@Field("isActive")
	private boolean isActive;
	
	@Field("isDeleted")
	private boolean isDeleted;
	
	@Field("createdBy")
	private String CreatedBy;
	
	@Field("createdOn")
	private String CreatedOn;
	
	@Field("modifiedBy")
	private String ModifiedBy;
	
	@Field("modifiedOn")
	private String ModifiedOn;

	public String getMetadataTypeId() {
		return metadataTypeId;
	}

	public void setMetadataTypeId(String metadataTypeId) {
		this.metadataTypeId = metadataTypeId;
	}

	public String getProductCategoryId() {
		return productCategoryId;
	}

	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public String getProductCategoryName() {
		return productCategoryName;
	}

	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}

	public String getProductCategoryCode() {
		return productCategoryCode;
	}

	public void setProductCategoryCode(String productCategoryCode) {
		this.productCategoryCode = productCategoryCode;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(String createdOn) {
		CreatedOn = createdOn;
	}

	public String getModifiedBy() {
		return ModifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		ModifiedBy = modifiedBy;
	}

	public String getModifiedOn() {
		return ModifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		ModifiedOn = modifiedOn;
	}

}
