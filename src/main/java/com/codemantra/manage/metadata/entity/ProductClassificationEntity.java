/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class ProductClassificationEntity
{
	@Field("ProductClassificationType")
	public String productClassificationType;

	@Field("Percent")
	public Double percent;

	@Field("ProductClassificationCode")
	public String productClassificationCode;

	public String getProductClassificationType() {
		return productClassificationType;
	}

	public void setProductClassificationType(String productClassificationType) {
		this.productClassificationType = productClassificationType;
	}

	public Double getPercent() {
		return percent;
	}

	public void setPercent(Double percent) {
		this.percent = percent;
	}

	public String getProductClassificationCode() {
		return productClassificationCode;
	}

	public void setProductClassificationCode(String productClassificationCode) {
		this.productClassificationCode = productClassificationCode;
	}

}
