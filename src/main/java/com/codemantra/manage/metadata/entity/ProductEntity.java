/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

public class ProductEntity{
	
	@Field("_id")
	private String Id;
	
	@Field("Product")
	private MetaDataEntity product;
	
	@Field("asset")
	private List<AssetEntity> asset;
	
	@Field("CustomFields")
	private Map<Object, Object> customFields;
	
	
	@Field("metadataId")
	private String metadataId;
	
	public MetaDataEntity getProduct() {
		return product;
	}

	public void setProduct(MetaDataEntity product) {
		this.product = product;
	}

	public List<AssetEntity> getAsset() {
		return asset;
	}

	public void setAsset(List<AssetEntity> asset) {
		this.asset = asset;
	}
	
	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public Map<Object, Object> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(Map<Object, Object> customFields) {
		this.customFields = customFields;
	}

	public String getMetadataId() {
		return metadataId;
	}

	public void setMetadataId(String metadataId) {
		this.metadataId = metadataId;
	}
}
