/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class ProductFormFeatureEntity
{
	@Field("ProductFormFeatureType")
	public String productFormFeatureType;

	@Field("ProductFormFeatureDescription")
	public List<String> productFormFeatureDescription;

	@Field("Product.ProductFormFeatureValue")
	public String productFormFeatureValue;

	public String getProductFormFeatureType() {
		return productFormFeatureType;
	}

	public void setProductFormFeatureType(String productFormFeatureType) {
		this.productFormFeatureType = productFormFeatureType;
	}

	public List<String> getProductFormFeatureDescription() {
		return productFormFeatureDescription;
	}

	public void setProductFormFeatureDescription(List<String> productFormFeatureDescription) {
		this.productFormFeatureDescription = productFormFeatureDescription;
	}

	public String getProductFormFeatureValue() {
		return productFormFeatureValue;
	}

	public void setProductFormFeatureValue(String productFormFeatureValue) {
		this.productFormFeatureValue = productFormFeatureValue;
	}

	
}
