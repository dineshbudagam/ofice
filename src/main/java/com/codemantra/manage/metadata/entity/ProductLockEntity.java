package com.codemantra.manage.metadata.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
* This is an entity representation for <strong>tProductLock</strong> collection.<br /> <br />
* Fields : <br /> 
* <strong>userId</strong> UserId, who is going to lock <br />
* <strong>productIds</strong> : productIds / metadataIds of products for which this lock is to be applied <br />
* <strong>lastLockedOn</strong> : This specifies when this lock was added/ modified <br />	
* <strong>isActive</strong> : An extra field -> to determine this lock is active/not , its default value is true 
* 
* @author Pavan Kumar Yekabote.
* */
@Document(collection="tProductLock")
public class ProductLockEntity {

	public enum LockType{
		LOCKED("locked"),
		SELF_LOCK("self_lock"),
		UNLOCKED("unlocked");
		
		private String value;
		private LockType(String lockName){
			this.value = lockName;
		}
		public String value() {
			return this.value;
		}
	}
	
	@Field("_id")
	Object id;
	
	/** UserId, who is going to lock */
	@Field("userId")
	String userId;
	
	/** productIds / metadataIds of products for which this lock is to be applied */
	@Field("productIds")
	List<String> productIds;
	
	/** This specifies when this lock was added/ modified */	
	@Field("lastLockedOn")
	Date lastLockedOn;
	
	/** An extra field -> to determine this lock is active/not  */
	@Field("isActive")
	boolean isActive = true;

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<String> productIds) {
		this.productIds = productIds;
	}

	public Date getLastLockedOn() {
		return lastLockedOn;
	}

	public void setLastLockedOn(Date lastLockedOn) {
		this.lastLockedOn = lastLockedOn;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	
}
