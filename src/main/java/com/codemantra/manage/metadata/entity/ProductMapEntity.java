package com.codemantra.manage.metadata.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection="products")
public class ProductMapEntity implements Cloneable{
	@Field("_id")
	private String Id;

	@Field("Product")
	private Map<Object, Object> Product;

	@Field("asset")
	private List<AssetEntity> asset;

	@Field("CustomFields")
	private Map<Object, Object> CustomFields;

	@Field("metadataId")
	private String metadataId;

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	
	@JsonProperty("Product")
	public Map<Object, Object> getProduct() {
		return Product;
	}

	public void setProduct(Map<Object, Object> Product) {
		this.Product = Product;
	}

	public List<AssetEntity> getAsset() {
		return asset;
	}

	public void setAsset(List<AssetEntity> asset) {
		this.asset = asset;
	}

	
	@JsonProperty("CustomFields")
	public Map<Object, Object> getCustomFields() {
		return CustomFields;
	}

	public void setCustomFields(Map<Object, Object> CustomFields) {
		this.CustomFields = CustomFields;
	}

	public String getMetadataId() {
		return metadataId;
	}

	public void setMetadataId(String metadataId) {
		this.metadataId = metadataId;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		
		return super.clone();
	}
	
	
	
}
