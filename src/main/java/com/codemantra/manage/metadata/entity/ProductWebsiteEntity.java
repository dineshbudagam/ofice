/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class ProductWebsiteEntity
{
	@Field("WebsiteRole")
	public String websiteRole;

	@Field("ProductWebsiteDescription")
	public String productWebsiteDescription;

	@Field("ProductWebsiteLink")
	public String productWebsiteLink;

	public String getWebsiteRole() {
		return websiteRole;
	}

	public void setWebsiteRole(String websiteRole) {
		this.websiteRole = websiteRole;
	}

	public String getProductWebsiteDescription() {
		return productWebsiteDescription;
	}

	public void setProductWebsiteDescription(String productWebsiteDescription) {
		this.productWebsiteDescription = productWebsiteDescription;
	}

	public String getProductWebsiteLink() {
		return productWebsiteLink;
	}

	public void setProductWebsiteLink(String productWebsiteLink) {
		this.productWebsiteLink = productWebsiteLink;
	}

}
