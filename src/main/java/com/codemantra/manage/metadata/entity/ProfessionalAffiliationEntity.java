/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class ProfessionalAffiliationEntity
{
	@Field("ProfessionalPosition")
	public String professionalPosition;
	
	@Field("Affiliation")
	public String affiliation;

	public String getProfessionalPosition() {
		return professionalPosition;
	}

	public void setProfessionalPosition(String professionalPosition) {
		this.professionalPosition = professionalPosition;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
}
