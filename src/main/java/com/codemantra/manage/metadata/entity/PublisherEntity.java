/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class PublisherEntity
{
	@Field("PublishingRole")
	public String publishingRole;
	
	@Field("PublisherName")
	public String publisherName;
	
	@Field("PublishingStatus")
	public String publishingStatus;

	public String getPublishingRole() {
		return publishingRole;
	}

	public void setPublishingRole(String publishingRole) {
		this.publishingRole = publishingRole;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getPublishingStatus() {
		return publishingStatus;
	}

	public void setPublishingStatus(String publishingStatus) {
		this.publishingStatus = publishingStatus;
	}
}