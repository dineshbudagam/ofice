/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class RelatedProductEntity
{
	@Field("RelationCode")
	public String relationCode;

	@Field("ISBN")
	public String isbn;

	@Field("EAN13")
	public String ean13;
	
	@Field("ProductIdentifier")
	public List<ProductIdentifierEntity> productIdentifier;
	
	@Field("Website")
	public List<WebsiteEntity> website;

	@Field("ProductForm")
	public String productForm;

	@Field("ProductFormDetail")
	public List<String> productFormDetail;
	
	@Field("ProductFormFeature")
	public List<ProductFormFeatureEntity> productFormFeature;

	@Field("BookFormDetail")
	public List<String> bookFormDetail;

	@Field("ProductPackaging")
	public String productPackaging;

	@Field("ProductFormDescription")
	public String productFormDescription;

	@Field("NumberOfPieces")
	public String numberOfPieces;

	@Field("TradeCategory")
	public String tradeCategory;

	@Field("ProductContentType")
	public List<String> productContentType;

	@Field("EpubType")
	public String epubType;

	@Field("EpubTypeVersion")
	public String epubTypeVersion;

	@Field("EpubTypeDescription")
	public String epubTypeDescription;

	@Field("EpubFormat")
	public String epubFormat;

	@Field("EpubFormatVersion")
	public String epubFormatVersion;

	@Field("EpubFormatDescription")
	public String epubFormatDescription;

	@Field("EpubTypeNote")
	public String epubTypeNote;
	
	@Field("Publisher")
	public List<PublisherEntity> publisher;

	public String getRelationCode() {
		return relationCode;
	}

	public void setRelationCode(String relationCode) {
		this.relationCode = relationCode;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getEan13() {
		return ean13;
	}

	public void setEan13(String ean13) {
		this.ean13 = ean13;
	}

	public List<ProductIdentifierEntity> getProductIdentifier() {
		return productIdentifier;
	}

	public void setProductIdentifier(List<ProductIdentifierEntity> productIdentifier) {
		this.productIdentifier = productIdentifier;
	}

	public List<WebsiteEntity> getWebsite() {
		return website;
	}

	public void setWebsite(List<WebsiteEntity> website) {
		this.website = website;
	}

	public String getProductForm() {
		return productForm;
	}

	public void setProductForm(String productForm) {
		this.productForm = productForm;
	}

	public List<String> getProductFormDetail() {
		return productFormDetail;
	}

	public void setProductFormDetail(List<String> productFormDetail) {
		this.productFormDetail = productFormDetail;
	}

	public List<ProductFormFeatureEntity> getProductFormFeature() {
		return productFormFeature;
	}

	public void setProductFormFeature(List<ProductFormFeatureEntity> productFormFeature) {
		this.productFormFeature = productFormFeature;
	}

	public List<String> getBookFormDetail() {
		return bookFormDetail;
	}

	public void setBookFormDetail(List<String> bookFormDetail) {
		this.bookFormDetail = bookFormDetail;
	}

	public String getProductPackaging() {
		return productPackaging;
	}

	public void setProductPackaging(String productPackaging) {
		this.productPackaging = productPackaging;
	}

	public String getProductFormDescription() {
		return productFormDescription;
	}

	public void setProductFormDescription(String productFormDescription) {
		this.productFormDescription = productFormDescription;
	}

	public String getNumberOfPieces() {
		return numberOfPieces;
	}

	public void setNumberOfPieces(String numberOfPieces) {
		this.numberOfPieces = numberOfPieces;
	}

	public String getTradeCategory() {
		return tradeCategory;
	}

	public void setTradeCategory(String tradeCategory) {
		this.tradeCategory = tradeCategory;
	}

	public List<String> getProductContentType() {
		return productContentType;
	}

	public void setProductContentType(List<String> productContentType) {
		this.productContentType = productContentType;
	}

	public String getEpubType() {
		return epubType;
	}

	public void setEpubType(String epubType) {
		this.epubType = epubType;
	}

	public String getEpubTypeVersion() {
		return epubTypeVersion;
	}

	public void setEpubTypeVersion(String epubTypeVersion) {
		this.epubTypeVersion = epubTypeVersion;
	}

	public String getEpubTypeDescription() {
		return epubTypeDescription;
	}

	public void setEpubTypeDescription(String epubTypeDescription) {
		this.epubTypeDescription = epubTypeDescription;
	}

	public String getEpubFormat() {
		return epubFormat;
	}

	public void setEpubFormat(String epubFormat) {
		this.epubFormat = epubFormat;
	}

	public String getEpubFormatVersion() {
		return epubFormatVersion;
	}

	public void setEpubFormatVersion(String epubFormatVersion) {
		this.epubFormatVersion = epubFormatVersion;
	}

	public String getEpubFormatDescription() {
		return epubFormatDescription;
	}

	public void setEpubFormatDescription(String epubFormatDescription) {
		this.epubFormatDescription = epubFormatDescription;
	}

	public String getEpubTypeNote() {
		return epubTypeNote;
	}

	public void setEpubTypeNote(String epubTypeNote) {
		this.epubTypeNote = epubTypeNote;
	}

	public List<PublisherEntity> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<PublisherEntity> publisher) {
		this.publisher = publisher;
	}

}
