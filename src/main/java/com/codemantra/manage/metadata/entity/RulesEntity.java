/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
22-05-2017			v1.0       	   Bharath Prasanna	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class RulesEntity {
	
	@Field("rule")
	private List<RuleEntity> rule;

	public List<RuleEntity> getRule() {
		return rule;
	}

	public void setRule(List<RuleEntity> rule) {
		this.rule = rule;
	}
}
