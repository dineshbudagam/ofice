/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class SalesRestrictionEntity
{
	@Field("SalesRestrictionType")
	public String salesRestrictionType;
	
	@Field("SalesOutlet")
	public List<SalesOutletEntity> salesOutlet;

	@Field("SalesRestrictionDetail")
	public String salesRestrictionDetail;

	public String getSalesRestrictionType() {
		return salesRestrictionType;
	}

	public void setSalesRestrictionType(String salesRestrictionType) {
		this.salesRestrictionType = salesRestrictionType;
	}

	public List<SalesOutletEntity> getSalesOutlet() {
		return salesOutlet;
	}

	public void setSalesOutlet(List<SalesOutletEntity> salesOutlet) {
		this.salesOutlet = salesOutlet;
	}

	public String getSalesRestrictionDetail() {
		return salesRestrictionDetail;
	}

	public void setSalesRestrictionDetail(String salesRestrictionDetail) {
		this.salesRestrictionDetail = salesRestrictionDetail;
	}
}
