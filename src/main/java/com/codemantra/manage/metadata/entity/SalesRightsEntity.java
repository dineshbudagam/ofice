/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class SalesRightsEntity
{
	@Field("SalesRightsType")
	public String salesRightsType;
	
	@Field("RightsCountry")
	public List<String> rightsCountry; // TODO: need to be contracted to merely a Set
	//public Set<Regions> Regions;
	
	@Field("RightRegions")
	public List<String> rightRegions; // only in Onix2
	//public Set<CountryCodes> CountriesExcluded; // only in Onix3
	//public Set<Regions> RegionsExcluded; // only in Onix3

	public String getSalesRightsType() {
		return salesRightsType;
	}

	public void setSalesRightsType(String salesRightsType) {
		this.salesRightsType = salesRightsType;
	}

	public List<String> getRightRegions() {
		return rightRegions;
	}

	public void setRightRegions(List<String> rightRegions) {
		this.rightRegions = rightRegions;
	}

	public List<String> getRightsCountry() {
		return rightsCountry;
	}

	public void setRightsCountry(List<String> rightsCountry) {
		this.rightsCountry = rightsCountry;
	}
}