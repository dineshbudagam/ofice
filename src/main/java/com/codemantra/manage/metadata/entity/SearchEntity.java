/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
29-12-2017			v1.1       	   	 Shahid	  				Search Service Rework
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.Date;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

public class SearchEntity {
	
	@Field("userId")
	private String userId;
	
	@Field("searchName")
	private String searchName;
	
	@Field("HeaderSearch")
	private HeaderSearchEntity headerSearch;
	
	@Field("AdvancedSearch")
	private AdvancedSearchEntity advancedSearch;
	
	@Field("ImprintAssetSearch")
	private ImprintAssetSearchEntity imprintAssetSearch;
	
	@Field("fieldDisplayName")
	private String fieldDisplayName;
	
	@Field("isActive")
	private boolean isActive;
	
	@Field("isDeleted")
	private boolean isDeleted;
	
	@Field("createdBy")
	private String createdBy;
	
	@Field("createdOn")
	private Date createdDate;
	
	@Field("modifiedBy")
	private String modifiedBy;
	
	@Field("modifiedOn")
	private Date modifiedDate;
	
	private String permissionGroupId;
	
	private int resultCount;
	
	private String changeSearchName;

	private Map<String, Object> columnFilterValues;
	
	private int skipCount;
	
	private long searchReultCount;
	
	private String searchflag;
	
	private boolean isExport;
	
	private boolean isPinnedToDashbaord = Boolean.FALSE;
	
	private Map<String, Object> inColumnFilterList;
	
	private Map<String, Object> ninColumnFilterList;
	
	private Map<String, Object> fromDateFilterList;
	
	private Map<String, Object> toDateFilterList;
	
	@Field("pageName")
	private String pageName;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public HeaderSearchEntity getHeaderSearch() {
		return headerSearch;
	}

	public void setHeaderSearch(HeaderSearchEntity headerSearch) {
		this.headerSearch = headerSearch;
	}

	public AdvancedSearchEntity getAdvancedSearch() {
		return advancedSearch;
	}

	public void setAdvancedSearch(AdvancedSearchEntity advancedSearch) {
		this.advancedSearch = advancedSearch;
	}

	public ImprintAssetSearchEntity getImprintAssetSearch() {
		return imprintAssetSearch;
	}

	public void setImprintAssetSearch(ImprintAssetSearchEntity imprintAssetSearch) {
		this.imprintAssetSearch = imprintAssetSearch;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getResultCount() {
		return resultCount;
	}

	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}

	public String getChangeSearchName() {
		return changeSearchName;
	}

	public void setChangeSearchName(String changeSearchName) {
		this.changeSearchName = changeSearchName;
	}

	public Map<String, Object> getColumnFilterValues() {
		return columnFilterValues;
	}

	public void setColumnFilterValues(Map<String, Object> columnFilterValues) {
		this.columnFilterValues = columnFilterValues;
	}

	public int getSkipCount() {
		return skipCount;
	}

	public void setSkipCount(int skipCount) {
		this.skipCount = skipCount;
	}

	public long getSearchReultCount() {
		return searchReultCount;
	}

	public void setSearchReultCount(long searchReultCount) {
		this.searchReultCount = searchReultCount;
	}

	public String getSearchflag() {
		return searchflag;
	}

	public void setSearchflag(String searchflag) {
		this.searchflag = searchflag;
	}

	public String getPermissionGroupId() {
		return permissionGroupId;
	}

	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}

	public boolean isExport() {
		return isExport;
	}

	public void setExport(boolean isExport) {
		this.isExport = isExport;
	}

	public boolean isPinnedToDashbaord() {
		return isPinnedToDashbaord;
	}

	public void setPinnedToDashbaord(boolean isPinnedToDashbaord) {
		this.isPinnedToDashbaord = isPinnedToDashbaord;
	}

	public Map<String, Object> getInColumnFilterList() {
		return inColumnFilterList;
	}

	public void setInColumnFilterList(Map<String, Object> inColumnFilterList) {
		this.inColumnFilterList = inColumnFilterList;
	}

	public Map<String, Object> getNinColumnFilterList() {
		return ninColumnFilterList;
	}

	public void setNinColumnFilterList(Map<String, Object> ninColumnFilterList) {
		this.ninColumnFilterList = ninColumnFilterList;
	}

	public Map<String, Object> getFromDateFilterList() {
		return fromDateFilterList;
	}

	public void setFromDateFilterList(Map<String, Object> fromDateFilterList) {
		this.fromDateFilterList = fromDateFilterList;
	}

	public Map<String, Object> getToDateFilterList() {
		return toDateFilterList;
	}

	public void setToDateFilterList(Map<String, Object> toDateFilterList) {
		this.toDateFilterList = toDateFilterList;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
}
