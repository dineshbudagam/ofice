/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class SeriesEntity
{
	@Field("SeriesISSN")
	public String seriesISSN;

	@Field("PublisherSeriesCode")
	public String publisherSeriesCode;

	@Field("SeriesIdentifier")
	public List<SeriesIdentifierEntity> seriesIdentifier;

	@Field("TitleOfSeries")
	public String titleOfSeries;
	
	@Field("Title")
	public List<TitleEntity> title;

	@Field("Contributor")
	public List<ContributorEntity> contributor;

	@Field("NumberWithinSeries")
	public String numberWithinSeries;

	@Field("YearOfAnnual")
	public String yearOfAnnual;

	public String getSeriesISSN() {
		return seriesISSN;
	}

	public void setSeriesISSN(String seriesISSN) {
		this.seriesISSN = seriesISSN;
	}

	public String getPublisherSeriesCode() {
		return publisherSeriesCode;
	}

	public void setPublisherSeriesCode(String publisherSeriesCode) {
		this.publisherSeriesCode = publisherSeriesCode;
	}

	public List<SeriesIdentifierEntity> getSeriesIdentifier() {
		return seriesIdentifier;
	}

	public void setSeriesIdentifier(List<SeriesIdentifierEntity> seriesIdentifier) {
		this.seriesIdentifier = seriesIdentifier;
	}

	public String getTitleOfSeries() {
		return titleOfSeries;
	}

	public void setTitleOfSeries(String titleOfSeries) {
		this.titleOfSeries = titleOfSeries;
	}

	public List<TitleEntity> getTitle() {
		return title;
	}

	public void setTitle(List<TitleEntity> title) {
		this.title = title;
	}

	public List<ContributorEntity> getContributor() {
		return contributor;
	}

	public void setContributor(List<ContributorEntity> contributor) {
		this.contributor = contributor;
	}

	public String getNumberWithinSeries() {
		return numberWithinSeries;
	}

	public void setNumberWithinSeries(String numberWithinSeries) {
		this.numberWithinSeries = numberWithinSeries;
	}

	public String getYearOfAnnual() {
		return yearOfAnnual;
	}

	public void setYearOfAnnual(String yearOfAnnual) {
		this.yearOfAnnual = yearOfAnnual;
	}
}
