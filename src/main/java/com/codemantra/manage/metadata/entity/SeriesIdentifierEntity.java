/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class SeriesIdentifierEntity
{
	@Field("SeriesIDType")
	public String seriesIDType;

	@Field("IDTypeName")
	public String idTypeName;

	@Field("IDValue")
	public String idValue;

	public String getSeriesIDType() {
		return seriesIDType;
	}

	public void setSeriesIDType(String seriesIDType) {
		this.seriesIDType = seriesIDType;
	}

	public String getIdTypeName() {
		return idTypeName;
	}

	public void setIdTypeName(String idTypeName) {
		this.idTypeName = idTypeName;
	}

	public String getIdValue() {
		return idValue;
	}

	public void setIdValue(String idValue) {
		this.idValue = idValue;
	}

}
