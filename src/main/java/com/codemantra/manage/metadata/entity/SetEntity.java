/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class SetEntity
{
	@Field("ISBNOfSet")
	public String isbnOfSet;

	@Field("EAN13OfSet")
	public String ean13OfSet;
	
	@Field("ProductIdentifier")
	public List<ProductIdentifierEntity> productIdentifier;

	@Field("TitleOfSet")
	public String titleOfSet;
	
	@Field("Title")
	public List<TitleEntity> title;

	@Field("SetPartNumber")
	public String setPartNumber;

	@Field("SetPartTitle")
	public String setPartTitle;

	@Field("ItemNumberWithinSet")
	public String itemNumberWithinSet;

	@Field("LevelSequenceNumber")
	public String levelSequenceNumber;

	@Field("SetItemTitle")
	public String setItemTitle;

	public String getIsbnOfSet() {
		return isbnOfSet;
	}

	public void setIsbnOfSet(String isbnOfSet) {
		this.isbnOfSet = isbnOfSet;
	}

	public String getEan13OfSet() {
		return ean13OfSet;
	}

	public void setEan13OfSet(String ean13OfSet) {
		this.ean13OfSet = ean13OfSet;
	}

	public List<ProductIdentifierEntity> getProductIdentifier() {
		return productIdentifier;
	}

	public void setProductIdentifier(List<ProductIdentifierEntity> productIdentifier) {
		this.productIdentifier = productIdentifier;
	}

	public String getTitleOfSet() {
		return titleOfSet;
	}

	public void setTitleOfSet(String titleOfSet) {
		this.titleOfSet = titleOfSet;
	}

	public List<TitleEntity> getTitle() {
		return title;
	}

	public void setTitle(List<TitleEntity> title) {
		this.title = title;
	}

	public String getSetPartNumber() {
		return setPartNumber;
	}

	public void setSetPartNumber(String setPartNumber) {
		this.setPartNumber = setPartNumber;
	}

	public String getSetPartTitle() {
		return setPartTitle;
	}

	public void setSetPartTitle(String setPartTitle) {
		this.setPartTitle = setPartTitle;
	}

	public String getItemNumberWithinSet() {
		return itemNumberWithinSet;
	}

	public void setItemNumberWithinSet(String itemNumberWithinSet) {
		this.itemNumberWithinSet = itemNumberWithinSet;
	}

	public String getLevelSequenceNumber() {
		return levelSequenceNumber;
	}

	public void setLevelSequenceNumber(String levelSequenceNumber) {
		this.levelSequenceNumber = levelSequenceNumber;
	}

	public String getSetItemTitle() {
		return setItemTitle;
	}

	public void setSetItemTitle(String setItemTitle) {
		this.setItemTitle = setItemTitle;
	}
		
}
