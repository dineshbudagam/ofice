/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "mStatus")
public class StatusEntity {

	@Field("code")
	private String Code;
	
	@Field("description")
	private String Description;
	
	@Field("type")
	private String StatusType;
	
	@Field("isActive")
	private boolean isActive;
	
	@Field("isDeleted")
	private boolean isDeleted;
	
	@Field("createdBy")
	private String CreatedBy;
	
	@Field("createdOn")
	private String CreatedOn;
	
	@Field("modifiedBy")
	private String ModifiedBy;
	
	@Field("modifiedOn")
	private String ModifiedOn;

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getStatusType() {
		return StatusType;
	}

	public void setStatusType(String statusType) {
		StatusType = statusType;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(String createdOn) {
		CreatedOn = createdOn;
	}

	public String getModifiedBy() {
		return ModifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		ModifiedBy = modifiedBy;
	}

	public String getModifiedOn() {
		return ModifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		ModifiedOn = modifiedOn;
	}

}
