/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class SubjectEntity
{
	@Field("SubjectSchemeIdentifier")
	public String subjectSchemeIdentifier;
	
	@Field("SubjectCode")
	public String subjectCode;

	@Field("SubjectHeadingText")
	public String subjectHeadingText;

	@Field("SubjectSchemeName")
	public String subjectSchemeName;

	@Field("SubjectSchemeVersion")
	public String subjectSchemeVersion;

	public String getSubjectSchemeIdentifier() {
		return subjectSchemeIdentifier;
	}

	public void setSubjectSchemeIdentifier(String subjectSchemeIdentifier) {
		this.subjectSchemeIdentifier = subjectSchemeIdentifier;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectHeadingText() {
		return subjectHeadingText;
	}

	public void setSubjectHeadingText(String subjectHeadingText) {
		this.subjectHeadingText = subjectHeadingText;
	}

	public String getSubjectSchemeName() {
		return subjectSchemeName;
	}

	public void setSubjectSchemeName(String subjectSchemeName) {
		this.subjectSchemeName = subjectSchemeName;
	}

	public String getSubjectSchemeVersion() {
		return subjectSchemeVersion;
	}

	public void setSubjectSchemeVersion(String subjectSchemeVersion) {
		this.subjectSchemeVersion = subjectSchemeVersion;
	}

}