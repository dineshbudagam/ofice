/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class SupplierIdentifierEntity
{
	@Field("SupplierIDType")
	public String supplierIDType;

	@Field("IdTypeName")
	public String idTypeName;

	@Field("IDValue")
	public String idValue;

	public String getSupplierIDType() {
		return supplierIDType;
	}

	public void setSupplierIDType(String supplierIDType) {
		this.supplierIDType = supplierIDType;
	}

	public String getIdTypeName() {
		return idTypeName;
	}

	public void setIdTypeName(String idTypeName) {
		this.idTypeName = idTypeName;
	}

	public String getIdValue() {
		return idValue;
	}

	public void setIdValue(String idValue) {
		this.idValue = idValue;
	}
}
