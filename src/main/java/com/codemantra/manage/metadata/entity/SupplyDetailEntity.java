/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class SupplyDetailEntity
{
	@Field("SupplierIdentifier")
	public List<SupplierIdentifierEntity> supplierIdentifier;
	
	/*public List<String> TelephoneNumber;
	public List<String> FaxNumber;
	public List<String> EmailAddress;*/
	
	/*public String NewSupplier;*/
	
	/*public List<Stock> Stock;*/
	
	@Field("SupplierRole")
	public String supplierRole;
	
	@Field("SupplierName")
	public String supplierName;
	
	@Field("Availability")
	public String availability; // name of an enum, AvailabilityStatuss or ProductAvailabilitys
	
	@Field("SupplierSAN")
	public String supplierSAN;
	
	@Field("SupplierEANLocationNumber")
	public String supplierEANLocationNumber;
	
	@Field("TelephoneNumber")
	public String telephoneNumber;
	
	@Field("FaxNumber")
	public String faxNumber;
	
	@Field("EmailAddress")
	public String emailAddress;
	
	@Field("Website")
	public WebsiteEntity website;
	
	@Field("SupplyToCountry")
	public List<String> supplyToCountry;
	
	@Field("SupplyToTerritory")
	public String supplyToTerritory;
	
	@Field("SupplyToRegion")
	public String supplyToRegion;
	
	@Field("SupplyToCountryExcluded")
	public String supplyToCountryExcluded;
	
	@Field("SupplyRestrictionDetail")
	public String supplyRestrictionDetail;
	
	@Field("ReturnsCodeType")
	public String returnsCodeType;
	
	@Field("ReturnsCode")
	public String returnsCode;
	
	@Field("LastDateForReturn")
	public String lastDateForReturn;
	
	@Field("AvailabilityCode")
	public String availabilityCode;
	
	@Field("ProductAvailability")
	public String productAvailability;
	
	@Field("IntermediaryAvailabilityCode")
	public String intermediaryAvailabilityCode;
	
	@Field("NewSupplier")
	public String newSupplier;
	
	@Field("DateFormat")
	public String dateFormat;
	
	@Field("ExpectedShipDate")
	public String expectedShipDate;
	
	@Field("OnSaleDate")
	public String onSaleDate;
	
	@Field("OrderTime")
	public String orderTime;
	
	@Field("Stock")
	public String stock;
	
	@Field("PackQuantity")
	public String packQuantity;
	
	@Field("AudienceRestrictionFlag")
	public String audienceRestrictionFlag;
	
	@Field("AudienceRestrictionNote")
	public String audienceRestrictionNote;
	
	@Field("PriceAmount")
	public Double priceAmount;
	
	@Field("UnpricedItemType")
	public String unpricedItemType;
	
	@Field("Reissue")
	public String reissue;
	
	@Field("Price")
	public List<PriceEntity> price;

	public List<SupplierIdentifierEntity> getSupplierIdentifier() {
		return supplierIdentifier;
	}

	public void setSupplierIdentifier(List<SupplierIdentifierEntity> supplierIdentifier) {
		this.supplierIdentifier = supplierIdentifier;
	}

	public String getSupplierRole() {
		return supplierRole;
	}

	public void setSupplierRole(String supplierRole) {
		this.supplierRole = supplierRole;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getSupplierSAN() {
		return supplierSAN;
	}

	public void setSupplierSAN(String supplierSAN) {
		this.supplierSAN = supplierSAN;
	}

	public String getSupplierEANLocationNumber() {
		return supplierEANLocationNumber;
	}

	public void setSupplierEANLocationNumber(String supplierEANLocationNumber) {
		this.supplierEANLocationNumber = supplierEANLocationNumber;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public WebsiteEntity getWebsite() {
		return website;
	}

	public void setWebsite(WebsiteEntity website) {
		this.website = website;
	}

	public List<String> getSupplyToCountry() {
		return supplyToCountry;
	}

	public void setSupplyToCountry(List<String> supplyToCountry) {
		this.supplyToCountry = supplyToCountry;
	}

	public String getSupplyToTerritory() {
		return supplyToTerritory;
	}

	public void setSupplyToTerritory(String supplyToTerritory) {
		this.supplyToTerritory = supplyToTerritory;
	}

	public String getSupplyToRegion() {
		return supplyToRegion;
	}

	public void setSupplyToRegion(String supplyToRegion) {
		this.supplyToRegion = supplyToRegion;
	}

	public String getSupplyToCountryExcluded() {
		return supplyToCountryExcluded;
	}

	public void setSupplyToCountryExcluded(String supplyToCountryExcluded) {
		this.supplyToCountryExcluded = supplyToCountryExcluded;
	}

	public String getSupplyRestrictionDetail() {
		return supplyRestrictionDetail;
	}

	public void setSupplyRestrictionDetail(String supplyRestrictionDetail) {
		this.supplyRestrictionDetail = supplyRestrictionDetail;
	}

	public String getReturnsCodeType() {
		return returnsCodeType;
	}

	public void setReturnsCodeType(String returnsCodeType) {
		this.returnsCodeType = returnsCodeType;
	}

	public String getReturnsCode() {
		return returnsCode;
	}

	public void setReturnsCode(String returnsCode) {
		this.returnsCode = returnsCode;
	}

	public String getLastDateForReturn() {
		return lastDateForReturn;
	}

	public void setLastDateForReturn(String lastDateForReturn) {
		this.lastDateForReturn = lastDateForReturn;
	}

	public String getAvailabilityCode() {
		return availabilityCode;
	}

	public void setAvailabilityCode(String availabilityCode) {
		this.availabilityCode = availabilityCode;
	}

	public String getProductAvailability() {
		return productAvailability;
	}

	public void setProductAvailability(String productAvailability) {
		this.productAvailability = productAvailability;
	}

	public String getIntermediaryAvailabilityCode() {
		return intermediaryAvailabilityCode;
	}

	public void setIntermediaryAvailabilityCode(String intermediaryAvailabilityCode) {
		this.intermediaryAvailabilityCode = intermediaryAvailabilityCode;
	}

	public String getNewSupplier() {
		return newSupplier;
	}

	public void setNewSupplier(String newSupplier) {
		this.newSupplier = newSupplier;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getExpectedShipDate() {
		return expectedShipDate;
	}

	public void setExpectedShipDate(String expectedShipDate) {
		this.expectedShipDate = expectedShipDate;
	}

	public String getOnSaleDate() {
		return onSaleDate;
	}

	public void setOnSaleDate(String onSaleDate) {
		this.onSaleDate = onSaleDate;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getPackQuantity() {
		return packQuantity;
	}

	public void setPackQuantity(String packQuantity) {
		this.packQuantity = packQuantity;
	}

	public String getAudienceRestrictionFlag() {
		return audienceRestrictionFlag;
	}

	public void setAudienceRestrictionFlag(String audienceRestrictionFlag) {
		this.audienceRestrictionFlag = audienceRestrictionFlag;
	}

	public String getAudienceRestrictionNote() {
		return audienceRestrictionNote;
	}

	public void setAudienceRestrictionNote(String audienceRestrictionNote) {
		this.audienceRestrictionNote = audienceRestrictionNote;
	}

	public Double getPriceAmount() {
		return priceAmount;
	}

	public void setPriceAmount(Double priceAmount) {
		this.priceAmount = priceAmount;
	}

	public String getUnpricedItemType() {
		return unpricedItemType;
	}

	public void setUnpricedItemType(String unpricedItemType) {
		this.unpricedItemType = unpricedItemType;
	}

	public String getReissue() {
		return reissue;
	}

	public void setReissue(String reissue) {
		this.reissue = reissue;
	}

	public List<PriceEntity> getPrice() {
		return price;
	}

	public void setPrice(List<PriceEntity> price) {
		this.price = price;
	}
}