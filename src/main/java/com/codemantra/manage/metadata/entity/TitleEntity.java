/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class TitleEntity
{
	@Field("TitleType")	
	public String titleType;
	
	@Field("TitleText")
	public String titleText;
	
	@Field("TitleWithoutPrefix")
	public String titleWithoutPrefix;
	
	@Field("Subtitle")
	public String subtitle;

	public String getTitleType() {
		return titleType;
	}

	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}

	public String getTitleText() {
		return titleText;
	}

	public void setTitleText(String titleText) {
		this.titleText = titleText;
	}

	public String getTitleWithoutPrefix() {
		return titleWithoutPrefix;
	}

	public void setTitleWithoutPrefix(String titleWithoutPrefix) {
		this.titleWithoutPrefix = titleWithoutPrefix;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	
}