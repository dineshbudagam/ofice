/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.codemantra.manage.metadata.model.AccountAccess;

@Document(collection = "mUser")
public class UserRightsEntity {

	@Field("userId")
	private String userId;
	
	@Field("firstName")
	private String firstName;
	
	@Field("accounts")
	private List<AccountAccess> accounts;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public List<AccountAccess> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AccountAccess> accounts) {
		this.accounts = accounts;
	}
}
