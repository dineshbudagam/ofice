package com.codemantra.manage.metadata.entity;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class Validation {

	@Field("regex")
	private String regex;	
	
	public String getRegex() {
	return regex;
	}
	
	
	public void setRegex(String regex) {
	this.regex = regex;
	}
	
	

}