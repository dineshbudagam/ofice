/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class WebsiteEntity
{
	@Field("WebsiteDescription")
	public String websiteDescription;

	@Field("WebsiteLink")
	public String websiteLink;

	@Field("WebsiteRole")
	public String websiteRole;

	public String getWebsiteDescription() {
		return websiteDescription;
	}

	public void setWebsiteDescription(String websiteDescription) {
		this.websiteDescription = websiteDescription;
	}

	public String getWebsiteLink() {
		return websiteLink;
	}

	public void setWebsiteLink(String websiteLink) {
		this.websiteLink = websiteLink;
	}

	public String getWebsiteRole() {
		return websiteRole;
	}

	public void setWebsiteRole(String websiteRole) {
		this.websiteRole = websiteRole;
	}
		
}
