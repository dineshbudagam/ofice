/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class WorkIdentifierEntity
{
	@Field("WorkIDType")
	public String workIDType;

	@Field("IdTypeName")
	public String idTypeName;

	@Field("IDValue")
	public String idValue;

	public String getWorkIDType() {
		return workIDType;
	}

	public void setWorkIDType(String workIDType) {
		this.workIDType = workIDType;
	}

	public String getIdTypeName() {
		return idTypeName;
	}

	public void setIdTypeName(String idTypeName) {
		this.idTypeName = idTypeName;
	}

	public String getIdValue() {
		return idValue;
	}

	public void setIdValue(String idValue) {
		this.idValue = idValue;
	}
		
}
