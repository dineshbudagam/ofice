package com.codemantra.manage.metadata.jsondiff;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestClass {
	
	public static void mainMethod(String[] args) throws JsonProcessingException, IOException {
		
		String oldpath = "{ \"name\":\"John\", \"age\":[{\r\n" + 
				"                \"ProductIDType\" : \"02\",\r\n" + 
				"                \"IDValue\" : \"1350075515\"\r\n" + 
				"            }], \"city\":\"New York\" }";
		String newpath = "{ \"name\":\"John\", \"age\":[{\r\n" + 
				"                \"ProductIDType\" : \"02\",\r\n" + 
				"                \"IDValue\" : \"1350075516\"\r\n" + 
				"            }], \"city\":\"New York\" }";
		
		ObjectMapper jackson = new ObjectMapper();
		JsonNode beforeNode = jackson.readTree(oldpath); 
		JsonNode afterNode = jackson.readTree(newpath); 
		JsonNode patchNode = JsonDiff.asJson(beforeNode, afterNode); 
		String diff = patchNode.toString();
		System.out.println(diff);
		//JsonDiff.asJson(, );
	}

}
