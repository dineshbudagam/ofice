/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class AccountAccess{
	
	private String accountCode;
	private String accountName;
	
	private List<ImprintAccess> imprints;
	private List<MetaDataTypeAccess> metadataType;
	private List<MetaDataAccess> metadata;	
	
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public List<ImprintAccess> getImprints() {
		return imprints;
	}
	public void setImprints(List<ImprintAccess> imprints) {
		this.imprints = imprints;
	}
	public List<MetaDataTypeAccess> getMetadataType() {
		return metadataType;
	}
	public void setMetadataType(List<MetaDataTypeAccess> metadataType) {
		this.metadataType = metadataType;
	}
	public List<MetaDataAccess> getMetadata() {
		return metadata;
	}
	public void setMetadata(List<MetaDataAccess> metadata) {
		this.metadata = metadata;
	}

}
