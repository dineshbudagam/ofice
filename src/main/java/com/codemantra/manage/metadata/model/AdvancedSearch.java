/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class AdvancedSearch {
	
	private String[] isbns;
	
	private String[] titles;
	
	private String[] authors;

	private String[] accounts;

	private String[] formats;

	private String[] status;

	private String[] imprints;

	private String[] productCategories;

	private String[] others;
	
	public String[] getIsbns() {
		return isbns;
	}

	public void setIsbns(String[] isbns) {
		this.isbns = isbns;
	}

	public String[] getAccounts() {
		return accounts;
	}

	public void setAccounts(String[] accounts) {
		this.accounts = accounts;
	}

	public String[] getFormats() {
		return formats;
	}

	public void setFormats(String[] formats) {
		this.formats = formats;
	}

	public String[] getStatus() {
		return status;
	}

	public void setStatus(String[] status) {
		this.status = status;
	}

	public String[] getImprints() {
		return imprints;
	}

	public void setImprints(String[] imprints) {
		this.imprints = imprints;
	}



	public String[] getProductCategories() {
		return productCategories;
	}

	public void setProductCategories(String[] productCategories) {
		this.productCategories = productCategories;
	}

	public String[] getOthers() {
		return others;
	}

	public void setOthers(String[] others) {
		this.others = others;
	}

	public String[] getTitles() {
		return titles;
	}

	public void setTitles(String[] titles) {
		this.titles = titles;
	}

	public String[] getAuthors() {
		return authors;
	}

	public void setAuthors(String[] authors) {
		this.authors = authors;
	}

/*	public String[] getpCategories() {
		return pCategories;
	}

	public void setpCategories(String[] pCategories) {
		this.pCategories = pCategories;
	}*/
}
