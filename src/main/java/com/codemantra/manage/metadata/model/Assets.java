/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;


import java.util.Date;
import java.util.List;
import java.util.Map;
 
//import com.codemantra.manage.upload.model.UploadMail;

public class Assets {
	private String productIDValue;
	private String assetId;
	private String formatName;
	private String formatId;
	private String title;
	private String bucket;
	private String folder;
	private long size;
	private List<FileDetail> filedetails;
	private String url;
	private String filename;
	private String downloadurl;
	private String shareemails;
	private int expiredays;
	private String loggeduser;
	private List<String> filePathList;
	private List<String> emailList;
	private String formatStatus;
	private String fileExtension;
	private String transactionStatus;
	private String transactionType;
	private List<BulkFiles> bulkFileList;
	private String absolutePath;
	private String permission;
	private List<String> format;
	private String loggedUserEmail;
	private String assetActivity;
	private String permissionGroupId;
	private String configId;
	private Map<String,String> productFormatList;
	private Map<String, Object> templateData;
	private List<UploadMail> uploadMailList;
	private String comments;
	
	private Date uploadedOn;
	 
	private String uploadedBy;
	 
	private boolean isDeleted;


	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public List<FileDetail> getFiledetails() {
		return filedetails;
	}

	public void setFiledetails(List<FileDetail> filedetails) {
		this.filedetails = filedetails;
	}

	public String getProductIDValue() {
		return productIDValue;
	}

	public void setProductIDValue(String productIDValue) {
		this.productIDValue = productIDValue;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDownloadurl() {
		return downloadurl;
	}

	public void setDownloadurl(String downloadurl) {
		this.downloadurl = downloadurl;
	}

	public String getShareemails() {
		return shareemails;
	}

	public void setShareemails(String shareemails) {
		this.shareemails = shareemails;
	}

	public int getExpiredays() {
		return expiredays;
	}

	public void setExpiredays(int expiredays) {
		this.expiredays = expiredays;
	}

	public String getLoggeduser() {
		return loggeduser;
	}

	public void setLoggeduser(String loggeduser) {
		this.loggeduser = loggeduser;
	}

	public List<String> getFilePathList() {
		return filePathList;
	}

	public void setFilePathList(List<String> filePathList) {
		this.filePathList = filePathList;
	}

	public List<String> getEmailList() {
		return emailList;
	}

	public void setEmailList(List<String> emailList) {
		this.emailList = emailList;
	}

	public String getFormatStatus() {
		return formatStatus;
	}

	public void setFormatStatus(String formatStatus) {
		this.formatStatus = formatStatus;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getFormatName() {
		return formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

	public String getFormatId() {
		return formatId;
	}

	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}

	public List<BulkFiles> getBulkFileList() {
		return bulkFileList;
	}

	public void setBulkFileList(List<BulkFiles> bulkFileList) {
		this.bulkFileList = bulkFileList;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public List<String> getFormat() {
		return format;
	}

	public void setFormat(List<String> format) {
		this.format = format;
	}

	public String getLoggedUserEmail() {
		return loggedUserEmail;
	}

	public void setLoggedUserEmail(String loggedUserEmail) {
		this.loggedUserEmail = loggedUserEmail;
	}

	public String getAssetActivity() {
		return assetActivity;
	}

	public void setAssetActivity(String assetActivity) {
		this.assetActivity = assetActivity;
	}

	public String getPermissionGroupId() {
		return permissionGroupId;
	}

	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}

	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	public Map<String, String> getProductFormatList() {
		return productFormatList;
	}

	public void setProductFormatList(Map<String, String> productFormatList) {
		this.productFormatList = productFormatList;
	}

	public Map<String, Object> getTemplateData() {
		return templateData;
	}

	public void setTemplateData(Map<String, Object> templateData) {
		this.templateData = templateData;
	}

	public List<UploadMail> getUploadMailList() {
		return uploadMailList;
	}

	public void setUploadMailList(List<UploadMail> uploadMailList) {
		this.uploadMailList = uploadMailList;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
