/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class Audience
{
	public String AudienceCodeType;

	public String AudienceCodeTypeName;

	public String AudienceCodeValue;

	public String getAudienceCodeType() {
		return AudienceCodeType;
	}

	public void setAudienceCodeType(String audienceCodeType) {
		AudienceCodeType = audienceCodeType;
	}

	public String getAudienceCodeTypeName() {
		return AudienceCodeTypeName;
	}

	public void setAudienceCodeTypeName(String audienceCodeTypeName) {
		AudienceCodeTypeName = audienceCodeTypeName;
	}

	public String getAudienceCodeValue() {
		return AudienceCodeValue;
	}

	public void setAudienceCodeValue(String audienceCodeValue) {
		AudienceCodeValue = audienceCodeValue;
	}
}
