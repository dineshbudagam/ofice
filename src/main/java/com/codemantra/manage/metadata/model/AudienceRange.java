/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class AudienceRange
{
	public List<String> AudienceRangePrecision;

	public String AudienceRangeQualifier;

	public List<String> AudienceRangeValue;

	public List<String> getAudienceRangePrecision() {
		return AudienceRangePrecision;
	}

	public void setAudienceRangePrecision(List<String> audienceRangePrecision) {
		AudienceRangePrecision = audienceRangePrecision;
	}

	public String getAudienceRangeQualifier() {
		return AudienceRangeQualifier;
	}

	public void setAudienceRangeQualifier(String audienceRangeQualifier) {
		AudienceRangeQualifier = audienceRangeQualifier;
	}

	public List<String> getAudienceRangeValue() {
		return AudienceRangeValue;
	}

	public void setAudienceRangeValue(List<String> audienceRangeValue) {
		AudienceRangeValue = audienceRangeValue;
	}

	
}
