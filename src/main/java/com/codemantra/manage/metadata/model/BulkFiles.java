package com.codemantra.manage.metadata.model;

import java.io.File;

public class BulkFiles {

	private String filePath;
	private long fileSize;
	private boolean fileStatus;
	private String fileStatusMessage;
	private String convertedFileSize;
	private File file;
	private String formatId;
	private String formatName;
	private String title;
	private String author;
	private String isbn;
	private String fileName;
	
	
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public boolean isFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(boolean fileStatus) {
		this.fileStatus = fileStatus;
	}
	public String getFileStatusMessage() {
		return fileStatusMessage;
	}
	public void setFileStatusMessage(String fileStatusMessage) {
		this.fileStatusMessage = fileStatusMessage;
	}
	public String getConvertedFileSize() {
		return convertedFileSize;
	}
	public void setConvertedFileSize(String convertedFileSize) {
		this.convertedFileSize = convertedFileSize;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
