/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.Date;
import java.util.List;

public class CDCMetaData {
	
	public String referenceId;
	
	public String ImprintName;
	
	public String CountryOfPublication;
	
	public String ProductCategory;
	
	public String updateType;
	
	public String title;
	
	public Object productHierarchyId;
	
	public boolean isSynced;
	
	public List<CDCMetaDataDetails> cdcMetaDataDetails;
	
	public Date LastModifiedOn;
	
	public String LastModifiedBy;
	
	public String updatedSource;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImprintName() {
		return ImprintName;
	}

	public void setImprintName(String imprintName) {
		ImprintName = imprintName;
	}

	public Object getCountryOfPublication() {
		return CountryOfPublication;
	}

	public Object getProductHierarchyId() {
		return productHierarchyId;
	}

	public void setProductHierarchyId(Object phId) {
		this.productHierarchyId = phId;
	}

	public boolean isSynced() {
		return isSynced;
	}

	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public void setCountryOfPublication(String countryOfPublication) {
		CountryOfPublication = countryOfPublication;
	}

	public String getProductCategory() {
		return ProductCategory;
	}

	public void setProductCategory(String productCategory) {
		ProductCategory = productCategory;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public List<CDCMetaDataDetails> getCdcMetaDataDetails() {
		return cdcMetaDataDetails;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	public void setCdcMetaDataDetails(List<CDCMetaDataDetails> cdcMetaDataDetails) {
		this.cdcMetaDataDetails = cdcMetaDataDetails;
	}

	public Date getLastModifiedOn() {
		return LastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		LastModifiedOn = lastModifiedOn;
	}

	public String getLastModifiedBy() {
		return LastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		LastModifiedBy = lastModifiedBy;
	}

	public String getUpdatedSource() {
		return updatedSource;
	}

	public void setUpdatedSource(String updatedSource) {
		this.updatedSource = updatedSource;
	}
}
