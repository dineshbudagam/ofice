/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-07-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;
import java.util.Map;

public class CodeList {
	
	private String statusMessage;

	private List<Map<String, String>> AgentRoles;
	
	private List<Map<String, String>> AudienceCodeTypes;
	
	private List<Map<String, String>> AudienceRangePrecisions;
	
	private List<Map<String, String>> AudienceRangeQualifiers;
	
	private List<Map<String, String>> AudienceRestrictionFlags;
	
	private List<Map<String, String>> Audiences;
	
	private List<Map<String, String>> AvailabilityStatuss;
	
	private List<Map<String, String>> BarcodeIndicators;
	
	private List<Map<String, String>> BarcodeIndicatorsList141;
	
	private List<Map<String, String>> BibleContentss;
	
	private List<Map<String, String>> BiblePurposes;
	
	private List<Map<String, String>> BibleTextFeatures;
	
	private List<Map<String, String>> BibleTextOrganizations;
	
	private List<Map<String, String>> BibleVersions;
	
	private List<Map<String, String>> BisacReturnableIndicators;
	
	private List<Map<String, String>> BookFormDetails;
	
	private List<Map<String, String>> ChineseSchoolGrades;
	
	private List<Map<String, String>> CitedContentTypes;
	
	private List<Map<String, String>> CollectionSequenceTypes;
	
	private List<Map<String, String>> CollectionTypes;
	
	private List<Map<String, String>> ComplexitySchemeIdentifiers;
	
	private List<Map<String, String>> ContentAudiences;
	
	private List<Map<String, String>> ContentDateRoles;
	
	private List<Map<String, String>> ContentSourceTypes;
	
	private List<Map<String, String>> ContributorPlaceRelators;
	
	private List<Map<String, String>> ContributorRoles;
	
	private List<Map<String, String>> CountryCodes;
	
	private List<Map<String, String>> CurrencyCodes;
	
	private List<Map<String, String>> CurrencyZones;
	
	private List<Map<String, String>> DateFormats;
	
	private List<Map<String, String>> DefaultLinearUnits;
	
	private List<Map<String, String>> DefaultUnitOfWeights;
	
	private List<Map<String, String>> DiscountCodeTypes;
	
	private List<Map<String, String>> DiscountTypes;
	
	private List<Map<String, String>> EditionTypes;
	
	private List<Map<String, String>> EpublicationAccessibilityDetailss;
	
	private List<Map<String, String>> EpublicationFormats;
	
	private List<Map<String, String>> EpublicationTechnicalProtections;
	
	private List<Map<String, String>> EpublicationTypes;
	
	private List<Map<String, String>> EpublicationVersionNumbers;
	
	private List<Map<String, String>> EuToySafetyDirectiveHazardWarnings;
	
	private List<Map<String, String>> ExtentTypes;
	
	private List<Map<String, String>> ExtentUnits;
	
	private List<Map<String, String>> FrontCoverImageFileFormats;
	
	private List<Map<String, String>> FrontCoverImageFileLinkTypes;
	
	private List<Map<String, String>> IllustratedNotIllustrateds;
	
	private List<Map<String, String>> IllustrationAndOtherContentTypes;
	
	private List<Map<String, String>> ImageAudioVideoFileFormats;
	
	private List<Map<String, String>> ImageAudioVideoFileLinkTypes;
	
	private List<Map<String, String>> ImageAudioVideoFileTypes;
	
	private List<Map<String, String>> LanguageCodes;
	
	private List<Map<String, String>> LanguageRoles;
	
	private List<Map<String, String>> LicenseExpressionTypes;
	
	private List<Map<String, String>> MainSubjectSchemeIdentifiers;
	
	private List<Map<String, String>> MarketDateRoles;
	
	private List<Map<String, String>> MarketPublishingStatuss;
	
	private List<Map<String, String>> MeasureTypes;
	
	private List<Map<String, String>> MeasureUnits;
	
	private List<Map<String, String>> MessageRecordStatusDetails;
	
	private List<Map<String, String>> MessageStatusDateRoles;
	
	private List<Map<String, String>> MessageStatuss;
	
	private List<Map<String, String>> NameCodeTypes;
	
	private List<Map<String, String>> NorthAmericanSchoolOrCollegeGrades;
	
	private List<Map<String, String>> NotificationOrUpdateTypes;
	
	private List<Map<String, String>> OnixAdultAudienceRatings;
	
	private List<Map<String, String>> OnixRetailSalesOutletIdss;
	
	private List<Map<String, String>> OnixReturnsConditionss;
	
	private List<Map<String, String>> OtherTextTypes;
	
	private List<Map<String, String>> PersonDateRoles;
	
	private List<Map<String, String>> PersonNameIdentifierTypes;
	
	private List<Map<String, String>> PersonOrganizationDateRoles;
	
	private List<Map<String, String>> PersonOrganizationNameTypes;
	
	private List<Map<String, String>> PositionOnProducts;
	
	private List<Map<String, String>> PriceCodeTypes;
	
	private List<Map<String, String>> PriceConditionQuantityTypes;
	
	private List<Map<String, String>> PriceConditionTypes;
	
	private List<Map<String, String>> PriceDateRoles;
	
	private List<Map<String, String>> PriceIdentifierTypes;
	
	private List<Map<String, String>> PriceStatuss;
	
	private List<Map<String, String>> PriceTypeQualifiers;
	
	private List<Map<String, String>> PriceTypes;
	
	private List<Map<String, String>> PrintedOnProducts;
	
	private List<Map<String, String>> PrizeOrAwardAchievements;
	
	private List<Map<String, String>> ProductAvailabilitys;
	
	private List<Map<String, String>> ProductClassificationTypes;
	
	private List<Map<String, String>> ProductCompositions;
	
	private List<Map<String, String>> ProductContactRoles;
	
	private List<Map<String, String>> ProductContentTypes;
	
	private List<Map<String, String>> ProductFormDetails;
	
	private List<Map<String, String>> ProductFormDetailsList175;
	
	private List<Map<String, String>> ProductFormFeatureTypes;
	
	private List<Map<String, String>> ProductFormFeatureValueBindingOrPageEdgeColors;
	
	private List<Map<String, String>> ProductFormFeatureValueDvdRegions;
	
	private List<Map<String, String>> ProductFormFeatureValueOperatingSystems;
	
	private List<Map<String, String>> ProductFormFeatureValueSpecialCoverMaterials;
	
	private List<Map<String, String>> ProductForms;
	
	private List<Map<String, String>> ProductFormsList150;
	
	private List<Map<String, String>> ProductIdentifierTypes;
	
	private List<Map<String, String>> ProductPackagingTypes;
	
	private List<Map<String, String>> ProductRelations;
	
	private List<Map<String, String>> Proximitys;
	
	private List<Map<String, String>> PublishingDateRoles;
	
	private List<Map<String, String>> PublishingRoles;
	
	private List<Map<String, String>> PublishingStatuss;
	
	private List<Map<String, String>> QuantityUnits;
	
	private List<Map<String, String>> RecordSourceTypes;
	
	private List<Map<String, String>> RecordStatuss;
	
	private List<Map<String, String>> ReligiousTextFeatures;
	
	private List<Map<String, String>> ReligiousTextFeatureTypes;
	
	private List<Map<String, String>> ResourceContentTypes;
	
	private List<Map<String, String>> ResourceFeatureTypes;
	
	private List<Map<String, String>> ResourceForms;
	
	private List<Map<String, String>> ResourceModes;
	
	private List<Map<String, String>> ReturnsConditionsCodeTypes;
	
	private List<Map<String, String>> RightsRegions;
	
	private List<Map<String, String>> RightsTypes;
	
	private List<Map<String, String>> SalesOutletIdentifierTypes;
	
	private List<Map<String, String>> SalesRestrictionTypes;
	
	private List<Map<String, String>> SalesRightsTypes;
	
	private List<Map<String, String>> SeriesIdentifierTypes;
	
	private List<Map<String, String>> StatusDetailCodeTypes;
	
	private List<Map<String, String>> StatusDetailTypeSeveritys;
	
	private List<Map<String, String>> StockQuantityCodeTypes;
	
	private List<Map<String, String>> StudyBibleTypes;
	
	private List<Map<String, String>> SubjectSchemeIdentifiers;
	
	private List<Map<String, String>> SupplierIdentifierTypes;
	
	private List<Map<String, String>> SupplierOwnCodeTypes;
	
	private List<Map<String, String>> SupplierRoles;
	
	private List<Map<String, String>> SupplyDateRoles;
	
	private List<Map<String, String>> SupplytoRegions;
	
	private List<Map<String, String>> SupportingResourceFileFormats;
	
	private List<Map<String, String>> TaxRateCodeds;
	
	private List<Map<String, String>> TaxTypes;
	
	private List<Map<String, String>> TextCaseFlags;
	
	private List<Map<String, String>> TextFormats;
	
	private List<Map<String, String>> TextItemIdentifierTypes;
	
	private List<Map<String, String>> TextItemTypes;
	
	private List<Map<String, String>> TextLinkTypes;
	
	private List<Map<String, String>> TextScriptCodes;
	
	private List<Map<String, String>> TextTypes;
	
	private List<Map<String, String>> ThesisTypes;
	
	private List<Map<String, String>> TitleElementLevels;
	
	private List<Map<String, String>> TitleTypes;
	
	private List<Map<String, String>> TradeCategorys;
	
	private List<Map<String, String>> TransliterationSchemes;
	
	private List<Map<String, String>> UnitOfPricings;
	
	private List<Map<String, String>> UnitOfUsages;
	
	private List<Map<String, String>> UnnamedPersonss;
	
	private List<Map<String, String>> UnpricedItemTypes;
	
	private List<Map<String, String>> UsageStatuss;
	
	private List<Map<String, String>> UsageTypes;
	
	private List<Map<String, String>> UsCpsiaChokingHazardWarnings;
	
	private List<Map<String, String>> UsCpsiaHazardWarnings;
	
	private List<Map<String, String>> Velocitys;
	
	private List<Map<String, String>> WebsiteRoles;
	
	private List<Map<String, String>> WorkIdentifierTypes;
	
	private List<Map<String, String>> WorkRelations;
	
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public List<Map<String, String>> getAgentRoles() {
		return AgentRoles;
	}
	public void setAgentRoles(List<Map<String, String>> agentRoles) {
		AgentRoles = agentRoles;
	}
	public List<Map<String, String>> getAudienceCodeTypes() {
		return AudienceCodeTypes;
	}
	public void setAudienceCodeTypes(List<Map<String, String>> audienceCodeTypes) {
		AudienceCodeTypes = audienceCodeTypes;
	}
	public List<Map<String, String>> getAudienceRangePrecisions() {
		return AudienceRangePrecisions;
	}
	public void setAudienceRangePrecisions(List<Map<String, String>> audienceRangePrecisions) {
		AudienceRangePrecisions = audienceRangePrecisions;
	}
	public List<Map<String, String>> getAudienceRangeQualifiers() {
		return AudienceRangeQualifiers;
	}
	public void setAudienceRangeQualifiers(List<Map<String, String>> audienceRangeQualifiers) {
		AudienceRangeQualifiers = audienceRangeQualifiers;
	}
	public List<Map<String, String>> getAudienceRestrictionFlags() {
		return AudienceRestrictionFlags;
	}
	public void setAudienceRestrictionFlags(List<Map<String, String>> audienceRestrictionFlags) {
		AudienceRestrictionFlags = audienceRestrictionFlags;
	}
	public List<Map<String, String>> getAudiences() {
		return Audiences;
	}
	public void setAudiences(List<Map<String, String>> audiences) {
		Audiences = audiences;
	}
	public List<Map<String, String>> getAvailabilityStatuss() {
		return AvailabilityStatuss;
	}
	public void setAvailabilityStatuss(List<Map<String, String>> availabilityStatuss) {
		AvailabilityStatuss = availabilityStatuss;
	}
	public List<Map<String, String>> getBarcodeIndicators() {
		return BarcodeIndicators;
	}
	public void setBarcodeIndicators(List<Map<String, String>> barcodeIndicators) {
		BarcodeIndicators = barcodeIndicators;
	}
	public List<Map<String, String>> getBarcodeIndicatorsList141() {
		return BarcodeIndicatorsList141;
	}
	public void setBarcodeIndicatorsList141(List<Map<String, String>> barcodeIndicatorsList141) {
		BarcodeIndicatorsList141 = barcodeIndicatorsList141;
	}
	public List<Map<String, String>> getBibleContentss() {
		return BibleContentss;
	}
	public void setBibleContentss(List<Map<String, String>> bibleContentss) {
		BibleContentss = bibleContentss;
	}
	public List<Map<String, String>> getBiblePurposes() {
		return BiblePurposes;
	}
	public void setBiblePurposes(List<Map<String, String>> biblePurposes) {
		BiblePurposes = biblePurposes;
	}
	public List<Map<String, String>> getBibleTextFeatures() {
		return BibleTextFeatures;
	}
	public void setBibleTextFeatures(List<Map<String, String>> bibleTextFeatures) {
		BibleTextFeatures = bibleTextFeatures;
	}
	public List<Map<String, String>> getBibleTextOrganizations() {
		return BibleTextOrganizations;
	}
	public void setBibleTextOrganizations(List<Map<String, String>> bibleTextOrganizations) {
		BibleTextOrganizations = bibleTextOrganizations;
	}
	public List<Map<String, String>> getBibleVersions() {
		return BibleVersions;
	}
	public void setBibleVersions(List<Map<String, String>> bibleVersions) {
		BibleVersions = bibleVersions;
	}
	public List<Map<String, String>> getBisacReturnableIndicators() {
		return BisacReturnableIndicators;
	}
	public void setBisacReturnableIndicators(List<Map<String, String>> bisacReturnableIndicators) {
		BisacReturnableIndicators = bisacReturnableIndicators;
	}
	public List<Map<String, String>> getBookFormDetails() {
		return BookFormDetails;
	}
	public void setBookFormDetails(List<Map<String, String>> bookFormDetails) {
		BookFormDetails = bookFormDetails;
	}
	public List<Map<String, String>> getChineseSchoolGrades() {
		return ChineseSchoolGrades;
	}
	public void setChineseSchoolGrades(List<Map<String, String>> chineseSchoolGrades) {
		ChineseSchoolGrades = chineseSchoolGrades;
	}
	public List<Map<String, String>> getCitedContentTypes() {
		return CitedContentTypes;
	}
	public void setCitedContentTypes(List<Map<String, String>> citedContentTypes) {
		CitedContentTypes = citedContentTypes;
	}
	public List<Map<String, String>> getCollectionSequenceTypes() {
		return CollectionSequenceTypes;
	}
	public void setCollectionSequenceTypes(List<Map<String, String>> collectionSequenceTypes) {
		CollectionSequenceTypes = collectionSequenceTypes;
	}
	public List<Map<String, String>> getCollectionTypes() {
		return CollectionTypes;
	}
	public void setCollectionTypes(List<Map<String, String>> collectionTypes) {
		CollectionTypes = collectionTypes;
	}
	public List<Map<String, String>> getComplexitySchemeIdentifiers() {
		return ComplexitySchemeIdentifiers;
	}
	public void setComplexitySchemeIdentifiers(List<Map<String, String>> complexitySchemeIdentifiers) {
		ComplexitySchemeIdentifiers = complexitySchemeIdentifiers;
	}
	public List<Map<String, String>> getContentAudiences() {
		return ContentAudiences;
	}
	public void setContentAudiences(List<Map<String, String>> contentAudiences) {
		ContentAudiences = contentAudiences;
	}
	public List<Map<String, String>> getContentDateRoles() {
		return ContentDateRoles;
	}
	public void setContentDateRoles(List<Map<String, String>> contentDateRoles) {
		ContentDateRoles = contentDateRoles;
	}
	public List<Map<String, String>> getContentSourceTypes() {
		return ContentSourceTypes;
	}
	public void setContentSourceTypes(List<Map<String, String>> contentSourceTypes) {
		ContentSourceTypes = contentSourceTypes;
	}
	public List<Map<String, String>> getContributorPlaceRelators() {
		return ContributorPlaceRelators;
	}
	public void setContributorPlaceRelators(List<Map<String, String>> contributorPlaceRelators) {
		ContributorPlaceRelators = contributorPlaceRelators;
	}
	public List<Map<String, String>> getContributorRoles() {
		return ContributorRoles;
	}
	public void setContributorRoles(List<Map<String, String>> contributorRoles) {
		ContributorRoles = contributorRoles;
	}
	public List<Map<String, String>> getCountryCodes() {
		return CountryCodes;
	}
	public void setCountryCodes(List<Map<String, String>> countryCodes) {
		CountryCodes = countryCodes;
	}
	public List<Map<String, String>> getCurrencyCodes() {
		return CurrencyCodes;
	}
	public void setCurrencyCodes(List<Map<String, String>> currencyCodes) {
		CurrencyCodes = currencyCodes;
	}
	public List<Map<String, String>> getCurrencyZones() {
		return CurrencyZones;
	}
	public void setCurrencyZones(List<Map<String, String>> currencyZones) {
		CurrencyZones = currencyZones;
	}
	public List<Map<String, String>> getDateFormats() {
		return DateFormats;
	}
	public void setDateFormats(List<Map<String, String>> dateFormats) {
		DateFormats = dateFormats;
	}
	public List<Map<String, String>> getDefaultLinearUnits() {
		return DefaultLinearUnits;
	}
	public void setDefaultLinearUnits(List<Map<String, String>> defaultLinearUnits) {
		DefaultLinearUnits = defaultLinearUnits;
	}
	public List<Map<String, String>> getDefaultUnitOfWeights() {
		return DefaultUnitOfWeights;
	}
	public void setDefaultUnitOfWeights(List<Map<String, String>> defaultUnitOfWeights) {
		DefaultUnitOfWeights = defaultUnitOfWeights;
	}
	public List<Map<String, String>> getDiscountCodeTypes() {
		return DiscountCodeTypes;
	}
	public void setDiscountCodeTypes(List<Map<String, String>> discountCodeTypes) {
		DiscountCodeTypes = discountCodeTypes;
	}
	public List<Map<String, String>> getDiscountTypes() {
		return DiscountTypes;
	}
	public void setDiscountTypes(List<Map<String, String>> discountTypes) {
		DiscountTypes = discountTypes;
	}
	public List<Map<String, String>> getEditionTypes() {
		return EditionTypes;
	}
	public void setEditionTypes(List<Map<String, String>> editionTypes) {
		EditionTypes = editionTypes;
	}
	public List<Map<String, String>> getEpublicationAccessibilityDetailss() {
		return EpublicationAccessibilityDetailss;
	}
	public void setEpublicationAccessibilityDetailss(List<Map<String, String>> epublicationAccessibilityDetailss) {
		EpublicationAccessibilityDetailss = epublicationAccessibilityDetailss;
	}
	public List<Map<String, String>> getEpublicationFormats() {
		return EpublicationFormats;
	}
	public void setEpublicationFormats(List<Map<String, String>> epublicationFormats) {
		EpublicationFormats = epublicationFormats;
	}
	public List<Map<String, String>> getEpublicationTechnicalProtections() {
		return EpublicationTechnicalProtections;
	}
	public void setEpublicationTechnicalProtections(List<Map<String, String>> epublicationTechnicalProtections) {
		EpublicationTechnicalProtections = epublicationTechnicalProtections;
	}
	public List<Map<String, String>> getEpublicationTypes() {
		return EpublicationTypes;
	}
	public void setEpublicationTypes(List<Map<String, String>> epublicationTypes) {
		EpublicationTypes = epublicationTypes;
	}
	public List<Map<String, String>> getEpublicationVersionNumbers() {
		return EpublicationVersionNumbers;
	}
	public void setEpublicationVersionNumbers(List<Map<String, String>> epublicationVersionNumbers) {
		EpublicationVersionNumbers = epublicationVersionNumbers;
	}
	public List<Map<String, String>> getEuToySafetyDirectiveHazardWarnings() {
		return EuToySafetyDirectiveHazardWarnings;
	}
	public void setEuToySafetyDirectiveHazardWarnings(List<Map<String, String>> euToySafetyDirectiveHazardWarnings) {
		EuToySafetyDirectiveHazardWarnings = euToySafetyDirectiveHazardWarnings;
	}
	public List<Map<String, String>> getExtentTypes() {
		return ExtentTypes;
	}
	public void setExtentTypes(List<Map<String, String>> extentTypes) {
		ExtentTypes = extentTypes;
	}
	public List<Map<String, String>> getExtentUnits() {
		return ExtentUnits;
	}
	public void setExtentUnits(List<Map<String, String>> extentUnits) {
		ExtentUnits = extentUnits;
	}
	public List<Map<String, String>> getFrontCoverImageFileFormats() {
		return FrontCoverImageFileFormats;
	}
	public void setFrontCoverImageFileFormats(List<Map<String, String>> frontCoverImageFileFormats) {
		FrontCoverImageFileFormats = frontCoverImageFileFormats;
	}
	public List<Map<String, String>> getFrontCoverImageFileLinkTypes() {
		return FrontCoverImageFileLinkTypes;
	}
	public void setFrontCoverImageFileLinkTypes(List<Map<String, String>> frontCoverImageFileLinkTypes) {
		FrontCoverImageFileLinkTypes = frontCoverImageFileLinkTypes;
	}
	public List<Map<String, String>> getIllustratedNotIllustrateds() {
		return IllustratedNotIllustrateds;
	}
	public void setIllustratedNotIllustrateds(List<Map<String, String>> illustratedNotIllustrateds) {
		IllustratedNotIllustrateds = illustratedNotIllustrateds;
	}
	public List<Map<String, String>> getIllustrationAndOtherContentTypes() {
		return IllustrationAndOtherContentTypes;
	}
	public void setIllustrationAndOtherContentTypes(List<Map<String, String>> illustrationAndOtherContentTypes) {
		IllustrationAndOtherContentTypes = illustrationAndOtherContentTypes;
	}
	public List<Map<String, String>> getImageAudioVideoFileFormats() {
		return ImageAudioVideoFileFormats;
	}
	public void setImageAudioVideoFileFormats(List<Map<String, String>> imageAudioVideoFileFormats) {
		ImageAudioVideoFileFormats = imageAudioVideoFileFormats;
	}
	public List<Map<String, String>> getImageAudioVideoFileLinkTypes() {
		return ImageAudioVideoFileLinkTypes;
	}
	public void setImageAudioVideoFileLinkTypes(List<Map<String, String>> imageAudioVideoFileLinkTypes) {
		ImageAudioVideoFileLinkTypes = imageAudioVideoFileLinkTypes;
	}
	public List<Map<String, String>> getImageAudioVideoFileTypes() {
		return ImageAudioVideoFileTypes;
	}
	public void setImageAudioVideoFileTypes(List<Map<String, String>> imageAudioVideoFileTypes) {
		ImageAudioVideoFileTypes = imageAudioVideoFileTypes;
	}
	public List<Map<String, String>> getLanguageCodes() {
		return LanguageCodes;
	}
	public void setLanguageCodes(List<Map<String, String>> languageCodes) {
		LanguageCodes = languageCodes;
	}
	public List<Map<String, String>> getLanguageRoles() {
		return LanguageRoles;
	}
	public void setLanguageRoles(List<Map<String, String>> languageRoles) {
		LanguageRoles = languageRoles;
	}
	public List<Map<String, String>> getLicenseExpressionTypes() {
		return LicenseExpressionTypes;
	}
	public void setLicenseExpressionTypes(List<Map<String, String>> licenseExpressionTypes) {
		LicenseExpressionTypes = licenseExpressionTypes;
	}
	public List<Map<String, String>> getMainSubjectSchemeIdentifiers() {
		return MainSubjectSchemeIdentifiers;
	}
	public void setMainSubjectSchemeIdentifiers(List<Map<String, String>> mainSubjectSchemeIdentifiers) {
		MainSubjectSchemeIdentifiers = mainSubjectSchemeIdentifiers;
	}
	public List<Map<String, String>> getMarketDateRoles() {
		return MarketDateRoles;
	}
	public void setMarketDateRoles(List<Map<String, String>> marketDateRoles) {
		MarketDateRoles = marketDateRoles;
	}
	public List<Map<String, String>> getMarketPublishingStatuss() {
		return MarketPublishingStatuss;
	}
	public void setMarketPublishingStatuss(List<Map<String, String>> marketPublishingStatuss) {
		MarketPublishingStatuss = marketPublishingStatuss;
	}
	public List<Map<String, String>> getMeasureTypes() {
		return MeasureTypes;
	}
	public void setMeasureTypes(List<Map<String, String>> measureTypes) {
		MeasureTypes = measureTypes;
	}
	public List<Map<String, String>> getMeasureUnits() {
		return MeasureUnits;
	}
	public void setMeasureUnits(List<Map<String, String>> measureUnits) {
		MeasureUnits = measureUnits;
	}
	public List<Map<String, String>> getMessageRecordStatusDetails() {
		return MessageRecordStatusDetails;
	}
	public void setMessageRecordStatusDetails(List<Map<String, String>> messageRecordStatusDetails) {
		MessageRecordStatusDetails = messageRecordStatusDetails;
	}
	public List<Map<String, String>> getMessageStatusDateRoles() {
		return MessageStatusDateRoles;
	}
	public void setMessageStatusDateRoles(List<Map<String, String>> messageStatusDateRoles) {
		MessageStatusDateRoles = messageStatusDateRoles;
	}
	public List<Map<String, String>> getMessageStatuss() {
		return MessageStatuss;
	}
	public void setMessageStatuss(List<Map<String, String>> messageStatuss) {
		MessageStatuss = messageStatuss;
	}
	public List<Map<String, String>> getNameCodeTypes() {
		return NameCodeTypes;
	}
	public void setNameCodeTypes(List<Map<String, String>> nameCodeTypes) {
		NameCodeTypes = nameCodeTypes;
	}
	public List<Map<String, String>> getNorthAmericanSchoolOrCollegeGrades() {
		return NorthAmericanSchoolOrCollegeGrades;
	}
	public void setNorthAmericanSchoolOrCollegeGrades(List<Map<String, String>> northAmericanSchoolOrCollegeGrades) {
		NorthAmericanSchoolOrCollegeGrades = northAmericanSchoolOrCollegeGrades;
	}
	public List<Map<String, String>> getNotificationOrUpdateTypes() {
		return NotificationOrUpdateTypes;
	}
	public void setNotificationOrUpdateTypes(List<Map<String, String>> notificationOrUpdateTypes) {
		NotificationOrUpdateTypes = notificationOrUpdateTypes;
	}
	public List<Map<String, String>> getOnixAdultAudienceRatings() {
		return OnixAdultAudienceRatings;
	}
	public void setOnixAdultAudienceRatings(List<Map<String, String>> onixAdultAudienceRatings) {
		OnixAdultAudienceRatings = onixAdultAudienceRatings;
	}
	public List<Map<String, String>> getOnixRetailSalesOutletIdss() {
		return OnixRetailSalesOutletIdss;
	}
	public void setOnixRetailSalesOutletIdss(List<Map<String, String>> onixRetailSalesOutletIdss) {
		OnixRetailSalesOutletIdss = onixRetailSalesOutletIdss;
	}
	public List<Map<String, String>> getOnixReturnsConditionss() {
		return OnixReturnsConditionss;
	}
	public void setOnixReturnsConditionss(List<Map<String, String>> onixReturnsConditionss) {
		OnixReturnsConditionss = onixReturnsConditionss;
	}
	public List<Map<String, String>> getOtherTextTypes() {
		return OtherTextTypes;
	}
	public void setOtherTextTypes(List<Map<String, String>> otherTextTypes) {
		OtherTextTypes = otherTextTypes;
	}
	public List<Map<String, String>> getPersonDateRoles() {
		return PersonDateRoles;
	}
	public void setPersonDateRoles(List<Map<String, String>> personDateRoles) {
		PersonDateRoles = personDateRoles;
	}
	public List<Map<String, String>> getPersonNameIdentifierTypes() {
		return PersonNameIdentifierTypes;
	}
	public void setPersonNameIdentifierTypes(List<Map<String, String>> personNameIdentifierTypes) {
		PersonNameIdentifierTypes = personNameIdentifierTypes;
	}
	public List<Map<String, String>> getPersonOrganizationDateRoles() {
		return PersonOrganizationDateRoles;
	}
	public void setPersonOrganizationDateRoles(List<Map<String, String>> personOrganizationDateRoles) {
		PersonOrganizationDateRoles = personOrganizationDateRoles;
	}
	public List<Map<String, String>> getPersonOrganizationNameTypes() {
		return PersonOrganizationNameTypes;
	}
	public void setPersonOrganizationNameTypes(List<Map<String, String>> personOrganizationNameTypes) {
		PersonOrganizationNameTypes = personOrganizationNameTypes;
	}
	public List<Map<String, String>> getPositionOnProducts() {
		return PositionOnProducts;
	}
	public void setPositionOnProducts(List<Map<String, String>> positionOnProducts) {
		PositionOnProducts = positionOnProducts;
	}
	public List<Map<String, String>> getPriceCodeTypes() {
		return PriceCodeTypes;
	}
	public void setPriceCodeTypes(List<Map<String, String>> priceCodeTypes) {
		PriceCodeTypes = priceCodeTypes;
	}
	public List<Map<String, String>> getPriceConditionQuantityTypes() {
		return PriceConditionQuantityTypes;
	}
	public void setPriceConditionQuantityTypes(List<Map<String, String>> priceConditionQuantityTypes) {
		PriceConditionQuantityTypes = priceConditionQuantityTypes;
	}
	public List<Map<String, String>> getPriceConditionTypes() {
		return PriceConditionTypes;
	}
	public void setPriceConditionTypes(List<Map<String, String>> priceConditionTypes) {
		PriceConditionTypes = priceConditionTypes;
	}
	public List<Map<String, String>> getPriceDateRoles() {
		return PriceDateRoles;
	}
	public void setPriceDateRoles(List<Map<String, String>> priceDateRoles) {
		PriceDateRoles = priceDateRoles;
	}
	public List<Map<String, String>> getPriceIdentifierTypes() {
		return PriceIdentifierTypes;
	}
	public void setPriceIdentifierTypes(List<Map<String, String>> priceIdentifierTypes) {
		PriceIdentifierTypes = priceIdentifierTypes;
	}
	public List<Map<String, String>> getPriceStatuss() {
		return PriceStatuss;
	}
	public void setPriceStatuss(List<Map<String, String>> priceStatuss) {
		PriceStatuss = priceStatuss;
	}
	public List<Map<String, String>> getPriceTypeQualifiers() {
		return PriceTypeQualifiers;
	}
	public void setPriceTypeQualifiers(List<Map<String, String>> priceTypeQualifiers) {
		PriceTypeQualifiers = priceTypeQualifiers;
	}
	public List<Map<String, String>> getPriceTypes() {
		return PriceTypes;
	}
	public void setPriceTypes(List<Map<String, String>> priceTypes) {
		PriceTypes = priceTypes;
	}
	public List<Map<String, String>> getPrintedOnProducts() {
		return PrintedOnProducts;
	}
	public void setPrintedOnProducts(List<Map<String, String>> printedOnProducts) {
		PrintedOnProducts = printedOnProducts;
	}
	public List<Map<String, String>> getPrizeOrAwardAchievements() {
		return PrizeOrAwardAchievements;
	}
	public void setPrizeOrAwardAchievements(List<Map<String, String>> prizeOrAwardAchievements) {
		PrizeOrAwardAchievements = prizeOrAwardAchievements;
	}
	public List<Map<String, String>> getProductAvailabilitys() {
		return ProductAvailabilitys;
	}
	public void setProductAvailabilitys(List<Map<String, String>> productAvailabilitys) {
		ProductAvailabilitys = productAvailabilitys;
	}
	public List<Map<String, String>> getProductClassificationTypes() {
		return ProductClassificationTypes;
	}
	public void setProductClassificationTypes(List<Map<String, String>> productClassificationTypes) {
		ProductClassificationTypes = productClassificationTypes;
	}
	public List<Map<String, String>> getProductCompositions() {
		return ProductCompositions;
	}
	public void setProductCompositions(List<Map<String, String>> productCompositions) {
		ProductCompositions = productCompositions;
	}
	public List<Map<String, String>> getProductContactRoles() {
		return ProductContactRoles;
	}
	public void setProductContactRoles(List<Map<String, String>> productContactRoles) {
		ProductContactRoles = productContactRoles;
	}
	public List<Map<String, String>> getProductContentTypes() {
		return ProductContentTypes;
	}
	public void setProductContentTypes(List<Map<String, String>> productContentTypes) {
		ProductContentTypes = productContentTypes;
	}
	public List<Map<String, String>> getProductFormDetails() {
		return ProductFormDetails;
	}
	public void setProductFormDetails(List<Map<String, String>> productFormDetails) {
		ProductFormDetails = productFormDetails;
	}
	public List<Map<String, String>> getProductFormDetailsList175() {
		return ProductFormDetailsList175;
	}
	public void setProductFormDetailsList175(List<Map<String, String>> productFormDetailsList175) {
		ProductFormDetailsList175 = productFormDetailsList175;
	}
	public List<Map<String, String>> getProductFormFeatureTypes() {
		return ProductFormFeatureTypes;
	}
	public void setProductFormFeatureTypes(List<Map<String, String>> productFormFeatureTypes) {
		ProductFormFeatureTypes = productFormFeatureTypes;
	}
	public List<Map<String, String>> getProductFormFeatureValueBindingOrPageEdgeColors() {
		return ProductFormFeatureValueBindingOrPageEdgeColors;
	}
	public void setProductFormFeatureValueBindingOrPageEdgeColors(
			List<Map<String, String>> productFormFeatureValueBindingOrPageEdgeColors) {
		ProductFormFeatureValueBindingOrPageEdgeColors = productFormFeatureValueBindingOrPageEdgeColors;
	}
	public List<Map<String, String>> getProductFormFeatureValueDvdRegions() {
		return ProductFormFeatureValueDvdRegions;
	}
	public void setProductFormFeatureValueDvdRegions(List<Map<String, String>> productFormFeatureValueDvdRegions) {
		ProductFormFeatureValueDvdRegions = productFormFeatureValueDvdRegions;
	}
	public List<Map<String, String>> getProductFormFeatureValueOperatingSystems() {
		return ProductFormFeatureValueOperatingSystems;
	}
	public void setProductFormFeatureValueOperatingSystems(
			List<Map<String, String>> productFormFeatureValueOperatingSystems) {
		ProductFormFeatureValueOperatingSystems = productFormFeatureValueOperatingSystems;
	}
	public List<Map<String, String>> getProductFormFeatureValueSpecialCoverMaterials() {
		return ProductFormFeatureValueSpecialCoverMaterials;
	}
	public void setProductFormFeatureValueSpecialCoverMaterials(
			List<Map<String, String>> productFormFeatureValueSpecialCoverMaterials) {
		ProductFormFeatureValueSpecialCoverMaterials = productFormFeatureValueSpecialCoverMaterials;
	}
	public List<Map<String, String>> getProductForms() {
		return ProductForms;
	}
	public void setProductForms(List<Map<String, String>> productForms) {
		ProductForms = productForms;
	}
	public List<Map<String, String>> getProductFormsList150() {
		return ProductFormsList150;
	}
	public void setProductFormsList150(List<Map<String, String>> productFormsList150) {
		ProductFormsList150 = productFormsList150;
	}
	public List<Map<String, String>> getProductIdentifierTypes() {
		return ProductIdentifierTypes;
	}
	public void setProductIdentifierTypes(List<Map<String, String>> productIdentifierTypes) {
		ProductIdentifierTypes = productIdentifierTypes;
	}
	public List<Map<String, String>> getProductPackagingTypes() {
		return ProductPackagingTypes;
	}
	public void setProductPackagingTypes(List<Map<String, String>> productPackagingTypes) {
		ProductPackagingTypes = productPackagingTypes;
	}
	public List<Map<String, String>> getProductRelations() {
		return ProductRelations;
	}
	public void setProductRelations(List<Map<String, String>> productRelations) {
		ProductRelations = productRelations;
	}
	public List<Map<String, String>> getProximitys() {
		return Proximitys;
	}
	public void setProximitys(List<Map<String, String>> proximitys) {
		Proximitys = proximitys;
	}
	public List<Map<String, String>> getPublishingDateRoles() {
		return PublishingDateRoles;
	}
	public void setPublishingDateRoles(List<Map<String, String>> publishingDateRoles) {
		PublishingDateRoles = publishingDateRoles;
	}
	public List<Map<String, String>> getPublishingRoles() {
		return PublishingRoles;
	}
	public void setPublishingRoles(List<Map<String, String>> publishingRoles) {
		PublishingRoles = publishingRoles;
	}
	public List<Map<String, String>> getPublishingStatuss() {
		return PublishingStatuss;
	}
	public void setPublishingStatuss(List<Map<String, String>> publishingStatuss) {
		PublishingStatuss = publishingStatuss;
	}
	public List<Map<String, String>> getQuantityUnits() {
		return QuantityUnits;
	}
	public void setQuantityUnits(List<Map<String, String>> quantityUnits) {
		QuantityUnits = quantityUnits;
	}
	public List<Map<String, String>> getRecordSourceTypes() {
		return RecordSourceTypes;
	}
	public void setRecordSourceTypes(List<Map<String, String>> recordSourceTypes) {
		RecordSourceTypes = recordSourceTypes;
	}
	public List<Map<String, String>> getRecordStatuss() {
		return RecordStatuss;
	}
	public void setRecordStatuss(List<Map<String, String>> recordStatuss) {
		RecordStatuss = recordStatuss;
	}
	public List<Map<String, String>> getReligiousTextFeatures() {
		return ReligiousTextFeatures;
	}
	public void setReligiousTextFeatures(List<Map<String, String>> religiousTextFeatures) {
		ReligiousTextFeatures = religiousTextFeatures;
	}
	public List<Map<String, String>> getReligiousTextFeatureTypes() {
		return ReligiousTextFeatureTypes;
	}
	public void setReligiousTextFeatureTypes(List<Map<String, String>> religiousTextFeatureTypes) {
		ReligiousTextFeatureTypes = religiousTextFeatureTypes;
	}
	public List<Map<String, String>> getResourceContentTypes() {
		return ResourceContentTypes;
	}
	public void setResourceContentTypes(List<Map<String, String>> resourceContentTypes) {
		ResourceContentTypes = resourceContentTypes;
	}
	public List<Map<String, String>> getResourceFeatureTypes() {
		return ResourceFeatureTypes;
	}
	public void setResourceFeatureTypes(List<Map<String, String>> resourceFeatureTypes) {
		ResourceFeatureTypes = resourceFeatureTypes;
	}
	public List<Map<String, String>> getResourceForms() {
		return ResourceForms;
	}
	public void setResourceForms(List<Map<String, String>> resourceForms) {
		ResourceForms = resourceForms;
	}
	public List<Map<String, String>> getResourceModes() {
		return ResourceModes;
	}
	public void setResourceModes(List<Map<String, String>> resourceModes) {
		ResourceModes = resourceModes;
	}
	public List<Map<String, String>> getReturnsConditionsCodeTypes() {
		return ReturnsConditionsCodeTypes;
	}
	public void setReturnsConditionsCodeTypes(List<Map<String, String>> returnsConditionsCodeTypes) {
		ReturnsConditionsCodeTypes = returnsConditionsCodeTypes;
	}
	public List<Map<String, String>> getRightsRegions() {
		return RightsRegions;
	}
	public void setRightsRegions(List<Map<String, String>> rightsRegions) {
		RightsRegions = rightsRegions;
	}
	public List<Map<String, String>> getRightsTypes() {
		return RightsTypes;
	}
	public void setRightsTypes(List<Map<String, String>> rightsTypes) {
		RightsTypes = rightsTypes;
	}
	public List<Map<String, String>> getSalesOutletIdentifierTypes() {
		return SalesOutletIdentifierTypes;
	}
	public void setSalesOutletIdentifierTypes(List<Map<String, String>> salesOutletIdentifierTypes) {
		SalesOutletIdentifierTypes = salesOutletIdentifierTypes;
	}
	public List<Map<String, String>> getSalesRestrictionTypes() {
		return SalesRestrictionTypes;
	}
	public void setSalesRestrictionTypes(List<Map<String, String>> salesRestrictionTypes) {
		SalesRestrictionTypes = salesRestrictionTypes;
	}
	public List<Map<String, String>> getSalesRightsTypes() {
		return SalesRightsTypes;
	}
	public void setSalesRightsTypes(List<Map<String, String>> salesRightsTypes) {
		SalesRightsTypes = salesRightsTypes;
	}
	public List<Map<String, String>> getSeriesIdentifierTypes() {
		return SeriesIdentifierTypes;
	}
	public void setSeriesIdentifierTypes(List<Map<String, String>> seriesIdentifierTypes) {
		SeriesIdentifierTypes = seriesIdentifierTypes;
	}
	public List<Map<String, String>> getStatusDetailCodeTypes() {
		return StatusDetailCodeTypes;
	}
	public void setStatusDetailCodeTypes(List<Map<String, String>> statusDetailCodeTypes) {
		StatusDetailCodeTypes = statusDetailCodeTypes;
	}
	public List<Map<String, String>> getStatusDetailTypeSeveritys() {
		return StatusDetailTypeSeveritys;
	}
	public void setStatusDetailTypeSeveritys(List<Map<String, String>> statusDetailTypeSeveritys) {
		StatusDetailTypeSeveritys = statusDetailTypeSeveritys;
	}
	public List<Map<String, String>> getStockQuantityCodeTypes() {
		return StockQuantityCodeTypes;
	}
	public void setStockQuantityCodeTypes(List<Map<String, String>> stockQuantityCodeTypes) {
		StockQuantityCodeTypes = stockQuantityCodeTypes;
	}
	public List<Map<String, String>> getStudyBibleTypes() {
		return StudyBibleTypes;
	}
	public void setStudyBibleTypes(List<Map<String, String>> studyBibleTypes) {
		StudyBibleTypes = studyBibleTypes;
	}
	public List<Map<String, String>> getSubjectSchemeIdentifiers() {
		return SubjectSchemeIdentifiers;
	}
	public void setSubjectSchemeIdentifiers(List<Map<String, String>> subjectSchemeIdentifiers) {
		SubjectSchemeIdentifiers = subjectSchemeIdentifiers;
	}
	public List<Map<String, String>> getSupplierIdentifierTypes() {
		return SupplierIdentifierTypes;
	}
	public void setSupplierIdentifierTypes(List<Map<String, String>> supplierIdentifierTypes) {
		SupplierIdentifierTypes = supplierIdentifierTypes;
	}
	public List<Map<String, String>> getSupplierOwnCodeTypes() {
		return SupplierOwnCodeTypes;
	}
	public void setSupplierOwnCodeTypes(List<Map<String, String>> supplierOwnCodeTypes) {
		SupplierOwnCodeTypes = supplierOwnCodeTypes;
	}
	public List<Map<String, String>> getSupplierRoles() {
		return SupplierRoles;
	}
	public void setSupplierRoles(List<Map<String, String>> supplierRoles) {
		SupplierRoles = supplierRoles;
	}
	public List<Map<String, String>> getSupplyDateRoles() {
		return SupplyDateRoles;
	}
	public void setSupplyDateRoles(List<Map<String, String>> supplyDateRoles) {
		SupplyDateRoles = supplyDateRoles;
	}
	public List<Map<String, String>> getSupplytoRegions() {
		return SupplytoRegions;
	}
	public void setSupplytoRegions(List<Map<String, String>> supplytoRegions) {
		SupplytoRegions = supplytoRegions;
	}
	public List<Map<String, String>> getSupportingResourceFileFormats() {
		return SupportingResourceFileFormats;
	}
	public void setSupportingResourceFileFormats(List<Map<String, String>> supportingResourceFileFormats) {
		SupportingResourceFileFormats = supportingResourceFileFormats;
	}
	public List<Map<String, String>> getTaxRateCodeds() {
		return TaxRateCodeds;
	}
	public void setTaxRateCodeds(List<Map<String, String>> taxRateCodeds) {
		TaxRateCodeds = taxRateCodeds;
	}
	public List<Map<String, String>> getTaxTypes() {
		return TaxTypes;
	}
	public void setTaxTypes(List<Map<String, String>> taxTypes) {
		TaxTypes = taxTypes;
	}
	public List<Map<String, String>> getTextCaseFlags() {
		return TextCaseFlags;
	}
	public void setTextCaseFlags(List<Map<String, String>> textCaseFlags) {
		TextCaseFlags = textCaseFlags;
	}
	public List<Map<String, String>> getTextFormats() {
		return TextFormats;
	}
	public void setTextFormats(List<Map<String, String>> textFormats) {
		TextFormats = textFormats;
	}
	public List<Map<String, String>> getTextItemIdentifierTypes() {
		return TextItemIdentifierTypes;
	}
	public void setTextItemIdentifierTypes(List<Map<String, String>> textItemIdentifierTypes) {
		TextItemIdentifierTypes = textItemIdentifierTypes;
	}
	public List<Map<String, String>> getTextItemTypes() {
		return TextItemTypes;
	}
	public void setTextItemTypes(List<Map<String, String>> textItemTypes) {
		TextItemTypes = textItemTypes;
	}
	public List<Map<String, String>> getTextLinkTypes() {
		return TextLinkTypes;
	}
	public void setTextLinkTypes(List<Map<String, String>> textLinkTypes) {
		TextLinkTypes = textLinkTypes;
	}
	public List<Map<String, String>> getTextScriptCodes() {
		return TextScriptCodes;
	}
	public void setTextScriptCodes(List<Map<String, String>> textScriptCodes) {
		TextScriptCodes = textScriptCodes;
	}
	public List<Map<String, String>> getTextTypes() {
		return TextTypes;
	}
	public void setTextTypes(List<Map<String, String>> textTypes) {
		TextTypes = textTypes;
	}
	public List<Map<String, String>> getThesisTypes() {
		return ThesisTypes;
	}
	public void setThesisTypes(List<Map<String, String>> thesisTypes) {
		ThesisTypes = thesisTypes;
	}
	public List<Map<String, String>> getTitleElementLevels() {
		return TitleElementLevels;
	}
	public void setTitleElementLevels(List<Map<String, String>> titleElementLevels) {
		TitleElementLevels = titleElementLevels;
	}
	public List<Map<String, String>> getTitleTypes() {
		return TitleTypes;
	}
	public void setTitleTypes(List<Map<String, String>> titleTypes) {
		TitleTypes = titleTypes;
	}
	public List<Map<String, String>> getTradeCategorys() {
		return TradeCategorys;
	}
	public void setTradeCategorys(List<Map<String, String>> tradeCategorys) {
		TradeCategorys = tradeCategorys;
	}
	public List<Map<String, String>> getTransliterationSchemes() {
		return TransliterationSchemes;
	}
	public void setTransliterationSchemes(List<Map<String, String>> transliterationSchemes) {
		TransliterationSchemes = transliterationSchemes;
	}
	public List<Map<String, String>> getUnitOfPricings() {
		return UnitOfPricings;
	}
	public void setUnitOfPricings(List<Map<String, String>> unitOfPricings) {
		UnitOfPricings = unitOfPricings;
	}
	public List<Map<String, String>> getUnitOfUsages() {
		return UnitOfUsages;
	}
	public void setUnitOfUsages(List<Map<String, String>> unitOfUsages) {
		UnitOfUsages = unitOfUsages;
	}
	public List<Map<String, String>> getUnnamedPersonss() {
		return UnnamedPersonss;
	}
	public void setUnnamedPersonss(List<Map<String, String>> unnamedPersonss) {
		UnnamedPersonss = unnamedPersonss;
	}
	public List<Map<String, String>> getUnpricedItemTypes() {
		return UnpricedItemTypes;
	}
	public void setUnpricedItemTypes(List<Map<String, String>> unpricedItemTypes) {
		UnpricedItemTypes = unpricedItemTypes;
	}
	public List<Map<String, String>> getUsageStatuss() {
		return UsageStatuss;
	}
	public void setUsageStatuss(List<Map<String, String>> usageStatuss) {
		UsageStatuss = usageStatuss;
	}
	public List<Map<String, String>> getUsageTypes() {
		return UsageTypes;
	}
	public void setUsageTypes(List<Map<String, String>> usageTypes) {
		UsageTypes = usageTypes;
	}
	public List<Map<String, String>> getUsCpsiaChokingHazardWarnings() {
		return UsCpsiaChokingHazardWarnings;
	}
	public void setUsCpsiaChokingHazardWarnings(List<Map<String, String>> usCpsiaChokingHazardWarnings) {
		UsCpsiaChokingHazardWarnings = usCpsiaChokingHazardWarnings;
	}
	public List<Map<String, String>> getUsCpsiaHazardWarnings() {
		return UsCpsiaHazardWarnings;
	}
	public void setUsCpsiaHazardWarnings(List<Map<String, String>> usCpsiaHazardWarnings) {
		UsCpsiaHazardWarnings = usCpsiaHazardWarnings;
	}
	public List<Map<String, String>> getVelocitys() {
		return Velocitys;
	}
	public void setVelocitys(List<Map<String, String>> velocitys) {
		Velocitys = velocitys;
	}
	public List<Map<String, String>> getWebsiteRoles() {
		return WebsiteRoles;
	}
	public void setWebsiteRoles(List<Map<String, String>> websiteRoles) {
		WebsiteRoles = websiteRoles;
	}
	public List<Map<String, String>> getWorkIdentifierTypes() {
		return WorkIdentifierTypes;
	}
	public void setWorkIdentifierTypes(List<Map<String, String>> workIdentifierTypes) {
		WorkIdentifierTypes = workIdentifierTypes;
	}
	public List<Map<String, String>> getWorkRelations() {
		return WorkRelations;
	}
	public void setWorkRelations(List<Map<String, String>> workRelations) {
		WorkRelations = workRelations;
	}


	
}
