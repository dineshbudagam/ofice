/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class CollectionIdentifier
{
	public String CollectionIDType;

	public String IdTypeName;

	public String IDValue;

	public String getCollectionIDType() {
		return CollectionIDType;
	}

	public void setCollectionIDType(String collectionIDType) {
		CollectionIDType = collectionIDType;
	}

	public String getIdTypeName() {
		return IdTypeName;
	}

	public void setIdTypeName(String idTypeName) {
		IdTypeName = idTypeName;
	}

	public String getIDValue() {
		return IDValue;
	}

	public void setIDValue(String iDValue) {
		IDValue = iDValue;
	}

		
}
