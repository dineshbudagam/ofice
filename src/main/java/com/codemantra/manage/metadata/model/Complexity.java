/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class Complexity
{
	public String ComplexityCode;

	public String ComplexitySchemeIdentifiers;

	public String getComplexityCode() {
		return ComplexityCode;
	}

	public void setComplexityCode(String complexityCode) {
		ComplexityCode = complexityCode;
	}

	public String getComplexitySchemeIdentifiers() {
		return ComplexitySchemeIdentifiers;
	}

	public void setComplexitySchemeIdentifiers(String complexitySchemeIdentifiers) {
		ComplexitySchemeIdentifiers = complexitySchemeIdentifiers;
	}

		
}
