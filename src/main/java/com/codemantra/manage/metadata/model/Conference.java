/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class Conference
{
	public String ConferenceRole;

	public String ConferenceName;

	public String ConferenceAcronym;

	public String ConferenceNumber;

	public String ConferenceTheme;

	public String ConferenceDate;

	public String ConferencePlace;

	public List<ConferenceSponsor> ConferenceSponsor;

	public List<Website> Website;

	public String getConferenceRole() {
		return ConferenceRole;
	}

	public void setConferenceRole(String conferenceRole) {
		ConferenceRole = conferenceRole;
	}

	public String getConferenceName() {
		return ConferenceName;
	}

	public void setConferenceName(String conferenceName) {
		ConferenceName = conferenceName;
	}

	public String getConferenceAcronym() {
		return ConferenceAcronym;
	}

	public void setConferenceAcronym(String conferenceAcronym) {
		ConferenceAcronym = conferenceAcronym;
	}

	public String getConferenceNumber() {
		return ConferenceNumber;
	}

	public void setConferenceNumber(String conferenceNumber) {
		ConferenceNumber = conferenceNumber;
	}

	public String getConferenceTheme() {
		return ConferenceTheme;
	}

	public void setConferenceTheme(String conferenceTheme) {
		ConferenceTheme = conferenceTheme;
	}

	public String getConferenceDate() {
		return ConferenceDate;
	}

	public void setConferenceDate(String conferenceDate) {
		ConferenceDate = conferenceDate;
	}

	public String getConferencePlace() {
		return ConferencePlace;
	}

	public void setConferencePlace(String conferencePlace) {
		ConferencePlace = conferencePlace;
	}

	public List<ConferenceSponsor> getConferenceSponsor() {
		return ConferenceSponsor;
	}

	public void setConferenceSponsor(List<ConferenceSponsor> conferenceSponsor) {
		ConferenceSponsor = conferenceSponsor;
	}

	public List<Website> getWebsite() {
		return Website;
	}

	public void setWebsite(List<Website> website) {
		Website = website;
	}

}
