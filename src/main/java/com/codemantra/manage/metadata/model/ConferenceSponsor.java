/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
21-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class ConferenceSponsor
{
	public ConferenceSponsorIdentifier ConferenceSponsorIdentifier;

	public String PersonName;

	public String CorporateName;

	public ConferenceSponsorIdentifier getConferenceSponsorIdentifier() {
		return ConferenceSponsorIdentifier;
	}

	public void setConferenceSponsorIdentifier(ConferenceSponsorIdentifier conferenceSponsorIdentifier) {
		ConferenceSponsorIdentifier = conferenceSponsorIdentifier;
	}

	public String getPersonName() {
		return PersonName;
	}

	public void setPersonName(String personName) {
		PersonName = personName;
	}

	public String getCorporateName() {
		return CorporateName;
	}

	public void setCorporateName(String corporateName) {
		CorporateName = corporateName;
	}

}
