/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class ConferenceSponsorIdentifier
{
	public String ConferenceSponsorIDType;

	public String IdTypeName;

	public String IdValue;

	public String getConferenceSponsorIDType() {
		return ConferenceSponsorIDType;
	}

	public void setConferenceSponsorIDType(String conferenceSponsorIDType) {
		ConferenceSponsorIDType = conferenceSponsorIDType;
	}

	public String getIdTypeName() {
		return IdTypeName;
	}

	public void setIdTypeName(String idTypeName) {
		IdTypeName = idTypeName;
	}

	public String getIdValue() {
		return IdValue;
	}

	public void setIdValue(String idValue) {
		IdValue = idValue;
	}
}
