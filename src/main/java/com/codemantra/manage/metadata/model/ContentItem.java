/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
16-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class ContentItem
{
	public String LevelSequenceNumber;

	public String TextItem;

	public List<Website> Website;

	public String ComponentTypeName;

	public String ComponentNumber;

	public String DistinctiveTitle;

	public List<Title> Title;

	public List<WorkIdentifier> WorkIdentifier;

	public List<Contributor> Contributor;

	public String ContributorStatement;

	public List<Subject> Subject;

	public List<PersonAsSubject> PersonAsSubject;

	public List<String> CorporateBodyAsSubject;

	public List<String> PlaceAsSubject;

	public List<OtherText> OtherText;

	public List<MediaFile> MediaFile;

	public String getLevelSequenceNumber() {
		return LevelSequenceNumber;
	}

	public void setLevelSequenceNumber(String levelSequenceNumber) {
		LevelSequenceNumber = levelSequenceNumber;
	}

	public String getTextItem() {
		return TextItem;
	}

	public void setTextItem(String textItem) {
		TextItem = textItem;
	}

	public List<Website> getWebsite() {
		return Website;
	}

	public void setWebsite(List<Website> website) {
		Website = website;
	}

	public String getComponentTypeName() {
		return ComponentTypeName;
	}

	public void setComponentTypeName(String componentTypeName) {
		ComponentTypeName = componentTypeName;
	}

	public String getComponentNumber() {
		return ComponentNumber;
	}

	public void setComponentNumber(String componentNumber) {
		ComponentNumber = componentNumber;
	}

	public String getDistinctiveTitle() {
		return DistinctiveTitle;
	}

	public void setDistinctiveTitle(String distinctiveTitle) {
		DistinctiveTitle = distinctiveTitle;
	}

	public List<Title> getTitle() {
		return Title;
	}

	public void setTitle(List<Title> title) {
		Title = title;
	}

	public List<WorkIdentifier> getWorkIdentifier() {
		return WorkIdentifier;
	}

	public void setWorkIdentifier(List<WorkIdentifier> workIdentifier) {
		WorkIdentifier = workIdentifier;
	}

	public List<Contributor> getContributor() {
		return Contributor;
	}

	public void setContributor(List<Contributor> contributor) {
		Contributor = contributor;
	}

	public String getContributorStatement() {
		return ContributorStatement;
	}

	public void setContributorStatement(String contributorStatement) {
		ContributorStatement = contributorStatement;
	}

	public List<Subject> getSubject() {
		return Subject;
	}

	public void setSubject(List<Subject> subject) {
		Subject = subject;
	}

	public List<PersonAsSubject> getPersonAsSubject() {
		return PersonAsSubject;
	}

	public void setPersonAsSubject(List<PersonAsSubject> personAsSubject) {
		PersonAsSubject = personAsSubject;
	}

	public List<String> getCorporateBodyAsSubject() {
		return CorporateBodyAsSubject;
	}

	public void setCorporateBodyAsSubject(List<String> corporateBodyAsSubject) {
		CorporateBodyAsSubject = corporateBodyAsSubject;
	}

	public List<String> getPlaceAsSubject() {
		return PlaceAsSubject;
	}

	public void setPlaceAsSubject(List<String> placeAsSubject) {
		PlaceAsSubject = placeAsSubject;
	}

	public List<OtherText> getOtherText() {
		return OtherText;
	}

	public void setOtherText(List<OtherText> otherText) {
		OtherText = otherText;
	}

	public List<MediaFile> getMediaFile() {
		return MediaFile;
	}

	public void setMediaFile(List<MediaFile> mediaFile) {
		MediaFile = mediaFile;
	}

		
}
