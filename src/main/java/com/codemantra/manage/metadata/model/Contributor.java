/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class Contributor
{
	public String SequenceNumber;

	public String ContributorRole;
	
	public String PersonNameKey;
	
	public String PersonNameBeforeKey;

	public List<String> LanguageCode;

	public String SequenceNumberWithinRole;

	public String PersonName;
	
	public String PersonNameInverted;
	
	public String TitlesBeforeNames;
	
	public String NamesBeforeKey;
	
	public String PrefixToKey;
	
	public String KeyNames;
	
	public String NamesAfterKey;
	
	public String SuffixToKey;
	
	public String LettersAfterNames;
	
	public String TitlesAfterNames;
	
	public List<String> Name;
	
	public List<PersonNameIdentifier> PersonNameIdentifier;
	
	public List<String> PersonDate;
	
	public List<ProfessionalAffiliation> ProfessionalAffiliation;

	public String CorporateName;

	public String BiographicalNote;

	public List<Website> Website;

	public String ContributorDescription;
	
	public String UnnamedPersons;
	
	public List<String> CountryCode;
	
	public List<String> RegionCode;

	public String getSequenceNumber() {
		return SequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		SequenceNumber = sequenceNumber;
	}

	public String getContributorRole() {
		return ContributorRole;
	}

	public void setContributorRole(String contributorRole) {
		ContributorRole = contributorRole;
	}

	public String getPersonNameKey() {
		return PersonNameKey;
	}

	public void setPersonNameKey(String personNameKey) {
		PersonNameKey = personNameKey;
	}

	public String getPersonNameBeforeKey() {
		return PersonNameBeforeKey;
	}

	public void setPersonNameBeforeKey(String personNameBeforeKey) {
		PersonNameBeforeKey = personNameBeforeKey;
	}

	public List<String> getLanguageCode() {
		return LanguageCode;
	}

	public void setLanguageCode(List<String> languageCode) {
		LanguageCode = languageCode;
	}

	public String getSequenceNumberWithinRole() {
		return SequenceNumberWithinRole;
	}

	public void setSequenceNumberWithinRole(String sequenceNumberWithinRole) {
		SequenceNumberWithinRole = sequenceNumberWithinRole;
	}

	public String getPersonName() {
		return PersonName;
	}

	public void setPersonName(String personName) {
		PersonName = personName;
	}

	public String getPersonNameInverted() {
		return PersonNameInverted;
	}

	public void setPersonNameInverted(String personNameInverted) {
		PersonNameInverted = personNameInverted;
	}

	public String getTitlesBeforeNames() {
		return TitlesBeforeNames;
	}

	public void setTitlesBeforeNames(String titlesBeforeNames) {
		TitlesBeforeNames = titlesBeforeNames;
	}

	public String getNamesBeforeKey() {
		return NamesBeforeKey;
	}

	public void setNamesBeforeKey(String namesBeforeKey) {
		NamesBeforeKey = namesBeforeKey;
	}

	public String getPrefixToKey() {
		return PrefixToKey;
	}

	public void setPrefixToKey(String prefixToKey) {
		PrefixToKey = prefixToKey;
	}

	public String getKeyNames() {
		return KeyNames;
	}

	public void setKeyNames(String keyNames) {
		KeyNames = keyNames;
	}

	public String getNamesAfterKey() {
		return NamesAfterKey;
	}

	public void setNamesAfterKey(String namesAfterKey) {
		NamesAfterKey = namesAfterKey;
	}

	public String getSuffixToKey() {
		return SuffixToKey;
	}

	public void setSuffixToKey(String suffixToKey) {
		SuffixToKey = suffixToKey;
	}

	public String getLettersAfterNames() {
		return LettersAfterNames;
	}

	public void setLettersAfterNames(String lettersAfterNames) {
		LettersAfterNames = lettersAfterNames;
	}

	public String getTitlesAfterNames() {
		return TitlesAfterNames;
	}

	public void setTitlesAfterNames(String titlesAfterNames) {
		TitlesAfterNames = titlesAfterNames;
	}

	public List<String> getName() {
		return Name;
	}

	public void setName(List<String> name) {
		Name = name;
	}

	public List<PersonNameIdentifier> getPersonNameIdentifier() {
		return PersonNameIdentifier;
	}

	public void setPersonNameIdentifier(List<PersonNameIdentifier> personNameIdentifier) {
		PersonNameIdentifier = personNameIdentifier;
	}

	public List<String> getPersonDate() {
		return PersonDate;
	}

	public void setPersonDate(List<String> personDate) {
		PersonDate = personDate;
	}

	public List<ProfessionalAffiliation> getProfessionalAffiliation() {
		return ProfessionalAffiliation;
	}

	public void setProfessionalAffiliation(List<ProfessionalAffiliation> professionalAffiliation) {
		ProfessionalAffiliation = professionalAffiliation;
	}

	public String getCorporateName() {
		return CorporateName;
	}

	public void setCorporateName(String corporateName) {
		CorporateName = corporateName;
	}

	public String getBiographicalNote() {
		return BiographicalNote;
	}

	public void setBiographicalNote(String biographicalNote) {
		BiographicalNote = biographicalNote;
	}

	public List<Website> getWebsite() {
		return Website;
	}

	public void setWebsite(List<Website> website) {
		Website = website;
	}

	public String getContributorDescription() {
		return ContributorDescription;
	}

	public void setContributorDescription(String contributorDescription) {
		ContributorDescription = contributorDescription;
	}

	public String getUnnamedPersons() {
		return UnnamedPersons;
	}

	public void setUnnamedPersons(String unnamedPersons) {
		UnnamedPersons = unnamedPersons;
	}

	public List<String> getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(List<String> countryCode) {
		CountryCode = countryCode;
	}

	public List<String> getRegionCode() {
		return RegionCode;
	}

	public void setRegionCode(List<String> regionCode) {
		RegionCode = regionCode;
	}	
}