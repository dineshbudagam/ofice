/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class CopyrightOwner
{
	public CopyrightOwnerIdentifier CopyrightOwnerIdentifier;

	public String PersonName;

	public String CorporateName;

	public CopyrightOwnerIdentifier getCopyrightOwnerIdentifier() {
		return CopyrightOwnerIdentifier;
	}

	public void setCopyrightOwnerIdentifier(CopyrightOwnerIdentifier copyrightOwnerIdentifier) {
		CopyrightOwnerIdentifier = copyrightOwnerIdentifier;
	}

	public String getPersonName() {
		return PersonName;
	}

	public void setPersonName(String personName) {
		PersonName = personName;
	}

	public String getCorporateName() {
		return CorporateName;
	}

	public void setCorporateName(String corporateName) {
		CorporateName = corporateName;
	}

}
