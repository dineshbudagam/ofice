/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class CopyrightOwnerIdentifier
{
	public String CopyrightOwnerIDType;

	public String IdTypeName;

	public String IdValue;

	public String getCopyrightOwnerIDType() {
		return CopyrightOwnerIDType;
	}

	public void setCopyrightOwnerIDType(String copyrightOwnerIDType) {
		CopyrightOwnerIDType = copyrightOwnerIDType;
	}

	public String getIdTypeName() {
		return IdTypeName;
	}

	public void setIdTypeName(String idTypeName) {
		IdTypeName = idTypeName;
	}

	public String getIdValue() {
		return IdValue;
	}

	public void setIdValue(String idValue) {
		IdValue = idValue;
	}
}
