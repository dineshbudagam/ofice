/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class CopyrightStatement
{
	public List<String> CopyrightYear;

	public List<CopyrightOwner> CopyrightOwner;

	public List<String> getCopyrightYear() {
		return CopyrightYear;
	}

	public void setCopyrightYear(List<String> copyrightYear) {
		CopyrightYear = copyrightYear;
	}

	public List<CopyrightOwner> getCopyrightOwner() {
		return CopyrightOwner;
	}

	public void setCopyrightOwner(List<CopyrightOwner> copyrightOwner) {
		CopyrightOwner = copyrightOwner;
	}

	
}
