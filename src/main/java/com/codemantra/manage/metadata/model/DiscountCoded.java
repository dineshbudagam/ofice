/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class DiscountCoded
{
	public String DiscountCodeType;

	public String DiscountCode;

	public String DiscountCodeTypeName;

	public String getDiscountCodeType() {
		return DiscountCodeType;
	}

	public void setDiscountCodeType(String discountCodeType) {
		DiscountCodeType = discountCodeType;
	}

	public String getDiscountCode() {
		return DiscountCode;
	}

	public void setDiscountCode(String discountCode) {
		DiscountCode = discountCode;
	}

	public String getDiscountCodeTypeName() {
		return DiscountCodeTypeName;
	}

	public void setDiscountCodeTypeName(String discountCodeTypeName) {
		DiscountCodeTypeName = discountCodeTypeName;
	}

}
