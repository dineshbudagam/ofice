/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class Extent
{
	public String ExtentType;

	public String ExtentUnit;

	public String ExtentValue;

	public String getExtentType() {
		return ExtentType;
	}

	public void setExtentType(String extentType) {
		ExtentType = extentType;
	}

	public String getExtentUnit() {
		return ExtentUnit;
	}

	public void setExtentUnit(String extentUnit) {
		ExtentUnit = extentUnit;
	}

	public String getExtentValue() {
		return ExtentValue;
	}

	public void setExtentValue(String extentValue) {
		ExtentValue = extentValue;
	}
	
}
