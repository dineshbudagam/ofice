package com.codemantra.manage.metadata.model;

public class FileObj {
	String path;
        private String lookupName;
        private String userId;
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

    /**
     * @return the lookupName
     */
    public String getLookupName() {
        return lookupName;
    }

    /**
     * @param lookupName the lookupName to set
     */
    public void setLookupName(String lookupName) {
        this.lookupName = lookupName;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
	
        
	

}
