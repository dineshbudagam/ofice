/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

import java.util.List;

public class GridHeaders
{
	public String userId;
	
	public String fieldId;
	
	public String fieldName;

	public String displayName;
	
	public int displayOrder;
	
	public boolean canDisplay;
	
	public boolean canActive;
	
	public boolean canDefault;
	
	public boolean canChange;
	
	public String pageName;
	
	public List<GridHeaders> headers;

	public String fieldType;
	
	public String lookUp;
	
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public boolean isCanDisplay() {
		return canDisplay;
	}

	public void setCanDisplay(boolean canDisplay) {
		this.canDisplay = canDisplay;
	}

	public boolean isCanActive() {
		return canActive;
	}

	public void setCanActive(boolean canActive) {
		this.canActive = canActive;
	}

	public boolean isCanDefault() {
		return canDefault;
	}

	public void setCanDefault(boolean canDefault) {
		this.canDefault = canDefault;
	}

	public boolean isCanChange() {
		return canChange;
	}

	public void setCanChange(boolean canChange) {
		this.canChange = canChange;
	}

	public List<GridHeaders> getHeaders() {
		return headers;
	}

	public void setHeaders(List<GridHeaders> headers) {
		this.headers = headers;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getLookUp() {
		return lookUp;
	}

	public void setLookUp(String lookUp) {
		this.lookUp = lookUp;
	}
}
