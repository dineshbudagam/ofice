/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class Header
{
	public String FromCompany;
	public String FromPerson;
	public String FromEmail;
	public List<String> ToCompany;
	public String SentDate;
	
	public String getFromCompany() {
		return FromCompany;
	}
	public void setFromCompany(String fromCompany) {
		FromCompany = fromCompany;
	}
	public String getFromPerson() {
		return FromPerson;
	}
	public void setFromPerson(String fromPerson) {
		FromPerson = fromPerson;
	}
	public String getFromEmail() {
		return FromEmail;
	}
	public void setFromEmail(String fromEmail) {
		FromEmail = fromEmail;
	}
	public List<String> getToCompany() {
		return ToCompany;
	}
	public void setToCompany(List<String> toCompany) {
		ToCompany = toCompany;
	}
	public String getSentDate() {
		return SentDate;
	}
	public void setSentDate(String sentDate) {
		SentDate = sentDate;
	}

	/*@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("FromCompany: ").append(FromCompany).append("\n");
		sb.append("FromPerson:  ").append(FromPerson).append("\n");
		sb.append("FromEmail:   ").append(FromEmail).append("\n");
		sb.append("ToCompany:   ").append(ToCompany).append("\n");
		sb.append("SentDate:    ").append(SentDate);
		return sb.toString();
	}*/
}
