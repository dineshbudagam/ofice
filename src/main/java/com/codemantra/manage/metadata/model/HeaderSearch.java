/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class HeaderSearch {

	private String metaDataType;
	
	private String metaDataCategory;
	
	private String searchText;
	
	public String getMetaDataType() {
		return metaDataType;
	}

	public void setMetaDataType(String metaDataType) {
		this.metaDataType = metaDataType;
	}

	public String getMetaDataCategory() {
		return metaDataCategory;
	}

	public void setMetaDataCategory(String metaDataCategory) {
		this.metaDataCategory = metaDataCategory;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
}
