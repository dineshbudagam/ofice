/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class Illustrations
{
	public String IllustrationType;

	public String IllustrationTypeDescription;

	public String Number;

	public String getIllustrationType() {
		return IllustrationType;
	}

	public void setIllustrationType(String illustrationType) {
		IllustrationType = illustrationType;
	}

	public String getIllustrationTypeDescription() {
		return IllustrationTypeDescription;
	}

	public void setIllustrationTypeDescription(String illustrationTypeDescription) {
		IllustrationTypeDescription = illustrationTypeDescription;
	}

	public String getNumber() {
		return Number;
	}

	public void setNumber(String number) {
		Number = number;
	}

	
	
}
