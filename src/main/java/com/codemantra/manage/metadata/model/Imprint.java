/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class Imprint
{
	public String NameCodeType;
	public String NameCodeTypeName;
	public String NameCodeValue;
	public String ImprintName;
	
	public String getNameCodeType() {
		return NameCodeType;
	}
	public void setNameCodeType(String nameCodeType) {
		NameCodeType = nameCodeType;
	}
	public String getNameCodeTypeName() {
		return NameCodeTypeName;
	}
	public void setNameCodeTypeName(String nameCodeTypeName) {
		NameCodeTypeName = nameCodeTypeName;
	}
	public String getNameCodeValue() {
		return NameCodeValue;
	}
	public void setNameCodeValue(String nameCodeValue) {
		NameCodeValue = nameCodeValue;
	}
	public String getImprintName() {
		return ImprintName;
	}
	public void setImprintName(String imprintName) {
		ImprintName = imprintName;
	}
	
	
}