/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class ImprintAssetSearch {
	
	private String[] imprintNames;
	
	private String[] assetDescription;

	public String[] getImprintNames() {
		return imprintNames;
	}

	public void setImprintNames(String[] imprintNames) {
		this.imprintNames = imprintNames;
	}

	public String[] getAssetDescription() {
		return assetDescription;
	}

	public void setAssetDescription(String[] assetDescription) {
		this.assetDescription = assetDescription;
	}
}
