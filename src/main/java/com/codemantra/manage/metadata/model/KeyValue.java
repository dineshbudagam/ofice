/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
24-07-2017		v1.0       	  Saravanan K	        Initial Version.
***********************************************************************************/
package com.codemantra.manage.metadata.model;

public class KeyValue {
	private String key;
	private Object value;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	
}
