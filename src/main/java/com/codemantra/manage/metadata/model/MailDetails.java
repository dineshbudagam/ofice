/**********************************************************************************
Date          	Version       Modified By      			Description  
***********		********     *************				*************	
19-06-2017		v1.0       	 Bharath Prasanna Y V	  	Initial Version.
***********************************************************************************/
package com.codemantra.manage.metadata.model;

import java.util.Map;

public class MailDetails {

	private String to_email;
	private String cc_email;
	private String contents;
	private String subject;
	private String email_from;
	private String header;
	private String header_value;
	private String company_email;
	private Map<String, Object> templateData;
	private String templateName;
	private String createdBy; 
	
	public String getTo_email() {
		return to_email;
	}

	public void setTo_email(String to_email) {
		this.to_email = to_email;
	}

	public String getCc_email() {
		return cc_email;
	}

	public void setCc_email(String cc_email) {
		this.cc_email = cc_email;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getEmail_from() {
		return email_from;
	}

	public void setEmail_from(String email_from) {
		this.email_from = email_from;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getHeader_value() {
		return header_value;
	}

	public void setHeader_value(String header_value) {
		this.header_value = header_value;
	}

	public String getCompany_email() {
		return company_email;
	}

	public void setCompany_email(String company_email) {
		this.company_email = company_email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Map<String, Object> getTemplateData() {
		return templateData;
	}

	public void setTemplateData(Map<String, Object> templateData) {
		this.templateData = templateData;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}
