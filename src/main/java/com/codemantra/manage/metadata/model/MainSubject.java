/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class MainSubject
{
	public String MainSubjectSchemeIdentifier;

	public String SubjectSchemeVersion;

	public String SubjectCode;

	public String SubjectHeadingText;

	public String getMainSubjectSchemeIdentifier() {
		return MainSubjectSchemeIdentifier;
	}

	public void setMainSubjectSchemeIdentifier(String mainSubjectSchemeIdentifier) {
		MainSubjectSchemeIdentifier = mainSubjectSchemeIdentifier;
	}

	public String getSubjectSchemeVersion() {
		return SubjectSchemeVersion;
	}

	public void setSubjectSchemeVersion(String subjectSchemeVersion) {
		SubjectSchemeVersion = subjectSchemeVersion;
	}

	public String getSubjectCode() {
		return SubjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		SubjectCode = subjectCode;
	}

	public String getSubjectHeadingText() {
		return SubjectHeadingText;
	}

	public void setSubjectHeadingText(String subjectHeadingText) {
		SubjectHeadingText = subjectHeadingText;
	}

}
