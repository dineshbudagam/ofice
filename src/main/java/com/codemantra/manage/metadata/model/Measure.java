/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class Measure
{
	public String MeasureTypeCode;

	public String MeasureUnitCode;

	public Double Measurement;

	

	public String getMeasureUnitCode() {
		return MeasureUnitCode;
	}

	public void setMeasureUnitCode(String measureUnitCode) {
		MeasureUnitCode = measureUnitCode;
	}

	public Double getMeasurement() {
		return Measurement;
	}

	public void setMeasurement(Double measurement) {
		Measurement = measurement;
	}

	public String getMeasureTypeCode() {
		return MeasureTypeCode;
	}

	public void setMeasureTypeCode(String measureTypeCode) {
		MeasureTypeCode = measureTypeCode;
	}
	
}
