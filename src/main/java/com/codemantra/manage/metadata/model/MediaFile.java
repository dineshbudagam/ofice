/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class MediaFile
{
	public String MediaFileTypeCode;

	public String MediaFileFormatCode;

	public String ImageResolution;

	public String MediaFileLinkTypeCode;

	public String MediaFileLink;

	public String TextWithDownload;

	public String DownloadCaption;

	public String DownloadCredit;

	public String DownloadCopyrightNotice;

	public String DownloadTerms;

	public String MediaFileDate;

	public String getMediaFileTypeCode() {
		return MediaFileTypeCode;
	}

	public void setMediaFileTypeCode(String mediaFileTypeCode) {
		MediaFileTypeCode = mediaFileTypeCode;
	}

	public String getMediaFileFormatCode() {
		return MediaFileFormatCode;
	}

	public void setMediaFileFormatCode(String mediaFileFormatCode) {
		MediaFileFormatCode = mediaFileFormatCode;
	}

	public String getImageResolution() {
		return ImageResolution;
	}

	public void setImageResolution(String imageResolution) {
		ImageResolution = imageResolution;
	}

	public String getMediaFileLinkTypeCode() {
		return MediaFileLinkTypeCode;
	}

	public void setMediaFileLinkTypeCode(String mediaFileLinkTypeCode) {
		MediaFileLinkTypeCode = mediaFileLinkTypeCode;
	}

	public String getMediaFileLink() {
		return MediaFileLink;
	}

	public void setMediaFileLink(String mediaFileLink) {
		MediaFileLink = mediaFileLink;
	}

	public String getTextWithDownload() {
		return TextWithDownload;
	}

	public void setTextWithDownload(String textWithDownload) {
		TextWithDownload = textWithDownload;
	}

	public String getDownloadCaption() {
		return DownloadCaption;
	}

	public void setDownloadCaption(String downloadCaption) {
		DownloadCaption = downloadCaption;
	}

	public String getDownloadCredit() {
		return DownloadCredit;
	}

	public void setDownloadCredit(String downloadCredit) {
		DownloadCredit = downloadCredit;
	}

	public String getDownloadCopyrightNotice() {
		return DownloadCopyrightNotice;
	}

	public void setDownloadCopyrightNotice(String downloadCopyrightNotice) {
		DownloadCopyrightNotice = downloadCopyrightNotice;
	}

	public String getDownloadTerms() {
		return DownloadTerms;
	}

	public void setDownloadTerms(String downloadTerms) {
		DownloadTerms = downloadTerms;
	}

	public String getMediaFileDate() {
		return MediaFileDate;
	}

	public void setMediaFileDate(String mediaFileDate) {
		MediaFileDate = mediaFileDate;
	}

}
