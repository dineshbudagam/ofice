/**********************************************************************************************************************
RDate          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MetaData
{	
	
	public String PubKey;
	public String LanguageKey;
	
	public String PinNo;
	public String RecordReference;
	
	public String NotificationType;

	public String DeletionCode;
	
	public String DeletionText;
	
	public String RecordSourceType;
	
	public String RecordSourceIdentifierType;
	
	public String RecordSourceIdentifier;
	
	public String RecordSourceName;
	
	public String ISBN;
	
	public String EAN13;
	
	public String UPC;

	public String PublisherProductNo;

	public String ISMN;

	public String DOI;

	public List<ProductIdentifier> ProductIdentifier;
	
	public String ReplacesISBN;

	public String ReplacesEAN13;

	public List<String> ProductFormDetail;

	public List<ProductFormFeature> ProductFormFeature;

	public List<String> BookFormDetail;

	public String ProductPackaging;

	public String ProductFormDescription;

	public String NumberOfPieces;
	
	public String TradeCategory;

	public List<String> ProductContentType;

	/*public List<ContainedItem> ContainedItem;*/

	public List<ProductClassification> ProductClassification;

	public String EpubType;

	public String EpubTypeVersion;
	
	public String EpubTypeDescription;
	
	public String EpubFormat;
	
	public String EpubFormatVersion;

	public String EpubFormatDescription;
	
	public String EpubSource;
	
	public String EpubSourceVersion;
	
	public String EpubSourceDescription;
	
	public String EpubTypeNote;

	public List<Series> Series;

	public String NoSeries;

	public List<Set> Sets;

	public String TextCaseFlag;

	public String DistinctiveTitle;
	
	public String TitlePrefix;
	
	public String TitleWithoutPrefix;
	
	public String Subtitle;
	
	public String TranslationOfTitle;

	public List<String> FormerTitle;

	public List<Title> Title;

	public List<WorkIdentifier> WorkIdentifier;

	public List<Website> Website;
	
	public String ThesisType;
	
	public String ThesisPresentedTo;
	
	public String ThesisYear;

	public List<Contributor> Contributor;
	
	public String ContributorStatement;
	
	public String NoContributor;
	
	public String ConferenceDescription;
	
	public String ConferenceRole;
	
	public String ConferenceName;
	
	public String ConferenceNumber;
	
	public String ConferenceDate;
	
	public String ConferencePlace;

	public List<Conference> Conference;

	/*public List<EditionTypeCode> EditionTypeCode;*/
	
	public String EditionNumber;
	
	public String EditionVersionNumber;
	
	public String EditionStatement;
	
	public String NoEdition;
	
	public String ReligiousText;

	/*public List<LanguageOfText> LanguageOfText;*/
	
	public String OriginalLanguage;

	public List<Language> Language;
	
	public String NumberOfPages;
	
	public String PagesRoman;
	
	public String PagesArabic;

	public List<Extent> Extent;
	
	public String NumberOfIllustrations;
	
	public String IllustrationsNote;

	public List<Illustrations> Illustrations;

	/*public List<MapScale> MapScale;*/
	
	public String BASICMainSubject;
	
	public String BASICVersion;
	
	public String BICMainSubject;
	
	public String BICVersion;

	public List<MainSubject> MainSubject;

	public List<Subject> Subject;

	public List<PersonAsSubject> PersonAsSubject;

	/*public List<CorporateBodyAsSubject> CorporateBodyAsSubject;

	public List<PlaceAsSubject> PlaceAsSubject;*/

	public List<Audience> Audience;
	
	public String USSchoolGrade;
	
	public String InterestAge;

	public List<AudienceRange> AudienceRange;
	
	public String AudienceDescription;

	public List<Complexity> Complexity;
	
	public String Annotation;
	
	public String MainDescription;

	public List<OtherText> OtherText;

	public List<String> ReviewQuote;
	
	public String CoverImageFormatCode;
	
	public String CoverImageLinkTypeCode;
	
	public String CoverImageLink;

	public List<MediaFile> MediaFile;

	public List<ProductWebsite> ProductWebsite;
	
	public String PrizesDescription;

	public List<Prize> Prize;

	public List<ContentItem> ContentItem;
	
	public List<Imprint> Imprint;
	
	public List<Publisher> Publisher;

	public String CityOfPublication;

	public String CountryOfPublication;

	public List<String> CopublisherName;

	public List<String> SponsorName;
	
	public String OriginalPublisher;

	public String PublishingStatus;
	
	public String PublishingStatusNote;
	
	public String AnnouncementDate;
	
	public String TradeAnnouncementDate;
	
	public String PublicationDate;

	public List<CopyrightStatement> CopyrightStatement;
	
	public String CopyrightYear;
	
	public String YearFirstPublished;

	public List<SalesRights> SalesRights;

	public List<NotForSale> NotForSale;

	public List<SalesRestriction> SalesRestriction;
	
	public String Height;
	
	public String Width;
	
	public String Thickness;
	
	public String Weight;

	public List<Measure> Measure;
	
	public String Dimensions;
	
	public String ReplacedByISBN;
	
	public String ReplacedByEAN13;
	
	public String AlternativeFormatISBN;
	
	public String AlternativeFormatEAN13;
	
	public String AlternativeProductISBN;
	
	public String AlternativeProductEAN13;

	public List<RelatedProduct> RelatedProduct;
	
	public String OutOfPrintDate;

	public List<SupplyDetail> SupplyDetail;

	/*public List<MarketRepresentation> MarketRepresentation;*/
	
	public String PromotionCampaign;
	
	public String PromotionContact;
	
	public String InitialPrintRun;

	/*public List<ReprintDetail> ReprintDetail;*/
	
	public String CopiesSold;
	
	public String BookClubAdoption;

	public String EditionType;

	public String ProductForm;
	
	public List<String> AudienceCode;
	
	public String MainTitle;
	
	public String NumberWithinSeries;
	
	public List<CollectionIdentifier> SeriesIdentifier;
	
	public List<Set> Set;
	
	public String BarcodeIndicator;
	
	public String UsageType;
	
	public String BisacReturnableIndicator;
	
	public String LSIPublisherID;
	
	/*public String PreflightStatus;*/
	
	public Date LastModifiedOn;
	
	public String LastModifiedBy;
	
	public boolean isDeleted;
	
	public boolean isActive= true;
	
	public String getRecordReference() {
		return RecordReference;
	}

	public void setRecordReference(String recordReference) {
		RecordReference = recordReference;
	}

	public String getNotificationType() {
		return NotificationType;
	}

	public void setNotificationType(String notificationType) {
		NotificationType = notificationType;
	}

	public String getDeletionCode() {
		return DeletionCode;
	}

	public void setDeletionCode(String deletionCode) {
		DeletionCode = deletionCode;
	}

	public String getDeletionText() {
		return DeletionText;
	}

	public void setDeletionText(String deletionText) {
		DeletionText = deletionText;
	}

	public String getRecordSourceType() {
		return RecordSourceType;
	}

	public void setRecordSourceType(String recordSourceType) {
		RecordSourceType = recordSourceType;
	}

	public String getRecordSourceIdentifierType() {
		return RecordSourceIdentifierType;
	}

	public void setRecordSourceIdentifierType(String recordSourceIdentifierType) {
		RecordSourceIdentifierType = recordSourceIdentifierType;
	}

	public String getRecordSourceIdentifier() {
		return RecordSourceIdentifier;
	}

	public void setRecordSourceIdentifier(String recordSourceIdentifier) {
		RecordSourceIdentifier = recordSourceIdentifier;
	}

	public String getRecordSourceName() {
		return RecordSourceName;
	}

	public void setRecordSourceName(String recordSourceName) {
		RecordSourceName = recordSourceName;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getEAN13() {
		return EAN13;
	}

	public void setEAN13(String eAN13) {
		EAN13 = eAN13;
	}

	public String getUPC() {
		return UPC;
	}

	public void setUPC(String uPC) {
		UPC = uPC;
	}

	public String getPublisherProductNo() {
		return PublisherProductNo;
	}

	public void setPublisherProductNo(String publisherProductNo) {
		PublisherProductNo = publisherProductNo;
	}

	public String getISMN() {
		return ISMN;
	}

	public void setISMN(String iSMN) {
		ISMN = iSMN;
	}

	public String getDOI() {
		return DOI;
	}

	public void setDOI(String dOI) {
		DOI = dOI;
	}

	public List<ProductIdentifier> getProductIdentifier() {
		return ProductIdentifier;
	}

	public void setProductIdentifier(List<ProductIdentifier> productIdentifier) {
		ProductIdentifier = productIdentifier;
	}

	public String getReplacesISBN() {
		return ReplacesISBN;
	}

	public void setReplacesISBN(String replacesISBN) {
		ReplacesISBN = replacesISBN;
	}

	public String getReplacesEAN13() {
		return ReplacesEAN13;
	}

	public void setReplacesEAN13(String replacesEAN13) {
		ReplacesEAN13 = replacesEAN13;
	}

	public List<String> getProductFormDetail() {
		return ProductFormDetail;
	}

	public void setProductFormDetail(List<String> productFormDetail) {
		ProductFormDetail = productFormDetail;
	}

	public List<ProductFormFeature> getProductFormFeature() {
		return ProductFormFeature;
	}

	public void setProductFormFeature(List<ProductFormFeature> productFormFeature) {
		ProductFormFeature = productFormFeature;
	}

	public List<String> getBookFormDetail() {
		return BookFormDetail;
	}

	public void setBookFormDetail(List<String> bookFormDetail) {
		BookFormDetail = bookFormDetail;
	}

	public String getProductPackaging() {
		return ProductPackaging;
	}

	public void setProductPackaging(String productPackaging) {
		ProductPackaging = productPackaging;
	}

	public String getProductFormDescription() {
		return ProductFormDescription;
	}

	public void setProductFormDescription(String productFormDescription) {
		ProductFormDescription = productFormDescription;
	}

	public String getNumberOfPieces() {
		return NumberOfPieces;
	}

	public void setNumberOfPieces(String numberOfPieces) {
		NumberOfPieces = numberOfPieces;
	}

	public String getTradeCategory() {
		return TradeCategory;
	}

	public void setTradeCategory(String tradeCategory) {
		TradeCategory = tradeCategory;
	}

	public List<String> getProductContentType() {
		return ProductContentType;
	}

	public void setProductContentType(List<String> productContentType) {
		ProductContentType = productContentType;
	}

	public List<ProductClassification> getProductClassification() {
		return ProductClassification;
	}

	public void setProductClassification(List<ProductClassification> productClassification) {
		ProductClassification = productClassification;
	}

	public String getEpubType() {
		return EpubType;
	}

	public void setEpubType(String epubType) {
		EpubType = epubType;
	}

	public String getEpubTypeVersion() {
		return EpubTypeVersion;
	}

	public void setEpubTypeVersion(String epubTypeVersion) {
		EpubTypeVersion = epubTypeVersion;
	}

	public String getEpubTypeDescription() {
		return EpubTypeDescription;
	}

	public void setEpubTypeDescription(String epubTypeDescription) {
		EpubTypeDescription = epubTypeDescription;
	}

	public String getEpubFormat() {
		return EpubFormat;
	}

	public void setEpubFormat(String epubFormat) {
		EpubFormat = epubFormat;
	}

	public String getEpubFormatVersion() {
		return EpubFormatVersion;
	}

	public void setEpubFormatVersion(String epubFormatVersion) {
		EpubFormatVersion = epubFormatVersion;
	}

	public String getEpubFormatDescription() {
		return EpubFormatDescription;
	}

	public void setEpubFormatDescription(String epubFormatDescription) {
		EpubFormatDescription = epubFormatDescription;
	}

	public String getEpubSource() {
		return EpubSource;
	}

	public void setEpubSource(String epubSource) {
		EpubSource = epubSource;
	}

	public String getEpubSourceVersion() {
		return EpubSourceVersion;
	}

	public void setEpubSourceVersion(String epubSourceVersion) {
		EpubSourceVersion = epubSourceVersion;
	}

	public String getEpubSourceDescription() {
		return EpubSourceDescription;
	}

	public void setEpubSourceDescription(String epubSourceDescription) {
		EpubSourceDescription = epubSourceDescription;
	}

	public String getEpubTypeNote() {
		return EpubTypeNote;
	}

	public void setEpubTypeNote(String epubTypeNote) {
		EpubTypeNote = epubTypeNote;
	}

	public List<Series> getSeries() {
		return Series;
	}

	public void setSeries(List<Series> series) {
		Series = series;
	}

	public String getNoSeries() {
		return NoSeries;
	}

	public void setNoSeries(String noSeries) {
		NoSeries = noSeries;
	}

	public List<Set> getSets() {
		return Sets;
	}

	public void setSets(List<Set> sets) {
		Sets = sets;
	}

	public String getTextCaseFlag() {
		return TextCaseFlag;
	}

	public void setTextCaseFlag(String textCaseFlag) {
		TextCaseFlag = textCaseFlag;
	}

	public String getDistinctiveTitle() {
		return DistinctiveTitle;
	}

	public void setDistinctiveTitle(String distinctiveTitle) {
		DistinctiveTitle = distinctiveTitle;
	}

	public String getTitlePrefix() {
		return TitlePrefix;
	}

	public void setTitlePrefix(String titlePrefix) {
		TitlePrefix = titlePrefix;
	}

	public String getTitleWithoutPrefix() {
		return TitleWithoutPrefix;
	}

	public void setTitleWithoutPrefix(String titleWithoutPrefix) {
		TitleWithoutPrefix = titleWithoutPrefix;
	}

	public String getSubtitle() {
		return Subtitle;
	}

	public void setSubtitle(String subtitle) {
		Subtitle = subtitle;
	}

	public String getTranslationOfTitle() {
		return TranslationOfTitle;
	}

	public void setTranslationOfTitle(String translationOfTitle) {
		TranslationOfTitle = translationOfTitle;
	}

	public List<String> getFormerTitle() {
		return FormerTitle;
	}

	public void setFormerTitle(List<String> formerTitle) {
		FormerTitle = formerTitle;
	}

	public List<Title> getTitle() {
		return Title;
	}

	public void setTitle(List<Title> title) {
		Title = title;
	}

	public List<WorkIdentifier> getWorkIdentifier() {
		return WorkIdentifier;
	}

	public void setWorkIdentifier(List<WorkIdentifier> workIdentifier) {
		WorkIdentifier = workIdentifier;
	}

	public List<Website> getWebsite() {
		return Website;
	}

	public void setWebsite(List<Website> website) {
		Website = website;
	}

	public String getThesisType() {
		return ThesisType;
	}

	public void setThesisType(String thesisType) {
		ThesisType = thesisType;
	}

	public String getThesisPresentedTo() {
		return ThesisPresentedTo;
	}

	public void setThesisPresentedTo(String thesisPresentedTo) {
		ThesisPresentedTo = thesisPresentedTo;
	}

	public String getThesisYear() {
		return ThesisYear;
	}

	public void setThesisYear(String thesisYear) {
		ThesisYear = thesisYear;
	}

	public List<Contributor> getContributor() {
		return Contributor;
	}

	public void setContributor(List<Contributor> contributor) {
		Contributor = contributor;
	}

	public String getContributorStatement() {
		return ContributorStatement;
	}

	public void setContributorStatement(String contributorStatement) {
		ContributorStatement = contributorStatement;
	}

	public String getNoContributor() {
		return NoContributor;
	}

	public void setNoContributor(String noContributor) {
		NoContributor = noContributor;
	}

	public String getConferenceDescription() {
		return ConferenceDescription;
	}

	public void setConferenceDescription(String conferenceDescription) {
		ConferenceDescription = conferenceDescription;
	}

	public String getConferenceRole() {
		return ConferenceRole;
	}

	public void setConferenceRole(String conferenceRole) {
		ConferenceRole = conferenceRole;
	}

	public String getConferenceName() {
		return ConferenceName;
	}

	public void setConferenceName(String conferenceName) {
		ConferenceName = conferenceName;
	}

	public String getConferenceNumber() {
		return ConferenceNumber;
	}

	public void setConferenceNumber(String conferenceNumber) {
		ConferenceNumber = conferenceNumber;
	}

	public String getConferenceDate() {
		return ConferenceDate;
	}

	public void setConferenceDate(String conferenceDate) {
		ConferenceDate = conferenceDate;
	}

	public String getConferencePlace() {
		return ConferencePlace;
	}

	public void setConferencePlace(String conferencePlace) {
		ConferencePlace = conferencePlace;
	}

	public List<Conference> getConference() {
		return Conference;
	}

	public void setConference(List<Conference> conference) {
		Conference = conference;
	}

	public String getEditionNumber() {
		return EditionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		EditionNumber = editionNumber;
	}

	public String getEditionVersionNumber() {
		return EditionVersionNumber;
	}

	public void setEditionVersionNumber(String editionVersionNumber) {
		EditionVersionNumber = editionVersionNumber;
	}

	public String getEditionStatement() {
		return EditionStatement;
	}

	public void setEditionStatement(String editionStatement) {
		EditionStatement = editionStatement;
	}

	public String getNoEdition() {
		return NoEdition;
	}

	public void setNoEdition(String noEdition) {
		NoEdition = noEdition;
	}

	public String getReligiousText() {
		return ReligiousText;
	}

	public void setReligiousText(String religiousText) {
		ReligiousText = religiousText;
	}

	public String getOriginalLanguage() {
		return OriginalLanguage;
	}

	public void setOriginalLanguage(String originalLanguage) {
		OriginalLanguage = originalLanguage;
	}

	public List<Language> getLanguage() {
		return Language;
	}

	public void setLanguage(List<Language> language) {
		Language = language;
	}

	public String getNumberOfPages() {
		return NumberOfPages;
	}

	public void setNumberOfPages(String numberOfPages) {
		NumberOfPages = numberOfPages;
	}

	public String getPagesRoman() {
		return PagesRoman;
	}

	public void setPagesRoman(String pagesRoman) {
		PagesRoman = pagesRoman;
	}

	public String getPagesArabic() {
		return PagesArabic;
	}

	public void setPagesArabic(String pagesArabic) {
		PagesArabic = pagesArabic;
	}

	public List<Extent> getExtent() {
		return Extent;
	}

	public void setExtent(List<Extent> extent) {
		Extent = extent;
	}

	public String getNumberOfIllustrations() {
		return NumberOfIllustrations;
	}

	public void setNumberOfIllustrations(String numberOfIllustrations) {
		NumberOfIllustrations = numberOfIllustrations;
	}

	public String getIllustrationsNote() {
		return IllustrationsNote;
	}

	public void setIllustrationsNote(String illustrationsNote) {
		IllustrationsNote = illustrationsNote;
	}

	public List<Illustrations> getIllustrations() {
		return Illustrations;
	}

	public void setIllustrations(List<Illustrations> illustrations) {
		Illustrations = illustrations;
	}

	public String getBASICMainSubject() {
		return BASICMainSubject;
	}

	public void setBASICMainSubject(String bASICMainSubject) {
		BASICMainSubject = bASICMainSubject;
	}

	public String getBASICVersion() {
		return BASICVersion;
	}

	public void setBASICVersion(String bASICVersion) {
		BASICVersion = bASICVersion;
	}

	public String getBICMainSubject() {
		return BICMainSubject;
	}

	public void setBICMainSubject(String bICMainSubject) {
		BICMainSubject = bICMainSubject;
	}

	public String getBICVersion() {
		return BICVersion;
	}

	public void setBICVersion(String bICVersion) {
		BICVersion = bICVersion;
	}

	public List<MainSubject> getMainSubject() {
		return MainSubject;
	}

	public void setMainSubject(List<MainSubject> mainSubject) {
		MainSubject = mainSubject;
	}

	public List<Subject> getSubject() {
		return Subject;
	}

	public void setSubject(List<Subject> subject) {
		Subject = subject;
	}

	public List<PersonAsSubject> getPersonAsSubject() {
		return PersonAsSubject;
	}

	public void setPersonAsSubject(List<PersonAsSubject> personAsSubject) {
		PersonAsSubject = personAsSubject;
	}

	public List<Audience> getAudience() {
		return Audience;
	}

	public void setAudience(List<Audience> audience) {
		Audience = audience;
	}

	public String getUSSchoolGrade() {
		return USSchoolGrade;
	}

	public void setUSSchoolGrade(String uSSchoolGrade) {
		USSchoolGrade = uSSchoolGrade;
	}

	public String getInterestAge() {
		return InterestAge;
	}

	public void setInterestAge(String interestAge) {
		InterestAge = interestAge;
	}

	public List<AudienceRange> getAudienceRange() {
		return AudienceRange;
	}

	public void setAudienceRange(List<AudienceRange> audienceRange) {
		AudienceRange = audienceRange;
	}

	public String getAudienceDescription() {
		return AudienceDescription;
	}

	public void setAudienceDescription(String audienceDescription) {
		AudienceDescription = audienceDescription;
	}

	public List<Complexity> getComplexity() {
		return Complexity;
	}

	public void setComplexity(List<Complexity> complexity) {
		Complexity = complexity;
	}

	public String getAnnotation() {
		return Annotation;
	}

	public void setAnnotation(String annotation) {
		Annotation = annotation;
	}

	public String getMainDescription() {
		return MainDescription;
	}

	public void setMainDescription(String mainDescription) {
		MainDescription = mainDescription;
	}

	public List<OtherText> getOtherText() {
		return OtherText;
	}

	public void setOtherText(List<OtherText> otherText) {
		OtherText = otherText;
	}

	public List<String> getReviewQuote() {
		return ReviewQuote;
	}

	public void setReviewQuote(List<String> reviewQuote) {
		ReviewQuote = reviewQuote;
	}

	public String getCoverImageFormatCode() {
		return CoverImageFormatCode;
	}

	public void setCoverImageFormatCode(String coverImageFormatCode) {
		CoverImageFormatCode = coverImageFormatCode;
	}

	public String getCoverImageLinkTypeCode() {
		return CoverImageLinkTypeCode;
	}

	public void setCoverImageLinkTypeCode(String coverImageLinkTypeCode) {
		CoverImageLinkTypeCode = coverImageLinkTypeCode;
	}

	public String getCoverImageLink() {
		return CoverImageLink;
	}

	public void setCoverImageLink(String coverImageLink) {
		CoverImageLink = coverImageLink;
	}

	public List<MediaFile> getMediaFile() {
		return MediaFile;
	}

	public void setMediaFile(List<MediaFile> mediaFile) {
		MediaFile = mediaFile;
	}

	public List<ProductWebsite> getProductWebsite() {
		return ProductWebsite;
	}

	public void setProductWebsite(List<ProductWebsite> productWebsite) {
		ProductWebsite = productWebsite;
	}

	public String getPrizesDescription() {
		return PrizesDescription;
	}

	public void setPrizesDescription(String prizesDescription) {
		PrizesDescription = prizesDescription;
	}

	public List<Prize> getPrize() {
		return Prize;
	}

	public void setPrize(List<Prize> prize) {
		Prize = prize;
	}

	public List<ContentItem> getContentItem() {
		return ContentItem;
	}

	public void setContentItem(List<ContentItem> contentItem) {
		ContentItem = contentItem;
	}

	public List<Imprint> getImprint() {
		return Imprint;
	}

	public void setImprint(List<Imprint> imprint) {
		Imprint = imprint;
	}

	public List<Publisher> getPublisher() {
		return Publisher;
	}

	public void setPublisher(List<Publisher> publisher) {
		Publisher = publisher;
	}

	public String getCityOfPublication() {
		return CityOfPublication;
	}

	public void setCityOfPublication(String cityOfPublication) {
		CityOfPublication = cityOfPublication;
	}

	public String getCountryOfPublication() {
		return CountryOfPublication;
	}

	public void setCountryOfPublication(String countryOfPublication) {
		CountryOfPublication = countryOfPublication;
	}

	public List<String> getCopublisherName() {
		return CopublisherName;
	}

	public void setCopublisherName(List<String> copublisherName) {
		CopublisherName = copublisherName;
	}

	public List<String> getSponsorName() {
		return SponsorName;
	}

	public void setSponsorName(List<String> sponsorName) {
		SponsorName = sponsorName;
	}

	public String getOriginalPublisher() {
		return OriginalPublisher;
	}

	public void setOriginalPublisher(String originalPublisher) {
		OriginalPublisher = originalPublisher;
	}

	public String getPublishingStatus() {
		return PublishingStatus;
	}

	public void setPublishingStatus(String publishingStatus) {
		PublishingStatus = publishingStatus;
	}

	public String getPublishingStatusNote() {
		return PublishingStatusNote;
	}

	public void setPublishingStatusNote(String publishingStatusNote) {
		PublishingStatusNote = publishingStatusNote;
	}

	public String getAnnouncementDate() {
		return AnnouncementDate;
	}

	public void setAnnouncementDate(String announcementDate) {
		AnnouncementDate = announcementDate;
	}

	public String getTradeAnnouncementDate() {
		return TradeAnnouncementDate;
	}

	public void setTradeAnnouncementDate(String tradeAnnouncementDate) {
		TradeAnnouncementDate = tradeAnnouncementDate;
	}

	public String getPublicationDate() {
		return PublicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		PublicationDate = publicationDate;
	}

	public List<CopyrightStatement> getCopyrightStatement() {
		return CopyrightStatement;
	}

	public void setCopyrightStatement(List<CopyrightStatement> copyrightStatement) {
		CopyrightStatement = copyrightStatement;
	}

	public String getCopyrightYear() {
		return CopyrightYear;
	}

	public void setCopyrightYear(String copyrightYear) {
		CopyrightYear = copyrightYear;
	}

	public String getYearFirstPublished() {
		return YearFirstPublished;
	}

	public void setYearFirstPublished(String yearFirstPublished) {
		YearFirstPublished = yearFirstPublished;
	}

	public List<SalesRights> getSalesRights() {
		return SalesRights;
	}

	public void setSalesRights(List<SalesRights> salesRights) {
		SalesRights = salesRights;
	}

	public List<NotForSale> getNotForSale() {
		return NotForSale;
	}

	public void setNotForSale(List<NotForSale> notForSale) {
		NotForSale = notForSale;
	}

	public List<SalesRestriction> getSalesRestriction() {
		return SalesRestriction;
	}

	public void setSalesRestriction(List<SalesRestriction> salesRestriction) {
		SalesRestriction = salesRestriction;
	}

	public String getHeight() {
		return Height;
	}

	public void setHeight(String height) {
		Height = height;
	}

	public String getWidth() {
		return Width;
	}

	public void setWidth(String width) {
		Width = width;
	}

	public String getThickness() {
		return Thickness;
	}

	public void setThickness(String thickness) {
		Thickness = thickness;
	}

	public String getWeight() {
		return Weight;
	}

	public void setWeight(String weight) {
		Weight = weight;
	}

	public List<Measure> getMeasure() {
		return Measure;
	}

	public void setMeasure(List<Measure> measure) {
		Measure = measure;
	}

	public String getDimensions() {
		return Dimensions;
	}

	public void setDimensions(String dimensions) {
		Dimensions = dimensions;
	}

	public String getReplacedByISBN() {
		return ReplacedByISBN;
	}

	public void setReplacedByISBN(String replacedByISBN) {
		ReplacedByISBN = replacedByISBN;
	}

	public String getReplacedByEAN13() {
		return ReplacedByEAN13;
	}

	public void setReplacedByEAN13(String replacedByEAN13) {
		ReplacedByEAN13 = replacedByEAN13;
	}

	public String getAlternativeFormatISBN() {
		return AlternativeFormatISBN;
	}

	public void setAlternativeFormatISBN(String alternativeFormatISBN) {
		AlternativeFormatISBN = alternativeFormatISBN;
	}

	public String getAlternativeFormatEAN13() {
		return AlternativeFormatEAN13;
	}

	public void setAlternativeFormatEAN13(String alternativeFormatEAN13) {
		AlternativeFormatEAN13 = alternativeFormatEAN13;
	}

	public String getAlternativeProductISBN() {
		return AlternativeProductISBN;
	}

	public void setAlternativeProductISBN(String alternativeProductISBN) {
		AlternativeProductISBN = alternativeProductISBN;
	}

	public String getAlternativeProductEAN13() {
		return AlternativeProductEAN13;
	}

	public void setAlternativeProductEAN13(String alternativeProductEAN13) {
		AlternativeProductEAN13 = alternativeProductEAN13;
	}

	public List<RelatedProduct> getRelatedProduct() {
		return RelatedProduct;
	}

	public void setRelatedProduct(List<RelatedProduct> relatedProduct) {
		RelatedProduct = relatedProduct;
	}

	public String getOutOfPrintDate() {
		return OutOfPrintDate;
	}

	public void setOutOfPrintDate(String outOfPrintDate) {
		OutOfPrintDate = outOfPrintDate;
	}

	public List<SupplyDetail> getSupplyDetail() {
		return SupplyDetail;
	}

	public void setSupplyDetail(List<SupplyDetail> supplyDetail) {
		SupplyDetail = supplyDetail;
	}

	public String getPromotionCampaign() {
		return PromotionCampaign;
	}

	public void setPromotionCampaign(String promotionCampaign) {
		PromotionCampaign = promotionCampaign;
	}

	public String getPromotionContact() {
		return PromotionContact;
	}

	public void setPromotionContact(String promotionContact) {
		PromotionContact = promotionContact;
	}

	public String getInitialPrintRun() {
		return InitialPrintRun;
	}

	public void setInitialPrintRun(String initialPrintRun) {
		InitialPrintRun = initialPrintRun;
	}

	public String getCopiesSold() {
		return CopiesSold;
	}

	public void setCopiesSold(String copiesSold) {
		CopiesSold = copiesSold;
	}

	public String getBookClubAdoption() {
		return BookClubAdoption;
	}

	public void setBookClubAdoption(String bookClubAdoption) {
		BookClubAdoption = bookClubAdoption;
	}

	public String getEditionType() {
		return EditionType;
	}

	public void setEditionType(String editionType) {
		EditionType = editionType;
	}

	public String getProductForm() {
		return ProductForm;
	}

	public void setProductForm(String productForm) {
		ProductForm = productForm;
	}

	public List<String> getAudienceCode() {
		return AudienceCode;
	}

	public void setAudienceCode(List<String> audienceCode) {
		AudienceCode = audienceCode;
	}

	public String getMainTitle() {
		return MainTitle;
	}

	public void setMainTitle(String mainTitle) {
		MainTitle = mainTitle;
	}

	public String getNumberWithinSeries() {
		return NumberWithinSeries;
	}

	public void setNumberWithinSeries(String numberWithinSeries) {
		NumberWithinSeries = numberWithinSeries;
	}

	public List<CollectionIdentifier> getSeriesIdentifier() {
		return SeriesIdentifier;
	}

	public void setSeriesIdentifier(List<CollectionIdentifier> seriesIdentifier) {
		SeriesIdentifier = seriesIdentifier;
	}

	public List<Set> getSet() {
		return Set;
	}

	public void setSet(List<Set> set) {
		Set = set;
	}

	public String getBarcodeIndicator() {
		return BarcodeIndicator;
	}

	public void setBarcodeIndicator(String barcodeIndicator) {
		BarcodeIndicator = barcodeIndicator;
	}

	public String getUsageType() {
		return UsageType;
	}

	public void setUsageType(String usageType) {
		UsageType = usageType;
	}

	public String getBisacReturnableIndicator() {
		return BisacReturnableIndicator;
	}

	public void setBisacReturnableIndicator(String bisacReturnableIndicator) {
		BisacReturnableIndicator = bisacReturnableIndicator;
	}
	
	public String getLSIPublisherID() {
		return LSIPublisherID;
	}

	public void setLSIPublisherID(String lSIPublisherID) {
		LSIPublisherID = lSIPublisherID;
	}

	/*public String getPreflightStatus() {
		return PreflightStatus;
	}

	public void setPreflightStatus(String preflightStatus) {
		PreflightStatus = preflightStatus;
	}*/

	public Date getLastModifiedOn() {
		return LastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		LastModifiedOn = lastModifiedOn;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getLastModifiedBy() {
		return LastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		LastModifiedBy = lastModifiedBy;
	}
	
	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getPubKey() {
		return PubKey;
	}

	public void setPubKey(String pubKey) {
		PubKey = pubKey;
	}

	public String getLanguageKey() {
		return LanguageKey;
	}

	public void setLanguageKey(String languageKey) {
		LanguageKey = languageKey;
	}

	public String getPinNo() {
		return PinNo;
	}

	public void setPinNo(String pinNo) {
		PinNo = pinNo;
	}
}
