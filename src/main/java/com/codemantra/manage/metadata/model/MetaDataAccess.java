/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

import java.util.List;

public class MetaDataAccess {
	
	private String metadataCode;
	private String metadataName;
	private List<String> values;
	
	public String getMetadataCode() {
		return metadataCode;
	}
	public void setMetadataCode(String metadataCode) {
		this.metadataCode = metadataCode;
	}
	public String getMetadataName() {
		return metadataName;
	}
	public void setMetadataName(String metadataName) {
		this.metadataName = metadataName;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}

}
