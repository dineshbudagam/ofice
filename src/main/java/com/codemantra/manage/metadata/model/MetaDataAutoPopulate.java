/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;
import java.util.Map;

public class MetaDataAutoPopulate {
	
	public String SourceField;
	
	public List<Map<String,String>> PopulateLookup;
	
	public String PopulateField;
	
	public String PopulateCondition;

	public String getSourceField() {
		return SourceField;
	}

	public void setSourceField(String sourceField) {
		SourceField = sourceField;
	}

	public List<Map<String, String>> getPopulateLookup() {
		return PopulateLookup;
	}

	public void setPopulateLookup(List<Map<String, String>> populateLookup) {
		PopulateLookup = populateLookup;
	}

	public String getPopulateField() {
		return PopulateField;
	}

	public void setPopulateField(String populateField) {
		PopulateField = populateField;
	}

	public String getPopulateCondition() {
		return PopulateCondition;
	}

	public void setPopulateCondition(String populateCondition) {
		PopulateCondition = populateCondition;
	}
}
