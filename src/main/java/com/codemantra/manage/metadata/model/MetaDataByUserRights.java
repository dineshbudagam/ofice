/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-07-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

public class MetaDataByUserRights {

	private String userId;
	
	private List<AccountAccess> accounts;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<AccountAccess> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AccountAccess> accounts) {
		this.accounts = accounts;
	}

}
