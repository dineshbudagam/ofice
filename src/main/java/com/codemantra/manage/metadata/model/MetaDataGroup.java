/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
25-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

import java.util.List;

public class MetaDataGroup {
	
	public String groupId;
	
	public String subGroupId;
		
	public String groupDisplayName;
	
	public String groupName;
	
	public String groupDisplayOrder;
	
	public Boolean isDisplay;
	
	public Boolean isActive;
	
	public Boolean isDeleted;
	
	public List<MetaDataFields> metaDataFields;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getSubGroupId() {
		return subGroupId;
	}

	public void setSubGroupId(String subGroupId) {
		this.subGroupId = subGroupId;
	}

	public String getGroupDisplayName() {
		return groupDisplayName;
	}

	public void setGroupDisplayName(String groupDisplayName) {
		this.groupDisplayName = groupDisplayName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDisplayOrder() {
		return groupDisplayOrder;
	}

	public void setGroupDisplayOrder(String groupDisplayOrder) {
		this.groupDisplayOrder = groupDisplayOrder;
	}

	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<MetaDataFields> getMetaDataFields() {
		return metaDataFields;
	}

	public void setMetaDataFields(List<MetaDataFields> metaDataFields) {
		this.metaDataFields = metaDataFields;
	}
}
