/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class MetaDataTypeAccess extends Access{
	
	private String metadataTypeCode;
	private String metadataTypeName;
	
	public String getMetadataTypeCode() {
		return metadataTypeCode;
	}
	public void setMetadataTypeCode(String metadataTypeCode) {
		this.metadataTypeCode = metadataTypeCode;
	}
	public String getMetadataTypeName() {
		return metadataTypeName;
	}
	public void setMetadataTypeName(String metadataTypeName) {
		this.metadataTypeName = metadataTypeName;
	}

}
