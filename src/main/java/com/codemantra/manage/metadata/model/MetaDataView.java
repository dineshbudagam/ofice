/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.Map;

public class MetaDataView {
		
	private String userId;
	
	private String permissionGroupId;
	
	private boolean exportCheck;
	
	private int resultCount;
	
	private int skipCount;
	
	private long searchReultCount;
	
	private String mDataSearchTxt;
	
	private String fieldDisplayName;
	
	private String idValue;
	
	private Map<String,Object> custMData;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPermissionGroupId() {
		return permissionGroupId;
	}

	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}

	public boolean isExportCheck() {
		return exportCheck;
	}

	public void setExportCheck(boolean exportCheck) {
		this.exportCheck = exportCheck;
	}

	public int getResultCount() {
		return resultCount;
	}

	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}

	public int getSkipCount() {
		return skipCount;
	}

	public void setSkipCount(int skipCount) {
		this.skipCount = skipCount;
	}

	public long getSearchReultCount() {
		return searchReultCount;
	}

	public void setSearchReultCount(long searchReultCount) {
		this.searchReultCount = searchReultCount;
	}

	public String getmDataSearchTxt() {
		return mDataSearchTxt;
	}

	public void setmDataSearchTxt(String mDataSearchTxt) {
		this.mDataSearchTxt = mDataSearchTxt;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}

	public String getIdValue() {
		return idValue;
	}

	public void setIdValue(String idValue) {
		this.idValue = idValue;
	}

	public Map<String, Object> getCustMData() {
		return custMData;
	}

	public void setCustMData(Map<String, Object> custMData) {
		this.custMData = custMData;
	}

}
