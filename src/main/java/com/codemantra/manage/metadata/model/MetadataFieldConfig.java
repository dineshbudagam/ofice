package com.codemantra.manage.metadata.model;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Field;

public class MetadataFieldConfig {

	private String metaDataFieldId;
	private String metaDataFieldName;
	private String displayOrderNo;
	private String groupId;
	private String subGroupId;
    private int editDisplayOrderNo;
	private String fieldDisplayName;
	private String fieldDataType;
	private String jsonPath;
	public String lookUp;
	private String jsonType;
	public String referencePath;
	public String referenceValue;
	public String referenceValuePath;
	public String minlength;
	public String maxlength;
	private Boolean isMandatory;
	private Boolean isDisplay;
	private Boolean isActive;
	private Boolean isDeleted;
	private Boolean isEdit;
	private Boolean permissionGroupField;
	private String permissionGroupValue;
	private String createdBy;
	private Date createdOn;
	private String modifiedBy;
	private Date modifiedOn;
	private String initRefPath;
	private String modelRefPath;

	public String getMetaDataFieldId() {
		return metaDataFieldId;
	}

	public void setMetaDataFieldId(String metaDataFieldId) {
		this.metaDataFieldId = metaDataFieldId;
	}

	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}

	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}

	public String getDisplayOrderNo() {
		return displayOrderNo;
	}

	public void setDisplayOrderNo(String displayOrderNo) {
		this.displayOrderNo = displayOrderNo;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}

	public String getFieldDataType() {
		return fieldDataType;
	}

	public void setFieldDataType(String fieldDataType) {
		this.fieldDataType = fieldDataType;
	}

	public String getJsonPath() {
		return jsonPath;
	}

	public void setJsonPath(String jsonPath) {
		this.jsonPath = jsonPath;
	}

	
	public String getLookUp() {
		return lookUp;
	}

	public void setLookUp(String lookUp) {
		this.lookUp = lookUp;
	}

	public String getJsonType() {
		return jsonType;
	}

	public void setJsonType(String jsonType) {
		this.jsonType = jsonType;
	}

	public String getReferencePath() {
		return referencePath;
	}

	public void setReferencePath(String referencePath) {
		this.referencePath = referencePath;
	}

	public String getReferenceValue() {
		return referenceValue;
	}

	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}

	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDelete) {
		this.isDeleted = isDelete;
	}

	public Boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Boolean getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getInitRefPath() {
		return initRefPath;
	}

	public void setInitRefPath(String initRefPath) {
		this.initRefPath = initRefPath;
	}

	public String getModelRefPath() {
		return modelRefPath;
	}

	public void setModelRefPath(String modelRefPath) {
		this.modelRefPath = modelRefPath;
	}

	public String getSubGroupId() {
		return subGroupId;
	}

	public void setSubGroupId(String subGroupId) {
		this.subGroupId = subGroupId;
	}

	public String getReferenceValuePath() {
		return referenceValuePath;
	}

	public void setReferenceValuePath(String referenceValuePath) {
		this.referenceValuePath = referenceValuePath;
	}

	public String getMinlength() {
		return minlength;
	}

	public void setMinlength(String minlength) {
		this.minlength = minlength;
	}

	public String getMaxlength() {
		return maxlength;
	}

	public void setMaxlength(String maxlength) {
		this.maxlength = maxlength;
	}
	public Boolean getPermissionGroupField() {
		return permissionGroupField;
	}
	public void setPermissionGroupField(Boolean permissionGroupField) {
		this.permissionGroupField = permissionGroupField;
	}
	public String getPermissionGroupValue() {
		return permissionGroupValue;
	}
	public void setPermissionGroupValue(String permissionGroupValue) {
		this.permissionGroupValue = permissionGroupValue;
	}

	public int getEditDisplayOrderNo() {
		return editDisplayOrderNo;
	}

	public void setEditDisplayOrderNo(int editDisplayOrderNo) {
		this.editDisplayOrderNo = editDisplayOrderNo;
	}

	
}
