package com.codemantra.manage.metadata.model;

import java.sql.Timestamp;
import java.util.List;

public class MetadataGroupConfig {

	private String groupId;
	private String subGroupId;
	private String groupName;
	private String groupDisplayName;
	private String groupDisplayOrder;
	private Boolean isDisplay;
	private Boolean isActive;
	private Boolean isDeleted;
	private List<MetadataFieldConfig> metadataFieldConfig;
	private List<MetadataFieldConfig> metadataSubFieldConfig;
	private String createdBy;
	private Timestamp createdOn;
	private String modifiedBy;
	private Timestamp modifiedOn;
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupDisplayName() {
		return groupDisplayName;
	}
	public void setGroupDisplayName(String groupDisplayName) {
		this.groupDisplayName = groupDisplayName;
	}
	public String getGroupDisplayOrder() {
		return groupDisplayOrder;
	}
	public void setGroupDisplayOrder(String groupDisplayOrder) {
		this.groupDisplayOrder = groupDisplayOrder;
	}
	public Boolean getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public List<MetadataFieldConfig> getMetadataFieldConfig() {
		return metadataFieldConfig;
	}
	public void setMetadataFieldConfig(List<MetadataFieldConfig> metadataFieldConfig) {
		this.metadataFieldConfig = metadataFieldConfig;
	}
	public String getSubGroupId() {
		return subGroupId;
	}
	public void setSubGroupId(String subGroupId) {
		this.subGroupId = subGroupId;
	}
	public List<MetadataFieldConfig> getMetadataSubFieldConfig() {
		return metadataSubFieldConfig;
	}
	public void setMetadataSubFieldConfig(List<MetadataFieldConfig> metadataSubFieldConfig) {
		this.metadataSubFieldConfig = metadataSubFieldConfig;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
}
