package com.codemantra.manage.metadata.model;

public class MetadataStageStatus {

	private String stageId;
	
	private String stageDisplayName;
	
	private String status;
	
	private String approvedDate;
	
	 private String approvalMethod;

	public String getStageId() {
		return stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}

	public String getStageDisplayName() {
		return stageDisplayName;
	}

	public void setStageDisplayName(String stageDisplayName) {
		this.stageDisplayName = stageDisplayName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getApprovalMethod() {
		return approvalMethod;
	}

	public void setApprovalMethod(String approvalMethod) {
		this.approvalMethod = approvalMethod;
	}
	 
	 
	 
	
	
	
}
