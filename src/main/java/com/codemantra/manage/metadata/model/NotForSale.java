/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class NotForSale
{
	public List<String> RightsCountry;

	public List<String> RightsTerritory;

	public String ISBN;

	public String EAN13;

	public List<ProductIdentifier> ProductIdentifier;

	public String PublisherName;

	public List<String> getRightsCountry() {
		return RightsCountry;
	}

	public void setRightsCountry(List<String> rightsCountry) {
		RightsCountry = rightsCountry;
	}

	public List<String> getRightsTerritory() {
		return RightsTerritory;
	}

	public void setRightsTerritory(List<String> rightsTerritory) {
		RightsTerritory = rightsTerritory;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getEAN13() {
		return EAN13;
	}

	public void setEAN13(String eAN13) {
		EAN13 = eAN13;
	}

	public List<ProductIdentifier> getProductIdentifier() {
		return ProductIdentifier;
	}

	public void setProductIdentifier(List<ProductIdentifier> productIdentifier) {
		ProductIdentifier = productIdentifier;
	}

	public String getPublisherName() {
		return PublisherName;
	}

	public void setPublisherName(String publisherName) {
		PublisherName = publisherName;
	}

	
}
