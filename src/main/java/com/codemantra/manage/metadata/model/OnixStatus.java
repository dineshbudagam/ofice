/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
22-05-2017			v1.0       	   Bharath Prasanna	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class OnixStatus {
	private String message;
	private boolean status;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
