/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class OtherText
{
	public String TextTypeCode;

	public String TextFormat;

	public String Text;

	public String TextLinkType;

	public String TextLink;

	public String TextAuthor;

	public String TextSourceCorporate;

	public String TextSourceTitle;

	public String TextPublicationDate;

	public String StartDate;

	public String EndDate;

	public String getTextTypeCode() {
		return TextTypeCode;
	}

	public void setTextTypeCode(String textTypeCode) {
		TextTypeCode = textTypeCode;
	}

	public String getTextFormat() {
		return TextFormat;
	}

	public void setTextFormat(String textFormat) {
		TextFormat = textFormat;
	}

	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
	}

	public String getTextLinkType() {
		return TextLinkType;
	}

	public void setTextLinkType(String textLinkType) {
		TextLinkType = textLinkType;
	}

	public String getTextLink() {
		return TextLink;
	}

	public void setTextLink(String textLink) {
		TextLink = textLink;
	}

	public String getTextAuthor() {
		return TextAuthor;
	}

	public void setTextAuthor(String textAuthor) {
		TextAuthor = textAuthor;
	}

	public String getTextSourceCorporate() {
		return TextSourceCorporate;
	}

	public void setTextSourceCorporate(String textSourceCorporate) {
		TextSourceCorporate = textSourceCorporate;
	}

	public String getTextSourceTitle() {
		return TextSourceTitle;
	}

	public void setTextSourceTitle(String textSourceTitle) {
		TextSourceTitle = textSourceTitle;
	}

	public String getTextPublicationDate() {
		return TextPublicationDate;
	}

	public void setTextPublicationDate(String textPublicationDate) {
		TextPublicationDate = textPublicationDate;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	
	
}
