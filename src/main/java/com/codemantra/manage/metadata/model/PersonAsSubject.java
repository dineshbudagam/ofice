/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class PersonAsSubject
{
	public String PersonName;

	public String PersonNameInverted;

	public String TitlesBeforeNames;

	public String NamesBeforeKey;

	public String PrefixToKey;

	public String KeyNames;

	public String NamesAfterKey;

	public String SuffixToKey;

	public String LettersAfterNames;

	public String TitlesAfterNames;

	public List<String> Name;

	public List<String> PersonNameIdentifier;

	public String getPersonName() {
		return PersonName;
	}

	public void setPersonName(String personName) {
		PersonName = personName;
	}

	public String getPersonNameInverted() {
		return PersonNameInverted;
	}

	public void setPersonNameInverted(String personNameInverted) {
		PersonNameInverted = personNameInverted;
	}

	public String getTitlesBeforeNames() {
		return TitlesBeforeNames;
	}

	public void setTitlesBeforeNames(String titlesBeforeNames) {
		TitlesBeforeNames = titlesBeforeNames;
	}

	public String getNamesBeforeKey() {
		return NamesBeforeKey;
	}

	public void setNamesBeforeKey(String namesBeforeKey) {
		NamesBeforeKey = namesBeforeKey;
	}

	public String getPrefixToKey() {
		return PrefixToKey;
	}

	public void setPrefixToKey(String prefixToKey) {
		PrefixToKey = prefixToKey;
	}

	public String getKeyNames() {
		return KeyNames;
	}

	public void setKeyNames(String keyNames) {
		KeyNames = keyNames;
	}

	public String getNamesAfterKey() {
		return NamesAfterKey;
	}

	public void setNamesAfterKey(String namesAfterKey) {
		NamesAfterKey = namesAfterKey;
	}

	public String getSuffixToKey() {
		return SuffixToKey;
	}

	public void setSuffixToKey(String suffixToKey) {
		SuffixToKey = suffixToKey;
	}

	public String getLettersAfterNames() {
		return LettersAfterNames;
	}

	public void setLettersAfterNames(String lettersAfterNames) {
		LettersAfterNames = lettersAfterNames;
	}

	public String getTitlesAfterNames() {
		return TitlesAfterNames;
	}

	public void setTitlesAfterNames(String titlesAfterNames) {
		TitlesAfterNames = titlesAfterNames;
	}

	public List<String> getName() {
		return Name;
	}

	public void setName(List<String> name) {
		Name = name;
	}

	public List<String> getPersonNameIdentifier() {
		return PersonNameIdentifier;
	}

	public void setPersonNameIdentifier(List<String> personNameIdentifier) {
		PersonNameIdentifier = personNameIdentifier;
	}

		
}
