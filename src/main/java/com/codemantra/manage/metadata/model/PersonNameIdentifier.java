/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class PersonNameIdentifier
{
	public String PersonNameIDType;

	public String IDTypeName;

	public String IDValue;

	public String getPersonNameIDType() {
		return PersonNameIDType;
	}

	public void setPersonNameIDType(String personNameIDType) {
		PersonNameIDType = personNameIDType;
	}

	public String getIDTypeName() {
		return IDTypeName;
	}

	public void setIDTypeName(String iDTypeName) {
		IDTypeName = iDTypeName;
	}

	public String getIDValue() {
		return IDValue;
	}

	public void setIDValue(String iDValue) {
		IDValue = iDValue;
	}
}
