/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class Price
{
	public String PriceTypeCode;
	
	public Double PriceAmount;
	
	public String CurrencyCode;
	
	public String PriceQualifier;

	public String PriceTypeDescription;

	public String PricePercent;

	public String MinimumOrderQuantity;

	/*public List<BatchBonus> BatchBonus;*/

	public String ClassOfTrade;

	public String BicDiscountGroupCode;

	public Double DiscountPercent;

	public String PriceStatus;

	public String CountryCode;

	public String Territory;

	public String CountryExcluded;

	public String TerritoryExcluded;

	public String TaxRateCode1;

	public Double TaxRatePercent1;

	public Double TaxableAmount1;

	public Double TaxAmount1;

	public String TaxRateCode2;

	public Double TaxRatePercent2;

	public Double TaxableAmount2;

	public Double TaxAmount2;

	public String PriceEffectiveFrom;

	public String PriceEffectiveUntil;

	public List<DiscountCoded> DiscountCoded;

	public String getPriceTypeCode() {
		return PriceTypeCode;
	}

	public void setPriceTypeCode(String priceTypeCode) {
		PriceTypeCode = priceTypeCode;
	}

	public Double getPriceAmount() {
		return PriceAmount;
	}

	public void setPriceAmount(Double priceAmount) {
		PriceAmount = priceAmount;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public String getPriceQualifier() {
		return PriceQualifier;
	}

	public void setPriceQualifier(String priceQualifier) {
		PriceQualifier = priceQualifier;
	}

	public String getPriceTypeDescription() {
		return PriceTypeDescription;
	}

	public void setPriceTypeDescription(String priceTypeDescription) {
		PriceTypeDescription = priceTypeDescription;
	}

	public String getPricePercent() {
		return PricePercent;
	}

	public void setPricePercent(String pricePercent) {
		PricePercent = pricePercent;
	}

	public String getMinimumOrderQuantity() {
		return MinimumOrderQuantity;
	}

	public void setMinimumOrderQuantity(String minimumOrderQuantity) {
		MinimumOrderQuantity = minimumOrderQuantity;
	}

	public String getClassOfTrade() {
		return ClassOfTrade;
	}

	public void setClassOfTrade(String classOfTrade) {
		ClassOfTrade = classOfTrade;
	}

	public String getBicDiscountGroupCode() {
		return BicDiscountGroupCode;
	}

	public void setBicDiscountGroupCode(String bicDiscountGroupCode) {
		BicDiscountGroupCode = bicDiscountGroupCode;
	}

	public Double getDiscountPercent() {
		return DiscountPercent;
	}

	public void setDiscountPercent(Double discountPercent) {
		DiscountPercent = discountPercent;
	}

	public String getPriceStatus() {
		return PriceStatus;
	}

	public void setPriceStatus(String priceStatus) {
		PriceStatus = priceStatus;
	}

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

	public String getTerritory() {
		return Territory;
	}

	public void setTerritory(String territory) {
		Territory = territory;
	}

	public String getCountryExcluded() {
		return CountryExcluded;
	}

	public void setCountryExcluded(String countryExcluded) {
		CountryExcluded = countryExcluded;
	}

	public String getTerritoryExcluded() {
		return TerritoryExcluded;
	}

	public void setTerritoryExcluded(String territoryExcluded) {
		TerritoryExcluded = territoryExcluded;
	}

	public String getTaxRateCode1() {
		return TaxRateCode1;
	}

	public void setTaxRateCode1(String taxRateCode1) {
		TaxRateCode1 = taxRateCode1;
	}

	public Double getTaxRatePercent1() {
		return TaxRatePercent1;
	}

	public void setTaxRatePercent1(Double taxRatePercent1) {
		TaxRatePercent1 = taxRatePercent1;
	}

	public Double getTaxableAmount1() {
		return TaxableAmount1;
	}

	public void setTaxableAmount1(Double taxableAmount1) {
		TaxableAmount1 = taxableAmount1;
	}

	public Double getTaxAmount1() {
		return TaxAmount1;
	}

	public void setTaxAmount1(Double taxAmount1) {
		TaxAmount1 = taxAmount1;
	}

	public String getTaxRateCode2() {
		return TaxRateCode2;
	}

	public void setTaxRateCode2(String taxRateCode2) {
		TaxRateCode2 = taxRateCode2;
	}

	public Double getTaxRatePercent2() {
		return TaxRatePercent2;
	}

	public void setTaxRatePercent2(Double taxRatePercent2) {
		TaxRatePercent2 = taxRatePercent2;
	}

	public Double getTaxableAmount2() {
		return TaxableAmount2;
	}

	public void setTaxableAmount2(Double taxableAmount2) {
		TaxableAmount2 = taxableAmount2;
	}

	public Double getTaxAmount2() {
		return TaxAmount2;
	}

	public void setTaxAmount2(Double taxAmount2) {
		TaxAmount2 = taxAmount2;
	}

	public String getPriceEffectiveFrom() {
		return PriceEffectiveFrom;
	}

	public void setPriceEffectiveFrom(String priceEffectiveFrom) {
		PriceEffectiveFrom = priceEffectiveFrom;
	}

	public String getPriceEffectiveUntil() {
		return PriceEffectiveUntil;
	}

	public void setPriceEffectiveUntil(String priceEffectiveUntil) {
		PriceEffectiveUntil = priceEffectiveUntil;
	}

	public List<DiscountCoded> getDiscountCoded() {
		return DiscountCoded;
	}

	public void setDiscountCoded(List<DiscountCoded> discountCoded) {
		DiscountCoded = discountCoded;
	}

}