/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

import java.util.List;

public class Prize
{
	public String PrizeCode;

	public String PrizeCountry;

	public List<String> PrizeJurys;

	public List<String> PrizeNames;

	public String PrizeYear;

	public String getPrizeCode() {
		return PrizeCode;
	}

	public void setPrizeCode(String prizeCode) {
		PrizeCode = prizeCode;
	}

	public String getPrizeCountry() {
		return PrizeCountry;
	}

	public void setPrizeCountry(String prizeCountry) {
		PrizeCountry = prizeCountry;
	}

	public List<String> getPrizeJurys() {
		return PrizeJurys;
	}

	public void setPrizeJurys(List<String> prizeJurys) {
		PrizeJurys = prizeJurys;
	}

	public List<String> getPrizeNames() {
		return PrizeNames;
	}

	public void setPrizeNames(List<String> prizeNames) {
		PrizeNames = prizeNames;
	}

	public String getPrizeYear() {
		return PrizeYear;
	}

	public void setPrizeYear(String prizeYear) {
		PrizeYear = prizeYear;
	}

	
	
}
