/**********************************************************************************************************************
RDate          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
	
	private String id;
	
	@JsonProperty("Product")
	private MetaData Product;

	private List<Assets> asset;

	@JsonProperty("CustomFields")
	private Map<Object, Object> CustomFields;
	
	@JsonProperty("metadataId")
	private String metadataId;
	
	
	
	public String getMetadataId() {
		return metadataId;
	}

	public void setMetadataId(String metadataId) {
		this.metadataId = metadataId;
	}

	public MetaData getProduct() {
		return Product;
	}

	public void setProduct(MetaData Product) {
		this.Product = Product;
	}

	public List<Assets> getAsset() {
		return asset;
	}

	public void setAsset(List<Assets> asset) {
		this.asset = asset;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<Object, Object> getCustomFields() {
		return CustomFields;
	}

	public void setCustomFields(Map<Object, Object> CustomFields) {
		this.CustomFields = CustomFields;
	}
}
