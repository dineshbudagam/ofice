/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class ProductClassification
{
	public String ProductClassificationType;

	public Double Percent;

	public String ProductClassificationCode;

	public String getProductClassificationType() {
		return ProductClassificationType;
	}

	public void setProductClassificationType(String productClassificationType) {
		ProductClassificationType = productClassificationType;
	}

	public Double getPercent() {
		return Percent;
	}

	public void setPercent(Double percent) {
		Percent = percent;
	}

	public String getProductClassificationCode() {
		return ProductClassificationCode;
	}

	public void setProductClassificationCode(String productClassificationCode) {
		ProductClassificationCode = productClassificationCode;
	}
	
}
