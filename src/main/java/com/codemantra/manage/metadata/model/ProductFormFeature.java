/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

import java.util.List;

public class ProductFormFeature
{
	public String ProductFormFeatureType;

	public List<String> ProductFormFeatureDescription;

	public String ProductFormFeatureValue;

	public String getProductFormFeatureType() {
		return ProductFormFeatureType;
	}

	public void setProductFormFeatureType(String productFormFeatureType) {
		ProductFormFeatureType = productFormFeatureType;
	}

	public List<String> getProductFormFeatureDescription() {
		return ProductFormFeatureDescription;
	}

	public void setProductFormFeatureDescription(List<String> productFormFeatureDescription) {
		ProductFormFeatureDescription = productFormFeatureDescription;
	}

	public String getProductFormFeatureValue() {
		return ProductFormFeatureValue;
	}

	public void setProductFormFeatureValue(String productFormFeatureValue) {
		ProductFormFeatureValue = productFormFeatureValue;
	}
}
