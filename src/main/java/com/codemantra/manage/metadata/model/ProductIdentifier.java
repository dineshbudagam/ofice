/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class ProductIdentifier
{
	public String ProductIDType;

	public String IDTypeName;

	public String IDValue;

	public String getProductIDType() {
		return ProductIDType;
	}

	public void setProductIDType(String productIDType) {
		ProductIDType = productIDType;
	}

	public String getIdTypeName() {
		return IDTypeName;
	}

	public void setIdTypeName(String IDTypeName) {
		this.IDTypeName = IDTypeName;
	}

	public String getIDValue() {
		return IDValue;
	}

	public void setIDValue(String iDValue) {
		IDValue = iDValue;
	}

	
}
