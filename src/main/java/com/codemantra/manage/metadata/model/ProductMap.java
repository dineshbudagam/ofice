package com.codemantra.manage.metadata.model;

import java.util.List;
import java.util.Map;


import com.codemantra.manage.metadata.entity.AssetEntity;

public class ProductMap {
	
	
	
	private String Id;

	
	private Map<Object, Object> Product;

	
	private List<AssetEntity> asset;

	
	private Map<Object, Object> CustomFields;

	private String metadataId;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public Map<Object, Object> getProduct() {
		return Product;
	}

	public void setProduct(Map<Object, Object> product) {
		Product = product;
	}

	public List<AssetEntity> getAsset() {
		return asset;
	}

	public void setAsset(List<AssetEntity> asset) {
		this.asset = asset;
	}

	public Map<Object, Object> getCustomFields() {
		return CustomFields;
	}

	public void setCustomFields(Map<Object, Object> customFields) {
		CustomFields = customFields;
	}

	public String getMetadataId() {
		return metadataId;
	}

	public void setMetadataId(String metadataId) {
		this.metadataId = metadataId;
	}
	
	

}
