package com.codemantra.manage.metadata.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="tProductStagesApproval")
public class ProductStagesApproval {


	@Field("metadataId")
	private String metadataId;
	
	@Field("productHierarchyId")
	private String productHierarchyId;
	
	@Field("approvedBy")
	private String approvedBy;
	
	@Field("approvedOn")
	private Date approvedOn;
	
	@Field("stageDisplayName")
	private String stageDisplayName;
	
	@Field("stageId")
	private String stageId;
	
	@Field("approvalMethod")
	private String approvalMethod;
	
	@Field("remarks")
	private String remarks;
	
	@Field("createdBy")
	private String createdBy;
	
	@Field("createdOn")
	private Date createdOn;
	
	@Field("modifiedBy")
	private String modifiedBy;
	
	@Field("modifiedOn")
	private Date modifiedOn;

	public String getMetadataId() {
		return metadataId;
	}

	public void setMetadataId(String metadataId) {
		this.metadataId = metadataId;
	}

	public String getProductHierarchyId() {
		return productHierarchyId;
	}

	public void setProductHierarchyId(String productHierarchyId) {
		this.productHierarchyId = productHierarchyId;
	}

	public String getStageId() {
		return stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public String getApprovalMethod() {
		return approvalMethod;
	}

	public void setApprovalMethod(String approvalMethod) {
		this.approvalMethod = approvalMethod;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getStageDisplayName() {
		return stageDisplayName;
	}

	public void setStageDisplayName(String stageDisplayName) {
		this.stageDisplayName = stageDisplayName;
	}
	
	

}
