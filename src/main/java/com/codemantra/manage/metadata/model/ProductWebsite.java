/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class ProductWebsite
{
	public String WebsiteRole;

	public String ProductWebsiteDescription;

	public String ProductWebsiteLink;

	public String getWebsiteRole() {
		return WebsiteRole;
	}

	public void setWebsiteRole(String websiteRole) {
		WebsiteRole = websiteRole;
	}

	public String getProductWebsiteDescription() {
		return ProductWebsiteDescription;
	}

	public void setProductWebsiteDescription(String productWebsiteDescription) {
		ProductWebsiteDescription = productWebsiteDescription;
	}

	public String getProductWebsiteLink() {
		return ProductWebsiteLink;
	}

	public void setProductWebsiteLink(String productWebsiteLink) {
		ProductWebsiteLink = productWebsiteLink;
	}
}
