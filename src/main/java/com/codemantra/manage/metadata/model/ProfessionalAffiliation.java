/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class ProfessionalAffiliation
{
	public String ProfessionalPosition;
	
	public String Affiliation;

	public String getProfessionalPosition() {
		return ProfessionalPosition;
	}

	public void setProfessionalPosition(String professionalPosition) {
		ProfessionalPosition = professionalPosition;
	}

	public String getAffiliation() {
		return Affiliation;
	}

	public void setAffiliation(String affiliation) {
		Affiliation = affiliation;
	}
	
}
