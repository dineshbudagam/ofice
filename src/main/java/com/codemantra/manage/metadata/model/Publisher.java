/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class Publisher
{
	public String PublishingRole;
	public String PublisherName;
	public String PublishingStatus;
	
	public String getPublishingRole() {
		return PublishingRole;
	}
	public void setPublishingRole(String publishingRole) {
		PublishingRole = publishingRole;
	}
	public String getPublisherName() {
		return PublisherName;
	}
	public void setPublisherName(String publisherName) {
		PublisherName = publisherName;
	}
	public String getPublishingStatus() {
		return PublishingStatus;
	}
	public void setPublishingStatus(String publishingStatus) {
		PublishingStatus = publishingStatus;
	}
}