/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class RelatedProduct
{
	public String RelationCode;

	public String ISBN;

	public String EAN13;

	public List<ProductIdentifier> ProductIdentifier;

	public List<Website> Website;

	public String ProductForm;

	public List<String> ProductFormDetail;

	public List<ProductFormFeature> ProductFormFeature;

	public List<String> BookFormDetail;

	public String ProductPackaging;

	public String ProductFormDescription;

	public String NumberOfPieces;

	public String TradeCategory;

	public List<String> ProductContentType;

	public String EpubType;

	public String EpubTypeVersion;

	public String EpubTypeDescription;

	public String EpubFormat;

	public String EpubFormatVersion;

	public String EpubFormatDescription;

	public String EpubTypeNote;

	public List<Publisher> Publisher;

	public String getRelationCode() {
		return RelationCode;
	}

	public void setRelationCode(String relationCode) {
		RelationCode = relationCode;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getEAN13() {
		return EAN13;
	}

	public void setEAN13(String eAN13) {
		EAN13 = eAN13;
	}

	public List<ProductIdentifier> getProductIdentifier() {
		return ProductIdentifier;
	}

	public void setProductIdentifier(List<ProductIdentifier> productIdentifier) {
		ProductIdentifier = productIdentifier;
	}

	public List<Website> getWebsite() {
		return Website;
	}

	public void setWebsite(List<Website> website) {
		Website = website;
	}

	public String getProductForm() {
		return ProductForm;
	}

	public void setProductForm(String productForm) {
		ProductForm = productForm;
	}

	public List<String> getProductFormDetail() {
		return ProductFormDetail;
	}

	public void setProductFormDetail(List<String> productFormDetail) {
		ProductFormDetail = productFormDetail;
	}

	public List<ProductFormFeature> getProductFormFeature() {
		return ProductFormFeature;
	}

	public void setProductFormFeature(List<ProductFormFeature> productFormFeature) {
		ProductFormFeature = productFormFeature;
	}

	public List<String> getBookFormDetail() {
		return BookFormDetail;
	}

	public void setBookFormDetail(List<String> bookFormDetail) {
		BookFormDetail = bookFormDetail;
	}

	public String getProductPackaging() {
		return ProductPackaging;
	}

	public void setProductPackaging(String productPackaging) {
		ProductPackaging = productPackaging;
	}

	public String getProductFormDescription() {
		return ProductFormDescription;
	}

	public void setProductFormDescription(String productFormDescription) {
		ProductFormDescription = productFormDescription;
	}

	public String getNumberOfPieces() {
		return NumberOfPieces;
	}

	public void setNumberOfPieces(String numberOfPieces) {
		NumberOfPieces = numberOfPieces;
	}

	public String getTradeCategory() {
		return TradeCategory;
	}

	public void setTradeCategory(String tradeCategory) {
		TradeCategory = tradeCategory;
	}

	public List<String> getProductContentType() {
		return ProductContentType;
	}

	public void setProductContentType(List<String> productContentType) {
		ProductContentType = productContentType;
	}

	public String getEpubType() {
		return EpubType;
	}

	public void setEpubType(String epubType) {
		EpubType = epubType;
	}

	public String getEpubTypeVersion() {
		return EpubTypeVersion;
	}

	public void setEpubTypeVersion(String epubTypeVersion) {
		EpubTypeVersion = epubTypeVersion;
	}

	public String getEpubTypeDescription() {
		return EpubTypeDescription;
	}

	public void setEpubTypeDescription(String epubTypeDescription) {
		EpubTypeDescription = epubTypeDescription;
	}

	public String getEpubFormat() {
		return EpubFormat;
	}

	public void setEpubFormat(String epubFormat) {
		EpubFormat = epubFormat;
	}

	public String getEpubFormatVersion() {
		return EpubFormatVersion;
	}

	public void setEpubFormatVersion(String epubFormatVersion) {
		EpubFormatVersion = epubFormatVersion;
	}

	public String getEpubFormatDescription() {
		return EpubFormatDescription;
	}

	public void setEpubFormatDescription(String epubFormatDescription) {
		EpubFormatDescription = epubFormatDescription;
	}

	public String getEpubTypeNote() {
		return EpubTypeNote;
	}

	public void setEpubTypeNote(String epubTypeNote) {
		EpubTypeNote = epubTypeNote;
	}

	public List<Publisher> getPublisher() {
		return Publisher;
	}

	public void setPublisher(List<Publisher> publisher) {
		Publisher = publisher;
	}

}
