/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class SalesOutlet
{
	public String SalesOutletIdentifier;

	public String SalesOutletName;

	public String getSalesOutletIdentifier() {
		return SalesOutletIdentifier;
	}

	public void setSalesOutletIdentifier(String salesOutletIdentifier) {
		SalesOutletIdentifier = salesOutletIdentifier;
	}

	public String getSalesOutletName() {
		return SalesOutletName;
	}

	public void setSalesOutletName(String salesOutletName) {
		SalesOutletName = salesOutletName;
	}

}
