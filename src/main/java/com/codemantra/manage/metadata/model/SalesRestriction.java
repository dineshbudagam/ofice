/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class SalesRestriction
{
	public String SalesRestrictionType;

	public List<SalesOutlet> SalesOutlet;

	public String SalesRestrictionDetail;

	public String getSalesRestrictionType() {
		return SalesRestrictionType;
	}

	public void setSalesRestrictionType(String salesRestrictionType) {
		SalesRestrictionType = salesRestrictionType;
	}

	public List<SalesOutlet> getSalesOutlet() {
		return SalesOutlet;
	}

	public void setSalesOutlet(List<SalesOutlet> salesOutlet) {
		SalesOutlet = salesOutlet;
	}

	public String getSalesRestrictionDetail() {
		return SalesRestrictionDetail;
	}

	public void setSalesRestrictionDetail(String salesRestrictionDetail) {
		SalesRestrictionDetail = salesRestrictionDetail;
	}

	
}
