/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class SalesRights
{
	public String SalesRightsType;
	public List<String> RightsCountry; // TODO: need to be contracted to merely a Set
	//public Set<Regions> Regions;
	public List<String> RightRegions; // only in Onix2
	//public Set<CountryCodes> CountriesExcluded; // only in Onix3
	//public Set<Regions> RegionsExcluded; // only in Onix3
	
	public String getSalesRightsType() {
		return SalesRightsType;
	}
	public void setSalesRightsType(String salesRightsType) {
		SalesRightsType = salesRightsType;
	}
	public List<String> getRightRegions() {
		return RightRegions;
	}
	public List<String> getRightsCountry() {
		return RightsCountry;
	}
	public void setRightsCountry(List<String> rightsCountry) {
		RightsCountry = rightsCountry;
	}
	public void setRightRegions(List<String> rightRegions) {
		RightRegions = rightRegions;
	}
}