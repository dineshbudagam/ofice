/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
13-06-2017			v1.0       	     Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.Date;
import java.util.Map;

public class Search {
	
	private HeaderSearch headerSearch;
	
	private AdvancedSearch advancedSearch;
	
	private ImprintAssetSearch imprintAssetSearch;
	
	private String searchName;
	
	private String userId;
	
	private String permissionGroupId;
	
	private String fieldDisplayName;
	
	private boolean isActive;
	
	private boolean isDeleted;
	
	private boolean exportCheck;
	
	private String createdBy;
	
	private Date createdDate;
	
	private String modifiedBy;
	
	private Date modifiedDate;
	
	private int resultCount;
	
	private String changeSearchName;
	
	private Map<String, Object> columnFilterValues;
	
	private int skipCount;
	
	private long searchReultCount;
	
	private String searchflag;
	
	private String suggestionText;
	
	private String filterFieldName;
	
	private Map<String, Object> inColumnFilterList;
	
	private Map<String, Object> ninColumnFilterList;
	
	private Map<String, Object> fromDateFilterList;
	
	private Map<String, Object> toDateFilterList;
	
	private String pageName;
	
	
	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getResultCount() {
		return resultCount;
	}

	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getChangeSearchName() {
		return changeSearchName;
	}

	public void setChangeSearchName(String changeSearchName) {
		this.changeSearchName = changeSearchName;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public Map<String, Object> getColumnFilterValues() {
		return columnFilterValues;
	}

	public void setColumnFilterValues(Map<String, Object> columnFilterValues) {
		this.columnFilterValues = columnFilterValues;
	}

	public int getSkipCount() {
		return skipCount;
	}

	public void setSkipCount(int skipCount) {
		this.skipCount = skipCount;
	}

	public long getSearchReultCount() {
		return searchReultCount;
	}

	public void setSearchReultCount(long searchReultCount) {
		this.searchReultCount = searchReultCount;
	}

	public HeaderSearch getHeaderSearch() {
		return headerSearch;
	}

	public void setHeaderSearch(HeaderSearch headerSearch) {
		this.headerSearch = headerSearch;
	}

	public AdvancedSearch getAdvancedSearch() {
		return advancedSearch;
	}

	public void setAdvancedSearch(AdvancedSearch advancedSearch) {
		this.advancedSearch = advancedSearch;
	}

	public ImprintAssetSearch getImprintAssetSearch() {
		return imprintAssetSearch;
	}

	public void setImprintAssetSearch(ImprintAssetSearch imprintAssetSearch) {
		this.imprintAssetSearch = imprintAssetSearch;
	}

	public String getSearchflag() {
		return searchflag;
	}

	public void setSearchflag(String searchflag) {
		this.searchflag = searchflag;
	}
	
	public String getPermissionGroupId() {
		return permissionGroupId;
	}

	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}
	
	public String getSuggestionText() {
		return suggestionText;
	}

	public void setSuggestionText(String suggestionText) {
		this.suggestionText = suggestionText;
	}
	
	public String getFilterFieldName() {
		return filterFieldName;
	}

	public void setFilterFieldName(String filterFieldName) {
		this.filterFieldName = filterFieldName;
	}

	public Map<String, Object> getInColumnFilterList() {
		return inColumnFilterList;
	}

	public void setInColumnFilterList(Map<String, Object> inColumnFilterList) {
		this.inColumnFilterList = inColumnFilterList;
	}

	public Map<String, Object> getNinColumnFilterList() {
		return ninColumnFilterList;
	}

	public void setNinColumnFilterList(Map<String, Object> ninColumnFilterList) {
		this.ninColumnFilterList = ninColumnFilterList;
	}

	public Map<String, Object> getFromDateFilterList() {
		return fromDateFilterList;
	}

	public void setFromDateFilterList(Map<String, Object> fromDateFilterList) {
		this.fromDateFilterList = fromDateFilterList;
	}

	public Map<String, Object> getToDateFilterList() {
		return toDateFilterList;
	}

	public void setToDateFilterList(Map<String, Object> toDateFilterList) {
		this.toDateFilterList = toDateFilterList;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}

	public boolean isExportCheck() {
		return exportCheck;
	}

	public void setExportCheck(boolean exportCheck) {
		this.exportCheck = exportCheck;
	}
	
	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	@Override
	public String toString() {
		return "Search [headerSearch=" + headerSearch + ", advancedSearch=" + advancedSearch + ", imprintAssetSearch="
				+ imprintAssetSearch + ", searchName=" + searchName + ", userId=" + userId + ", permissionGroupId="
				+ permissionGroupId + ", fieldDisplayName=" + fieldDisplayName + ", isActive=" + isActive
				+ ", isDeleted=" + isDeleted + ", exportCheck=" + exportCheck + ", createdBy=" + createdBy
				+ ", createdDate=" + createdDate + ", modifiedBy=" + modifiedBy + ", modifiedDate=" + modifiedDate
				+ ", resultCount=" + resultCount + ", changeSearchName=" + changeSearchName + ", columnFilterValues="
				+ columnFilterValues + ", skipCount=" + skipCount + ", searchReultCount=" + searchReultCount
				+ ", searchflag=" + searchflag + ", suggestionText=" + suggestionText + "]";
	}
	
	
	
}
