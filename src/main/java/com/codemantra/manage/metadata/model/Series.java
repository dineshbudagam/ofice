/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class Series
{
	public String SeriesISSN;

	public String PublisherSeriesCode;

	public List<SeriesIdentifier> SeriesIdentifier;

	public String TitleOfSeries;

	public List<Title> Title;

	public List<Contributor> Contributor;

	public String NumberWithinSeries;

	public String YearOfAnnual;

	public String getSeriesISSN() {
		return SeriesISSN;
	}

	public void setSeriesISSN(String seriesISSN) {
		SeriesISSN = seriesISSN;
	}

	public String getPublisherSeriesCode() {
		return PublisherSeriesCode;
	}

	public void setPublisherSeriesCode(String publisherSeriesCode) {
		PublisherSeriesCode = publisherSeriesCode;
	}

	public List<SeriesIdentifier> getSeriesIdentifier() {
		return SeriesIdentifier;
	}

	public void setSeriesIdentifier(List<SeriesIdentifier> seriesIdentifier) {
		SeriesIdentifier = seriesIdentifier;
	}

	public String getTitleOfSeries() {
		return TitleOfSeries;
	}

	public void setTitleOfSeries(String titleOfSeries) {
		TitleOfSeries = titleOfSeries;
	}

	public List<Title> getTitle() {
		return Title;
	}

	public void setTitle(List<Title> title) {
		Title = title;
	}

	public List<Contributor> getContributor() {
		return Contributor;
	}

	public void setContributor(List<Contributor> contributor) {
		Contributor = contributor;
	}

	public String getNumberWithinSeries() {
		return NumberWithinSeries;
	}

	public void setNumberWithinSeries(String numberWithinSeries) {
		NumberWithinSeries = numberWithinSeries;
	}

	public String getYearOfAnnual() {
		return YearOfAnnual;
	}

	public void setYearOfAnnual(String yearOfAnnual) {
		YearOfAnnual = yearOfAnnual;
	}

	
}
