/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class SeriesIdentifier
{
	public String SeriesIDType;

	public String IDTypeName;

	public String IDValue;

	public String getSeriesIDType() {
		return SeriesIDType;
	}

	public void setSeriesIDType(String seriesIDType) {
		SeriesIDType = seriesIDType;
	}

	public String getIDTypeName() {
		return IDTypeName;
	}

	public void setIDTypeName(String iDTypeName) {
		IDTypeName = iDTypeName;
	}

	public String getIDValue() {
		return IDValue;
	}

	public void setIDValue(String iDValue) {
		IDValue = iDValue;
	}

	
}
