/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class Set
{
	public String ISBNOfSet;

	public String EAN13OfSet;

	public List<ProductIdentifier> ProductIdentifier;

	public String TitleOfSet;

	public List<Title> Title;

	public String SetPartNumber;

	public String SetPartTitle;

	public String ItemNumberWithinSet;

	public String LevelSequenceNumber;

	public String SetItemTitle;

	public String getISBNOfSet() {
		return ISBNOfSet;
	}

	public void setISBNOfSet(String iSBNOfSet) {
		ISBNOfSet = iSBNOfSet;
	}

	public String getEAN13OfSet() {
		return EAN13OfSet;
	}

	public void setEAN13OfSet(String eAN13OfSet) {
		EAN13OfSet = eAN13OfSet;
	}

	public List<ProductIdentifier> getProductIdentifier() {
		return ProductIdentifier;
	}

	public void setProductIdentifier(List<ProductIdentifier> productIdentifier) {
		ProductIdentifier = productIdentifier;
	}

	public String getTitleOfSet() {
		return TitleOfSet;
	}

	public void setTitleOfSet(String titleOfSet) {
		TitleOfSet = titleOfSet;
	}

	public List<Title> getTitle() {
		return Title;
	}

	public void setTitle(List<Title> title) {
		Title = title;
	}

	public String getSetPartNumber() {
		return SetPartNumber;
	}

	public void setSetPartNumber(String setPartNumber) {
		SetPartNumber = setPartNumber;
	}

	public String getSetPartTitle() {
		return SetPartTitle;
	}

	public void setSetPartTitle(String setPartTitle) {
		SetPartTitle = setPartTitle;
	}

	public String getItemNumberWithinSet() {
		return ItemNumberWithinSet;
	}

	public void setItemNumberWithinSet(String itemNumberWithinSet) {
		ItemNumberWithinSet = itemNumberWithinSet;
	}

	public String getLevelSequenceNumber() {
		return LevelSequenceNumber;
	}

	public void setLevelSequenceNumber(String levelSequenceNumber) {
		LevelSequenceNumber = levelSequenceNumber;
	}

	public String getSetItemTitle() {
		return SetItemTitle;
	}

	public void setSetItemTitle(String setItemTitle) {
		SetItemTitle = setItemTitle;
	}

}
