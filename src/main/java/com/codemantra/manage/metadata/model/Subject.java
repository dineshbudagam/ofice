/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class Subject
{
	public String SubjectSchemeIdentifier;
	
	public String SubjectCode;

	public String SubjectHeadingText;

	public String SubjectSchemeName;

	public String SubjectSchemeVersion;

	public String getSubjectSchemeIdentifier() {
		return SubjectSchemeIdentifier;
	}

	public void setSubjectSchemeIdentifier(String subjectSchemeIdentifier) {
		SubjectSchemeIdentifier = subjectSchemeIdentifier;
	}

	public String getSubjectCode() {
		return SubjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		SubjectCode = subjectCode;
	}

	public String getSubjectSchemeName() {
		return SubjectSchemeName;
	}

	public void setSubjectSchemeName(String subjectSchemeName) {
		SubjectSchemeName = subjectSchemeName;
	}

	public String getSubjectSchemeVersion() {
		return SubjectSchemeVersion;
	}

	public void setSubjectSchemeVersion(String subjectSchemeVersion) {
		SubjectSchemeVersion = subjectSchemeVersion;
	}

	public String getSubjectHeadingText() {
		return SubjectHeadingText;
	}

	public void setSubjectHeadingText(String subjectHeadingText) {
		SubjectHeadingText = subjectHeadingText;
	}
	
}