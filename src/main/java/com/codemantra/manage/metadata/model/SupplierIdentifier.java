/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class SupplierIdentifier
{
	public String SupplierIDType;

	public String IdTypeName;

	public String IDValue;

	public String getSupplierIDType() {
		return SupplierIDType;
	}

	public void setSupplierIDType(String supplierIDType) {
		SupplierIDType = supplierIDType;
	}

	public String getIdTypeName() {
		return IdTypeName;
	}

	public void setIdTypeName(String idTypeName) {
		IdTypeName = idTypeName;
	}

	public String getIDValue() {
		return IDValue;
	}

	public void setIDValue(String iDValue) {
		IDValue = iDValue;
	}
}
