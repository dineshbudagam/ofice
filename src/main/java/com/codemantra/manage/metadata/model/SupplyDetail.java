/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

import java.util.List;

public class SupplyDetail
{
	public List<SupplierIdentifier> SupplierIdentifier;
	
	public String SupplierSAN;
	
	public String SupplierEANLocationNumber;
	
	public String SupplierName;

	public String TelephoneNumber;

	public String FaxNumber;
	
	public String EmailAddress;
	
	/*public List<String> TelephoneNumber;
	public List<String> FaxNumber;
	public List<String> EmailAddress;*/
	
	public List<Website> Website;

	public String SupplierRole;

	public List<String> SupplyToCountry;
	
	public String SupplyToTerritory;

	public String SupplyToRegion;
	
	public List<String> SupplyToCountryExcluded;
	
	public String SupplyRestrictionDetail;
	
	public String ReturnsCodeType;
	
	public String ReturnsCode;
	
	public String LastDateForReturn;

	public String AvailabilityCode;

	public String ProductAvailability;
	
	public String IntermediaryAvailabilityCode;
	
	/*public String NewSupplier;*/
	
	public String DateFormat;
	
	public String ExpectedShipDate;
	
	public String OnSaleDate;
	
	public String OrderTime;
	
	/*public List<Stock> Stock;*/
	
	public String PackQuantity;
	
	public String AudienceRestrictionFlag;
	
	public String AudienceRestrictionNote;
	
	public Double PriceAmount;
	
	public String UnpricedItemType;
	
	public List<Price> Price;

	public String Reissue;

	public List<SupplierIdentifier> getSupplierIdentifier() {
		return SupplierIdentifier;
	}

	public void setSupplierIdentifier(List<SupplierIdentifier> supplierIdentifier) {
		SupplierIdentifier = supplierIdentifier;
	}

	public String getSupplierSAN() {
		return SupplierSAN;
	}

	public void setSupplierSAN(String supplierSAN) {
		SupplierSAN = supplierSAN;
	}

	public String getSupplierEANLocationNumber() {
		return SupplierEANLocationNumber;
	}

	public void setSupplierEANLocationNumber(String supplierEANLocationNumber) {
		SupplierEANLocationNumber = supplierEANLocationNumber;
	}

	public String getSupplierName() {
		return SupplierName;
	}

	public void setSupplierName(String supplierName) {
		SupplierName = supplierName;
	}

	public String getTelephoneNumber() {
		return TelephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		TelephoneNumber = telephoneNumber;
	}

	public String getFaxNumber() {
		return FaxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		FaxNumber = faxNumber;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddresss(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public List<Website> getWebsite() {
		return Website;
	}

	public void setWebsite(List<Website> website) {
		Website = website;
	}

	public String getSupplierRole() {
		return SupplierRole;
	}

	public void setSupplierRole(String supplierRole) {
		SupplierRole = supplierRole;
	}

	public List<String> getSupplyToCountry() {
		return SupplyToCountry;
	}

	public void setSupplyToCountry(List<String> supplyToCountry) {
		SupplyToCountry = supplyToCountry;
	}

	public String getSupplyToTerritory() {
		return SupplyToTerritory;
	}

	public void setSupplyToTerritory(String supplyToTerritory) {
		SupplyToTerritory = supplyToTerritory;
	}

	public List<String> getSupplyToCountryExcluded() {
		return SupplyToCountryExcluded;
	}

	public void setSupplyToCountryExcluded(List<String> supplyToCountryExcluded) {
		SupplyToCountryExcluded = supplyToCountryExcluded;
	}

	public String getSupplyRestrictionDetail() {
		return SupplyRestrictionDetail;
	}

	public void setSupplyRestrictionDetail(String supplyRestrictionDetail) {
		SupplyRestrictionDetail = supplyRestrictionDetail;
	}

	public String getReturnsCodeType() {
		return ReturnsCodeType;
	}

	public void setReturnsCodeType(String returnsCodeType) {
		ReturnsCodeType = returnsCodeType;
	}

	public String getReturnsCode() {
		return ReturnsCode;
	}

	public void setReturnsCode(String returnsCode) {
		ReturnsCode = returnsCode;
	}

	public String getLastDateForReturn() {
		return LastDateForReturn;
	}

	public void setLastDateForReturn(String lastDateForReturn) {
		LastDateForReturn = lastDateForReturn;
	}

	public String getAvailabilityCode() {
		return AvailabilityCode;
	}

	public void setAvailabilityCode(String availabilityCode) {
		AvailabilityCode = availabilityCode;
	}

	public String getProductAvailability() {
		return ProductAvailability;
	}

	public void setProductAvailability(String productAvailability) {
		ProductAvailability = productAvailability;
	}

	public String getIntermediaryAvailabilityCode() {
		return IntermediaryAvailabilityCode;
	}

	public void setIntermediaryAvailabilityCode(String intermediaryAvailabilityCode) {
		IntermediaryAvailabilityCode = intermediaryAvailabilityCode;
	}

	public String getDateFormat() {
		return DateFormat;
	}

	public void setDateFormat(String dateFormat) {
		DateFormat = dateFormat;
	}

	public String getExpectedShipDate() {
		return ExpectedShipDate;
	}

	public void setExpectedShipDate(String expectedShipDate) {
		ExpectedShipDate = expectedShipDate;
	}

	public String getOnSaleDate() {
		return OnSaleDate;
	}

	public void setOnSaleDate(String onSaleDate) {
		OnSaleDate = onSaleDate;
	}

	public String getOrderTime() {
		return OrderTime;
	}

	public void setOrderTime(String orderTime) {
		OrderTime = orderTime;
	}

	public String getPackQuantity() {
		return PackQuantity;
	}

	public void setPackQuantity(String packQuantity) {
		PackQuantity = packQuantity;
	}

	public String getAudienceRestrictionFlag() {
		return AudienceRestrictionFlag;
	}

	public void setAudienceRestrictionFlag(String audienceRestrictionFlag) {
		AudienceRestrictionFlag = audienceRestrictionFlag;
	}

	public String getAudienceRestrictionNote() {
		return AudienceRestrictionNote;
	}

	public void setAudienceRestrictionNote(String audienceRestrictionNote) {
		AudienceRestrictionNote = audienceRestrictionNote;
	}

	public Double getPriceAmount() {
		return PriceAmount;
	}

	public void setPriceAmount(Double priceAmount) {
		PriceAmount = priceAmount;
	}

	public String getUnpricedItemType() {
		return UnpricedItemType;
	}

	public void setUnpricedItemType(String unpricedItemType) {
		UnpricedItemType = unpricedItemType;
	}

	public List<Price> getPrice() {
		return Price;
	}

	public void setPrice(List<Price> price) {
		Price = price;
	}

	public String getReissue() {
		return Reissue;
	}

	public void setReissue(String reissue) {
		Reissue = reissue;
	}

	public String getSupplyToRegion() {
		return SupplyToRegion;
	}

	public void setSupplyToRegion(String supplyToRegion) {
		SupplyToRegion = supplyToRegion;
	}
}