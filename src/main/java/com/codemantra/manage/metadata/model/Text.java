/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class Text
{
	public String TextType;
	public String TextFormat;
	public String Text;
	public String getTextType() {
		return TextType;
	}
	public void setTextType(String textType) {
		TextType = textType;
	}
	public String getTextFormat() {
		return TextFormat;
	}
	public void setTextFormat(String textFormat) {
		TextFormat = textFormat;
	}
	public String getText() {
		return Text;
	}
	public void setText(String text) {
		Text = text;
	}
}