/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.model;

public class Title
{
	public String TitleType;
	public String TitleText;
	public String TitleWithoutPrefix;
	public String Subtitle;
	public String getTitleType() {
		return TitleType;
	}
	public void setTitleType(String titleType) {
		TitleType = titleType;
	}
	public String getTitleText() {
		return TitleText;
	}
	public void setTitleText(String titleText) {
		TitleText = titleText;
	}
	public String getTitleWithoutPrefix() {
		return TitleWithoutPrefix;
	}
	public void setTitleWithoutPrefix(String titleWithoutPrefix) {
		TitleWithoutPrefix = titleWithoutPrefix;
	}
	public String getSubtitle() {
		return Subtitle;
	}
	public void setSubtitle(String subtitle) {
		Subtitle = subtitle;
	}
	
	
	
	
}