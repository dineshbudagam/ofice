/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
20-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.metadata.model;

public class Website
{
	public String WebsiteDescription;

	public String WebsiteLink;

	public String WebsiteRole;

	public String getWebsiteDescription() {
		return WebsiteDescription;
	}

	public void setWebsiteDescription(String websiteDescription) {
		WebsiteDescription = websiteDescription;
	}

	public String getWebsiteLink() {
		return WebsiteLink;
	}

	public void setWebsiteLink(String websiteLink) {
		WebsiteLink = websiteLink;
	}

	public String getWebsiteRole() {
		return WebsiteRole;
	}

	public void setWebsiteRole(String websiteRole) {
		WebsiteRole = websiteRole;
	}
}
