/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
08-06-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.service;

import java.util.List;
import java.util.Map;

import com.codemantra.manage.metadata.dto.APIResponse;
import com.codemantra.manage.metadata.model.FileObj;
import com.codemantra.manage.metadata.model.MetaDataGroup;
import com.codemantra.manage.metadata.model.MetaDataView;
import com.codemantra.manage.metadata.model.MetaDataXML;
import com.codemantra.manage.metadata.model.Product;
import com.codemantra.manage.support.tables.request.entities.SupportRequestEntity;

public interface MetaDataService {
	
	public APIResponse<Object> getAllProductHierarchies();
	
	public APIResponse<Object> getCreateProductHierarchyFields(String productHierarchyId);
	
	public APIResponse<Object> getCurrentProductHierarchyFields(String productHierarchyId);

	public APIResponse<Object> getCurrentProductHierarchyFields(List<String> productHierarchyIds);

	public APIResponse<Object> getAllLookUpData();
	
	public List<MetaDataGroup> getMetaDataFields();
	
	public APIResponse<Object> getMetadataConfigDetails();
	
	public APIResponse<Object> existIsbnCheck(String isbnValue);
	
	public APIResponse<Object> saveMetaData(Map<String,Object> obj);
	
	public APIResponse<Object> editMetaData(MetaDataView editMdata);
	
	public APIResponse<Object> updateMetaData(Map<String, Object> obj);
	
	public APIResponse<Object> customTemplateMData(Map<String, Object> custMdata);
	
	public APIResponse<Object> uploadMetaData(FileObj obj);
	
	public APIResponse<Object> exportMetaDataXml(MetaDataXML metaDataXml);

	public APIResponse<Object> getProducts();

	public APIResponse<Object> getProductWithRelations(String id,String pHid, String loggedUser);

	public APIResponse<Object> updateProductWithSync(Product product, String userId);

	public APIResponse<Object> saveProduct(Map<Object, Object> product, String loggedUser);
	
	public Map<String,Object> getSupportData(SupportRequestEntity entity);
           
    public Map<String,Object> getCascadingData(SupportRequestEntity entity);

	public APIResponse<Object> getOrderId(String reqFields);

	public APIResponse<Object> productSync(Map<String, Object> reqFields);

	public APIResponse<Object> getISBNValue();

	public APIResponse<Object> getSupportTablesList();

    public APIResponse<Object> getSupportTableMapping();
    
    public APIResponse<Object> getConfigurationForSupportTable(SupportRequestEntity entity);

    public APIResponse<Object> viewSearchForSupportTable(SupportRequestEntity lookupName) ;
    
    public APIResponse<Object> downloadExcelForSupportTable(SupportRequestEntity lookupName);
        
    public APIResponse<Object> uploadExcelForSupportTable(FileObj fileObj);
    
    public APIResponse<Object> exportSupportTable(SupportRequestEntity supportEntity);
    
    public APIResponse<Object> addOneSupportTableRecord(SupportRequestEntity entity);
        
    public APIResponse<Object> editOneSupportTableRecord(SupportRequestEntity entity);
       

	public APIResponse<Object> checkLockOnProduct(String userId,String pHId, String productId);

	public APIResponse<Object> removeLockOnProduct(String userId);

	public APIResponse<Object> lockProduct(String userId,String pHId, String productId);
 
        public APIResponse<Object> actiDeactivateSupportConfig(SupportRequestEntity entity);
        
        public APIResponse<Object> getSiblingsData(Map<String, Object> obj);
        
        public APIResponse<Object> getSupplementStatus(Map<String, Object> obj);
        
        public APIResponse<Object> updateManualStageApprove(Map<String, String> obj);

	public APIResponse<Object> retrieveFieldsBasedOnAvailCode(String availCode, String metadataId);

        public APIResponse<Object> createSupportTable(SupportRequestEntity entity);

        public APIResponse<Object> getSupportTableInfo(SupportRequestEntity entity);

	public APIResponse<Object> checkForDuplicate(Map<String, Object> reqFields); 
        
        public APIResponse<Object> mergeStageRecords(String loggedUser, String stageId, String phId,String metadataIds,String parentID); 
        
}
