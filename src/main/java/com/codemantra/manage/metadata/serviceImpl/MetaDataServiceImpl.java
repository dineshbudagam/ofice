/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
08-06-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.metadata.serviceImpl;

import static org.apache.poi.ss.usermodel.Cell.CELL_TYPE_BLANK;
import static org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregationOptions;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;
import static org.springframework.data.mongodb.core.aggregation.Fields.fields;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jongo.Jongo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.aggregation.UnwindOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.codemantra.manage.metadata.dao.MetaDataDao;
import com.codemantra.manage.metadata.dto.APIResponse;
import com.codemantra.manage.metadata.dto.APIResponse.ResponseCode;
import com.codemantra.manage.metadata.entity.AssetEntity;
import com.codemantra.manage.metadata.entity.CMetaFieldLogicCondition;
import com.codemantra.manage.metadata.entity.CMetaFieldLogicCustomFieldsString;
import com.codemantra.manage.metadata.entity.CMetaFieldLogicEntity;
import com.codemantra.manage.metadata.entity.CodeListEntity;
import com.codemantra.manage.metadata.entity.InactiveTitlesEntity;
import com.codemantra.manage.metadata.entity.JsonDbPathConditionEntity;
import com.codemantra.manage.metadata.entity.ManageConfigEntity;
import com.codemantra.manage.metadata.entity.MetaDataAutoPopulateEntity;
import com.codemantra.manage.metadata.entity.MetaDataFieldsEntity;
import com.codemantra.manage.metadata.entity.MetaDataGroupEntity;
import com.codemantra.manage.metadata.entity.MetaDataProductHierarchyStageEntity;
import com.codemantra.manage.metadata.entity.MetadataFields;
import com.codemantra.manage.metadata.entity.ModuleAccessEntity;
import com.codemantra.manage.metadata.entity.PermissionGroupEntity;
import com.codemantra.manage.metadata.entity.ProductEntity;
import com.codemantra.manage.metadata.entity.ProductHierarchyEntity;
import com.codemantra.manage.metadata.entity.ProductLockEntity.LockType;
import com.codemantra.manage.metadata.entity.ProductMapEntity;
import com.codemantra.manage.metadata.entity.ProductStagesApprovalEntity;
import com.codemantra.manage.metadata.entity.RoleEntity;
import com.codemantra.manage.metadata.entity.RuleEntity;
import com.codemantra.manage.metadata.entity.RulesEntity;
import com.codemantra.manage.metadata.entity.SequenceId;
import com.codemantra.manage.metadata.entity.SubModuleAccessEntity;
import com.codemantra.manage.metadata.entity.UserEntity;
import com.codemantra.manage.metadata.jsondiff.JsonDiff;
import com.codemantra.manage.metadata.model.Assets;
import com.codemantra.manage.metadata.model.CDCMetaData;
import com.codemantra.manage.metadata.model.CDCMetaDataDetails;
import com.codemantra.manage.metadata.model.FileObj;
import com.codemantra.manage.metadata.model.KeyValue;
import com.codemantra.manage.metadata.model.MetaData;
import com.codemantra.manage.metadata.model.MetaDataAutoPopulate;
import com.codemantra.manage.metadata.model.MetaDataFields;
import com.codemantra.manage.metadata.model.MetaDataGroup;
import com.codemantra.manage.metadata.model.MetaDataView;
import com.codemantra.manage.metadata.model.MetaDataXML;
import com.codemantra.manage.metadata.model.MetadataFieldConfig;
import com.codemantra.manage.metadata.model.MetadataGroupConfig;
import com.codemantra.manage.metadata.model.Product;
import com.codemantra.manage.metadata.model.ProductStagesApproval;
import com.codemantra.manage.metadata.service.MetaDataService;
import com.codemantra.manage.support.tables.entities.CascadingBean;
import com.codemantra.manage.support.tables.entities.Count;
import com.codemantra.manage.support.tables.entities.HeaderInfo;
import com.codemantra.manage.support.tables.entities.SupportTableConfigMain;
import com.codemantra.manage.support.tables.request.entities.FieldInfo;
import com.codemantra.manage.support.tables.request.entities.PageInfo;
import com.codemantra.manage.support.tables.request.entities.SupportRequestEntity;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import net.minidev.json.parser.JSONParser;

@Service("metaDataService")
public class MetaDataServiceImpl implements MetaDataService {
	private static final Logger logger = LoggerFactory.getLogger(MetaDataServiceImpl.class);
	RestTemplate restTemplate = null;
	Gson gson = null;

	@PostConstruct
	public void init() {
		this.restTemplate = new RestTemplate();
		this.gson = new Gson();
	}

	@Autowired
	MetaDataDao metaDataDao;

	// @Autowired
	// ObjectMapper mapper;

	@Autowired
	@Qualifier("primaryMongoTemplate")
	MongoTemplate mongoTemplate;

	@Autowired
	private Jongo jongo;

	static MongoTemplate cdcmongoTemplate;

	@Value(value = "${Product.Lock.Interval}")
	private long PRODUCT_LOCK_INTERVAL;

	@Value(value = "${SupportTableConfigCollection}")
	private String supportTableConfig;

	@Value(value = "${SupportTableQueriesCollection}")
	private String supportTableQueries;

	@Value(value = "${supportKey}")
	private String supportKey;

	@Value(value = "${separatorSymbol}")
	private String separatorSymbol;

	@Value(value = "${SupportTableCollection}")
	private String supportTableCollection;

	@Value(value = "${SupportMappingCollection}")
	private String supportMappingCollection;

	@Value(value = "${DEFAULT_PAGE_SIZE}")
	private Integer defaultPageSize;

	@Value("${ELASTICSEARCH.SERVICE.INDEX.URL}")
	String elasticSearchIndexURL;

	@Value("${ELASTICSEARCH.SERVICE.INDEX.UPDATE.URL}")
	String elasticSearchIndexUpdateURL;

	public static MongoTemplate getCdcmongoTemplate() {
		return cdcmongoTemplate;
	}

	@Autowired
	@Qualifier("cdcmongoTemplate")
	public void setCdcmongoTemplate(MongoTemplate cdcmongoTemplate) {
		MetaDataServiceImpl.cdcmongoTemplate = cdcmongoTemplate;
	}

	public static String metadataResults;

	public static String getMetadataResults() {
		return metadataResults;
	}

	@Value("${COLLECTION.TRANSACTIONS.STATIC.MDATARESULTS}")
	public static void setMetadataResults(String metadataResults) {
		MetaDataServiceImpl.metadataResults = metadataResults;
	}

	public static String metaDataFieldDetails;

	public static String getMetaDataFieldDetails() {
		return metaDataFieldDetails;
	}

	@Value("${COLLECTION.CONFIG.STATIC.MDATAFIELDS}")
	public void setMetaDataFieldDetails(String metaDataFieldDetails) {
		MetaDataServiceImpl.metaDataFieldDetails = metaDataFieldDetails;
	}

	public static String cdcMetadataTransctions;

	public static String getCdcMetadataTransctions() {
		return cdcMetadataTransctions;
	}

	@Value("${COLLECTION.TRANSACTIONS.CDC}")
	public static void setCdcMetadataTransctions(String cdcMetadataTransctions) {
		MetaDataServiceImpl.cdcMetadataTransctions = cdcMetadataTransctions;
	}

	@Value("${CUSTOMMDATA.DOWNLOAD.FILEPATH}")
	String strCustomDownloadPath;

	@Value("${CUSTOMMDATA.UPLOAD.FILEPATH}")
	String strCustomUploadPath;

	@Value("${RESPONSE.COMMON.SUCCESS}")
	String success;

	@Value("${RESPONSE.COMMON.FAILURE}")
	String failure;

	@Value("${RESPONSE.CODELIST.SUCCESS}")
	String successCodelist;

	@Value("${RESPONSE.CODELIST.FAILURE}")
	String failureCodeList;

	@Value("${RESPONSE.MDATA.CREATE.SUCCESS}")
	String successCreation;

	@Value("${RESPONSE.MDATA.CREATE.FAILURE}")
	String failureCreation;

	@Value("${RESPONSE.MDATA.RETRIEVE.SUCCESS}")
	String successRetrieval;

	@Value("${RESPONSE.MDATA.RETRIEVE.FAILURE}")
	String failureRetrieval;

	@Value("${RESPONSE.MDATA.UPDATE.SUCCESS}")
	String successUpdate;

	@Value("${RESPONSE.MDATA.UPDATE.FAILURE}")
	String failureUpdate;

	@Value("${REFPATH.PRODUCTIDTYPE.ISBN13}")
	String refPathIsbn13;

	@Value("${REFPATH.PRODUCTIDTYPE.ISBN10}")
	String refPathIsbn10;

	@Value("${REFPATH.PRODUCTIDTYPE.BIBLIO}")
	String refPathBiblio;

	@Value("${FORMAT.TEMPLATE.DATE}")
	String templateDateFormat;

	@Value("${FORMAT.TEMPLATE.FILE}")
	String templateFileFormat;

	@Value("${CUSTOM.TEMPLATE.TEMPLATENAME}")
	String templateName;

	@Value("${CUSTOM.TEMPLATE.KEYSHEET}")
	String templateKeySheet;

	@Value("${CUSTOM.TEMPLATE.KEYFIELD}")
	String templateKeyField;

	@Value("${RESPONSE.TEMPLATE.DOWNLOAD.SUCCESS}")
	String successDownload;

	@Value("${RESPONSE.TEMPLATE.DOWNLOAD.FAILURE}")
	String failureDownload;

	@Value("${RESPONSE.TEMPLATE.UPLOAD.SUCCESS}")
	String successUpload;

	@Value("${RESPONSE.TEMPLATE.UPLOAD.FAILURE}")
	String failureUpload;

	@Value("${VALIDATE.ISBN.EXIST}")
	String isbnExist;

	@Value("${VALIDATE.ISBN.NOTEXIST}")
	String isbnNotExist;

	@Value("${COLLECTION.CONFIG.MDATAVIEW}")
	String metaDataFieldView;

	@Value("${COLLECTION.CONFIG.MANAGE}")
	String manageConfig;

	public static String SIGN_HIDDEN_CELL_PREFIX = "$";
	public static String SIGN_ITEM_SPLITTER = ",";
	public static String SIGN_KEYVALUE_SPLITTER = ":";
	public static String SIGN_TABLE_REFERENCE_SPLITTER = "@";
	public static String SIGN_SHEETNAME_COLUMNNAME_SPLITTER = "#";

	@Override
	public APIResponse<Object> getAllProductHierarchies() {
		APIResponse<Object> response = new APIResponse<>();

		List<ProductHierarchyEntity> productHierarchyFields = metaDataDao.getAllProductHierarchies();
		if (null != productHierarchyFields && productHierarchyFields.size() > 0) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Successfully retrieved product hierarchy fields");
			response.setData(productHierarchyFields);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("No Records Found for the provided Hierarchy Id");
		}
		return response;
	}

	@Override
	public APIResponse<Object> getCurrentProductHierarchyFields(String productHierarchyId) {
		Map<String, List<? extends Object>> fields = new HashMap<String, List<? extends Object>>();
		APIResponse<Object> response = new APIResponse<>();

		try {
			fields = metaDataDao.getCurrentFieldData(productHierarchyId);

		} catch (Exception e) {
			logger.error("Error occurred while retrieving ProductHierarchyFields." + e);
		}
		if (fields != null && fields.get("Fields") != null && !fields.get("Fields").isEmpty()) {
			response.setCode(ResponseCode.SUCCESS.toString());
			response.setStatus(success);
			response.setStatusMessage(successCodelist);
			response.setData(fields);
		} else {
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus(failure);
			response.setStatusMessage("Metadata fields not found");
		}
		return response;

	}

	@Override
	public APIResponse<Object> getCurrentProductHierarchyFields(List<String> productHierarchyId) {
		Map<String, List<? extends Object>> fields = new HashMap<String, List<? extends Object>>();
		APIResponse<Object> response = new APIResponse<>();

		try {
			fields = metaDataDao.getCurrentFieldData(productHierarchyId);

		} catch (Exception e) {
			logger.error("Error occurred while retrieving ProductHierarchyFields." + e);
		}
		if (fields != null && fields.get("Fields") != null && !fields.get("Fields").isEmpty()) {
			response.setCode(ResponseCode.SUCCESS.toString());
			response.setStatus(success);
			response.setStatusMessage(successCodelist);
			response.setData(fields);
		} else {
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus(failure);
			response.setStatusMessage("Metadata fields not found");
		}
		return response;

	}

	/* Method Use - Used to populate Fields - Starts here */
	@Override
	public APIResponse<Object> getCreateProductHierarchyFields(String productHierarchyId) {
		Map<String, List<? extends Object>> fields = new HashMap<String, List<? extends Object>>();
		// Map<String,Map<String,List<? extends Object>>> finalFields = new
		// HashMap<String, Map<String,List<? extends Object>>>();
		APIResponse<Object> response = new APIResponse<>();

		try {
			fields = metaDataDao.getFieldData(productHierarchyId);
			// fields.put("Fields", metaDataField);
			// List<MetaDataProductHierarchyGroupsEntity> metadataGroups =
			// metaDataDao.getGroupData(productHierarchyId);
			// fields.put("Groups", metadataGroups);

		} catch (Exception e) {
			logger.error("Error occurred while retrieving ProductHierarchyFields." + e);
		}
		if (fields != null && fields.get("Fields") != null && !fields.get("Fields").isEmpty()) {
			response.setCode(ResponseCode.SUCCESS.toString());
			response.setStatus(success);
			response.setStatusMessage(successCodelist);
			response.setData(fields);
		} else {
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus(failure);
			response.setStatusMessage("Metadata fields not found");
		}
		return response;

		// List<MetaDataProductHierarchyStageEntity> metadataStages = null;
		/*
		 * MetaDataProductHierarchyEntity metadataProductHierarchy = null; try { while
		 * (productHierarchyId != null) { metadataProductHierarchy =
		 * metaDataDao.getProductHierarchyData(productHierarchyId); // Getting all
		 * stages corresponding ProductHierarchy
		 * List<MetaDataProductHierarchyStageEntity> metadataStages =
		 * metaDataDao.getStageData(productHierarchyId); fields.put("Stages",
		 * metadataStages);
		 * 
		 * 
		 * // List<MetaDataProductHierarchyGroupsEntity> metadataGroups =
		 * metadataStages.stream().map(metadataGroup->
		 * metaDataDao.getGroupData(productHierarchyId,metadataStage.getStageId())).
		 * collect(Collectors.toList());
		 * 
		 * List<MetaDataProductHierarchyGroupsEntity> metadataGroups = new
		 * ArrayList<MetaDataProductHierarchyGroupsEntity>();
		 * for(MetaDataProductHierarchyStageEntity metadataStage : metadataStages) {
		 * List<MetaDataProductHierarchyGroupsEntity> metadataGroup =
		 * metaDataDao.getGroupData(productHierarchyId,metadataStage.getStageId());
		 * metadataGroups.addAll(metadataGroup); } fields.put("Groups", metadataGroups);
		 * 
		 * List<MetaDataProductHierarchyFieldsEntity> metaDataFields = new
		 * ArrayList<MetaDataProductHierarchyFieldsEntity>();
		 * for(MetaDataProductHierarchyStageEntity metadataStage : metadataStages) {
		 * for(MetaDataProductHierarchyGroupsEntity metadataGroup : metadataGroups) {
		 * List<MetaDataProductHierarchyFieldsEntity> metaDataField =
		 * metaDataDao.getFieldData(metadataStage.getStageId(),metadataGroup.getGroupId(
		 * )); metaDataFields.addAll(metaDataField); } } fields.put("Fields",
		 * metaDataFields); finalFields.put(productHierarchyId, fields);
		 * productHierarchyId = metadataProductHierarchy.getParent_id(); } } catch
		 * (Exception e) {
		 * logger.error("Error occurred while retrieving Look up(Code list) data." + e);
		 * } if (finalFields != null && metadataProductHierarchy != null) {
		 * response.setCode(ResponseCode.SUCCESS.toString());
		 * response.setStatus(success); response.setStatusMessage(successCodelist);
		 * response.setData(finalFields); } else {
		 * response.setCode(ResponseCode.ERROR.toString()); response.setStatus(failure);
		 * response.setStatusMessage(failureCodeList); } return response;
		 */
	}

	/* Method Use - Used to populate dropdown values - Starts here */
	@Override
	public APIResponse<Object> getAllLookUpData() {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> codeListData = null;
		try {
			List<MetaDataFieldsEntity> metadataFields = metaDataDao.getFieldData();
			List<CodeListEntity> codeLists = metaDataDao.getCodelistData();
			codeListData = new HashMap<>();
			for (MetaDataFieldsEntity metaDataFieldsEntity : metadataFields) {
				if (null != metaDataFieldsEntity.getLookUp()) {
					for (CodeListEntity codeListEntity : codeLists) {
						Method[] m = codeListEntity.getClass().getDeclaredMethods();
						boolean value = false;
						for (Method method : m) {
							if (null != metaDataFieldsEntity.getLookUp()
									&& !metaDataFieldsEntity.getLookUp().isEmpty()) {
								if (method.getName().contains("get")
										&& method.getName().contains(metaDataFieldsEntity.getLookUp())) {
									Method gs1Method = codeListEntity.getClass()
											.getMethod("get" + metaDataFieldsEntity.getLookUp(), new Class[] {});
									Object str1 = gs1Method.invoke(codeListEntity, null);
									codeListData.put(metaDataFieldsEntity.getLookUp(), str1);
									value = true;
									break;
								}
							}
						}
						if (value) {
							break;
						}
					}
				}
			}
			if (codeListData.size() > 0) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(success);
				response.setStatusMessage(successCodelist);
				response.setData(codeListData);
			} else {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(failure);
				response.setStatusMessage(failureCodeList);
			}

		} catch (Exception e) {
			logger.error("Error occurred while retrieving Look up(Code list) data." + e);
		}
		return response;
	}
	/* Method Use - Used to populate dropdown values - Ends here */

	/* Method Use - Used to get metadata field details - Starts here */
	@Override
	public List<MetaDataGroup> getMetaDataFields() {
		List<MetaDataGroupEntity> groups = metaDataDao.getMetadataGroup();
		List<MetaDataGroup> metaDataFieldsList = new ArrayList<>();
		MetaDataGroup m = null;
		Map<String, Object> finalData = new HashMap<>();
		try {
			for (MetaDataGroupEntity metaDataGroupEntity : groups) {
				m = new MetaDataGroup();
				m.setGroupDisplayOrder(metaDataGroupEntity.getGroupDisplayOrder());
				m.setGroupName(metaDataGroupEntity.groupName);
				m.setMetaDataFields(
						metaDataFieldsEntityToView(metaDataDao.getMetadataFields(metaDataGroupEntity.groupId)));
				metaDataFieldsList.add(m);
			}
			finalData.put("MetaDataFields", metaDataFieldsList);
		} catch (Exception e) {
			logger.error("Error occurred while retrieving metadata fields." + e);
		}
		return metaDataFieldsList;
	}

	private List<MetaDataFields> metaDataFieldsEntityToView(List<MetaDataFieldsEntity> entityList) {
		List<MetaDataFields> metaDataFieldsView = new ArrayList<MetaDataFields>();
		for (MetaDataFieldsEntity metaDataFieldsEntity : entityList) {
			metaDataFieldsView.add(MetaDataFieldsEntityToVo(metaDataFieldsEntity));
		}
		return metaDataFieldsView;
	}

	private MetaDataFields MetaDataFieldsEntityToVo(MetaDataFieldsEntity metaDataFieldsEntity) {
		return new ModelMapper().map(metaDataFieldsEntity, MetaDataFields.class);
	}

	private ProductMapEntity ProductMapEntityToVo(ProductMapEntity productEntity) {
		ProductMapEntity product = new ProductMapEntity();

		product.setProduct(productEntity.getProduct());
		product.setCustomFields(productEntity.getCustomFields());
		product.setMetadataId(productEntity.getMetadataId());
		return product;
	}

	private Product MetaDataEntityToVo(ProductEntity productEntity) {
		Product product = new Product();
		MetaData m = new ModelMapper().map(productEntity.getProduct(), MetaData.class);
		product.setProduct(m);
		return product;
	}

	private static Product MetaDataEntityToVos(ProductEntity productEntity) {
		Product product = new Product();
		MetaData m = new ModelMapper().map(productEntity.getProduct(), MetaData.class);
		product.setProduct(m);
		return product;
	}
	/* Method Use - Used to get metadata field details - Ends here */

	/*
	 * Method Use - Used to get metadata field and metadata group configuration -
	 * Starts here
	 */
	@Override
	public APIResponse<Object> getMetadataConfigDetails() {
		List<MetaDataGroupEntity> mgc = null;
		APIResponse<Object> response = null;
		try {
			response = new APIResponse<>();
			mgc = metaDataDao.getGroupData();

			if (mgc.size() > 0) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(success);
				response.setStatusMessage(success);
				response.setData(convertGroupEntityToVO(mgc));
			} else {
				response.setCode(ResponseCode.ERROR.toString());
				response.setStatus(failure);
				response.setStatusMessage(failure);
			}
		} catch (Exception e) {
			logger.error("Error occurred while retrieving metadata config details::" + e);
		}
		return response;
	}

	private List<MetadataGroupConfig> convertGroupEntityToVO(List<MetaDataGroupEntity> entity) {
		List<MetadataGroupConfig> metadataGroupList = new ArrayList<>();
		MetadataGroupConfig metadataGroup = null;
		String refPath = "";
		for (MetaDataGroupEntity mge : entity) {
			metadataGroup = new MetadataGroupConfig();
			metadataGroup.setGroupName(mge.getGroupName());
			metadataGroup.setGroupDisplayName(mge.getGroupDisplayName());
			metadataGroup.setGroupDisplayOrder(mge.getGroupDisplayOrder());
			metadataGroup.setGroupId(mge.getGroupId());
			metadataGroup.setSubGroupId(mge.getSubGroupId());
			MetadataFieldConfig gmd = null;
			MetadataFieldConfig sgmd = null;
			List<MetadataFieldConfig> fields = new ArrayList<>();
			List<MetadataFieldConfig> subFields = new ArrayList<>();
			String updatedRefPath = "";
			List<MetaDataFieldsEntity> fieldEntities = metaDataDao.getFieldDataByGroupId(mge.getGroupId());
			for (MetaDataFieldsEntity metaDataFieldsEntity : fieldEntities) {
				if ((metaDataFieldsEntity.getSubGroupId()).equals("1")) {
					sgmd = new MetadataFieldConfig();
					sgmd.setGroupId(metaDataFieldsEntity.getGroupId());
					sgmd.setSubGroupId(metaDataFieldsEntity.getSubGroupId());
					sgmd.setFieldDisplayName(metaDataFieldsEntity.getFieldDisplayName());
					sgmd.setDisplayOrderNo(metaDataFieldsEntity.getDisplayOrderNo());
					sgmd.setMetaDataFieldName(metaDataFieldsEntity.getMetaDataFieldName());
					sgmd.setJsonPath(metaDataFieldsEntity.getJsonPath());
					sgmd.setJsonType(metaDataFieldsEntity.getJsonType());
					sgmd.setFieldDataType(metaDataFieldsEntity.getFieldDataType());
					sgmd.setReferencePath(metaDataFieldsEntity.getReferencePath());
					sgmd.setReferenceValue(metaDataFieldsEntity.getReferenceValue());
					sgmd.setReferenceValuePath(metaDataFieldsEntity.getReferenceValuePath());
					sgmd.setLookUp(metaDataFieldsEntity.getLookUp());
					sgmd.setIsMandatory(metaDataFieldsEntity.getIsMandatory());
					sgmd.setPermissionGroupField(metaDataFieldsEntity.getPermissionGroupField());
					sgmd.setPermissionGroupValue(metaDataFieldsEntity.getPermissionGroupValue());
					String jsonPath = metaDataFieldsEntity.getJsonPath();
					if (!(metaDataFieldsEntity.getMetaDataFieldName()).equalsIgnoreCase("formatId")) {
						if (metaDataFieldsEntity.getMetaDataFieldName().equalsIgnoreCase("ISBN13")) {
							refPath = refPathIsbn13;
							sgmd.setModelRefPath(refPath);
							updatedRefPath = refPath;
						} else if (metaDataFieldsEntity.getMetaDataFieldName().equalsIgnoreCase("ISBN10")) {
							refPath = refPathIsbn10;
							sgmd.setModelRefPath(refPath);
							updatedRefPath = refPath;
						} else if (metaDataFieldsEntity.getMetaDataFieldName().equalsIgnoreCase("BiblioEditionID")) {
							refPath = refPathBiblio;
							sgmd.setModelRefPath(refPath);
							updatedRefPath = refPath;
						} else {
							refPath = jsonPath.substring(2, jsonPath.length());
							sgmd.setModelRefPath(refPath);
							updatedRefPath = metaDataFieldsEntity.getJsonPath().substring(2,
									metaDataFieldsEntity.getJsonPath().length());
						}
					}
					if ("List".equals(metaDataFieldsEntity.getJsonType())) {
						initMDataArray(sgmd, updatedRefPath, "");
					} else {
						updatedRefPath = updatedRefPath.concat("=\'\'");
						String finalRefPath = "Product." + updatedRefPath;
						sgmd.setInitRefPath(finalRefPath);
					}
					subFields.add(sgmd);
				} else {
					gmd = new MetadataFieldConfig();
					gmd.setFieldDisplayName(metaDataFieldsEntity.getFieldDisplayName());
					gmd.setDisplayOrderNo(metaDataFieldsEntity.getDisplayOrderNo());
					gmd.setMetaDataFieldName(metaDataFieldsEntity.getMetaDataFieldName());
					gmd.setJsonPath(metaDataFieldsEntity.getJsonPath());
					gmd.setJsonType(metaDataFieldsEntity.getJsonType());
					gmd.setFieldDataType(metaDataFieldsEntity.getFieldDataType());
					gmd.setReferencePath(metaDataFieldsEntity.getReferencePath());
					gmd.setReferenceValue(metaDataFieldsEntity.getReferenceValue());
					gmd.setReferenceValuePath(metaDataFieldsEntity.getReferenceValuePath());
					gmd.setMaxlength(metaDataFieldsEntity.getMaxlength());
					gmd.setMinlength(metaDataFieldsEntity.getMinlength());
					gmd.setLookUp(metaDataFieldsEntity.getLookUp());
					gmd.setIsMandatory(metaDataFieldsEntity.getIsMandatory());
					gmd.setPermissionGroupField(metaDataFieldsEntity.getPermissionGroupField());
					gmd.setPermissionGroupValue(metaDataFieldsEntity.getPermissionGroupValue());
					String jsonPath = metaDataFieldsEntity.getJsonPath();
					if (!(metaDataFieldsEntity.getMetaDataFieldName()).equalsIgnoreCase("formatId")) {
						if (metaDataFieldsEntity.getMetaDataFieldName().equalsIgnoreCase("ISBN13")) {
							refPath = refPathIsbn13;
							gmd.setModelRefPath(refPath);
							updatedRefPath = refPath;
						} else if (metaDataFieldsEntity.getMetaDataFieldName().equalsIgnoreCase("ISBN10")) {
							refPath = refPathIsbn10;
							gmd.setModelRefPath(refPath);
							updatedRefPath = refPath;
						} else if (metaDataFieldsEntity.getMetaDataFieldName().equalsIgnoreCase("BiblioEditionID")) {
							refPath = refPathBiblio;
							gmd.setModelRefPath(refPath);
							updatedRefPath = refPath;
						} else {
							refPath = jsonPath.substring(2, jsonPath.length());
							gmd.setModelRefPath(refPath);
							updatedRefPath = metaDataFieldsEntity.getJsonPath().substring(2,
									metaDataFieldsEntity.getJsonPath().length());
						}
					}
					if ("List".equals(metaDataFieldsEntity.getJsonType())) {
						initMDataArray(gmd, updatedRefPath, "");
					} else {
						updatedRefPath = updatedRefPath.concat("=\'\'");
						String finalRefPath = "Product." + updatedRefPath;
						gmd.setInitRefPath(finalRefPath);
					}
					fields.add(gmd);
				}
			}
			metadataGroup.setMetadataFieldConfig(fields);
			metadataGroup.setMetadataSubFieldConfig(subFields);
			metadataGroupList.add(metadataGroup);
		}
		return metadataGroupList;
	}

	public static void initMDataArray(MetadataFieldConfig m, String initial, String combined) {
		String finalString = "";
		if (initial.contains(".")) {
			String test[] = initial.split("\\.", 2);
			if (combined.equals("")) {
				combined = combined.concat(test[0]);
			} else
				combined = combined.concat(":").concat(combined.substring(combined.lastIndexOf(":") + 1)).concat(".")
						.concat(test[0]);
			initMDataArray(m, test[1], combined);
		} else {
			combined = combined.concat(":")
					.concat(combined.substring(combined.lastIndexOf(":") + 1).concat(".").concat(initial));
			String finalArray[] = combined.split(":");
			for (int i = 1; i < finalArray.length - 1; i++) {
				if (i == finalArray.length) {
					if ("".equals(finalString)) {
						finalString = finalArray[i] + "=[]";
					} else {
						finalString += ";" + finalArray[i] + "=[]";
					}
				} else {
					int f = finalArray[i].lastIndexOf("[");
					int f2 = finalArray[i].lastIndexOf("]");
					StringBuilder s2 = new StringBuilder(finalArray[i]);
					if (f2 != finalArray.length - 1) {
						s2 = s2.replace(f, f2 + 1, "");
					}
					finalString = "Product." + s2 + "=[]";
				}
			}
			m.setInitRefPath(finalString);
		}
	}
	/*
	 * Method Use - Used to get metadata field and metadata group configuration -
	 * Ends here
	 */

	/*
	 * Method Use - Used to insert new metadata and check the isbn is exists -
	 * Starts here
	 */
	@Override
	public APIResponse<Object> existIsbnCheck(String isbnValue) {
		APIResponse<Object> response = new APIResponse<Object>();
		boolean existCheck = false;
		try {
			existCheck = metaDataDao.existIsbnCheck(isbnValue);
			if (existCheck) {
				response.setCode(ResponseCode.FAILURE.toString());
				response.setStatus(failure);
				response.setStatusMessage(isbnExist);
			} else {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(success);
				response.setStatusMessage(isbnNotExist);
			}
		} catch (Exception e) {
			logger.error("Error occurred while checking isbn exists::" + e);
		}
		return response;
	}

	@Override
	public APIResponse<Object> saveMetaData(Map<String, Object> obj) {
		APIResponse<Object> response = new APIResponse<Object>();
		boolean result = false;
		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();
		String newIsbn = "";
		Map<Object, Object> finalMap = new HashMap<>();

		boolean activeCheck = false;

		try {

			Map<String, Object> finalObj = (Map<String, Object>) (obj);

			String json1 = JSON.parse(JonixJson.toJson(finalObj)).toString();

			DocumentContext docCtx = JsonPath.parse(json1);
			newIsbn = docCtx.read("$.Product.ProductIdentifier[?(@.ProductIDType=='15')].IDValue").toString();
			newIsbn = newIsbn.replace("[", "").replace("]", "").replace("\"", "");

			Product finalProd = new Gson().fromJson(json1, Product.class);

			finalMap = getAutoPopulateFields(json1);

			activeCheck = checkInactiveTitles(json1);

			finalProd.getProduct().setActive(activeCheck);
			finalProd.setCustomFields(finalMap);
			finalProd.setId(newIsbn);
			finalProd.getProduct().setLastModifiedOn(currentDate);
			finalProd.getProduct().setDeleted(false);
			result = metaDataDao.saveMetaData(finalProd);

			if (result) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(success);
				response.setStatusMessage(successCreation);
			} else {
				response.setCode(ResponseCode.FAILURE.toString());
				response.setStatus(failure);
				response.setStatusMessage(failureCreation);
			}
		} catch (Exception e) {
			logger.error("Error occurred while retrieving saved metadata." + e);
		}
		return response;
	}
	/*
	 * Method Use - Used to insert new metadata and check the isbn is exists - Ends
	 * here
	 */

	/*
	 * Method Use - Used to check inactive titles and autopopulate fields - Starts
	 * here
	 */
	private boolean checkInactiveTitles(String jsonValue) {
		String ruleValue = null;
		InactiveTitlesEntity inactiveEntity = null;
		boolean activeCheck = true;

		try {
			Query query3 = new Query();
			query3.addCriteria(Criteria.where("_id").is("InactiveTitleRule"));

			inactiveEntity = mongoTemplate.findOne(query3, InactiveTitlesEntity.class, manageConfig);
			for (RulesEntity rules : inactiveEntity.getRules()) {
				DocumentContext docCtx1 = JsonPath.parse(jsonValue);
				List<Boolean> check = new ArrayList<>();
				boolean brCheck = false;
				for (RuleEntity rule : rules.getRule()) {

					try {
						ruleValue = docCtx1.read(rule.getFieldPath()).toString();
					} catch (PathNotFoundException pne) {
						logger.error("Error occurred while Path Reading." + pne);
					}
					if (null != ruleValue) {
						brCheck = true;
						ruleValue = ruleValue.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
						switch (rule.getOperator()) {
						case "==":
							if (ruleValue.equals(rule.getFieldValue())) {
								check.add(false);
							} else {
								check.add(true);
							}
							break;
						case "!=":
							if (!ruleValue.equals(rule.getFieldValue())) {
								check.add(false);
							} else {
								check.add(true);
							}
							break;
						}
					} else {
						activeCheck = true;
					}
				}
				if (brCheck) {
					if (null != check && check.size() > 0) {
						if (check.contains(true)) {
							activeCheck = true;
						} else {
							activeCheck = false;
						}

						break;
					}
				}
			}
		} catch (Exception ex) {
			logger.error("Error occurred while Inctive title checking." + ex.getMessage());
		}
		return activeCheck;
	}

	private Map<Object, Object> getAutoPopulateFields(String jsonValue) {
		List<MetaDataAutoPopulate> autoPopulate = new ArrayList<>();
		List<MetaDataAutoPopulateEntity> autoPopulateEntity = null;
		Map<Object, Object> finalMap = new HashMap<>();
		String sourceFieldValue = "";
		Map<String, String> fieldPath = new HashMap<>();
		try {
			autoPopulateEntity = metaDataDao.getAutoPopulateFieldDetails();
			autoPopulate = autoPopulateEntityToVo(autoPopulateEntity);
			for (MetaDataAutoPopulate populate : autoPopulate) {
				Query query3 = new Query();
				query3.addCriteria(Criteria.where("metaDataFieldName").is(populate.getSourceField()));
				query3.fields().include("jsonPath");
				query3.fields().exclude("_id");

				fieldPath = mongoTemplate.findOne(query3, Map.class, metaDataFieldView);
				if (null != fieldPath) {
					DocumentContext docCtx1 = JsonPath.parse(jsonValue);
					sourceFieldValue = docCtx1.read(fieldPath.get("jsonPath")).toString();
				}
				sourceFieldValue = sourceFieldValue.replace("[", "").replace("]", "").replace("\"", "").replace("\\",
						"");
				if (null != sourceFieldValue) {
					for (Map<String, String> map : populate.getPopulateLookup()) {
						boolean breakCheck = false;
						for (Entry<String, String> d1 : map.entrySet()) {
							switch (populate.getPopulateCondition()) {
							case "equals":
								if (sourceFieldValue.equals(d1.getValue())) {
									finalMap.put(populate.getPopulateField(), map.get("Value"));
									breakCheck = true;
									break;
								}
								break;
							case "startsWith":
								if (!sourceFieldValue.startsWith(d1.getValue())) {
									finalMap.put(populate.getPopulateField(), map.get("Value"));
									breakCheck = true;
									break;
								}
								break;
							}
						}
						if (breakCheck) {
							break;
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.error("Error occurred while autopopulate:." + ex.getMessage());
		}
		return finalMap;
	}

	private List<MetaDataAutoPopulate> autoPopulateEntityToVo(List<MetaDataAutoPopulateEntity> entityList) {

		List<MetaDataAutoPopulate> populateFieldsView = new ArrayList<MetaDataAutoPopulate>();
		for (MetaDataAutoPopulateEntity populateFieldsEntity : entityList) {
			populateFieldsView.add(MetaDataPopulateEntityToVo(populateFieldsEntity));
		}
		return populateFieldsView;
	}

	private MetaDataAutoPopulate MetaDataPopulateEntityToVo(MetaDataAutoPopulateEntity metaDataFieldsEntity) {
		return new ModelMapper().map(metaDataFieldsEntity, MetaDataAutoPopulate.class);
	}

	/*
	 * Method Use - Used to check inactive titles and autopopulate fields - Ends
	 * here
	 */

	/*
	 * Method Use - Used to edit existing metadata and update the same - Starts here
	 */
	@Override
	public APIResponse<Object> editMetaData(MetaDataView editMdata) {
		Map<String, Object> fieldValue = null;
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> products = null;
		List<Map<String, Object>> groupProduct = null;
		List<MetaDataGroupEntity> mgc = null;
		try {
			products = new HashMap<>();
			Map<String, Object> viewResult = metaDataDao.editMetaData(editMdata.getIdValue());
			ProductEntity resultEntities = (ProductEntity) viewResult.get("Product");
			Product searchResultList = MetaDataEntityToVo(resultEntities);
			String jsonString = new Gson().toJson(searchResultList);
			/*
			 * JSONObject json = new JSONObject(jsonString); String xml =
			 * XML.toString(json); System.out.println("xml:::::"+xml);
			 */
			DocumentContext docCtx = JsonPath.parse(jsonString);
			mgc = metaDataDao.getGroupData();
			for (MetaDataGroupEntity mge : mgc) {
				List<MetaDataFieldsEntity> fieldEntity = metaDataDao.getFieldDataByGroupId(mge.getGroupId());
				fieldValue = new HashMap<>();
				if (mge.getGroupId().equals("1") || mge.getGroupId().equals("6")) {
					for (MetaDataFieldsEntity metaDataFieldsEntity : fieldEntity) {
						groupProduct = new ArrayList<>();
						JsonPath jsonPath = JsonPath.compile(metaDataFieldsEntity.getJsonPath());
						Object obj = null;
						if (null != jsonPath.getPath()) {
							try {
								obj = docCtx.read(jsonPath);
								fieldValue.put(metaDataFieldsEntity.getMetaDataFieldName(), obj.toString()
										.replace("[", "").replace("]", "").replace("\"", "").replace("\\", ""));
							} catch (PathNotFoundException e) {
								e.printStackTrace();
							}
							groupProduct.add(fieldValue);
						}
					}
					products.put(mge.getGroupDisplayName(), groupProduct);
				} else {
					JsonPath jsonPath = JsonPath.compile("$.Product." + mge.getGroupName());
					Object obj = null;
					if (null != jsonPath.getPath()) {
						try {
							obj = docCtx.read(jsonPath);
						} catch (PathNotFoundException e) {
							e.printStackTrace();
						}
					}
					products.put(mge.getGroupDisplayName(), obj);
				}
			}
			if (products.size() > 0) {
				response.setCode(ResponseCode.RECORDS_FOUND.toString());
				response.setStatus(success);
				response.setStatusMessage(successRetrieval);
				response.setData(products);
			} else {
				response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
				response.setStatus(failure);
				response.setStatusMessage(failureRetrieval);
			}
		} catch (Exception e) {
			logger.error("Error occurred while retrieving metadata::" + e.getMessage());
		}
		return response;
	}

	@Override
	public APIResponse<Object> updateMetaData(Map<String, Object> obj) {
		APIResponse<Object> response = new APIResponse<Object>();
		boolean result = false;
		String idValue = null;

		try {
			Map<String, Object> updatedProduct = (Map<String, Object>) obj;
			String jsonString = new Gson().toJson(updatedProduct);
			jsonString = JonixJson.toJson(updatedProduct).toString();
			logger.info("Object from UI " + jsonString);
			DocumentContext docCtx = JsonPath.parse(jsonString);
			idValue = docCtx.read("$.Product.ProductIdentifier[?(@.ProductIDType=='15')].IDValue").toString();
			idValue = idValue.replace("[", "").replace("]", "").replace("\"", "");

			result = updateMetaData(docCtx, obj, idValue);

			if (result) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(success);
				response.setStatusMessage(successUpdate);
			} else {
				response.setCode(ResponseCode.FAILURE.toString());
				response.setStatus("failure");
				response.setStatusMessage(failureUpdate);
			}
		} catch (Exception e) {
			logger.error("Error occurred while update metadata." + e);
		}
		return response;
	}

	public boolean updateMetaData(DocumentContext docCtx, Object obj, String idValue) {
		boolean result = false;

		ProductEntity metaDataentity = new ProductEntity();

		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();

		DBObject dbObj = null;
		DBObject dbObj1 = null;
		Gson gson = new Gson();

		JsonNode node1 = null;
		JsonNode node2 = null;
		JsonNode node3 = null;

		ObjectMapper mapper = new ObjectMapper();

		List<Assets> assetList = new ArrayList<>();
		Assets asset = null;

		Map<Object, Object> finalMap = new HashMap<>();

		String imprintName = null;
		String countryOfPublication = null;
		String productCategory = null;
		String title = null;

		boolean activeCheck = true;

		try {
			Query q = new Query();
			q.addCriteria(Criteria.where("Product.ProductIdentifier.ProductIDType").is("15")
					.and("Product.ProductIdentifier.IDValue").is(idValue));

			metaDataentity = mongoTemplate.findOne(q, ProductEntity.class, "products");
			Product metadataUpdate = MetaDataEntityToVo(metaDataentity);
			String json = new Gson().toJson(obj);
			// logger.info("value from db "+JonixJson.toJson(metadataUpdate));
			try {
				countryOfPublication = docCtx.read("$.Product.CountryOfPublication").toString();
				countryOfPublication = countryOfPublication.replace("[", "").replace("]", "").replace("\"", "")
						.replace("\\", "");
				imprintName = docCtx.read("$.Product.Imprint[0].ImprintName").toString();
				imprintName = imprintName.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
				title = docCtx.read("$.Product.Title[0].TitleText").toString();
				title = title.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
				Query qry = new Query();
				qry.addCriteria(Criteria.where("metaDataFieldName").is("ProductCategoryCode"));

				MetadataFieldConfig config = mongoTemplate.findOne(qry, MetadataFieldConfig.class,
						"metaDataFieldConfig");
				if (null != config.getJsonPath()) {
					productCategory = docCtx.read(config.getJsonPath()).toString();
					productCategory = productCategory.replace("[", "").replace("]", "").replace("\"", "").replace("\\",
							"");
				}
			} catch (Exception e) {
				logger.error("Exception while reading the metadata values from Json object" + e.getMessage());
			}

			Product inputProd = new Gson().fromJson(json, Product.class);

			dbObj = (DBObject) JSON.parse(gson.toJson(inputProd));
			dbObj1 = (DBObject) JSON.parse(gson.toJson(metadataUpdate));
			node1 = mapper.convertValue(dbObj, JsonNode.class);
			node2 = mapper.convertValue(dbObj1, JsonNode.class);

			node3 = merge(node1, node2, inputProd.getProduct().getLastModifiedBy(), idValue, imprintName,
					countryOfPublication, productCategory, title);

			String json1 = JSON.parse(JonixJson.toJson(node3)).toString();

			Product finalProd = new Gson().fromJson(json1, Product.class);

			if (null != metaDataentity && null != metaDataentity.getProduct()) {
				if (null != metaDataentity.getAsset() && metaDataentity.getAsset().size() > 0) {
					for (AssetEntity assets : metaDataentity.getAsset()) {
						asset = new ModelMapper().map(assets, Assets.class);
						assetList.add(asset);
					}
					finalProd.setAsset(assetList);
				}

				if (null != metaDataentity.getCustomFields()) {
					finalProd.setCustomFields(metaDataentity.getCustomFields());
				}
			}

			finalMap = getAutoPopulateFields(json1);

			activeCheck = checkInactiveTitles(json1);

			finalProd.getProduct().setActive(activeCheck);

			finalProd.setCustomFields(finalMap);

			finalProd.setId(metaDataentity.getId());
			finalProd.getProduct().setLastModifiedOn(currentDate);
			finalProd.getProduct().setDeleted(false);

			mongoTemplate.save(finalProd, "products");
			result = true;

		} catch (Exception e) {
			logger.error("Error occurred while update metadata db." + e);
		}

		return result;
	}

	@Autowired
	public static JsonNode merge(JsonNode newJson, JsonNode oldJson, String modifiedBy, String idValue,
			String imprintName, String countryOfPublication, String productCategory, String title) {
		Iterator<Map.Entry<String, JsonNode>> newJsonFieldsIterator = newJson.fields();
		Iterator<Map.Entry<String, JsonNode>> oldJsonFieldsIterator = oldJson.fields();

		CDCMetaData cdc = new CDCMetaData();
		boolean chk = false;
		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();
		List<CDCMetaDataDetails> cdcDetailsList = new ArrayList<>();
		while (newJsonFieldsIterator.hasNext()) {
			chk = true;
			CDCMetaDataDetails cdcDetails = new CDCMetaDataDetails();
			Map.Entry<String, JsonNode> newJsonEntry = newJsonFieldsIterator.next();
			JsonNode subNode = newJsonEntry.getValue();

			if (subNode.getNodeType().equals(JsonNodeType.OBJECT)) {
				boolean isNewBlock = true;
				oldJsonFieldsIterator = oldJson.fields();
				while (oldJsonFieldsIterator.hasNext()) {
					Map.Entry<String, JsonNode> oldJsonEntry = oldJsonFieldsIterator.next();
					if (oldJsonEntry.getKey().equals(newJsonEntry.getKey())) {
						merge(newJsonEntry.getValue(), oldJsonEntry.getValue(), modifiedBy, idValue, imprintName,
								countryOfPublication, productCategory, title);
						isNewBlock = false;
					}
				}
				if (isNewBlock) {
					((ObjectNode) oldJson).replace(newJsonEntry.getKey(), newJsonEntry.getValue());
				}
			} else if (subNode.getNodeType().equals(JsonNodeType.ARRAY)) {
				boolean newEntry = true;
				oldJsonFieldsIterator = oldJson.fields();
				while (oldJsonFieldsIterator.hasNext()) {
					Map.Entry<String, JsonNode> oldJsonEntry = oldJsonFieldsIterator.next();
					if (oldJsonEntry.getKey().equals(newJsonEntry.getKey())) {
						updateArray(newJsonEntry.getValue(), oldJsonEntry, cdcDetails);
						newEntry = false;
					}
				}
				if (newEntry) {
					((ObjectNode) oldJson).replace(newJsonEntry.getKey(), newJsonEntry.getValue());
				}
			}
			ValueNode valueNode = null;
			JsonNode incomingValueNode = newJsonEntry.getValue();
			switch (subNode.getNodeType()) {
			case STRING:
				valueNode = new TextNode(incomingValueNode.textValue());
				break;
			case NUMBER:
				valueNode = new IntNode(incomingValueNode.intValue());
				break;
			/*
			 * case BOOLEAN: valueNode =
			 * BooleanNode.valueOf(incomingValueNode.booleanValue());
			 */
			}
			if (valueNode != null) {
				updateObject(oldJson, valueNode, newJsonEntry, cdcDetails);
			}
			System.out.println(cdcDetails);
			if (null != cdcDetails.getFieldName()) {
				cdcDetailsList.add(cdcDetails);
			}
		}
		cdc.setReferenceId(idValue);
		cdc.setUpdatedSource("UI");
		cdc.setImprintName(imprintName);
		cdc.setCountryOfPublication(countryOfPublication);
		cdc.setProductCategory(productCategory);
		cdc.setCdcMetaDataDetails(cdcDetailsList);
		cdc.setTitle(title);
		cdc.setLastModifiedBy(modifiedBy);
		cdc.setLastModifiedOn(currentDate);
		if (chk && cdcDetailsList.size() > 0) {
			MetaDataServiceImpl.getCdcmongoTemplate().save(cdc, "tCDCMetaData");
		}

		return oldJson;
	}

	private static void updateArray(JsonNode newJsonEntryValue, Map.Entry<String, JsonNode> oldJson,
			CDCMetaDataDetails cdcDetails) {
		updateCDCArray(newJsonEntryValue, oldJson, cdcDetails);
		oldJson.setValue(newJsonEntryValue);
	}

	private static void updateCDCArray(JsonNode newJsonEntryValue, Map.Entry<String, JsonNode> oldJson,
			CDCMetaDataDetails cdcDetails) {
		Iterator<Map.Entry<String, JsonNode>> newJsonIterator = newJsonEntryValue.fields();
		String fieldPath = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			Query query2 = new Query();
			List<Map<String, String>> newJsonResult = mapper.convertValue(newJsonEntryValue, List.class);
			List<Map<String, String>> oldJsonResult = mapper.convertValue(oldJson.getValue(), List.class);
			for (Map<String, String> newNode : newJsonResult) {
				for (Entry<String, String> newJsonEntry : newNode.entrySet()) {
					System.out.println("Key: " + newJsonEntry.getKey() + "Value: " + newJsonEntry.getValue());
					for (Map<String, String> oldNode : oldJsonResult) {
						for (Entry<String, String> oldJsonEntry : oldNode.entrySet()) {
							if (newJsonEntry.getKey().equals(oldJsonEntry.getKey())) {
								if (!newJsonEntry.getValue().equals(oldJsonEntry.getValue())) {
									query2.addCriteria(Criteria.where("metaDataFieldName").is(newJsonEntry.getKey()));
									// query2.fields().include("jsonPath");
									query2.fields().exclude("_id");
									MetadataFieldConfig config = MetaDataServiceImpl.getCdcmongoTemplate().findOne(
											query2, MetadataFieldConfig.class,
											MetaDataServiceImpl.getMetaDataFieldDetails());
									if (null != config) {
										cdcDetails.setFieldName(config.getFieldDisplayName());
										cdcDetails.setFieldPath(config.getJsonPath());
										cdcDetails.setOldValue(oldJsonEntry.getValue().toString());
										cdcDetails.setNewValue(newJsonEntry.getValue());
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.error("Error occurred while CDC saved metadata." + ex);
		}
	}

	private static void updateObject(JsonNode mergeInTo, ValueNode valueToBePlaced,
			Map.Entry<String, JsonNode> toBeMerged, CDCMetaDataDetails cdcDetails) {
		boolean newEntry = true;

		String fieldPath = "";
		try {
			Query query2 = new Query();
			Iterator<Map.Entry<String, JsonNode>> mergedIterator = mergeInTo.fields();
			while (mergedIterator.hasNext()) {
				Map.Entry<String, JsonNode> entry = mergedIterator.next();
				if (entry.getKey().equals(toBeMerged.getKey())) {
					newEntry = false;
					if (!entry.getValue().equals(valueToBePlaced)) {
						query2.addCriteria(Criteria.where("metaDataFieldName").is(entry.getKey()));
						// query2.fields().include("jsonPath");
						query2.fields().exclude("_id");
						// query2.fields().exclude("createdOn");
						// query2.fields().exclude("modifiedOn");
						MetadataFieldConfig mconfig = MetaDataServiceImpl.getCdcmongoTemplate().findOne(query2,
								MetadataFieldConfig.class, MetaDataServiceImpl.getMetaDataFieldDetails());
						if (null != mconfig) {
							cdcDetails.setFieldName(mconfig.getFieldDisplayName());
							cdcDetails.setFieldPath(mconfig.getJsonPath());
							cdcDetails.setOldValue(entry.getValue().toString());
							cdcDetails.setNewValue(valueToBePlaced.toString());
						}
						entry.setValue(valueToBePlaced);
					} else {
						entry.setValue(valueToBePlaced);
					}
				}
			}
			if (newEntry) {
				((ObjectNode) mergeInTo).replace(toBeMerged.getKey(), toBeMerged.getValue());
				query2.addCriteria(Criteria.where("metaDataFieldName").is(toBeMerged.getKey()));
				// query2.fields().include("jsonPath");
				// query2.fields().exclude("_id");
				MetadataFieldConfig mconfig = MetaDataServiceImpl.getCdcmongoTemplate().findOne(query2,
						MetadataFieldConfig.class, MetaDataServiceImpl.getMetaDataFieldDetails());
				if (null != mconfig) {
					cdcDetails.setFieldName(mconfig.getFieldDisplayName());
					cdcDetails.setFieldPath(mconfig.getJsonPath());
					cdcDetails.setOldValue(toBeMerged.getValue().toString());
					cdcDetails.setNewValue(valueToBePlaced.toString());
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	/*
	 * Method Use - Used to edit existing metadata and update the same - Ends here
	 */

	/*
	 * Method Use - Used to customize metadata for excel download and upload -
	 * Starts here
	 */
	@Override
	public APIResponse<Object> customTemplateMData(Map<String, Object> custMdata) {
		boolean result = false;
		APIResponse<Object> response = new APIResponse<>();
		SimpleDateFormat sm = new SimpleDateFormat(templateDateFormat);
		String strDate = sm.format(new Date());
		String fileName = strCustomDownloadPath + templateName + strDate + templateFileFormat;

		try {
			Set<String> sheetNames = new HashSet<>();
			File file = new File(fileName);
			OutputStream fos = null;
			CellStyle style = null;
			DataFormat format = null;
			XSSFWorkbook workBook = new XSSFWorkbook();
			format = workBook.createDataFormat();
			style = workBook.createCellStyle();
			style.setDataFormat(format.getFormat("@"));

			for (Entry<String, Object> data : custMdata.entrySet()) {
				sheetNames.add(data.getValue().toString());
			}
			Map<String, List<String>> finalData = new HashMap<>();
			for (String sheetName : sheetNames) {
				List<String> headers = new ArrayList<>();
				if (!sheetName.equalsIgnoreCase("Basic")) {
					headers.add("ISBN13");
				}
				for (Entry<String, Object> data : custMdata.entrySet()) {
					if (sheetName.equals(data.getValue().toString())) {
						String withoutspace = data.getKey().replaceAll("\\s", "");
						headers.add(withoutspace);
					}
				}
				finalData.put(sheetName, headers);
			}
			for (String sheetName : sheetNames) {
				XSSFSheet sheet = workBook.createSheet(sheetName);
				int currentRowNumber = 0;
				List<String> headerData = finalData.get(sheetName);
				Row rowHeader = sheet.createRow(currentRowNumber);
				for (int i = 0; i < headerData.size(); i++) {
					Cell headerCell = rowHeader.createCell(i);
					headerCell.setCellValue(headerData.get(i));
					sheet.setColumnWidth(i, 3000);
					sheet.setDefaultColumnStyle(i, style);
				}
			}
			fos = new FileOutputStream(file);

			workBook.write(fos);
			fos.flush();
			result = true;
			if (result) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(success);
				response.setStatusMessage(successDownload);
				response.setData(templateName + strDate + templateFileFormat);
			} else {
				response.setCode(ResponseCode.FAILURE.toString());
				response.setStatus(failure);
				response.setStatusMessage(failureDownload);
				response.setData(templateName + strDate + templateFileFormat);
			}
		} catch (Exception e) {
			logger.error("Error occurred while retrieving custom template xlsx::" + e);
		}
		return response;
	}

	@Override
	public APIResponse<Object> uploadMetaData(FileObj obj) {
		boolean result = false;
		boolean basicExist = false;
		APIResponse<Object> response = new APIResponse<>();
		String targetName = strCustomUploadPath + obj.getPath();

		String idValue = "";
		boolean existCheck = false;
		try {
			List<JSONObject> jsonObjects = null;
			Workbook workbook = getWorkbook(targetName);

			int k = workbook.getNumberOfSheets();
			int n = ++k;

			String[] sheetList = new String[n];

			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetList[i] = workbook.getSheetName(i);
			}

			basicExist = Arrays.asList(sheetList).contains("Basic");
			if (basicExist) {
				jsonObjects = processExcelValuesWithBasic(workbook, sheetList);
			} else {
				jsonObjects = processExcelValuesExceptBasic(workbook, sheetList);
			}
			for (JSONObject json : jsonObjects) {
				Map<String, Object> jsonObj = new HashMap<String, Object>();

				jsonObj = toMap(json);

				String jsonString = new Gson().toJson(jsonObj);

				DocumentContext docCtx = JsonPath.parse(jsonString);
				idValue = docCtx.read("$.Product.ProductIdentifier[?(@.ProductIDType=='15')].IDValue").toString();
				idValue = idValue.replace("[", "").replace("]", "").replace("\"", "");
				existCheck = metaDataDao.existIsbnCheck(idValue);
				if (existCheck) {
					result = uploadExcelMetaData(jsonObj, idValue);
				} else {
					result = saveExcelMetaData(jsonObj, idValue);
				}
			}

			if (result) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(success);
				response.setStatusMessage(successUpload);
			} else {
				response.setCode(ResponseCode.FAILURE.toString());
				response.setStatus(failure);
				response.setStatusMessage(failureUpload);
			}

		} catch (Exception e) {
			logger.error("Error occurred while processing upload metadata::" + e);
		}
		return response;
	}

	public boolean saveExcelMetaData(Map<String, Object> obj, String IsbnValue) {
		APIResponse<Object> response = new APIResponse<Object>();
		boolean result = false;
		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();
		boolean activeCheck = true;

		Map<Object, Object> finalMap = new HashMap<>();

		try {
			Map<String, Object> finalObj = (Map<String, Object>) (obj);

			String json1 = JSON.parse(JonixJson.toJson(finalObj)).toString();

			Product finalProd = new Gson().fromJson(json1, Product.class);

			finalMap = getAutoPopulateFields(json1);

			activeCheck = checkInactiveTitles(json1);

			finalProd.getProduct().setActive(activeCheck);

			finalProd.setCustomFields(finalMap);
			finalProd.setId(IsbnValue);
			finalProd.getProduct().setLastModifiedOn(currentDate);
			finalProd.getProduct().setDeleted(false);
			result = metaDataDao.saveUploadMetaData(finalProd);

			if (result) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus(success);
				response.setStatusMessage(successCreation);
			} else {
				response.setCode(ResponseCode.FAILURE.toString());
				response.setStatus(failure);
				response.setStatusMessage(failureCreation);
			}
		} catch (Exception e) {
			logger.error("Error occurred while retrieving saved metadata." + e);
		}
		return result;
	}

	public boolean uploadExcelMetaData(Object obj, String idValue) {
		boolean result = false;

		DBObject dbObj = null;
		DBObject dbObj1 = null;
		Gson gson = new Gson();

		JsonNode node1 = null;
		JsonNode node2 = null;
		JsonNode node3 = null;

		ObjectMapper mapper = new ObjectMapper();

		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();

		List<Assets> assetList = new ArrayList<>();
		Assets asset = null;

		boolean activeCheck = false;
		Map<Object, Object> finalMap = new HashMap<>();

		try {
			Map<String, Object> viewResult = metaDataDao.editMetaData(idValue);

			ProductEntity metaDataentity = (ProductEntity) viewResult.get("Product");

			Product metadataUpdate = MetaDataEntityToVo(metaDataentity);

			String json = new Gson().toJson(obj);
			DocumentContext docCtx = JsonPath.parse(json);
			String countryOfPublication = null;
			String imprintName = null;
			String productCategory = null;
			String title = null;

			try {
				countryOfPublication = docCtx.read("$.Product.CountryOfPublication").toString();
				countryOfPublication = countryOfPublication.replace("[", "").replace("]", "").replace("\"", "")
						.replace("\\", "");
				imprintName = docCtx.read("$.Product.Imprint[0].ImprintName").toString();
				imprintName = imprintName.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
				title = docCtx.read("$.Product.Title[0].TitleText").toString();
				title = title.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
				Query qry = new Query();
				qry.addCriteria(Criteria.where("metaDataFieldName").is("ProductCategoryCode"));

				MetadataFieldConfig config = mongoTemplate.findOne(qry, MetadataFieldConfig.class,
						"metaDataFieldConfig");
				if (null != config.getJsonPath()) {
					productCategory = docCtx.read(config.getJsonPath()).toString();
				}
			} catch (Exception e) {
				logger.error("Exception while reading the metadata values from Json object");
			}

			Product inputProd = new Gson().fromJson(json, Product.class);

			dbObj = (DBObject) JSON.parse(gson.toJson(inputProd));
			dbObj1 = (DBObject) JSON.parse(gson.toJson(metadataUpdate));
			node1 = mapper.convertValue(dbObj, JsonNode.class);
			node2 = mapper.convertValue(dbObj1, JsonNode.class);

			node3 = merge(node1, node2, inputProd.getProduct().getLastModifiedBy(), idValue, imprintName,
					countryOfPublication, productCategory, title);

			String json1 = JSON.parse(JonixJson.toJson(node3)).toString();

			Product finalProd = new Gson().fromJson(json1, Product.class);

			if (null != metaDataentity && null != metaDataentity.getProduct()) {
				if (null != metaDataentity.getAsset() && metaDataentity.getAsset().size() > 0) {
					for (AssetEntity assets : metaDataentity.getAsset()) {
						asset = new ModelMapper().map(assets, Assets.class);
						assetList.add(asset);
					}
					finalProd.setAsset(assetList);
				}

				if (null != metaDataentity.getCustomFields()) {
					finalProd.setCustomFields(metaDataentity.getCustomFields());
				}
			}

			finalMap = getAutoPopulateFields(json1);

			activeCheck = checkInactiveTitles(json1);

			finalProd.getProduct().setActive(activeCheck);

			finalProd.setCustomFields(finalMap);

			finalProd.setId(metaDataentity.getId());
			finalProd.getProduct().setLastModifiedOn(currentDate);
			finalProd.getProduct().setDeleted(false);

			result = metaDataDao.saveUploadMetaData(finalProd);

		} catch (Exception e) {
			logger.error("Error occurred while uplaod metadata template xlsx::" + e);
		}
		return result;
	}

	public static Workbook getWorkbook(String targetName) throws IOException, InvalidFormatException {
		File excelFile = new File(targetName);
		FileInputStream inp = new FileInputStream(excelFile);
		Workbook workbook = WorkbookFactory.create(inp);
		return workbook;
	}

	public static List<JSONObject> processExcelValuesWithBasic(Workbook workbook, String[] sheetList) {
		JSONObject json1 = null;
		MetaDataFieldsEntity fieldEntity = null;
		List<JSONObject> finalJsonObect = new ArrayList<>();
		JSONArray finalJson = new JSONArray();
		String referencePath = "";
		String referenceValuePath = "";
		String isbn = "";
		JSONObject json = null;

		try {
			for (String sheetName : sheetList) {
				if (null != sheetName) {
					if (sheetName.equalsIgnoreCase("Basic")) {
						JSONArray rows = ExcelParser.parseSheet(workbook, sheetName);
						for (int i = 0; i < rows.length(); i++) {
							json = new JSONObject();
							json1 = new JSONObject();
							for (String sheetName1 : sheetList) {
								if (null != sheetName1) {
									if (sheetName1.equalsIgnoreCase("Basic")) {
										JSONObject objects = rows.getJSONObject(i);
										String[] elementNames = JSONObject.getNames(objects);
										System.out.println("ISBN Values::::" + objects.get("ISBN13").toString());
										for (String fieldName : elementNames) {
											fieldEntity = new MetaDataFieldsEntity();
											Query query2 = new Query();
											query2.addCriteria(Criteria.where("isActive").is(true).and("isDeleted")
													.is(false).and("metaDataFieldName").is(fieldName));
											query2.fields().exclude("_id");
											fieldEntity = MetaDataServiceImpl.getCdcmongoTemplate().findOne(query2,
													MetaDataFieldsEntity.class,
													MetaDataServiceImpl.getMetaDataFieldDetails());
											if (null != fieldEntity.getJsonType()) {
												if (!("List").equals(fieldEntity.getJsonType())) {
													json.put(fieldEntity.getMetaDataFieldName(),
															objects.get(fieldName).toString());
												} else {
													JSONObject json2 = new JSONObject();
													JSONArray test = new JSONArray();
													referencePath = fieldEntity.getReferencePath();
													referenceValuePath = fieldEntity.getReferenceValuePath();
													String[] arrOfStr = referencePath.split("\\.");
													String[] arrOfIdtype = new String[5];

													if (null != referenceValuePath) {
														arrOfIdtype = referenceValuePath.split("\\.");
													}
													if (null != fieldEntity.getReferenceValue()) {
														if (json.length() > 0) {
															if (json.has(arrOfStr[1])) {
																JSONArray productId = new JSONArray();
																productId = json.getJSONArray(arrOfStr[1]);
																if ("ISBN13".equals(fieldName)) {
																	isbn = objects.get(fieldName).toString();
																}
																json2.put(arrOfStr[2],
																		objects.get(fieldName).toString());
																json2.put(arrOfIdtype[2],
																		fieldEntity.getReferenceValue());
																json.put(arrOfStr[1], productId.put(json2));
															} else {
																json2.put(arrOfStr[2],
																		objects.get(fieldName).toString());
																json2.put(arrOfIdtype[2],
																		fieldEntity.getReferenceValue());
																if ("ISBN13".equals(fieldName)) {
																	isbn = objects.get(fieldName).toString();
																}
																test.put(0, json2);
																json.put(arrOfStr[1], test);
															}
														} else {
															json2.put(arrOfStr[2], objects.get(fieldName).toString());
															json2.put(arrOfIdtype[2], fieldEntity.getReferenceValue());
															test.put(0, json2);
															if ("ISBN13".equals(fieldName)) {
																isbn = objects.get(fieldName).toString();
															}
															json.put(arrOfStr[1], test);
														}
													} else {
														if (json.length() > 0) {
															if (json.has(arrOfStr[1])) {
																json2.put(fieldEntity.getMetaDataFieldName(),
																		objects.get(fieldName).toString());
																json2 = json.getJSONArray(arrOfStr[1]).optJSONObject(0);
																json2.put(fieldEntity.getMetaDataFieldName(),
																		objects.get(fieldName).toString());
															} else {
																json2.put(fieldEntity.getMetaDataFieldName(),
																		objects.get(fieldName).toString());
																test.put(0, json2);
																json.put(arrOfStr[1], test);
															}
														} else {
															json2.put(fieldEntity.getMetaDataFieldName(),
																	objects.get(fieldName).toString());
															test.put(0, json2);
															json.put(arrOfStr[1], test);
														}
													}
												}
											}
										}
									} else {
										if (null != sheetName1) {
											if (!sheetName1.equalsIgnoreCase("Basic")) {

												ParsedSheet parsedSheet = new ParsedSheet(workbook, sheetName1);
												parsedSheet.parseSheet();
												List<Row> targetRow = ExcelParser.findRowsByColumn(parsedSheet,
														"ISBN13", isbn);
												JSONArray array = new JSONArray();
												int count = 0;
												for (Row row : targetRow) {
													JSONObject jsonObject = ExcelParser.parseRow(row, parsedSheet,
															sheetName1);
													array.put(count, jsonObject);
													count++;
												}
												json.put(sheetName1, array);
												System.out.println("json - Other sheets::" + json);
											}
										}
									}
								}
							}
							json1.accumulate("Product", json);
							finalJsonObect.add(json1);
							System.out.println("json1 - Other sheets::" + json1);
							System.out.println("finalJsonObect - Other sheets::" + finalJsonObect);
						}
						break;
					} else {
						if (null != sheetName) {
							if (!sheetName.equalsIgnoreCase("Basic")) {
								ParsedSheet parsedSheet = new ParsedSheet(workbook, sheetName);
								parsedSheet.parseSheet();
								List<Row> targetRow = ExcelParser.findRowsByColumn(parsedSheet, "ISBN13", isbn);
								JSONArray array = new JSONArray();
								int count = 0;
								for (Row row : targetRow) {
									JSONObject jsonObject = ExcelParser.parseRow(row, parsedSheet, sheetName);
									array.put(count, jsonObject);
									count++;
								}
								json.put(sheetName, array);
								System.out.println("json - Other sheets::" + json);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.error("Error uploading file::" + ex.getMessage());
		}
		return finalJsonObect;
	}

	public static List<JSONObject> processExcelValuesExceptBasic(Workbook workbook, String[] sheetList) {
		JSONObject json1 = new JSONObject();
		List<JSONObject> finalJsonObect = new ArrayList<>();
		String isbn = "";
		JSONObject json = new JSONObject();
		ProductEntity resultEntities = new ProductEntity();
		Product searchResultList = new Product();
		ProductEntity metaDataentity = null;
		Map<String, Object> editMdataResult = null;
		String jsonString = null;
		String finalJsonString = null;
		JSONParser parser = new JSONParser();
		MetaDataFieldsEntity fieldEntity = null;

		try {
			for (String sheetName : sheetList) {
				if (null != sheetName) {
					JSONArray rows = parseSheet(workbook, sheetName);
					for (int i = 0; i < rows.length(); i++) {
						JSONObject objects = rows.getJSONObject(i);
						String[] elementNames = JSONObject.getNames(objects);
						isbn = objects.getString("ISBN13");
						try {
							editMdataResult = new HashMap<>();
							Query query2 = new Query();
							query2.addCriteria(Criteria.where("Product.ProductIdentifier.ProductIDType").is("15")
									.and("Product.ProductIdentifier.IDValue").is(isbn));
							metaDataentity = MetaDataServiceImpl.getCdcmongoTemplate().findOne(query2,
									ProductEntity.class, MetaDataServiceImpl.getMetadataResults());
							editMdataResult.put("Product", metaDataentity);
						} catch (Exception e) {
							logger.error("Error occurred while retrieving Metadata::" + e.getMessage());
						}
						if (null != editMdataResult) {
							resultEntities = (ProductEntity) editMdataResult.get("Product");
							searchResultList = MetaDataEntityToVos(resultEntities);
							jsonString = new Gson().toJson(searchResultList);
							for (String fieldName : elementNames) {
								fieldEntity = new MetaDataFieldsEntity();
								Query query2 = new Query();
								query2.addCriteria(Criteria.where("isActive").is(true).and("isDeleted").is(false)
										.and("metaDataFieldName").is(fieldName));
								query2.fields().exclude("_id");
								fieldEntity = MetaDataServiceImpl.getCdcmongoTemplate().findOne(query2,
										MetaDataFieldsEntity.class, MetaDataServiceImpl.getMetaDataFieldDetails());
								if (null != finalJsonString) {
									finalJsonString = JsonPath.parse(finalJsonString)
											.set(fieldEntity.getJsonPath(), objects.get(fieldName).toString()).json();
								} else {
									finalJsonString = JsonPath.parse(jsonString)
											.set(fieldEntity.getJsonPath(), objects.get(fieldName).toString()).json();
								}
							}
						} else {
						}
					}
				}
			}
			json1 = (JSONObject) parser.parse(finalJsonString);
			finalJsonObect.add(json1);
			System.out.println("json1 - Other sheets::" + json1);
			System.out.println("finalJsonObect - Other sheets::" + finalJsonObect);

		} catch (Exception ex) {
			logger.error("Error uploading file::" + ex.getMessage());
		}
		return finalJsonObect;
	}

	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = object.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	public static JSONArray constructJsonArray(Workbook workbook, String[] sheetList) {
		JSONArray json = new JSONArray();

		for (String sheetName : sheetList) {
			if (null != sheetName) {
				JSONArray rows = ExcelParser.parseSheet(workbook, sheetName);
				for (int i = 0; i < rows.length(); i++) {
					json.put(rows.get(i));
				}
			}
		}
		return json;
	}
	/*
	 * Method Use - Used to customize metadata for excel download and upload - Ends
	 * here
	 */

	/* Method Use - Used for excel parser - starts here */
	public static Row findRowByColumn(ParsedSheet sheet, String key, String value) {
		int index = sheet.indexOfKey(key);
		Row row = null;
		if (index == -1)
			throw new IllegalArgumentException("Couldn't find key " + key + " in the provided sheet.");
		for (Iterator<Row> rowsIT = sheet.getSheet().rowIterator(); rowsIT.hasNext();) {
			row = rowsIT.next();
			System.out.println(row);
			Cell cell = row.getCell(index);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			switch (sheet.getType(index)) {
			case BASIC:
				if (cell == null)
					continue;
				if (cell.getCellType() == CELL_TYPE_BLANK)
					continue;
				String cellValue = getCellStringValue(cell);
				if (cellValue.equals(value))
					return row;
				break;
			default:
				throw new IllegalArgumentException(
						"Reference search doesn't support the type " + sheet.getType(index) + " of key " + key + ".");
			}
		}
		return row;
	}

	public static JSONArray parseSheet(Workbook workbook, String configName) {
		JSONArray rows = new JSONArray();
		ParsedSheet parsedSheet = new ParsedSheet(workbook, configName);
		parsedSheet.parseSheet();
		Sheet sheet = parsedSheet.getSheet();
		for (Iterator<Row> rowsIT = sheet.rowIterator(); rowsIT.hasNext();) {
			Row row = rowsIT.next();
			if (row.getRowNum() <= parsedSheet.nameRowIndex)
				continue;
			JSONObject jsonRow = parseRow(row, parsedSheet, configName);
			rows.put(jsonRow);
		}
		return rows;
	}

	public static JSONObject parseRow(Row row, ParsedSheet parsedSheet, String configName) {
		JSONObject jsonRow = new JSONObject();
		for (int index = 0; index < parsedSheet.width; index++) {
			Cell cell = row.getCell(index);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			String key = parsedSheet.getKey(index);
			if (key.startsWith(SIGN_HIDDEN_CELL_PREFIX))
				continue;
			ParsedCellType type = parsedSheet.getType(index);
			switch (type) {
			case BASIC:
				if (cell != null && cell.getCellType() == CELL_TYPE_STRING) {
					if (cell.getStringCellValue().equalsIgnoreCase("null")) {
						jsonRow.put(key, JSONObject.NULL);
						continue;
					}
				}
			case TIME:
				if (cell != null && cell.getCellType() == CELL_TYPE_STRING) {
					if (cell.getStringCellValue().equalsIgnoreCase("null")) {
						jsonRow.put(key, JSONObject.NULL);
						continue;
					}
				}
			case OBJECT:
				if (cell == null || cell.getCellType() == CELL_TYPE_BLANK) {
					jsonRow.put(key, JSONObject.NULL);
					continue;
				}
				break;
			case REFERENCE:
				if (cell == null || cell.getCellType() == CELL_TYPE_BLANK) {
					jsonRow.put(key.substring(0, key.indexOf("@")), JSONObject.NULL);
					continue;
				}
				break;
			case ARRAY_STRING:
			case ARRAY_BOOLEAN:
			case ARRAY_DOUBLE:
				if (cell == null || cell.getCellType() == CELL_TYPE_BLANK) {
					jsonRow.put(key, new ArrayList());
					continue;
				}
				break;
			default:
				throw new IllegalArgumentException("Unhandled empty cell of " + type + " type.");
			}
			ArrayList result;
			JSONArray jsonArray;
			JSONObject jsonObject;
			String stringCellValue = "";
			switch (type) {
			case BASIC:
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_NUMERIC:
					if (!configName.equalsIgnoreCase("Basic")) {
						if (!key.equalsIgnoreCase("ISBN13")) {
							jsonRow.put(key, cell.getNumericCellValue());
						}
					} else {
						jsonRow.put(key, cell.getNumericCellValue());
					}
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					if (!configName.equalsIgnoreCase("Basic")) {
						if (!key.equalsIgnoreCase("ISBN13")) {
							jsonRow.put(key, cell.getBooleanCellValue());
						}
					} else {
						jsonRow.put(key, cell.getBooleanCellValue());
					}
					break;
				default:
					if (!configName.equalsIgnoreCase("Basic")) {
						if (!key.equalsIgnoreCase("ISBN13")) {
							jsonRow.put(key, cell.getStringCellValue());
						}
					} else {
						jsonRow.put(key, cell.getStringCellValue());
					}
					break;
				}
				;
				break;
			case TIME:
				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
					Date time = cell.getDateCellValue();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(time);
					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
					String timeStr = sdf.format(calendar.getTime());
					jsonRow.put(key, timeStr);
				} else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					jsonRow.put(key, cell.getStringCellValue());
				} else {
					throw new IllegalArgumentException("Unhandled cell of " + cell.getCellType() + " type at " + "row "
							+ row.getRowNum() + "column " + index);
				}
				break;
			case ARRAY_STRING:
				result = ExcelParser.<ArrayList<String>>parseCellData(type, stringCellValue);
				jsonArray = new JSONArray(result);
				jsonRow.put(key, jsonArray);
				break;
			case ARRAY_BOOLEAN:
				result = ExcelParser.<ArrayList<Boolean>>parseCellData(type, stringCellValue);
				jsonArray = new JSONArray(result);
				jsonRow.put(key, jsonArray);
				break;
			case ARRAY_DOUBLE:
				result = ExcelParser.<ArrayList<Double>>parseCellData(type, stringCellValue);
				jsonArray = new JSONArray(result);
				jsonRow.put(key, jsonArray);
				break;
			case OBJECT:
				jsonObject = ExcelParser.<JSONObject>parseCellData(type, cell.getStringCellValue());
				jsonRow.put(key, jsonObject);
				break;
			case REFERENCE:
				// Split key to get real key, target sheet name and target
				// column name
				// Key example: monster@monsterSheet#monsterId
				String[] keyAndTarget = key.split(SIGN_TABLE_REFERENCE_SPLITTER);
				key = keyAndTarget[0];
				// Split sheet name and column name
				String[] realTarget = keyAndTarget[1].split(SIGN_SHEETNAME_COLUMNNAME_SPLITTER);
				String targetSheetName = realTarget[0];
				String targetKey = realTarget[1];
				String targetValue = getCellStringValue(cell);
				Sheet targetSheet = parsedSheet.getSheet(targetSheetName);
				ParsedSheet parsedTargetSheet = new ParsedSheet(targetSheet.getWorkbook(), targetSheetName);
				parsedTargetSheet.parseSheet();
				Row targetRow = findRowByColumn(parsedTargetSheet, targetKey, targetValue);
				jsonObject = parseRow(targetRow, parsedTargetSheet, configName);
				jsonRow.put(key, jsonObject);
				break;
			default:
				throw new IllegalArgumentException("Unsupported type " + type + " found.");
			}
		}
		return jsonRow;
	}

	private static String getCellStringValue(Cell cell) {
		switch (cell.getCellType()) {
		case CELL_TYPE_BLANK:
			break;
		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue() + "";
		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue() + "";
		default:
			return cell.getStringCellValue();
		}
		return null;
	}

	public static <W> W parseCellData(ParsedCellType type, String cellValue) throws NumberFormatException {
		Object object = null;
		String[] items = cellValue.split(SIGN_ITEM_SPLITTER);
		switch (type) {
		case ARRAY_STRING:
			ArrayList<String> arrayString = new ArrayList<>();
			for (String item : items) {
				item = item.trim();
				arrayString.add(item);
			}
			object = arrayString;
			break;
		case ARRAY_BOOLEAN:
			ArrayList<Boolean> arrayBoolean = new ArrayList<>();
			for (String item : items) {
				item = item.trim();
				arrayBoolean.add(Boolean.parseBoolean(item));
			}
			object = arrayBoolean;
			break;

		case ARRAY_DOUBLE:
			ArrayList<Double> arrayDouble = new ArrayList<>();
			for (String item : items) {
				item = item.trim();
				arrayDouble.add(Double.parseDouble(item));
			}
			object = arrayDouble;
			break;
		case OBJECT:
			JSONObject obj = new JSONObject();
			for (String item : items) {
				String temp = item.trim();
				String[] keyValue = item.split(SIGN_KEYVALUE_SPLITTER);
				String key = keyValue[0], value = keyValue[1];
				key = key.trim();
				value = value.trim();
				// Handle the null child
				if (value.equalsIgnoreCase("null")) {
					obj.put(key, JSONObject.NULL);
					continue;
				}
				if (value.startsWith("\"")) {
					obj.put(key, value.substring(1, value.length() - 1));
					continue;
				}
				try {
					obj.put(key, Double.parseDouble(value));
				} catch (NumberFormatException e) {
					if (Boolean.parseBoolean(value)) {
						obj.put(key, true);
					} else if (value.equalsIgnoreCase("false")) {
						obj.put(key, false);
					} else {
						obj.put(key, value);
					}
				}
			}

			object = obj;
			break;
		}
		return (W) object;
	}
	/* Method Use - Used for excel parser - ends here */

	@Override
	public APIResponse<Object> exportMetaDataXml(MetaDataXML metaDataXml) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> products = null;
		StringBuilder appendToXml = new StringBuilder();
		String xml = null;

		try {
			products = new HashMap<>();
			List<ProductMapEntity> getProducts = metaDataDao.getProducts(metaDataXml.getIsbns());

			for (ProductMapEntity productMap : getProducts) {
				Map<String, Object> finalData = new HashMap<>();
				finalData.put("Product", productMap.getProduct());
				JSONObject json = new JSONObject(finalData);
				xml = XML.toString(json);
				appendToXml = appendToXml.append(xml);
			}

			if (getProducts.size() > 0) {
				response.setCode(ResponseCode.RECORDS_FOUND.toString());
				response.setStatus(success);
				response.setStatusMessage(successRetrieval);
				response.setData(appendToXml);
			} else {
				response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
				response.setStatus(failure);
				response.setStatusMessage(failureRetrieval);
			}
		} catch (Exception e) {
			logger.error("Error occurred while retrieving metadata::" + e.getMessage());
		}
		return response;
	}

	/**
	 * * Get each product, mapped to each productHierarchyID <br />
	 * * Save current product and assign currentProduct's metadataId <br />
	 * as parentId for upcoming product from next productHierarchy<br />
	 * * Each product hierarchy contains only one product object.
	 * 
	 * Increments metadataSeqId on each product is saved. Sequence collection if
	 * stored successfully
	 * 
	 * @throws IllegalArgumentException
	 * @author Pavan Kumar Yekabote
	 * 
	 */

	@SuppressWarnings("unchecked")
	@Override
	public APIResponse<Object> saveProduct(Map<Object, Object> productMap, String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		Map<Object, Object> pro = new HashMap<Object, Object>();
		// Since product can't be directly stored
		// Convert it to form ProductMapEntity DB class
		Gson gson = new Gson();
		boolean quickCreate = true, multiCreate = false;
		Map<Object, Object> viewResult = new HashMap<Object, Object>();

		Query logicQuery = new Query();
		logicQuery.addCriteria(Criteria.where("isActive").is(true).and("isDeleted").is(false));
		List<String> fieldLogicCalcHierarchy = mongoTemplate.getCollection("cMetaFieldLogics")
				.distinct("productHierarchyId", logicQuery.getQueryObject());

		try {
			// Fetch group, if no value found set group to book
			if (productMap.get("group") == null)
				productMap.put("group", "book");

			if (productMap.get("quickCreate") == null) {
				quickCreate = true;
			} else {
				quickCreate = (Boolean) productMap.get("quickCreate");
			}

			if (productMap.get("multiCreate") != null) {
				multiCreate = (Boolean) productMap.get("multiCreate");
			}
			// Fetch all productHierarchy with groupname in sorted hierarchal order
			List<ProductHierarchyEntity> pHList = metaDataDao.getProductHierarchiesBy((String) productMap.get("group"));

			String referenceParentID = "";

			boolean emptyProducts = false;

			for (ProductHierarchyEntity pH : pHList) {
				Product product = gson.fromJson(gson.toJson(productMap.get(pH.getId())), Product.class);
				if (product == null)
					continue;

				emptyProducts = true;

			}

			if (!emptyProducts) {
				response.setCode(APIResponse.ResponseCode.FAILURE.toString());
				response.setStatus(failure);
				response.setStatusMessage("Error Occurred!! while Storing Data");

				return response;
			}

			for (ProductHierarchyEntity pH : pHList) {
				Query query = new Query();
				String mid = "";
				SequenceId sid = null;
				Product product = gson.fromJson(gson.toJson(productMap.get(pH.getId())), Product.class);
				if (product == null)
					continue;

				query.addCriteria(Criteria.where("_id").is("metadataIdSeq"));
				Update update = new Update();
				update.inc("seq", 1);

				sid = mongoTemplate.findAndModify(query, update, SequenceId.class);

				if (sid == null)
					throw new IllegalArgumentException("Could not find sequence Id");

				mid += sid.getSeq();
				product.setMetadataId(mid);
				if (referenceParentID.equals("")) {
					referenceParentID = mid;
				}

				// Converting to JSON String for setting AutoGen key to product
				if (pH.getAutoGenField() != null) {
					String proJson = new Gson().toJson(product);
					DocumentContext doc = JsonPath.parse(proJson);
					doc.set(pH.getAutoGenJsonPath(), mid);
					product = new Gson().fromJson(doc.jsonString(), Product.class);
				}

				pro.put(pH.getId(), product);

			}

			ProductMapEntity parentProduct = new ProductMapEntity();
			List<ProductMapEntity> parentList = new ArrayList<ProductMapEntity>();

			// Fetching all the metadata and creating it's hashmap
			List<MetaDataFieldsEntity> metaDataFieldList = null;
			Query q1 = new Query();
			q1.addCriteria(Criteria.where("isActive").is(true).and("isDeleted").is(false));
			metaDataFieldList = mongoTemplate.find(q1, MetaDataFieldsEntity.class, "cMetadataFields");

			Map<String, MetaDataFieldsEntity> fieldsMap = metaDataFieldList.stream()
					.filter(enitity -> enitity.getMetaDataFieldName() != null)
					.collect(Collectors.toMap(book -> book.getMetaDataFieldName(), book -> book, (k, v) -> {
						return k;
					}));

			for (ProductHierarchyEntity pH : pHList) {
				// Fetch each product in hierarchical order from request object
				Product product = gson.fromJson(gson.toJson(pro.get(pH.getId())), Product.class);
				if (product == null)
					continue;
				//
				boolean isValid = true, checkForMandatoryUniqueFields = true;

				if (!quickCreate) {
					q1 = new Query();
					q1.addCriteria(Criteria.where("metadataId").is(product.getCustomFields().get("parentID"))
							.and("CustomFields.productHierarchyId").is(pH.getParent_id()));
					parentProduct = (ProductMapEntity) mongoTemplate.findOne(q1, ProductMapEntity.class);
					if (parentProduct.getCustomFields() != null
							&& parentProduct.getCustomFields().containsKey("parents")) {
						referenceParentID = parentProduct.getCustomFields().get("referenceParentID").toString();
						while (parentProduct != null) {
							parentProduct.getCustomFields().remove("parents");
							parentList.add(parentProduct);
							// Fetch next hierarchal parent

							String parentId = (String) parentProduct.getCustomFields().get("parentID");
							Query query = new Query();
							query.addCriteria(Criteria.where("_id")
									.is(parentProduct.getCustomFields().get("productHierarchyId")));
							ProductHierarchyEntity parentpHId = mongoTemplate.findOne(query,
									ProductHierarchyEntity.class);

							q1 = new Query();
							q1.addCriteria(Criteria.where("metadataId").is(parentId)
									.and("CustomFields.productHierarchyId").is(parentpHId.getParent_id()));
							parentProduct = mongoTemplate.findOne(q1, ProductMapEntity.class);
							if (parentId == null || (parentId != null && parentId.trim().equals("")))
								parentProduct = null;
						}
					}

					product.getCustomFields().put("parents", parentList);
					// product.getCustomFields().put("parentID", parentProduct.getMetadataId());

					String productJson = gson.toJson(product.getProduct());

					LinkedHashMap<Object, Object> prodMap = gson.fromJson(productJson, LinkedHashMap.class);

					// Set product object's data to ProductMapEntity object pMap to write in DB
					ProductMapEntity pMap = new ProductMapEntity();
					pMap.setProduct(prodMap);
					pMap.setCustomFields(product.getCustomFields());
					pMap.setMetadataId(product.getMetadataId());

					checkForMandatoryUniqueFields = this.metaDataDao.validateMandatoryUniqueFields(pMap);
					if (!checkForMandatoryUniqueFields) {
						response.setCode(APIResponse.ResponseCode.FAILURE.toString());
						response.setStatusMessage("Mandatory field values seem to be duplicated, Create failed.");
						response.setStatus(failure);
						return response;
					}

					isValid = this.metaDataDao.validateFieldsV2(pMap);

				} else {
					// Set parent Block
					if (parentProduct.getCustomFields() != null
							&& parentProduct.getCustomFields().containsKey("parents")) {
						referenceParentID = parentProduct.getCustomFields().get("referenceParentID").toString();
						parentProduct.getCustomFields().remove("parents");
						parentList.add(parentProduct);
						product.getCustomFields().put("parents", parentList);
						product.getCustomFields().put("parentID", parentProduct.getMetadataId());

					} else {
						if (product.getCustomFields().containsKey("parentID")) {
							q1 = new Query();
							q1.addCriteria(Criteria.where("metadataId").is(product.getCustomFields().get("parentID"))
									.and("CustomFields.productHierarchyId").is(pH.getParent_id()));
							parentProduct = (ProductMapEntity) mongoTemplate.findOne(q1, ProductMapEntity.class);
							ProductMapEntity temp = parentProduct;
							String parentHierarchyId = parentProduct.getCustomFields().get("productHierarchyId")
									.toString();
							referenceParentID = parentProduct.getCustomFields().get("referenceParentID").toString();
							parentProduct.getCustomFields().remove("parents");
							parentList.add(parentProduct);

							ProductHierarchyEntity phParent = pHList.stream()
									.filter(p -> p.getId().equals(parentHierarchyId)).findFirst().orElse(null);
							while (phParent.getParent_id() != null) {
								q1 = new Query();
								q1.addCriteria(Criteria.where("metadataId").is(temp.getCustomFields().get("parentID"))
										.and("CustomFields.productHierarchyId").is(phParent.getParent_id()));
								ProductMapEntity parent = (ProductMapEntity) mongoTemplate.findOne(q1,
										ProductMapEntity.class);
								if (parent != null) {
									parent.getCustomFields().remove("parents");
									parentList.add(parent);
								}
								phParent = pHList.stream()
										.filter(p -> p.getId()
												.equals(parent.getCustomFields().get("productHierarchyId").toString()))
										.findFirst().orElse(null);
							}

							product.getCustomFields().put("parents", parentList);
							product.getCustomFields().put("parentID", parentProduct.getMetadataId());

						} else {
							product.getCustomFields().put("parents", parentList);
							product.getCustomFields().put("parentID", "");
						}
					}

					if (multiCreate) {
						String productJson = gson.toJson(product.getProduct());
						LinkedHashMap<Object, Object> prodMap = gson.fromJson(productJson, LinkedHashMap.class);
						// Set product object's data to ProductMapEntity object pMap to write in DB
						ProductMapEntity pMap = new ProductMapEntity();
						pMap.setProduct(prodMap);
						pMap.setCustomFields(product.getCustomFields());
						pMap.setMetadataId(product.getMetadataId());
						checkForMandatoryUniqueFields = this.metaDataDao.validateMandatoryUniqueFields(pMap);
						if (!checkForMandatoryUniqueFields) {
							response.setCode(APIResponse.ResponseCode.FAILURE.toString());
							response.setStatusMessage("Mandatory field values seem to be duplicated, Create failed.");
							response.setStatus(failure);
							return response;
						}
						isValid = this.metaDataDao.validateFieldsV2(pMap);
						if (!isValid) {
							response.setCode(APIResponse.ResponseCode.FAILURE.toString());
							response.setStatusMessage("Field values seem to be duplicated, Create failed.");
							response.setStatus(failure);
							return response;
						}
					}
				}

				if (!quickCreate && !isValid) {
					response.setCode(APIResponse.ResponseCode.FAILURE.toString());
					response.setStatusMessage("Field values seem to be duplicated, Create failed.");
					response.setStatus(failure);
					return response;
				}

				String productJson = gson.toJson(product);
				if (pro.size() != fieldLogicCalcHierarchy.size()) {
					List<ProductMapEntity> completeParentList = getCompleteParents(parentList);
					Map<Object, Object> phAndProds = getParentsAndChildren(
							new Gson().fromJson(productJson, ProductMapEntity.class), pH, completeParentList);
					for (Entry<Object, Object> phAndProd : phAndProds.entrySet()) {
						if (!pro.containsKey(phAndProd.getKey())) {
							pro.put(phAndProd.getKey(), phAndProd.getValue());
						}
					}
				}

				// ProductJson is passed to fieldLogicManipulations function to create auto
				// calculated fields which then adds to product json and returns product object

				Product prod = fieldLogicManipulations(pro, productJson, pH.getId(), false, metaDataFieldList,
						fieldsMap);
				String finalproductJson = gson.toJson(prod);

				// Set product object's data to ProductMapEntity object pMap to write in DB
				ProductMapEntity pMap = (ProductMapEntity) gson.fromJson(finalproductJson, ProductMapEntity.class);
				// Set modified date manually, since this should not be taken from UI or any
				// other source
				if (!quickCreate && isValid) {
					String syncField = pH.getSyncField().substring(pH.getSyncField().lastIndexOf(".") + 1);

					if (pH.getSyncField() != null && pMap.getCustomFields().get(syncField).toString().equals("Yes")) {

						/*
						 * ProductMapEntity siblingConditionalProd =
						 * metaDataDao.getSiblingsData(referenceParentID,
						 * pMap.getCustomFields().get("productHierarchyId"),syncField);
						 * siblingConditionalProd.getCustomFields().put("IsMaster", "No");
						 * metaDataDao.updateProductsWithSync(siblingConditionalProd);
						 */

					}

				}

				if (pH.isTagCreation()) {
					Query q = new Query();
					q.addCriteria(Criteria.where("userId").is(loggedUser));
					UserEntity usr = mongoTemplate.findOne(q, UserEntity.class);
					String userName = usr.getFirstName();
					pMap.getCustomFields().put("createdBy", userName);
					pMap.getCustomFields().put("createdOn",
							new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
					pMap.getCustomFields().put("createdOnISO", new Date());
					pMap.getCustomFields().put("updatedBy", userName);
					pMap.getCustomFields().put("updatedOnISO", new Date());
					pMap.getCustomFields().put("updatedOn",
							new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
				}

				// seting lock
				if (parentProduct.getCustomFields() != null
						&& parentProduct.getCustomFields().get("isLocked") != null) {
					if ((boolean) parentProduct.getCustomFields().get("isLocked")) {
						pMap.getCustomFields().put("isLocked", true);
						pMap.getCustomFields().put("lockedByUsername",
								parentProduct.getCustomFields().get("lockedByUsername"));
						pMap.getCustomFields().put("lockedByUserId",
								parentProduct.getCustomFields().get("lockedByUserId"));
						pMap.getCustomFields().put("lockedOn", parentProduct.getCustomFields().get("lockedOn"));
					}
				}

				APIResponse<Object> statusWithProductId = metaDataDao.saveProduct(pMap, referenceParentID);
				List<CDCMetaDataDetails> cdcMetaDataDetails = null;
				saveCdcMetadata(cdcMetaDataDetails, loggedUser, pMap.getMetadataId(),
						pMap.getCustomFields().get("productHierarchyId"),
						pMap.getCustomFields().get("Titletext").toString(), "Insert", false);

				if (statusWithProductId.getStatus().equals(APIResponse.ResponseCode.SUCCESS.toString())) {

					String currProdHierarchy = pMap.getCustomFields().get("productHierarchyId").toString();
					storeInProductStagesApprovalNew(currProdHierarchy, loggedUser, pMap, false, null);

					// ELASTIC SEARCH INDEXING
					Query q = new Query();
					q.addCriteria(Criteria.where("metadataId").is(pMap.getMetadataId())
							.and("CustomFields.productHierarchyId").is(pH.getId()));
					ProductMapEntity savedProd = mongoTemplate.findOne(q, ProductMapEntity.class, "products");
					boolean status = insertForIndexing(savedProd, false);
					q = new Query();
					Update up = new Update();
					q.addCriteria(Criteria.where("metadataId").is(pMap.getMetadataId()));
					if (!status) {
						up.set("CustomFields.isIndexed", false);
						mongoTemplate.updateFirst(q, up, "products");
						logger.info("ELASTIC SEARCH INDEXING -> Failed indexing the product");
					} else {
						up.set("CustomFields.isIndexed", true);
						mongoTemplate.updateFirst(q, up, "products");
					}
					parentProduct = (ProductMapEntity) statusWithProductId.getData();
					if (pH.isViewProduct()) {
						viewResult.put("customFieldsTitleTextValue", parentProduct.getCustomFields().get("Titletext"));
						viewResult.put("UID", parentProduct.getCustomFields().get("UID"));
						viewResult.put("productHierarchyName",
								parentProduct.getCustomFields().get("productHierarchyName"));
						viewResult.put("productHierarchyId", parentProduct.getCustomFields().get("productHierarchyId"));
						viewResult.put("metadataId", parentProduct.getMetadataId());
					}

					if (pH.isUpdateISBN()) {
						String proJson = new Gson().toJson(parentProduct);
						DocumentContext newDoc = JsonPath.parse(proJson);
						String isbn = "";
						try {
							// Unique Identifier from UID path
							isbn = newDoc.read(pH.getIsbnPath()).toString();
							isbn = isbn.replace("[", "").replace("]", "").replace("\"", "");
						} catch (Exception e) {
							// TODO: handle exception
							isbn = "";
						}

						if (!(isbn.equals(""))) {
							Query query = new Query();
							Update update = new Update();
							query.addCriteria(Criteria.where("SupportKey").is("ISBN").and("Key").is(isbn));

							update.set("Value.Status", "N");

							mongoTemplate.updateFirst(query, update, "supportTableData");
						}
					}

				} else
					throw new IOException("Error occured while saving product to DB : " + pH.getId());
			}

			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			if (!(viewResult.isEmpty())) {
				response.setData(viewResult);
			}
			response.setStatus(success);
			response.setStatusMessage("Success!! Your Data has been Stored");
		} catch (Exception e) {
			if (e instanceof IllegalArgumentException)
				logger.error("Error occured while saving product " + e);
			logger.error("Error occured while saving product ", e);

			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatus(failure);
			response.setStatusMessage("Error Occurred!! while Storing Data");
		}

		return response;
	}

	private void storeInProductStagesApprovalNew(String currProdHierarchy, String loggedUser, ProductMapEntity pMap,
			boolean isUpdate, ProductMapEntity pDBMap) {
		Query qry = null;
		boolean status = false;
		// List<String> usrApproveAccessStages = approvedAccessStages(loggedUser);
		List<ProductStagesApproval> psas = new ArrayList<>();
		ProductStagesApproval psa = null;
		qry = new Query();
		qry.addCriteria(Criteria.where("productHierarchyId").is(currProdHierarchy).and("isActive").is(true)
				.and("isDeleted").is(false).and("isKeyField").is(true));
		List<MetadataFields> cMFields = mongoTemplate.find(qry, MetadataFields.class);

		Date todayDate = new Date();
		qry = new Query();
		qry.addCriteria(Criteria.where("createProductHierarchyId").in(currProdHierarchy).and("isActive").is(true)
				.and("isDeleted").is(false));
		List<MetaDataProductHierarchyStageEntity> cMStages = mongoTemplate.find(qry,
				MetaDataProductHierarchyStageEntity.class, "cMetadataStages");

		if (!isUpdate) {
			for (MetaDataProductHierarchyStageEntity cMStage : cMStages) {
				
					List<MetadataFields> sMetaFields = cMFields.stream()
							.filter(sMFields -> sMFields.getStageId().equals(cMStage.getStageId()))
							.collect(Collectors.toList());
					status = checkAllFieldsExistsInProduct(sMetaFields, pMap);
				
					psa = setProductStagesApproval(currProdHierarchy, loggedUser, pMap, cMStage, status);
			
				psas.add(psa);
			}
		} else {
			List<String> unAprStages = new ArrayList<String>();
			unAprStages.add("InProgress");
			unAprStages.add("Awaiting for approval");
			qry = new Query();
			qry.addCriteria(Criteria.where("metadataId").is(pMap.getMetadataId()).and("productHierarchyId")
					.is(currProdHierarchy).and("remarks").in(unAprStages));
			List<ProductStagesApprovalEntity> storedPsas = mongoTemplate.find(qry, ProductStagesApprovalEntity.class);
			List<String> unAprvStageIds = storedPsas.stream().map(s -> s.getStageId()).collect(Collectors.toList());
			List<String> awaitingStageIds = storedPsas.stream()
					.filter(s -> s.getRemarks().equals("Awaiting for approval")).map(m -> m.getStageId())
					.collect(Collectors.toList());
			List<Criteria> updateCrias = new ArrayList<Criteria>();
			for (MetaDataProductHierarchyStageEntity cMStage : cMStages) {
				if (cMStage.getApprovalMethod() != null && !awaitingStageIds.contains(cMStage.getStageId())) {
					List<Object> inputFieldVals = getKeyFieldValues(pMap, cMFields, cMStage.getStageId());
					List<Object> dbFieldVals = getKeyFieldValues(pDBMap, cMFields, cMStage.getStageId());
					if (!inputFieldVals.equals(dbFieldVals)) {
						psa = setProductStagesApproval(currProdHierarchy, loggedUser, pMap, cMStage, status);
						psas.add(psa);
					}
				} else if (cMStage.getApprovalMethod() == null) {
					List<MetadataFields> sMetaFields = cMFields.stream()
							.filter(sMFields -> sMFields.getStageId().equals(cMStage.getStageId()))
							.collect(Collectors.toList());
					status = checkAllFieldsExistsInProduct(sMetaFields, pMap);
					if (!status && !unAprvStageIds.contains(cMStage.getStageId())) {
						psa = setProductStagesApproval(currProdHierarchy, loggedUser, pMap, cMStage, status);
						psas.add(psa);
					} else if( status && unAprvStageIds.contains(cMStage.getStageId())) {
						updateCrias.add(Criteria.where("metadataId").is(pMap.getMetadataId()).
								and("stageId").is(cMStage.getStageId()).and("remarks").is("InProgress"));
					}
						status = false;
				}
			}
			if(!updateCrias.isEmpty()) {
				Criteria criteria = new Criteria().orOperator(updateCrias.toArray(new Criteria[updateCrias.size()]));

				Update update = new Update();
				update.set("remarks", "Completed").set("approvedBy", loggedUser).set("approvedOn",
						new Date());
				Query Query = new Query().addCriteria(criteria);
				mongoTemplate.updateMulti(Query, update, ProductStagesApprovalEntity.class);
			}
				
		}
		if (!psas.isEmpty())
			mongoTemplate.insert(psas, "tProductStagesApproval");
		

	}

	private ProductStagesApproval setProductStagesApproval(String currProdHierarchy, String loggedUser,
			ProductMapEntity pMap, MetaDataProductHierarchyStageEntity cMStage, boolean status) {
		ProductStagesApproval psa = new ProductStagesApproval();
        
		if (status && cMStage.getApprovalMethod() != null)
			psa.setRemarks("Awaiting for approval");
		else if(!status)
        	psa.setRemarks("InProgress");
		else if (status) {
			psa.setRemarks("Completed");
			psa.setApprovedBy(loggedUser);
			psa.setApprovedOn(new Date());
		}
		psa.setApprovalMethod(cMStage.getApprovalMethod() == null ? "AUTOAPPROVE" : cMStage.getApprovalMethod());

		psa.setMetadataId(pMap.getMetadataId());
		psa.setProductHierarchyId(currProdHierarchy);
		psa.setStageId(cMStage.getStageId());
		psa.setCreatedBy(loggedUser);
		psa.setCreatedOn(new Date());
		psa.setStageDisplayName(cMStage.getStageDisplayName());
		return psa;
	}

	private List<Object> getKeyFieldValues(ProductMapEntity pMap, List<MetadataFields> cMFields, String stageId) {

		List<MetadataFields> sMetaFields = cMFields.stream().filter(sMFields -> sMFields.getStageId().equals(stageId))
				.collect(Collectors.toList());
		List<Object> list = new ArrayList<Object>();

		if (sMetaFields.size() > 0) {
			for (MetadataFields metaField : sMetaFields) {
				String jsonPath = metaField.getJsonPath();
				try {
					String json = new Gson().toJson(pMap);
					DocumentContext inDocCtx = JsonPath.parse(json);
					Object obj = inDocCtx.read(jsonPath);
					if (obj != null)
						list.add(obj);
				} catch (Exception e) {

				}

			}
		}
		return list;

	}

	private boolean calculateKeyFields(ProductMapEntity pMap, List<MetadataFields> cMFields) {
		boolean status = true;
		String json = new Gson().toJson(pMap);
		List<MetadataFields> fieldsWithPhid = null;
		DocumentContext inDocCtx = JsonPath.parse(json);
		fieldsWithPhid = cMFields.stream()
				.filter(p -> p.getProductHierarchyId().equals(pMap.getCustomFields().get("productHierarchyId")))
				.collect(Collectors.toList());
		status = statusOfKeyFieldswithHierarchy(inDocCtx, fieldsWithPhid);
		if (status) {
			List<ProductMapEntity> parents = getCustomfieldsParents(pMap);
			for (ProductMapEntity parent : parents) {
				json = new Gson().toJson(pMap);
				inDocCtx = JsonPath.parse(json);
				fieldsWithPhid = cMFields.stream()
						.filter(p -> p.getProductHierarchyId().equals(pMap.getCustomFields().get("productHierarchyId")))
						.collect(Collectors.toList());
				status = statusOfKeyFieldswithHierarchy(inDocCtx, fieldsWithPhid);
				if (!status)
					break;
			}
		} else
			return status;

		return status;
	}

	private boolean statusOfKeyFieldswithHierarchy(DocumentContext inDocCtx, List<MetadataFields> fieldsWithPhid) {
		boolean status = true;
		for (MetadataFields metaField : fieldsWithPhid) {
			String jsonPath = metaField.getJsonPath();
			try {

				Object objVal = inDocCtx.read(jsonPath);
				if (objVal == null) {
					status = false;
				}
			} catch (Exception e) {
				status = false;
			}
			if (!status)
				break;
		}
		return status;
	}

	private boolean checkAllFieldsExistsInProduct(List<MetadataFields> sMetaFields, ProductMapEntity pMap) {

		boolean status = true;
		if (sMetaFields.size() > 0) {
			for (MetadataFields metaField : sMetaFields) {
				String jsonPath = metaField.getJsonPath();
				try {
					String json = new Gson().toJson(pMap);
					DocumentContext inDocCtx = JsonPath.parse(json);
					Object obj = inDocCtx.read(jsonPath);
					if (obj == null) {
						status = false;
					}
				} catch (Exception e) {
					status = false;
				}
				if (!status)
					break;
			}
		} else {
			status = true;
		}
		return status;
	}

	private List<String> approvedAccessStages(String loggedUser) {
		List<String> stages = new ArrayList<>();

		Query queryUser = new Query();
		queryUser.addCriteria(
				Criteria.where("userId").is(loggedUser).and("isActive").is(true).and("isDeleted").is(false));
		UserEntity userEntity = mongoTemplate.findOne(queryUser, UserEntity.class);

		RoleEntity role = null;

		if (userEntity != null) {
			String permissionGroupId = userEntity.getPermissionGroupId();
			if (permissionGroupId != null) {
				Query queryPG = new Query();
				queryPG.addCriteria(Criteria.where("permissionGroupId").is(permissionGroupId).and("isActive").is(true)
						.and("isDeleted").is(false));
				PermissionGroupEntity pg = mongoTemplate.findOne(queryPG, PermissionGroupEntity.class);
				String roleId = pg.getRoleId();
				if (roleId != null) {
					Query queryRole = new Query();
					queryRole.addCriteria(
							Criteria.where("roleId").is(roleId).and("isActive").is(true).and("isDeleted").is(false));
					role = mongoTemplate.findOne(queryRole, RoleEntity.class);
				}
			}
		}

		ModuleAccessEntity moduleStages = null;
		if (role != null) {
			List<ModuleAccessEntity> maEntity = role.getModule();
			moduleStages = maEntity.stream().filter(p -> p.getModuleId().equals("MOD000014")).findFirst().orElse(null);
		}

		if (moduleStages != null) {
			List<SubModuleAccessEntity> moduleAccessPerStages = moduleStages.getSubmodule();

			for (SubModuleAccessEntity moduleAccessPerStage : moduleAccessPerStages) {
				if (moduleAccessPerStage.getActivityAccess().contains("APPROVE")) {
					stages.add(moduleAccessPerStage.getAccessId());
				}
			}

		}

		return stages;
	}

	private void storeInProductStagesApprovalOld(String currProdHierarchy, String loggedUser, ProductMapEntity pMap,
			boolean isUpdate) {
		try {
			// checking value of productStageAssetsApproval

			boolean checkStagesApproved = false;
			Query qry = new Query();
			qry.addCriteria(Criteria.where("_id").is("stagesApprove"));
			ManageConfigEntity manageConfig = mongoTemplate.findOne(qry, ManageConfigEntity.class);

			checkStagesApproved = (boolean) manageConfig.getConfig().get("productStageAssetsApproval");

			List<ProductStagesApproval> psas = new ArrayList<>();
			List<ProductStagesApproval> updatePsas = new ArrayList<>();
			// checkStagesApproved = false;

			if (checkStagesApproved) {
				qry = new Query();
				qry.addCriteria(Criteria.where("createProductHierarchyId").in(currProdHierarchy).and("isActive")
						.is(true).and("isDeleted").is(false));
				List<MetaDataProductHierarchyStageEntity> cMStages = mongoTemplate.find(qry,
						MetaDataProductHierarchyStageEntity.class, "cMetadataStages");

				for (MetaDataProductHierarchyStageEntity cMStage : cMStages) {
					ProductStagesApproval psa = new ProductStagesApproval();
					psa.setMetadataId(pMap.getMetadataId());
					psa.setApprovedOn(new Date());
					psa.setProductHierarchyId(currProdHierarchy);
					psa.setApprovedBy(loggedUser);
					psa.setRemarks("Auto Approved");
					psa.setStageId(cMStage.getStageId());
					psas.add(psa);
				}

			} else {
				// check if user has approval rights
				// if has, return list of stages with approval rights
				// else return empty list

				List<String> userApprovalRightsStages = checkStagesRights(loggedUser);

				if (userApprovalRightsStages != null && userApprovalRightsStages.size() > 0) {
					// check if all key field is filled or not
					qry = new Query();
					qry.addCriteria(Criteria.where("productHierarchyId").is(currProdHierarchy).and("isActive").is(true)
							.and("isDeleted").is(false).and("isKeyField").is(true));
					List<MetadataFields> cMFields = mongoTemplate.find(qry, MetadataFields.class);

					qry = new Query();
					qry.addCriteria(Criteria.where("createProductHierarchyId").in(currProdHierarchy).and("isActive")
							.is(true).and("isDeleted").is(false));
					List<MetaDataProductHierarchyStageEntity> cMStages = mongoTemplate.find(qry,
							MetaDataProductHierarchyStageEntity.class, "cMetadataStages");

					List<String> userApprovalRightsStagesId = cMStages.stream()
							.filter(p -> userApprovalRightsStages.contains(p.getStageId()))
							.map(MetaDataProductHierarchyStageEntity::getStageId).collect(Collectors.toList());

					for (MetaDataProductHierarchyStageEntity mStages : cMStages) {
						List<MetadataFields> sMetaFields = cMFields.stream()
								.filter(sMFields -> sMFields.getStageId().equals(mStages.getStageId()))
								.collect(Collectors.toList());

						boolean status = true;
						if (sMetaFields.size() > 0) {
							for (MetadataFields metaField : sMetaFields) {
								String jsonPath = metaField.getJsonPath();
								try {
									String json = new Gson().toJson(pMap);
									DocumentContext inDocCtx = JsonPath.parse(json);
									Object obj = inDocCtx.read(jsonPath);
									if (obj == null) {
										status = false;
									}
								} catch (Exception e) {
									status = false;
								}

							}
						} else {
							status = true;
						}
						ProductStagesApproval psa = new ProductStagesApproval();
						psa.setMetadataId(pMap.getMetadataId());
						psa.setApprovedOn(new Date());
						psa.setProductHierarchyId(currProdHierarchy);
						psa.setApprovedBy(loggedUser);

						if (status && userApprovalRightsStagesId.contains(mStages.getStageId())) {
							psa.setRemarks("Manual Approved");
							psa.setStageId(mStages.getStageId());
							psas.add(psa);
						}
					}
				}

			}

			if (psas.size() > 0) {
				if (!isUpdate) {
					mongoTemplate.insert(psas, "tProductStagesApproval");
				} else {
					qry = new Query();
					qry.addCriteria(Criteria.where("metadataId").is(pMap.getMetadataId()).and("productHierarchyId")
							.is(currProdHierarchy));
					List<ProductStagesApproval> storedPsas = mongoTemplate.find(qry, ProductStagesApproval.class);
					if (storedPsas != null && storedPsas.size() > 0) {
						List<String> storedStagesId = storedPsas.stream().map(p -> p.getStageId())
								.collect(Collectors.toList());
						for (ProductStagesApproval psa : psas) {
							String stageId = psa.getStageId();
							if (!storedStagesId.contains(stageId)) {
								updatePsas.add(psa);
							}
						}
					}
					if (updatePsas.size() > 0) {

						mongoTemplate.insert(updatePsas, "tProductStagesApproval");
					}
				}
			}
		} catch (Exception e) {
			logger.info("error: storeInProductStagesApproval " + e.getMessage());
		}
	}

	private List<String> checkStagesRights(String loggedUser) {
		List<String> stages = new ArrayList<>();

		Query queryUser = new Query();
		queryUser.addCriteria(
				Criteria.where("userId").is(loggedUser).and("isActive").is(true).and("isDeleted").is(false));
		UserEntity userEntity = mongoTemplate.findOne(queryUser, UserEntity.class);

		RoleEntity role = null;

		if (userEntity != null) {
			String permissionGroupId = userEntity.getPermissionGroupId();
			if (permissionGroupId != null) {
				Query queryPG = new Query();
				queryPG.addCriteria(Criteria.where("permissionGroupId").is(permissionGroupId).and("isActive").is(true)
						.and("isDeleted").is(false));
				PermissionGroupEntity pg = mongoTemplate.findOne(queryPG, PermissionGroupEntity.class);
				String roleId = pg.getRoleId();
				if (roleId != null) {
					Query queryRole = new Query();
					queryRole.addCriteria(
							Criteria.where("roleId").is(roleId).and("isActive").is(true).and("isDeleted").is(false));
					role = mongoTemplate.findOne(queryRole, RoleEntity.class);
				}
			}
		}

		ModuleAccessEntity moduleStages = null;
		if (role != null) {
			List<ModuleAccessEntity> maEntity = role.getModule();
			moduleStages = maEntity.stream().filter(p -> p.getModuleId().equals("MOD000014")).findFirst().orElse(null);
		}
		boolean checkStagesApproved = false;
		Query q = new Query();
		q.addCriteria(Criteria.where("_id").is("stagesApprove"));
		ManageConfigEntity manageConfig = mongoTemplate.findOne(q, ManageConfigEntity.class);

		checkStagesApproved = (boolean) manageConfig.getConfig().get("productStageAssetsApproval");
		if (moduleStages != null) {
			List<SubModuleAccessEntity> moduleAccessPerStages = moduleStages.getSubmodule();
			if (!checkStagesApproved) {
				for (SubModuleAccessEntity moduleAccessPerStage : moduleAccessPerStages) {
					if (moduleAccessPerStage.getActivityAccess().contains("APPROVE")) {
						stages.add(moduleAccessPerStage.getAccessId());
					}
				}
			}
		}

		return stages;
	}

	@SuppressWarnings("rawtypes")
	private boolean insertForIndexing(ProductMapEntity pMap, boolean isUpdate) {
		boolean result = false;
		if (isUpdate) {
			String tempUrlStorage = elasticSearchIndexUpdateURL;
			if (elasticSearchIndexUpdateURL != null && elasticSearchIndexUpdateURL.trim().length() > 0)
				try {
					elasticSearchIndexUpdateURL = tempUrlStorage;
					elasticSearchIndexUpdateURL += pMap.getId();

					ResponseEntity<Map> response = restTemplate.postForEntity(elasticSearchIndexUpdateURL, pMap,
							Map.class);
					if (response.getStatusCode() != null
							&& Integer.parseInt(response.getBody().get("code").toString()) == 200) {
						result = true;
					}
					elasticSearchIndexUpdateURL = tempUrlStorage;
				} catch (Exception postException) {
					logger.error("Error while fetching from search service :: ", postException);
				}
		} else {
			if (elasticSearchIndexURL != null && elasticSearchIndexURL.trim().length() > 0)
				try {
					ResponseEntity<Map> response = restTemplate.postForEntity(elasticSearchIndexURL, pMap, Map.class);
					if (response.getStatusCode() != null
							&& Integer.parseInt(response.getBody().get("code").toString()) == 200) {
						result = true;
					}
				} catch (Exception postException) {
					logger.error("Error while fetching from search service :: ", postException);
				}
		}
		return result;
	}

	public <T> boolean isNull(T obj) {

		if (obj instanceof Collection<?>)
			return (obj == null || ((Collection<?>) obj).isEmpty());
		else if (obj instanceof String)
			return (obj == null || ((String) obj).trim().isEmpty());
		else if (obj instanceof Object[])
			return (obj == null || ((Object[]) obj).length == 0);
		else
			return obj == null;

	}

	/**
	 * Loops through all the field logics and calculates the fields that holds true
	 * for "condition"
	 *
	 * @return calculated Product object
	 * @author Rupesh Maharjan
	 **/
	@SuppressWarnings({ "unchecked", "deprecation" })
	private Product fieldLogicManipulations(Map<Object, Object> pro, String productJson, String pH, boolean isEdit,
			List<MetaDataFieldsEntity> metaDataFieldList, Map<String, MetaDataFieldsEntity> fieldsMap) {

		String reqJson = productJson;
		Object jsonPathValue = null;
		List<CMetaFieldLogicEntity> fieldLogicList = null;
		MetaDataFieldsEntity currentConditionField, metaField = null;
		List<String> objectList = null;
		Query q = new Query();
		boolean checkForCondition = true;
		boolean checkForNullValues = true;

		if (isEdit)
			q.addCriteria(Criteria.where("isActive").is(true).and("isDeleted").is(false).and("checkOnEdit").is(true)
					.and("productHierarchyId").is(pH));
		else
			q.addCriteria(
					Criteria.where("isActive").is(true).and("isDeleted").is(false).and("productHierarchyId").is(pH));

		fieldLogicList = mongoTemplate.find(q, CMetaFieldLogicEntity.class, "cMetaFieldLogics");
		logger.info("total fieldLogicList " + fieldLogicList.size());

		if (fieldLogicList.size() == 0) {
			return new Gson().fromJson(productJson, Product.class);
		}

		String prodJson = JSON.parse(JonixJson.toJson(reqJson)).toString();

		DocumentContext docCtx = JsonPath.parse(prodJson);

		String parentId = "";
		try {
			parentId = docCtx.read("$.CustomFields.parentID");
			parentId = parentId.replace("[", "").replace("]", "").replace("\"", "");
		} catch (Exception e) {
			logger.warn("cannot find parentID");
		}
		if (fieldLogicList != null) {
			for (CMetaFieldLogicEntity currentLogic : fieldLogicList) {
				checkForCondition = true;
				checkForNullValues = true;
				logger.info("checking for " + currentLogic.getMetaDataFieldName());
				if (currentLogic.getMetaDataFieldName() != null) {
					metaField = fieldsMap.get(currentLogic.getMetaDataFieldName());
				}
				if (currentLogic.getCondition() != null && currentLogic.getCondition().size() > 0) {
					System.out.println("Total conditions: " + currentLogic.getCondition().size());
					for (CMetaFieldLogicCondition currentCondition : currentLogic.getCondition()) {
						System.out.println("conditions " + currentCondition.getMetaDataFieldName());
						jsonPathValue = null;
						if (!checkForCondition)
							break;
						try {
							currentConditionField = fieldsMap.get(currentCondition.getMetaDataFieldName());
							String mPH = currentConditionField.getProductHierarchyId();

							DocumentContext temp_docCtx = null;
							if (mPH.equals(pH)) {
								temp_docCtx = docCtx;
							} else {
								// Fetch each product in hierarchal order from request object
								Product product = new Gson().fromJson(new Gson().toJson(pro.get(mPH)), Product.class);
								if (product == null)
									continue;

								String temp_prod_mPH = new Gson().toJson(product);
								temp_docCtx = JsonPath.parse(temp_prod_mPH);
							}
							switch (currentCondition.getOperator()) {
							case "=":
								try {
									jsonPathValue = temp_docCtx.read(currentConditionField.getJsonPath()).toString();
								} catch (Exception e) {
									System.err.println(
											e.getMessage() + " = " + currentConditionField.getMetaDataFieldName());
								}
								if (jsonPathValue instanceof String || jsonPathValue instanceof Integer
										|| jsonPathValue instanceof Double) {
									if (jsonPathValue.toString().replace("[", "").replace("]", "").replace("\"", "")
											.replace("\\", "").equals(currentCondition.getValue()))
										continue;
									else {
										checkForCondition = false;
										break;
									}
								} else if (jsonPathValue instanceof Boolean) {
									if (jsonPathValue == currentCondition.getValue())
										continue;
									else {
										checkForCondition = false;
										break;
									}
								}

							case ">":
								// currentConditionField =
								// fieldsMap.get(currentCondition.getMetaDataFieldName());
								try {
									jsonPathValue = temp_docCtx.read(currentConditionField.getJsonPath());
								} catch (Exception e) {
									System.err.println(
											e.getMessage() + " = " + currentConditionField.getMetaDataFieldName());
								}

								if (Double.parseDouble((String) jsonPathValue) > Double
										.parseDouble((String) currentCondition.getValue()))
									continue;
								else {
									checkForCondition = false;
									break;
								}

							case ">=":
								// currentConditionField =
								// fieldsMap.get(currentCondition.getMetaDataFieldName());
								try {
									jsonPathValue = temp_docCtx.read(currentConditionField.getJsonPath());
								} catch (Exception e) {
									System.err.println(
											e.getMessage() + " = " + currentConditionField.getMetaDataFieldName());
								}
								if (Double.valueOf(jsonPathValue.toString()) >= (Double
										.valueOf(currentCondition.getValue().toString())))
									continue;
								else {
									checkForCondition = false;
									break;
								}

							case "<":
								// currentConditionField =
								// fieldsMap.get(currentCondition.getMetaDataFieldName());
								try {
									jsonPathValue = temp_docCtx.read(currentConditionField.getJsonPath());
								} catch (Exception e) {
									System.err.println(
											e.getMessage() + " = " + currentConditionField.getMetaDataFieldName());
								}
								if (Double.parseDouble((String) jsonPathValue) < Double
										.parseDouble((String) currentCondition.getValue()))
									continue;
								else {
									checkForCondition = false;
									break;
								}

							case "<=":
								// currentConditionField =
								// fieldsMap.get(currentCondition.getMetaDataFieldName());
								try {
									jsonPathValue = temp_docCtx.read(currentConditionField.getJsonPath());
								} catch (Exception e) {
									System.err.println(
											e.getMessage() + " = " + currentConditionField.getMetaDataFieldName());
								}
								if (Double.parseDouble((String) jsonPathValue) <= Double
										.parseDouble((String) currentCondition.getValue()))
									continue;
								else {
									checkForCondition = false;
									break;
								}

							case "!=":
								// currentConditionField =
								// fieldsMap.get(currentCondition.getMetaDataFieldName());
								try {
									jsonPathValue = temp_docCtx.read(currentConditionField.getJsonPath());
								} catch (Exception e) {
									System.err.println(
											e.getMessage() + " = " + currentConditionField.getMetaDataFieldName());
								}
								if (jsonPathValue instanceof Double || jsonPathValue instanceof Integer) {
									if ((Double) jsonPathValue != (Double) currentCondition.getValue())
										continue;
									else {
										checkForCondition = false;
										break;
									}
								}

								if (jsonPathValue instanceof String) {
									if (jsonPathValue.toString()
											.equalsIgnoreCase(currentCondition.getValue().toString()))
										continue;
									else {
										checkForCondition = false;
										break;
									}
								}

								break;

							case "Not in":
								// currentConditionField =
								// fieldsMap.get(currentCondition.getMetaDataFieldName());
								try {
									jsonPathValue = temp_docCtx.read(currentConditionField.getJsonPath()).toString();
								} catch (Exception e) {
									System.err.println(
											e.getMessage() + " = " + currentConditionField.getMetaDataFieldName());
								}
								objectList = (List<String>) currentCondition.getValue();

								if (objectList.contains(jsonPathValue)) {
									checkForCondition = false;
									break;
								} else
									continue;

							case "In":
								// currentConditionField =
								// fieldsMap.get(currentCondition.getMetaDataFieldName());
								try {
									jsonPathValue = temp_docCtx.read(currentConditionField.getJsonPath());
									if (jsonPathValue.toString().contains("["))
										jsonPathValue = jsonPathValue.toString().replace("[", "").replace("]", "")
												.replace("\"", "");
								} catch (Exception e) {
									System.err.println(
											e.getMessage() + " = " + currentConditionField.getMetaDataFieldName());
								}
								objectList = (List<String>) currentCondition.getValue();
								if (objectList.contains(jsonPathValue))
									continue;
								else {
									checkForCondition = false;
									break;
								}

							default:
								break;
							}
						} catch (Exception e) {
							// e.printStackTrace();
							logger.warn(e.getMessage());
							checkForCondition = false;
						}
					}
				}

				if (!checkForCondition) {
					// docCtx.delete(metaField.getJsonPathRemove());
					continue;
				} else {
					List<MetaDataFieldsEntity> fieldsRequiredEntity = null;
					Map<String, String> substituteData = null;
					if (currentLogic.getFieldsRequired() != null) {
						fieldsRequiredEntity = fieldsMap.keySet().stream()
								.filter(p -> currentLogic.getFieldsRequired().indexOf(p) != -1)
								.map(p -> fieldsMap.get(p)).collect(Collectors.toList());
						substituteData = new HashMap<String, String>();

						for (MetaDataFieldsEntity fieldToCheckNull : fieldsRequiredEntity) {
							try {
								String mPh = fieldToCheckNull.getProductHierarchyId();
								DocumentContext temp_docCtx_req_fields = null;

								if (pH.equals(mPh)) {
									temp_docCtx_req_fields = docCtx;
								} else {
									Product product = new Gson().fromJson(new Gson().toJson(pro.get(mPh)),
											Product.class);
									if (product == null) {
										checkForNullValues = false;
										break;
									}

									String temp_prod_mPh = new Gson().toJson(product);
									temp_docCtx_req_fields = JsonPath.parse(temp_prod_mPh);
								}

								Object fieldValue = temp_docCtx_req_fields.read(fieldToCheckNull.getJsonPath())
										.toString();
								if (fieldValue != null && !fieldValue.equals("") && !fieldValue.equals("[]")) {
									substituteData.put(fieldToCheckNull.getMetaDataFieldName(),
											StringEscapeUtils.unescapeJava(fieldValue.toString().replace("[", "")
													.replace("]", "").replace("\"", "")));
									continue;
								} else {
									try {
										String jsonPath = metaField.getJsonPath();
										Object val = docCtx.read(jsonPath);
										if (val != null || val != "" || val.toString() != "[]") {
											docCtx.put(metaField.getJsonDbPathTransform(),
													metaField.getJsonDbPathElement(), "");
											logger.info("removed the field :: " + metaField.getMetaDataFieldName());
										}
									} catch (Exception e) {
									}
									throw new Exception(fieldToCheckNull.getFieldDisplayName() + " is null/empty.");
								}
							} catch (Exception e) {
								logger.warn(e.getMessage());
								checkForNullValues = false;
								break;
							}
						}
					}
					if (!checkForNullValues) {
						continue;
					} else {
						System.out.println(currentLogic.getMetaDataFieldName() + " operation: "
								+ currentLogic.getOperation().toUpperCase());
						SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
						SimpleDateFormat format2 = new SimpleDateFormat("MM-dd-yyyy");
						switch (currentLogic.getOperation().toUpperCase()) {
						case "STRING_SUBSTITUTE":
							try {
								if (currentLogic.getStaticValue().contains("${ParentISBN}") && !parentId.equals("")) {
									Query query = new Query();
									query.addCriteria(Criteria.where("CustomFields.productHierarchyId").is(pH)
											.and("CustomFields.parentID").is(parentId).and("CustomFields.ParentChild")
											.is("Yes"));
									Product parentProd = mongoTemplate.findOne(query, Product.class, "products");
									String parentProdJson = new Gson().toJson(parentProd);
									DocumentContext parentProd_docCtx = JsonPath.parse(parentProdJson);
									try {
										Object parentIsbn = parentProd_docCtx.read(
												"$.Product.ProductIdentifier[?(@.ProductIDType == '15')].IDValue");
										substituteData.put("ParentISBN", parentIsbn.toString().replace("[", "")
												.replace("]", "").replace("\"", ""));
									} catch (Exception e) {
										logger.warn("parent ISBN not found");
									}
								}
								String calculatedValue = StrSubstitutor.replace(currentLogic.getStaticValue(),
										substituteData, "${", "}");
								if (metaField.getJsonType().equalsIgnoreCase("Single"))
									processForSingleType(metaField, docCtx, calculatedValue);
								else {
									processJsonDbPathCondition(metaField, calculatedValue, docCtx);

								}

							} catch (Exception e) {
								System.err.println(e.getMessage());
							}
							break;
						case "CUSTOM_FIELD":
							try {
								String finalVal = "";
								MetaDataFieldsEntity metadataFieldEntityObj1 = null;
								for (CMetaFieldLogicCustomFieldsString cfs : currentLogic.getCustomFieldsString()) {
									metadataFieldEntityObj1 = fieldsMap.get(cfs.getMetaDataFieldName());

									String mPh = metadataFieldEntityObj1.getProductHierarchyId();
									Product product = new Gson().fromJson(new Gson().toJson(pro.get(mPh)),
											Product.class);
									if (product == null) {
										break;
									}
									String temp_prod_mPh = new Gson().toJson(product);
									DocumentContext temp_docCtx_req_fields = JsonPath.parse(temp_prod_mPh);
									Object val = temp_docCtx_req_fields.read(metadataFieldEntityObj1.getJsonPath());

									switch (cfs.getOperation().toUpperCase()) {
									case "MID": {
										if (val instanceof Collection<?>)
											val = ((List<?>) val).stream().map(e -> String.valueOf(e))
													.collect(Collectors.joining(""));

										System.out.println(val.toString());
										System.out.println(
												val.toString().substring(cfs.getStartIndex(), cfs.getEndIndex()));
										if (null != val && val.toString().trim().length() > 0) {
											finalVal = finalVal
													+ val.toString().substring(cfs.getStartIndex(), cfs.getEndIndex());
											logger.info("Value after MID: " + finalVal);
										}
									}
										break;

									case "RIGHT": {
										// val = docCtx.read(metadataFieldEntityObj1.getJsonPath());
										if (val instanceof Collection<?>)
											val = ((List<?>) val).stream().map(e -> String.valueOf(e))
													.collect(Collectors.joining(""));

										System.out.println(val.toString()
												.substring(cfs.getCharacters() > val.toString().length() ? 0
														: (val.toString().length() - cfs.getCharacters())));
										if (null != val && val.toString().trim().length() > 0) {
											finalVal = finalVal + val.toString()
													.substring(cfs.getCharacters() > val.toString().length() ? 0
															: (val.toString().length() - cfs.getCharacters()));
											logger.info("Value after RIGHT: " + finalVal);
										}

									}
										break;

									case "LEFT": {
										// val = docCtx.read(metadataFieldEntityObj1.getJsonPath());
										if (val instanceof Collection<?>)
											val = ((List<?>) val).stream().map(e -> String.valueOf(e))
													.collect(Collectors.joining(""));

										System.out.println(val.toString().substring(0,
												val.toString().length() > cfs.getEndIndex() ? cfs.getEndIndex()
														: val.toString().length()));
										if (null != val && val.toString().trim().length() > 0) {
											finalVal = finalVal + val.toString().substring(0,
													val.toString().length() > cfs.getEndIndex() ? cfs.getEndIndex()
															: val.toString().length());
											logger.info("Value after LEFT: " + finalVal);
										}
									}
										break;

									case "VALUE": {

										finalVal = finalVal + cfs.getSubstituteText();
										logger.info("Value after subtitute: " + finalVal);

									}
										break;

									case "REVERSE": {
										// val = docCtx.read(metadataFieldEntityObj1.getJsonPath());
										if (val instanceof Collection<?>)
											val = ((List<?>) val).stream().map(e -> String.valueOf(e))
													.collect(Collectors.joining(""));

										System.out.println(new StringBuilder(val.toString()).reverse().toString());

										if (null != val && val.toString().trim().length() > 0) {
											finalVal = finalVal
													+ new StringBuilder(val.toString()).reverse().toString();
											logger.info("Value after REVERSE: " + finalVal);
										}
									}
										break;
									}
								}

								if (finalVal.trim().length() > 0) {
									processJsonDbPathCondition(metaField, finalVal, docCtx);
								}
							} catch (Exception e) {
								System.out.println(e.getMessage());
							}
							break;

						case "GET_MONTH":

							try {
								String strdate = substituteData.get(currentLogic.getFieldsRequired().get(0));
								Date mydate = format2.parse(strdate);
								// Date date =
								// format1.parse(substituteData.get(currentLogic.getFieldsRequired().get(0)));
								if (metaField.getJsonType().equalsIgnoreCase("Single")) {
									String value = format1.format(mydate).substring(4, 6);
									processForSingleType(metaField, docCtx, value);
								} else
									processJsonDbPathCondition(metaField, format1.format(mydate).substring(5, 7),
											docCtx);

							} catch (Exception e) {
								logger.info("GET_MONTH ", e);
								continue;
							}
							break;

						case "GET_YEAR":

							try {
								String strdate = substituteData.get(currentLogic.getFieldsRequired().get(0));
								Date mydate = format2.parse(strdate);
								// Date date =
								// format1.parse(substituteData.get(currentLogic.getFieldsRequired().get(0)));
								if (metaField.getJsonType().equalsIgnoreCase("Single")) {
									String value = format1.format(mydate).substring(0, 4);
									processForSingleType(metaField, docCtx, value);
								} else
									processJsonDbPathCondition(metaField, format1.format(mydate).substring(0, 4),
											docCtx);
							} catch (Exception e) {
								logger.info("GET_YEAR " + e.getMessage());
								continue;
							}

							break;

						case "GET_DAY":

							try {
								String strdate = substituteData.get(currentLogic.getFieldsRequired().get(0));
								Date mydate = new Date(strdate);
								// Date date =
								// format1.parse(substituteData.get(currentLogic.getFieldsRequired().get(0)));
								if (metaField.getJsonType().equalsIgnoreCase("Single")) {
									String value = format1.format(mydate).substring(7, 9);
									processForSingleType(metaField, docCtx, value);
								} else
									processJsonDbPathCondition(metaField, format1.format(mydate).substring(0, 4),
											docCtx);
							} catch (Exception e) {
								logger.info("GET_DAY " + e.getMessage());
								continue;
							}

							break;

						case "CUSTOM_LOGIC_FOR_VOLUMENO":

							try {
								String[] arrOfStr = substituteData.get(currentLogic.getFieldsRequired().get(0))
										.split("/");
								if (arrOfStr[1].length() == 2) {
									int docNumVal = Integer.parseInt(arrOfStr[1]);
									System.out.println(new Date().toString().split(" ")[5]);
									// int curryearVal = Integer
									// .parseInt((new Date().toString().split(" ")[5].substring(2, 4)));
									if (docNumVal <= 70) {
										String value = (new Date().toString().split(" ")[5].substring(0, 2)
												+ arrOfStr[1]);
										if (metaField.getJsonType().equalsIgnoreCase("Single")) {
											processForSingleType(metaField, docCtx, value);
										} else
											processJsonDbPathCondition(metaField, value, docCtx);
									} else {
										String value = ((Integer
												.parseInt(new Date().toString().split(" ")[5].substring(0, 2)) - 1)
												+ arrOfStr[1]);
										if (metaField.getJsonType().equalsIgnoreCase("Single")) {
											processForSingleType(metaField, docCtx, value);
										} else
											processJsonDbPathCondition(metaField, value, docCtx);
									}
								}
							} catch (Exception e) {
								logger.info("CUSTOM_LOGIC_FOR_VOLUMENO " + e.getMessage());
								continue;
							}
							break;

						case "CUSTOM_LOGIC_FOR_ISSUENO":

							try {
								String[] arrOfStr = substituteData.get(currentLogic.getFieldsRequired().get(0))
										.split("/");
								if (metaField.getJsonType().equalsIgnoreCase("Single"))
									processForSingleType(metaField, docCtx, arrOfStr[2]);
								else
									processJsonDbPathCondition(metaField, arrOfStr[2], docCtx);

							} catch (Exception e) {
								logger.info("CUSTOM_LOGIC_FOR_ISSUENO " + e.getMessage());
								continue;
							}
							break;

						case "CUSTOM_LOGIC_FOR_REPEC_ID":

							try {
								String fieldStr = substituteData.get(currentLogic.getFieldsRequired().get(0));// .split("\\.|\\/");
								String finalStr = null;
								if (fieldStr.contains(".")) {
									finalStr = fieldStr.substring(fieldStr.lastIndexOf(".") + 1);
								} else {
									finalStr = fieldStr.substring(fieldStr.indexOf("/") + 1);
								}
								if (finalStr != null) {
									if (metaField.getJsonType().equalsIgnoreCase("Single")) {
										String value = currentLogic.getStaticValue() + finalStr;
										processForSingleType(metaField, docCtx, value);
									} else
										processJsonDbPathCondition(metaField, currentLogic.getStaticValue() + finalStr,
												docCtx);
								}
							} catch (Exception e) {
								logger.info("CUSTOM_LOGIC_FOR_REPEC_ID " + e.getMessage());
								continue;
							}
							break;

						case "CUSTOM_LOGIC_FOR_PRICE":

							try {

								Object[] myparams = substituteData.values().toArray();
								List<Map> resultData = jongo.runCommand(currentLogic.getDbQuery(), myparams)
										.throwOnError().field("cursor").as(Map.class);

								Map<String, Object> fieldData = (Map<String, Object>) resultData.get(0)
										.get("PriceDetails");

								System.out.println(fieldData.toString());

								for (Map.Entry<String, Object> entry : fieldData.entrySet()) {
									System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
									metaField = fieldsMap.get(entry.getKey());
									if (metaField.getJsonType().equalsIgnoreCase("Single"))
										processForSingleType(metaField, docCtx, (String) entry.getValue());
									else
										processJsonDbPathCondition(metaField, entry.getValue(), docCtx);
								}

							} catch (Exception e) {
								logger.warn("CUSTOM_LOGIC_FOR_PRICE " + e.getMessage());
								continue;
							}
							break;

						case "ARITHMETIC":
							try {

								String calculatedValue = StrSubstitutor.replace(currentLogic.getStaticValue(),
										substituteData, "${", "}");

								ScriptEngine engine = new ScriptEngineManager().getEngineByExtension("js");

								Object result = engine.eval(calculatedValue.trim().replaceAll("%", "/100"));
								String value = String.valueOf(result);
								if (metaField.getJsonType().equalsIgnoreCase("Single"))
									processForSingleType(metaField, docCtx, value);
								else {
									processJsonDbPathCondition(metaField, value, docCtx);
								}

							} catch (Exception e) {
								// Don't do anything
								logger.info("Error in ARITHMETIC transformation");
								e.printStackTrace();
							}

							break;
						default:
							break;
						}
					}

				}

			}

		}
		Product fs = new Gson().fromJson(StringEscapeUtils.unescapeJava(docCtx.jsonString()), Product.class);
		return fs;
	}

	public void processForSingleType(MetaDataFieldsEntity metaField, DocumentContext docCtx, String value) {
		String refPath = "$." + metaField.getReferencePath();
		String jsonPath = metaField.getJsonPath();

		if (!refPath.equals(jsonPath)) {
			String mypath = jsonPath.substring(0, jsonPath.lastIndexOf("."));
			try {
				docCtx.read(mypath);
				if (docCtx.read(mypath) == null) {
					docCtx.set(mypath, new HashMap<>());
				}
			} catch (Exception e) {
				docCtx.put("$", mypath.substring(mypath.lastIndexOf(".") + 1), new HashMap<>());
			}
			docCtx = docCtx.put(mypath, refPath.substring(refPath.lastIndexOf(".") + 1), value);
		} else {
			String mypath = refPath.substring(0, refPath.lastIndexOf("."));
			try {
				docCtx.read(mypath);
				if (docCtx.read(mypath) == null) {
					docCtx.set(mypath, new HashMap<>());
				}
			} catch (Exception e) {
				docCtx.put("$", mypath.substring(mypath.lastIndexOf(".") + 1), new HashMap<>());
			}
			docCtx = docCtx.put(mypath, refPath.substring(refPath.lastIndexOf(".") + 1), value);
		}
	}

	public void processJsonDbPathCondition(MetaDataFieldsEntity metaField, Object calculatedValue,
			DocumentContext docCtx) {
		if (metaField.getJsonDbPathCondition() != null && metaField.getJsonDbPathCondition().size() > 0) {
			LinkedHashMap<String, Object> insertObj = new LinkedHashMap<>();
			for (JsonDbPathConditionEntity currentCondition : metaField.getJsonDbPathCondition()) {
				if (currentCondition.getCondition().equals("=="))
					insertObj.put(currentCondition.getField(), currentCondition.getValue());
			}
			insertObj.put(metaField.getReferencePath().substring(metaField.getReferencePath().lastIndexOf('.') + 1,
					metaField.getReferencePath().length()), calculatedValue);
			String refPath = "$."
					+ metaField.getReferencePath().substring(0, metaField.getReferencePath().lastIndexOf('.'));
			String refPathTemp = refPath;

			try {
				int refLength = refPath.split("\\.").length;
				String newRefPath = "";
				if (refLength == 4) {
					newRefPath = refPath.substring(0, refPath.lastIndexOf(".")) + "[0]."
							+ refPath.substring(refPath.lastIndexOf(".") + 1);
					refPath = newRefPath;
				}
				docCtx.read(refPath);
				Object val = docCtx.read(metaField.getJsonPath());
				if (val != null && !val.toString().equals("[]")) {
					docCtx.put(metaField.getJsonDbPathTransform(), metaField.getJsonDbPathElement(), calculatedValue);
				} else {
					docCtx.add(refPath, insertObj);
				}
			} catch (PathNotFoundException pnfe) {
				refPath = refPathTemp;
				logger.warn("processJsonDbPathCondition " + pnfe.getMessage());
				List<Object> insertList1 = new ArrayList<>();
				insertList1.add(insertObj);

				String[] keys = refPath.split("\\.");
				if (keys.length == 3) {
					docCtx.put("$." + keys[1], keys[2], insertList1);
				}
				if (keys.length == 4) {
					String rf = "$";
					String tempParent = rf;
					for (int i = 1; i < 4; i++) {
						try {
							tempParent = rf;
							if (docCtx.read(tempParent) instanceof ArrayList) {
								rf = rf + "[0]." + keys[i];
							} else {
								rf = rf + "." + keys[i];
							}
							docCtx.read(rf);
						} catch (PathNotFoundException e) {
							if (docCtx.read(tempParent) instanceof Map) {
								docCtx = docCtx.put(tempParent, keys[i], new ArrayList<>());
							}
							if (docCtx.read(tempParent) instanceof List) {
								Map<String, Object> mymap = new HashMap<>();
								mymap.put(keys[i], new ArrayList<>());
								docCtx = docCtx.add(tempParent, mymap);
							}
						}
					}

					docCtx.add(rf, insertList1.get(0));
				}
			}
		}
	}

	/**
	 * List all the products available in database return APIResponse with list of
	 * products
	 * 
	 * @author Pavan Kumar Yekabote
	 */
	@Override
	public APIResponse<Object> getProducts() {

		APIResponse<Object> response = new APIResponse<>();
		List<ProductMapEntity> products = metaDataDao.getProducts();

		if (products != null && products.size() > 0) {
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setStatus(success);
			response.setData(products);
			response.setStatusMessage(successRetrieval);
		} else {
			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatus(failure);
			response.setData(products);
			response.setStatusMessage(failureRetrieval);
		}
		return response;
	}

	/**
	 * Find current id product, and fetch its parent, siblings, children
	 * 
	 * @param id
	 *            => product id
	 * @return {@link APIResponse} with Map ( current, parent, siblings, children,
	 *         list of products )
	 * @author Pavan Kumar Yekabote
	 */
	@Override
	public APIResponse<Object> getProductWithRelations(String id, String pHid, String loggedUser) {

		APIResponse<Object> response = new APIResponse<>();
		Map<Object, Object> productWithRelations = metaDataDao.getProductWithRelations(id, pHid, loggedUser);

		if (productWithRelations != null) {
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setData(productWithRelations);
			response.setStatus("success");
			response.setStatusMessage("Product with all relations has been fetched successfully");
		} else {
			response.setCode(APIResponse.ResponseCode.NO_RECORDS_FOUND.toString());
			response.setData(null);
			response.setStatus(failure);
			response.setStatusMessage(failureRetrieval);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public APIResponse<Object> updateProductWithSync(Product product, String userId) {
		APIResponse<Object> response = new APIResponse<>();
		Map<Object, Object> viewResult = new HashMap<Object, Object>();
		// Check if this product is locked.
		LockType lockType = metaDataDao.isProductLocked(product.getMetadataId(),
				product.getCustomFields().get("productHierarchyId").toString(), userId);
		if (lockType == LockType.LOCKED) {
			response.setCode(APIResponse.ResponseCode.ERROR.toString());
			response.setStatusMessage("This product can't be updated, since it is locked.");
			return response;
		}

		// Since product metadata in Product class is a PoJo...
		// Convert it to form as a Map, so that it can be set in ProductMapEntity DB
		// class
		Gson gson = new Gson();

		// For pH - autoGenField
		Query qry = new Query();

		qry.addCriteria(Criteria.where("_id").is(product.getCustomFields().get("productHierarchyId")));
		ProductHierarchyEntity pHEntity = mongoTemplate.findOne(qry, ProductHierarchyEntity.class);

		// Converting to JSON String again to set Titletext and UID
		String newproJson = new Gson().toJson(product);
		DocumentContext newDoc = JsonPath.parse(newproJson);

		String uid = "", titleText = "";
		try {
			// Unique Identifier from UID path
			uid = newDoc.read(pHEntity.getUidPath()).toString();
			uid = uid.replace("[", "").replace("]", "").replace("\"", "");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			// Unique Identifier from UID path
			titleText = newDoc.read(pHEntity.getTitlePath()).toString();
			titleText = titleText.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
		} catch (Exception e) {
			// TODO: handle exception
		}

		product.getCustomFields().put("Titletext", titleText);
		product.getCustomFields().put("UID", uid);
		product.getCustomFields().put("productHierarchyName", pHEntity.getProductHierarchyName());

		// Set Parents Block in CustomField - Chella
		List<ProductMapEntity> parentList = new ArrayList<ProductMapEntity>();

		Query q1 = new Query();
		q1.addCriteria(Criteria.where("metadataId").is(product.getCustomFields().get("parentID"))
				.and("CustomFields.productHierarchyId").is(pHEntity.getParent_id()));
		ProductMapEntity parentProduct = (ProductMapEntity) mongoTemplate.findOne(q1, ProductMapEntity.class);

		// getting product from DB
		ProductMapEntity pDBmap = metaDataDao.getProduct(product.getMetadataId(),
				product.getCustomFields().get("productHierarchyId"));
		String json = new Gson().toJson(pDBmap);

		ProductMapEntity editedPhIdDBProduct = new Gson().fromJson(json, ProductMapEntity.class);// ProductMapEntityToVo(pDBmap);

		editedPhIdDBProduct.getCustomFields().remove("parents");// removing parents because we need to capture current
																// level
		ProductMapEntity editedPhIdInputProduct = gson.fromJson(newproJson, ProductMapEntity.class);
		List<CDCMetaDataDetails> cdcMetaDataDetails = cdcCall(editedPhIdInputProduct, editedPhIdDBProduct);// calling
																											// cdc
																											// before
		// sync

		saveCdcMetadata(cdcMetaDataDetails, userId, product.getMetadataId(),
				product.getCustomFields().get("productHierarchyId"), titleText, "Update", false);

		if (parentProduct != null) {
			// referenceParentID=parentProduct.getCustomFields().get("referenceParentID").toString();

			while (parentProduct != null) {
				parentProduct.getCustomFields().remove("parents");
				parentList.add(parentProduct);
				// Fetch next parent
				q1 = new Query();
				String parentId = (String) parentProduct.getCustomFields().get("parentID");
				if (parentId == null || (parentId != null && parentId.trim().equals(""))) {
					parentProduct = null;
				} else {
					Query query = new Query();
					query.addCriteria(
							Criteria.where("_id").is(parentProduct.getCustomFields().get("productHierarchyId")));
					ProductHierarchyEntity parentpHId = mongoTemplate.findOne(query, ProductHierarchyEntity.class);

					q1.addCriteria(Criteria.where("metadataId").is(parentId).and("CustomFields.productHierarchyId")
							.is(parentpHId.getParent_id()));
					parentProduct = mongoTemplate.findOne(q1, ProductMapEntity.class);
				}

			}
		}

		String productJson = gson.toJson(product.getProduct());

		LinkedHashMap<Object, Object> productMap = gson.fromJson(productJson, LinkedHashMap.class);
		List<ProductMapEntity> completeParentList = getCompleteParents(parentList);

		// Set product object's data to ProductMapEntity object pMap to write in DB
		ProductMapEntity pMap = new ProductMapEntity();
		pMap.setProduct(productMap);
		pMap.setCustomFields(product.getCustomFields());
		pMap.setMetadataId(product.getMetadataId());
		boolean checkForMandatoryUniqueFields = this.metaDataDao.validateMandatoryUniqueFields(pMap);
		if (!checkForMandatoryUniqueFields) {
			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatusMessage("Mandatory field values seem to be duplicated, Create failed.");
			response.setStatus(failure);
			return response;
		}
		boolean isUpdated = false, isValid = this.metaDataDao.validateFieldsV2(pMap);
		if (isValid) {
			Map<Object, Object> phAndProds = getParentsAndChildren(pMap, pHEntity, completeParentList);

			ProductMapEntity finalpMap = performFieldLogics(phAndProds, parentList, pMap, userId);
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(product.getCustomFields().get("productHierarchyId")));
			ProductHierarchyEntity editedpHId = mongoTemplate.findOne(query, ProductHierarchyEntity.class);

			if (editedpHId.getSyncField() != null) {
				String syncField = editedpHId.getSyncField().substring(editedpHId.getSyncField().lastIndexOf(".") + 1);
				if (product.getCustomFields().get(syncField).equals("Yes")) {
					ProductMapEntity coditionalSync = metaDataDao.getMasterSiblingData(
							product.getCustomFields().get("referenceParentID"),
							product.getCustomFields().get("productHierarchyId"), syncField);
					coditionalSync.getCustomFields().put(syncField, "No");
					if (!coditionalSync.getMetadataId().equals(product.getMetadataId()))
						isUpdated = metaDataDao.updateProductsWithSync(coditionalSync);

				}
			}
			try {
				if (pHEntity.isTagCreation()) {
					Query q2 = new Query();
					q2.addCriteria(Criteria.where("userId").is(userId));
					UserEntity usr = mongoTemplate.findOne(q2, UserEntity.class);
					String userName = usr.getFirstName();
					finalpMap.getCustomFields().put("updatedBy", userName);
					finalpMap.getCustomFields().put("updatedOn",
							new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
					finalpMap.getCustomFields().put("updatedOnISO", new Date());
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			isUpdated = metaDataDao.updateProductsWithSync(finalpMap);

			if (isUpdated) {

				storeInProductStagesApprovalNew(pMap.getCustomFields().get("productHierarchyId").toString(), userId,
						finalpMap, true, pDBmap);

				Query forId = new Query();
				forId.addCriteria(Criteria.where("metadataId").is(finalpMap.getMetadataId()));
				ProductEntity prod = mongoTemplate.findOne(forId, ProductEntity.class, "products");
				finalpMap.setId(prod.getId());

				viewResult.put("customFieldsTitleTextValue", finalpMap.getCustomFields().get("Titletext"));
				viewResult.put("UID", finalpMap.getCustomFields().get("UID"));
				viewResult.put("productHierarchyName", finalpMap.getCustomFields().get("productHierarchyName"));
				viewResult.put("productHierarchyId", finalpMap.getCustomFields().get("productHierarchyId"));
				viewResult.put("metadataId", finalpMap.getMetadataId());

				// ELASTIC SEARCH INDEXING
				boolean status = insertForIndexing(finalpMap, true);
				Query q2 = new Query();
				Update up = new Update();
				q2.addCriteria(Criteria.where("metadataId").is(finalpMap.getMetadataId()));
				if (!status) {
					up.set("CustomFields.isIndexed", false);
					mongoTemplate.updateFirst(q2, up, "products");
					logger.info("ELASTIC SEARCH INDEXING -> Failed indexing the product");
				} else {
					up.set("CustomFields.isIndexed", true);
					mongoTemplate.updateFirst(q2, up, "products");
				}

				List<CDCMetaDataDetails> parentcdcMetaDataDetails = null;
				cdcMetaDataDetails = new ArrayList<CDCMetaDataDetails>();
				List<ProductMapEntity> dbParentMapList = getCustomfieldsParents(pDBmap);// (List<ProductMapEntity>)
																						// pDBmap.getCustomFields()
				// .get("parents");
				List<ProductMapEntity> inputParentMapList = (List<ProductMapEntity>) finalpMap.getCustomFields()
						.get("parents");
				// calculating cdc after sync
				if (dbParentMapList != null && inputParentMapList != null) {
					for (ProductMapEntity inputProduct : inputParentMapList) {
						for (ProductMapEntity dbProduct : dbParentMapList) {
							if (inputProduct != null && dbProduct != null
									&& inputProduct.getCustomFields().get("productHierarchyId")
											.equals(dbProduct.getCustomFields().get("productHierarchyId")))
								parentcdcMetaDataDetails = cdcCall(inputProduct, dbProduct);

						}
						if (parentcdcMetaDataDetails != null)
							cdcMetaDataDetails.addAll(parentcdcMetaDataDetails);
					}

				}
				finalpMap.getCustomFields().remove("parents");
				// editedPhIdInputProduct = gson.fromJson(newproJson, ProductMapEntity.class);
				parentcdcMetaDataDetails = cdcCall(finalpMap, editedPhIdDBProduct);
				if (cdcMetaDataDetails != null)
					cdcMetaDataDetails.addAll(parentcdcMetaDataDetails);

				saveCdcMetadata(cdcMetaDataDetails, userId, product.getMetadataId(),
						product.getCustomFields().get("productHierarchyId"), titleText, "Update", true);

			}

			String referenceParentId = (String) product.getCustomFields().get("referenceParentID");
			if (referenceParentId != null)
				metaDataDao.removeLockOnProduct(userId, referenceParentId);
			if (isUpdated) {
				response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
				response.setData(viewResult);
				response.setStatusMessage("Success!! Your Data has been Stored");
			} else {
				response.setCode(APIResponse.ResponseCode.FAILURE.toString());
				response.setData(null);
				response.setStatusMessage("Error Occurred!! while Storing Data");
			}
		} else {
			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatusMessage("Field values seem to be duplicated, Update failed.");
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	private ProductMapEntity performFieldLogics(Map<Object, Object> phAndProds, List<ProductMapEntity> parentList,
			ProductMapEntity pMap, String loggedUser) {
		Gson gson = new Gson();
		// Fetching all the metadata and creating it's hashmap
		List<MetaDataFieldsEntity> metaDataFieldList = null;
		Query query = new Query();
		query.addCriteria(Criteria.where("isActive").is(true).and("isDeleted").is(false));
		metaDataFieldList = mongoTemplate.find(query, MetaDataFieldsEntity.class, "cMetadataFields");

		Map<String, MetaDataFieldsEntity> fieldsMap = metaDataFieldList.stream()
				.filter(enitity -> enitity.getMetaDataFieldName() != null)
				.collect(Collectors.toMap(book -> book.getMetaDataFieldName(), book -> book, (k, v) -> {
					return k;
				}));

		String currProdHierarchy = pMap.getCustomFields().get("productHierarchyId").toString();

		Map<String, ProductMapEntity> afterFieldLogic = new HashMap<>();
		List<String> processedHierarchies = new ArrayList<>();

		for (Entry<Object, Object> phAndProd : phAndProds.entrySet()) {
			String prodJson = gson.toJson(phAndProd.getValue());
			Product prod = fieldLogicManipulations(phAndProds, prodJson, phAndProd.getKey().toString(), true,
					metaDataFieldList, fieldsMap);
			String prJson = new Gson().toJson(prod);
			ProductMapEntity p = new Gson().fromJson(StringEscapeUtils.unescapeJava(prJson), ProductMapEntity.class);
			afterFieldLogic.put(phAndProd.getKey().toString(), p);
		}
		List<ProductMapEntity> updatedParentList = new ArrayList<>();
		for (ProductMapEntity parent : parentList) {
			String parentHierarchy = parent.getCustomFields().get("productHierarchyId").toString();
			ProductMapEntity afterLogicProduct = afterFieldLogic.get(parentHierarchy);
			afterLogicProduct.setId(parent.getId());
			afterLogicProduct.getProduct().put("LastModifiedOn", new Date());
			Date lockedDate = getLockedDate(afterLogicProduct.getCustomFields());
			if (lockedDate != null) {
				afterLogicProduct.getCustomFields().put("lockedOn", lockedDate);
			}
			if (parentHierarchy.equals("PH0001")) {
				Query q1 = new Query();
				q1.addCriteria(Criteria.where("userId").is(loggedUser));
				UserEntity usr = mongoTemplate.findOne(q1, UserEntity.class);
				String userName = usr.getFirstName();
				afterLogicProduct.getCustomFields().put("updatedBy", userName);
				afterLogicProduct.getCustomFields().put("updatedOn",
						new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
				afterLogicProduct.getCustomFields().put("updatedOnISO", new Date());

			}
			updatedParentList.add(afterLogicProduct);
			try {
				Query q = new Query();
				Update update = new Update();
				q.addCriteria(Criteria.where("metadataId").is(parent.getMetadataId()));
				update.set("Product", afterLogicProduct.getProduct()).set("CustomFields",
						afterLogicProduct.getCustomFields());
				mongoTemplate.updateFirst(q, update, "products");

				// ELASTIC SEARCH INDEXING
				boolean status = insertForIndexing(afterLogicProduct, true);
				Query q1 = new Query();
				Update up = new Update();
				q1.addCriteria(Criteria.where("metadataId").is(afterLogicProduct.getMetadataId()));
				if (!status) {
					up.set("CustomFields.isIndexed", false);
					mongoTemplate.updateFirst(q1, up, "products");
					logger.info("ELASTIC SEARCH INDEXING -> Failed indexing the product");
				} else {
					up.set("CustomFields.isIndexed", true);
					mongoTemplate.updateFirst(q1, up, "products");
				}

			} catch (Exception e) {
			}
			processedHierarchies.add(parentHierarchy);
		}

		for (ProductMapEntity parent : updatedParentList) {
			try {
				parent.getCustomFields().remove("parents");
			} catch (Exception e) {
			}
		}

		Collections.reverse(updatedParentList);

		ProductMapEntity currProd = afterFieldLogic.get(currProdHierarchy);
		currProd.getCustomFields().put("parents", updatedParentList);

		Date lockedDate = getLockedDate(currProd.getCustomFields());
		if (lockedDate != null) {
			currProd.getCustomFields().put("lockedOn", lockedDate);
		}

		String prodJson = gson.toJson(currProd.getProduct());

		LinkedHashMap<Object, Object> prodMap = gson.fromJson(prodJson, LinkedHashMap.class);

		// Set modified date manually, since this should not be taken from UI or any
		// other source
		// product.getProduct().setLastModifiedOn(new Date());

		prodMap.put("LastModifiedOn", new Date());

		// Set product object's data to ProductMapEntity object pMap to write in DB
		ProductMapEntity finalpMap = new ProductMapEntity();
		finalpMap.setProduct(prodMap);
		finalpMap.setCustomFields(currProd.getCustomFields());
		finalpMap.setMetadataId(currProd.getMetadataId());
		processedHierarchies.add(currProdHierarchy);

		for (Entry<String, ProductMapEntity> afterLogicProd : afterFieldLogic.entrySet()) {
			if (processedHierarchies.contains(afterLogicProd.getKey())) {
				continue;
			}
			ProductMapEntity child = afterLogicProd.getValue();
			child.getProduct().put("LastModifiedOn", new Date());
			Date lockedDate1 = getLockedDate(child.getCustomFields());
			if (lockedDate1 != null) {
				child.getCustomFields().put("lockedOn", lockedDate1);
			}

			Query forId = new Query();
			forId.addCriteria(Criteria.where("metadataId").is(child.getMetadataId()));
			ProductEntity prod = mongoTemplate.findOne(forId, ProductEntity.class, "products");
			child.setId(prod.getId());

			try {
				Query q = new Query();
				Update update = new Update();
				q.addCriteria(Criteria.where("metadataId").is(child.getMetadataId()));
				update.set("Product", child.getProduct()).set("CustomFields", child.getCustomFields());
				mongoTemplate.updateFirst(q, update, "products");

				// ELASTIC SEARCH INDEXING
				boolean status = insertForIndexing(child, true);
				Query q1 = new Query();
				Update up = new Update();
				q1.addCriteria(Criteria.where("metadataId").is(child.getMetadataId()));
				if (!status) {
					up.set("CustomFields.isIndexed", false);
					mongoTemplate.updateFirst(q1, up, "products");
					logger.info("ELASTIC SEARCH INDEXING -> Failed indexing the product");
				} else {
					up.set("CustomFields.isIndexed", true);
					mongoTemplate.updateFirst(q1, up, "products");
				}

			} catch (Exception e) {
			}
		}
		return finalpMap;
	}

	@SuppressWarnings("unchecked")
	private List<ProductMapEntity> getCustomfieldsParents(ProductMapEntity product) {
		List<ProductMapEntity> finalProd = new LinkedList<>();
		try {
			List<Map<Object, Object>> parentList = (List<Map<Object, Object>>) product.getCustomFields().get("parents");
			for (Map<Object, Object> parent : parentList) {
				String json = new Gson().toJson(parent);
				finalProd.add(new Gson().fromJson(json, ProductMapEntity.class));

			}

		} catch (Exception e) {
			logger.error("error in getCustomfieldsParents :" + e.getMessage());
		}
		return finalProd;

	}

	@SuppressWarnings("deprecation")
	private Date getLockedDate(Map<Object, Object> customFields) {
		try {
			if (customFields.containsKey("lockedOn")) {
				String lockedDateStr = customFields.get("lockedOn").toString();
				Date lockedDate = new Date(lockedDateStr);
				return lockedDate;
			}
		} catch (Exception e) {
		}
		return null;
	}

	public void saveCdcMetadata(List<CDCMetaDataDetails> cdcMetaDataDetails, String userId, String metadataId,
			Object phId, String title, String updateType, boolean isSynced) {
		boolean isSaved = false;

		CDCMetaData cdcMetaData = new CDCMetaData();
		cdcMetaData.setCdcMetaDataDetails(cdcMetaDataDetails);
		cdcMetaData.setReferenceId(metadataId);
		cdcMetaData.setProductHierarchyId(phId);
		cdcMetaData.setTitle(title);
		cdcMetaData.setSynced(isSynced);
		cdcMetaData.setUpdateType(updateType);
		cdcMetaData.setUpdatedSource("UI");
		cdcMetaData.setLastModifiedBy(userId);
		cdcMetaData.setLastModifiedOn(new Date());
		isSaved = metaDataDao.saveCdcMetadata(cdcMetaData);
		if (isSaved)
			logger.info("Saved metadata details in tCDCMetadata collection");
	}

	public List<CDCMetaDataDetails> cdcCall(ProductMapEntity pInputMap, ProductMapEntity pDBmap) {
		// ProductMapEntity pDBmap =
		// metaDataDao.getProduct(product.getMetadataId(),product.getCustomFields().get("productHierarchyId"),false);
		String inputJson = new Gson().toJson(pInputMap);
		String dbJson = new Gson().toJson(pDBmap);

		/*
		 * logger.info("inputjson " + json); logger.info("dbjson " + dbJson);
		 */

		// Product inputProd = new Gson().fromJson(json, Product.class);

		// finalProd.setCustomFields(finalMap);
		ObjectMapper mapper = new ObjectMapper();
		DBObject dbObj = (DBObject) JSON.parse(inputJson);
		DBObject dbObj1 = (DBObject) JSON.parse(dbJson);
		JsonNode node1 = mapper.convertValue(dbObj, JsonNode.class);
		JsonNode node2 = mapper.convertValue(dbObj1, JsonNode.class);
		JsonNode patchNode = JsonDiff.asJson(node2, node1);

		logger.info("Difference in JSON " + patchNode.toString());

		List<CDCMetaDataDetails> cdcMetadataDetails = updateCDCMetaData(patchNode, inputJson, dbJson);

		return cdcMetadataDetails;

	}

	private List<ProductMapEntity> getCompleteParents(List<ProductMapEntity> parentList) {
		List<ProductMapEntity> cParents = new ArrayList<>();
		for (ProductMapEntity p : parentList) {
			Query qu = new Query();
			qu.addCriteria(Criteria.where("metadataId").is(p.getMetadataId()).and("CustomFields.productHierarchyId")
					.is(p.getCustomFields().get("productHierarchyId")));
			ProductMapEntity pro = mongoTemplate.findOne(qu, ProductMapEntity.class);
			cParents.add(pro);
		}

		return cParents;
	}

	private Map<Object, Object> getParentsAndChildren(ProductMapEntity product, ProductHierarchyEntity pHEntity,
			List<ProductMapEntity> parentList) {
		Map<Object, Object> result = new HashMap<>();
		String prodHierarchy = product.getCustomFields().get("productHierarchyId").toString();
		result.put(prodHierarchy, product);
		String metadataId = product.getMetadataId();

		Query query = new Query();
		List<Criteria> orCriteria = new ArrayList<>();
		orCriteria.add(Criteria.where("CustomFields.IsMaster").is("Yes"));
		orCriteria.add(Criteria.where("CustomFields.ParentChild").is("Yes"));

		List<Criteria> andCriteria = new ArrayList<>();
		andCriteria.add(Criteria.where("CustomFields.parents").elemMatch(
				Criteria.where("metadataId").is(metadataId).and("CustomFields.productHierarchyId").is(prodHierarchy)));

		Criteria c = new Criteria().orOperator(orCriteria.toArray(new Criteria[orCriteria.size()]));
		andCriteria.add(c);

		Criteria cs = new Criteria().andOperator(andCriteria.toArray(new Criteria[andCriteria.size()]));
		query.addCriteria(cs);

		List<ProductMapEntity> children = mongoTemplate.find(query, ProductMapEntity.class, "products");

		for (ProductMapEntity child : children) {
			result.put(child.getCustomFields().get("productHierarchyId"), child);
		}

		for (ProductMapEntity parent : parentList) {
			result.put(parent.getCustomFields().get("productHierarchyId"), parent);
		}

		return result;
	}

	@Scheduled(fixedRate = 3600000) // 3600000 - 1hrString metadataId,String pHid
	public void syncScheduler() {
		logger.info("Scheduler running");
		List<ProductMapEntity> productList = null;
		int updatedCount = 0;
		try {
			productList = metaDataDao.getLastModifiedProducts();
			if (productList != null && productList.size() > 0) {
				for (ProductMapEntity product : productList) {

					metaDataDao.updateToSyncProductInChildren(product);
					updatedCount++;
				}

				if (updatedCount == productList.size())
					logger.info(updatedCount + " products have been updated to their children");
			}

		} catch (Exception e) {
			logger.error("Error occured while fetching recent process...");
		}

		// For unlocking locked products back PRODUCT_LOCK_INTERVAL nHours
		long hoursInMillis = TimeUnit.MILLISECONDS.convert(PRODUCT_LOCK_INTERVAL, TimeUnit.HOURS);
		metaDataDao.removeLockOnProductsIn(hoursInMillis);
	}

	private List<CDCMetaDataDetails> updateCDCMetaData(JsonNode jsonNode, String inPutJson, String dBjson) {
		List<CDCMetaDataDetails> cdcMetadataDetails = null;
		try {
			cdcMetadataDetails = new ArrayList<>();

			// CDCMetaData cdcMetaData = new CDCMetaData();
			ObjectMapper mapper = new ObjectMapper();
			if (null != jsonNode) {
				Iterator<JsonNode> jsonIterator = jsonNode.elements();
				while (jsonIterator.hasNext()) {
					JsonNode fieldName = jsonIterator.next();
					JsonNode operation = fieldName.path("op");
					// JsonNode path = fieldName.path("path");
					String operationValue = operation.asText();

					String path = fieldName.path("path").asText();
					String field = null, fieldReferencePath = null;
					fieldReferencePath = path.replaceAll("[0-9]+", "").replace("[", "").replace("]", "").substring(2);
					Query query = new Query();
					if (operationValue.equalsIgnoreCase("add")) {
						query.addCriteria(Criteria.where("referencePath").regex(fieldReferencePath));
					} else {
						query.addCriteria(Criteria.where("referencePath").is(fieldReferencePath));

					}

					if (fieldReferencePath != null) {
						List<MetaDataFieldsEntity> fieldList = new ArrayList<>();
						fieldList = mongoTemplate.find(query, MetaDataFieldsEntity.class, "metaDataFieldConfig");
						if (fieldList != null && fieldList.size() > 0) {
							for (MetaDataFieldsEntity metaFields : fieldList) {
								field = null;

								System.out.println("jsonpathhhhh : " + metaFields.getJsonPath());
								DocumentContext inDocCtx = JsonPath.parse(inPutJson);
								DocumentContext dbDocCtx = JsonPath.parse(dBjson);
								Object obj = null;
								Object obj1 = null;
								JsonPath jsonPath = JsonPath.compile(metaFields.getJsonPath());

								if (operationValue.equalsIgnoreCase("add")) {
									try {

										obj = inDocCtx.read(jsonPath);
										logger.info("NewValue : " + obj + "");

										field = metaFields.getMetaDataFieldName();

									} catch (PathNotFoundException e) {
										// Don't do anything
										logger.error("Error in identifying FieldDisplayName ",
												metaFields.getMetaDataFieldName());
									}
								} else {
									try {
										obj = inDocCtx.read(jsonPath);
										obj1 = dbDocCtx.read(jsonPath);
										logger.info("NewValue : " + obj + "");
										logger.info("DBValue : " + obj1 + "");
										if (!obj.equals(obj1)) {
											field = metaFields.getMetaDataFieldName();
										}
									} catch (PathNotFoundException e) {
										// Don't do anything
										logger.error("Error in identifying FieldDisplayName ",
												metaFields.getMetaDataFieldName());
									}
								}

								if (field != null) {
									if (operationValue.equalsIgnoreCase("replace")) {
										if (!path.contains("isActive") && !path.contains("LastModifiedOn")
												&& !path.contains("asset") && !path.contains("Lock")
												&& !path.contains("$.Id") && !path.contains("lock")) {
											// Below will provide last index of . as field name
											/*
											 * if (null != path) { field = path.substring(path.lastIndexOf(".")); field
											 * = field.replace(".", ""); if (field != null && field.contains("[")) {
											 * field = field.substring(0, field.indexOf("[")); } }
											 */

											if (fieldName.path("value").isObject()
													|| fieldName.path("value").isArray()) {
												for (final JsonNode objNode : fieldName.path("value")) {
													if (null != objNode) {
														String val = objNode.toString();
														if (!"".equals(val) && val.length() > 0) {
															CDCMetaDataDetails cdcDetails = new CDCMetaDataDetails();
															cdcDetails.setFieldName(field);
															cdcDetails.setFieldPath(path);
															// cdcDetails.setOldValue(oldValue);
															cdcDetails.setNewValue(objNode.toString());
															cdcMetadataDetails.add(cdcDetails);
															break;
														}
													}
												}
											} else {
												String value = fieldName.path("value").asText();
												if (null != value && !"".equals(value) && value.length() > 0) {
													String oldValue = fieldName.path("oldValue").asText();
													String newValue = fieldName.path("newValue").asText();
													// CDCMetaData cdc = new CDCMetaData();
													CDCMetaDataDetails cdcDetails = new CDCMetaDataDetails();
													cdcDetails.setFieldName(field);
													cdcDetails.setFieldPath(path);
													cdcDetails.setOldValue(oldValue);
													cdcDetails.setNewValue(newValue);
													cdcMetadataDetails.add(cdcDetails);
												}
											}
										}
									} else if (operationValue.equalsIgnoreCase("add")) {
										if (!path.contains("isActive") && !path.contains("LastModifiedOn")
												&& !path.contains("asset") && !path.contains("Lock")
												&& !path.contains("$.Id") && !path.contains("lock")) {
											// Below will provide last index of . as field name
											/*
											 * if (null != path) { field = path.substring(path.lastIndexOf(".")); field
											 * = field.replace(".", ""); if (field != null && field.contains("[")) {
											 * field = field.substring(0, field.indexOf("[")); } }
											 */
											if (metaFields.getJsonDbPathList() != null) {
												JsonNode node = fieldName.get("value");

												Map<Object, Object> map = mapper.convertValue(node, Map.class);
												List<Object> values = new ArrayList<Object>(map.values());
												List<Object> keys = new ArrayList<Object>(map.keySet());
												int lastIndex = keys.size() - 1;
												String refVar = keys.get(lastIndex).toString();

												fieldList = fieldList.stream()
														.filter(f -> f.getReferencePath().contains(refVar))
														.collect(Collectors.toList());
												if (fieldList.size() > 1) {
													for (MetaDataFieldsEntity metaFieldsNew : fieldList) {

														String val = metaFieldsNew.getJsonDbPathCondition().stream()
																.filter(f -> f.getValue()
																		.equals(values.get(lastIndex - 1)))
																.map(m -> m.getField()).collect(Collectors.joining());
														if (val.equals(keys.get(lastIndex - 1))) {
															field = metaFieldsNew.getMetaDataFieldName();
															break;
														}

													}
												}
												if (!path.contains("isActive") && !path.contains("LastModifiedOn")
														&& !path.contains("asset") && !path.contains("Lock")
														&& !path.contains("$.Id") && !path.contains("lock")) {
													CDCMetaDataDetails cdcDetails = new CDCMetaDataDetails();
													cdcDetails.setFieldName(field);
													cdcDetails.setFieldPath(path + "." + refVar);
													// cdcDetails.setOldValue(oldValue);
													cdcDetails.setNewValue(map.get(refVar).toString());
													cdcMetadataDetails.add(cdcDetails);

													break;
												}

												System.out.println(map);

											}
											if (fieldName.path("value").isObject()
													|| fieldName.path("value").isArray()) {
												for (final JsonNode objNode : fieldName.path("value")) {
													if (null != objNode) {
														String val = objNode.toString();
														if (!"".equals(val) && val.length() > 0) {
															CDCMetaDataDetails cdcDetails = new CDCMetaDataDetails();
															cdcDetails.setFieldName(field);
															cdcDetails.setFieldPath(path);
															// cdcDetails.setOldValue(oldValue);
															cdcDetails.setNewValue(objNode.toString());
															cdcMetadataDetails.add(cdcDetails);
															break;
														}
													}
												}
											} else {
												String value = fieldName.path("value").asText();
												if (null != value && !"".equals(value) && value.length() > 0) {
													CDCMetaDataDetails cdcDetails = new CDCMetaDataDetails();
													cdcDetails.setFieldName(field);
													cdcDetails.setFieldPath(path);
													// cdcDetails.setOldValue(oldValue);
													cdcDetails.setNewValue(value);
													cdcMetadataDetails.add(cdcDetails);
												}
											}
										}
									} else if (operationValue.equalsIgnoreCase("remove")) {
										if (!path.contains("isActive") && !path.contains("LastModifiedOn")
												&& !path.contains("asset") && !path.contains("Lock")
												&& !path.contains("$.Id") && !path.contains("lock")) {
											//// Below will provide last index of . as field name
											/*
											 * if (null != path) { field = path.substring(path.lastIndexOf(".")); field
											 * = field.replace(".", ""); if (field != null && field.contains("[")) {
											 * field = field.substring(0, field.indexOf("[")); } }
											 */

											CDCMetaDataDetails cdcDetails = new CDCMetaDataDetails();
											cdcDetails.setFieldName(field);
											cdcDetails.setFieldPath(path);
											cdcMetadataDetails.add(cdcDetails);
											/*
											 * if (fieldName.path("value").isObject()) { for (final JsonNode objNode :
											 * fieldName.path("value")) { if(null != objNode ) { String val =
											 * objNode.toString(); if(!"".equals(val) && val.length() > 0) {
											 * CDCMetaDataDetails cdcDetails = new CDCMetaDataDetails();
											 * cdcDetails.setFieldName(field); cdcDetails.setFieldPath(path); //
											 * cdcDetails.setOldValue(oldValue);
											 * cdcDetails.setNewValue(objNode.toString());
											 * cdcMetadataDetails.add(cdcDetails); break; } } } } else { String value =
											 * fieldName.path("value").asText(); if(null != value && !"".equals(value)
											 * && value.length() > 0) { CDCMetaDataDetails cdcDetails = new
											 * CDCMetaDataDetails(); cdcDetails.setFieldName(field);
											 * cdcDetails.setFieldPath(path); // cdcDetails.setOldValue(oldValue);
											 * cdcDetails.setNewValue(value); cdcMetadataDetails.add(cdcDetails); } }
											 */ }
									}
								}

							}
						}
					}

				}
			}
			// cdcMetaData.setCdcMetaDataDetails(cdcMetadataDetails);
			// cdcMetaData.setReferenceId(MetadataId);
			// cdcMetaData.setProductHierarchyId(phId);
			// cdcMetaData.setUpdateType(updateType);
			// cdcMetaData.setSynced(isSynced);
			// /*
			// * cdcMetaData.setImprintName(imprintName);
			// * cdcMetaData.setCountryOfPublication(countryOfPublication);
			// * cdcMetaData.setProductCategory(productCategory);
			// */
			// cdcMetaData.setLastModifiedOn(currentDate);
			// if (null != userId) {
			// cdcMetaData.setLastModifiedBy(userId);
			// }
			// cdcMetaData.setUpdatedSource("UI");
			// cdcMetaData.setTitle(title);
			//
			// if (cdcMetadataDetails.size() > 0 || "Insert".equalsIgnoreCase(updateType)
			// || "update".equalsIgnoreCase(updateType)) {
			// logger.info("saving cdc details");
			// mongoTemplate.save(cdcMetaData, "tCDCMetaData");
			// logger.info("Saved metadata details in CDCMetadata collection");
			// }
		} catch (Exception e) {
			logger.error("Error while calculateing CDC Metadata " + e.getMessage());
		}
		return cdcMetadataDetails;
	}

	private Boolean getColumnFilterSuggestionsStatus(SupportRequestEntity e) {
		// aMp<String,Object> filterFields = e.getFilterByFields();
		List<FieldInfo> filterFields = e.getFilterByFields();
		String searchText = e.getSearchText();
		String selectedFieldName = e.getSelectedFieldName();
		if (selectedFieldName != null && (searchText != null && searchText.trim().length() >= 0)) {
			return true;
		}
		return false;
	}

	@Override
	public Map<String, Object> getSupportData(SupportRequestEntity entity) {

		String lookupName = entity.getLookupName();
		Query query = new Query();
		Criteria c1 = new Criteria();
		List<HeaderInfo> headerList = new LinkedList<HeaderInfo>();
		boolean isColumnFilter = getColumnFilterSuggestionsStatus(entity);
		long count1 = 0;
		Object o = null;
		LinkedHashMap m11 = null;
		Object obj = null;
		List listObjects = null;
		List<Map> queryMap1 = null;
		String searchText = entity.getSearchText();
		String selectedFieldName = entity.getSelectedFieldName();
		Object val = "";
		Boolean includeInActiveRecords = entity.getIncludeInActive();
		Boolean isCascade = false;
		List<SupportTableConfigMain> supportTableConfigObjects = null;
		SupportTableConfigMain config = null;
		// Map<String,Object> filterByFields = entity.getFilterByFields();
		List<FieldInfo> filterByFields = entity.getFilterByFields();
		List<String> selectedFieldNames = entity.getSelectedFieldNames();
		String collectionNameRef = null;
		List<FieldInfo> filterByNinFields = entity.getNinFilterByFields();
		PageInfo pageInfo = entity.getPageInfo();
		Integer count = defaultPageSize;
		Integer skip = 0;
		Integer pageNo = 1;
		if (pageInfo != null) {
			count = pageInfo.getCount();
			pageNo = pageInfo.getPageNo();
		}

		skip = (pageNo * count) - count;
		String dataType = null;
		Integer limit = count;
		if (searchText == null) {
			searchText = "";
		}
		FieldInfo fieldInfo = null;
		FieldInfo fieldInfoNin = null;
		Object filterObjects = null;
		String fieldType = null;
		Boolean isCode = null;
		Map condition = null;
		// List<String> canSortFields = new LinkedList<>();
		String sortByField = entity.getSortByField();
		String sortByDirection = entity.getSortByDirection();
		searchText = Pattern.quote(searchText);
		String referencePath = null;
		String headerName = null;
		FieldInfo info2 = null;
		String fieldName = null;
		Sort sortField = null;
		c1.and("lookupName").is(lookupName);
		c1.and("isDeleted").is(false).and("isActive").is(true);
		query.addCriteria(c1);
		query.with(new Sort(Sort.Direction.ASC, "displayOrderNo"));
		List<String> configuredFields = mongoTemplate.getCollection(supportTableConfig).distinct("fieldName",
				query.getQueryObject());
		supportTableConfigObjects = mongoTemplate.find(query, SupportTableConfigMain.class, supportTableConfig);
		this.metaDataDao.isFilterFirst(entity);
		List<Criteria> criteriaList = new ArrayList<Criteria>();
		c1 = new Criteria();
		Field field1 = new Field();
		Field field2 = new Field();
		Query query1 = new Query();
		query1.fields().exclude("_id").exclude("SupportKey");
		List<HeaderInfo> headerInfoList = null;
		Query query2 = new Query();
		query2.fields().exclude("_id").exclude("SupportKey");
		int maxCascadingField = -1;
		Integer displayOrderNo = -1;
		Integer rowLimit = 1;
		String temp = "";
		Criteria c11[] = null;
		Boolean isDisplay = true;
		Map<String, Object> returnMap = new LinkedHashMap<>();
		// c.and("isActive").is(true).and("isDeleted").is(false); //Commented for the
		// time being for testing
		// c.and(supportKey).is(lookupName);
		criteriaList.add(Criteria.where(supportKey).is(lookupName));
		if (includeInActiveRecords == null || (includeInActiveRecords != null && includeInActiveRecords == false)) {
			// c.and("isDeleted").is(false).and("isActive").is(true);
			criteriaList.add(Criteria.where("isDeleted").is(false));
			criteriaList.add(Criteria.where("isActive").is(true));
		}

		AggregationResults<Map> groupResults = null;
		Aggregation agg = null;

		ProjectionOperation projectOperations = project().andExclude("_id");
		GroupOperation groupOperation = null;
		UnwindOperation unwindOper = null;
		List<AggregationOperation> operList = null;
		if (isColumnFilter) {
			operList = new LinkedList<>();
		}
		List<String> tempList = new ArrayList<>();
		/*
		 * if (filterByFields != null && filterByFields.size() > 0 || selectedFieldName
		 * != null) { if (filterByFields != null) for (FieldInfo f : filterByFields) {
		 * tempList.add(f.getFieldName()); } if (selectedFieldName != null) {
		 * tempList.add(selectedFieldName); } for (String s1 : tempList) { if
		 * (!configuredFields.contains(s1)) { throw new IllegalStateException( s1 +
		 * " field name is not configured, select a configured field name from headers"
		 * ); } } }
		 */

		for (int i = 0; i < supportTableConfigObjects.size(); i++) {
			headerName = supportTableConfigObjects.get(i).getDisplayName();
			fieldName = supportTableConfigObjects.get(i).getFieldName();
			referencePath = supportTableConfigObjects.get(i).getReferencePath();
			dataType = supportTableConfigObjects.get(i).getDataType();
			Boolean canSort = supportTableConfigObjects.get(i).getCanSort();
			Boolean canFilter = supportTableConfigObjects.get(i).getFilter();
			isCascade = supportTableConfigObjects.get(i).getIsCascade();
			headerInfoList = supportTableConfigObjects.get(i).getFieldArray();
			displayOrderNo = supportTableConfigObjects.get(i).getDisplayOrderNo();
			isDisplay = supportTableConfigObjects.get(i).getIsDisplay();
			Boolean defaultSort = supportTableConfigObjects.get(i).getDefaultSort();
			collectionNameRef = supportTableConfigObjects.get(i).getCollectionNameRef();
			if (sortByDirection != null && sortByField != null && sortByField.equals(fieldName)) {
				sortField = new Sort(Sort.Direction.fromString(sortByDirection), referencePath);
				query1.with(sortField);
			} else if (defaultSort != null && defaultSort && sortByField == null) {
				sortField = new Sort(Sort.Direction.ASC, referencePath);
				query1.with(sortField);
			}
			try {

				fieldInfo = filterByFields.get(filterByFields.indexOf(new FieldInfo(fieldName)));
				if (fieldInfo != null) {
					filterObjects = fieldInfo.getFieldValue();
					if (filterObjects instanceof List) {
						criteriaList.add(Criteria.where(referencePath).in(((List) filterObjects).toArray()));
						// c.and(referencePath).in(((List) filterObjects).toArray());
					} else {
						criteriaList.add(Criteria.where(referencePath).in(filterObjects));
						// c.and(referencePath).in(filterObjects);
					}
				}
			} catch (Exception e) {
			}
			/*
			 * if (filterByFields != null && filterByFields.size() > 0 && (fieldInfo != null
			 * && fieldName.equals(fieldInfo.getFieldName()))) { filterObjects =
			 * fieldInfo.getFieldValue(); if (filterObjects instanceof List) {
			 * criteriaList.add(Criteria.where(referencePath).in(((List)
			 * filterObjects).toArray())); //c.and(referencePath).in(((List)
			 * filterObjects).toArray()); } else {
			 * criteriaList.add(Criteria.where(referencePath).in(filterObjects));
			 * //c.and(referencePath).in(filterObjects); }
			 * 
			 * if (isCascade != null && isCascade && headerInfoList != null &&
			 * headerInfoList.size() > 0 && maxCascadingField <= displayOrderNo) {
			 * maxCascadingField = displayOrderNo; config =
			 * supportTableConfigObjects.get(i); } }
			 */
			if (isCascade != null && isCascade && headerInfoList != null && headerInfoList.size() > 0
					&& maxCascadingField <= displayOrderNo) {
				maxCascadingField = displayOrderNo;
				config = supportTableConfigObjects.get(i);
			}
			headerList.add(
					new HeaderInfo(headerName, canSort, canFilter, dataType, fieldName, isDisplay, displayOrderNo));
			try {
				fieldInfoNin = filterByNinFields.get(filterByNinFields.indexOf(new FieldInfo(fieldName)));
				if (fieldInfoNin != null) {
					filterObjects = fieldInfoNin.getFieldValue();
					if (filterObjects instanceof List) {
						criteriaList.add(Criteria.where(referencePath).nin(((List) filterObjects).toArray()));

					} else {
						criteriaList.add(Criteria.where(referencePath).nin(filterObjects));

					}
				}

			} catch (Exception e) {
			}
			/*
			 * if (filterByNinFields != null && filterByNinFields.size() > 0 && (fieldInfo
			 * != null && fieldName.equals(fieldInfo.getFieldName()))) { filterObjects =
			 * fieldInfo.getFieldValue(); if (filterObjects instanceof List) {
			 * criteriaList.add(Criteria.where(referencePath).nin(((List)
			 * filterObjects).toArray())); //c.and(referencePath).nin(((List)
			 * filterObjects).toArray()); } else {
			 * criteriaList.add(Criteria.where(referencePath).nin(filterObjects));
			 * //c.and(referencePath).nin(filterObjects); } }
			 */
			if (isColumnFilter) {
				if (selectedFieldName.equals(fieldName)) {
					if (dataType.equalsIgnoreCase("array")) {
						unwindOper = unwind(referencePath, true);

					}
					projectOperations = projectOperations.and(referencePath).as("code");
					projectOperations = projectOperations.and(referencePath).as("description");
					groupOperation = group(fields().and("code").and("code").and("description"))
					// .push("description").as("description")
					;
					if (dataType.equalsIgnoreCase("string") || dataType.equalsIgnoreCase("array")) {
						criteriaList.add(Criteria.where(referencePath).regex(searchText, "i"));
						// c.and(referencePath).regex(searchText, "i");
					}

				}

			}

		}
		if (isCascade != null && isCascade && headerInfoList != null && config != null) {
			headerInfoList = config.getFieldArray();
			headerList.clear();
			// query1 = new Query();

			// query1.fields().exclude("_id");

			// query1.fields().include(config.getReferencePath());
			operList = new LinkedList<>();
			org.springframework.data.mongodb.core.aggregation.Fields fields = fields();
			fields = fields.and(config.getReferencePath());
			fields = fields().and(config.getReferencePath());
			projectOperations = project();
			SortOperation sortOperation = null;
			for (HeaderInfo f : headerInfoList) {
				// query1.fields().include(f.getReferencePath());
				if (f.getDefaultSort()) {
					sortOperation = sort(new Sort(Sort.Direction.ASC, f.getReferencePath()));
				}
				fields = fields.and(f.getReferencePath());
				headerList.add(f);
				projectOperations = projectOperations.and("_id." + f.getReferencePath()).as(f.getReferencePath());
			}
			Criteria c = new Criteria();
			c11 = new Criteria[criteriaList.size()];
			for (int i = 0; i < criteriaList.size(); i++) {
				c11[i] = criteriaList.get(i);
			}
			c.andOperator(c11);
			MatchOperation match = match(c);
			groupOperation = group(fields);
			operList.add(match);
			operList.add(groupOperation);
			operList.add(projectOperations);
			operList.add(sortOperation);
			agg = newAggregation(operList);
			System.out.println("agg1 : : : : " + agg);
			queryMap1 = mongoTemplate.aggregate(agg, supportTableCollection, Map.class).getMappedResults();
			returnMap.put("data", queryMap1);
			returnMap.put("count", count1);
			returnMap.put("headerList", headerList);
			return returnMap;
		}
		query1.skip(skip);
		query1.limit(limit);
		Criteria c = new Criteria();
		c11 = new Criteria[criteriaList.size()];
		for (int i = 0; i < criteriaList.size(); i++) {
			c11[i] = criteriaList.get(i);
		}
		c.andOperator(c11);
		query1.addCriteria(c);
		query1.fields().exclude("_id");
		// System.out.println("------" + query1.getQueryObject());
		if (!isColumnFilter) {
			queryMap1 = mongoTemplate.find(query1, Map.class, supportTableCollection);
			count1 = mongoTemplate.count(query1, Count.class, supportTableCollection);
		} else {
			// System.out.println("c : : : : : " + c.getCriteriaObject());
			// System.out.println("groupOperation : : : : : " + groupOperation.getFields());
			// System.out.println("projectOperations : : : : : " +
			// projectOperations.getFields());
			// System.out.println("sort : : : : : " + sort(sortField));
			/* Query needs To be refactored */

			if (unwindOper != null) {
				operList.add(unwindOper);
			}
			operList.add(match(c));
			operList.add(projectOperations);
			operList.add(groupOperation);
			operList.add(sort(new Sort(Sort.Direction.ASC, "code")));
			operList.add(limit(defaultPageSize));
			agg = newAggregation(operList);
			DBObject cursor = new BasicDBObject("batchSize", defaultPageSize);
			agg = agg.withOptions(newAggregationOptions().cursor(cursor).build());
			System.out.println("agg1 : : : : " + agg);
			queryMap1 = mongoTemplate.aggregate(agg, supportTableCollection, Map.class).getMappedResults();
			// headerList.clear();
		}
		// System.out.println("query1 : : : : " + query1);
		for (Map m : queryMap1) {
			LinkedHashMap m12 = new LinkedHashMap();
			for (int i = 0; i < supportTableConfigObjects.size(); i++) {
				temp = "";
				if (m.get("Value") instanceof Map) // Have to be refactored
				{
					m11 = (LinkedHashMap) m.get("Value"); // Have to be refactored
					val = "";
					condition = supportTableConfigObjects.get(i).getCondition();
					referencePath = supportTableConfigObjects.get(i).getReferencePath().replace("Value.", ""); // Have
																												// to be
																												// refactored
					o = m11.get(referencePath);
					if (condition != null) {
						String operation = condition.get("operation").toString();
						if (operation != null && operation.equalsIgnoreCase("concat")) {
							Object fields = condition.get("fields");
							if (fields instanceof List) {
								List<String> fieldObjects = (List<String>) fields;

								Object obj1 = null;
								for (String field : fieldObjects) {
									obj1 = m11.get(field);
									if (obj1 != null) {
										temp += obj1.toString();
									} else {
										temp += "" + field;
									}
								}
								m11.put(referencePath, temp);
							}

						}
						o = temp;
					}
					if (!supportTableConfigObjects.get(i).getReferencePath().equals("Key")) // Have to be refactored
					{
						if (o instanceof List) {
							listObjects = (List) o;
							for (int i1 = 0; i1 < listObjects.size(); i1++) {
								if (i1 < listObjects.size() - 1) {
									val += (listObjects.get(i1).toString() + separatorSymbol);
								} else {
									val += (listObjects.get(i1).toString());
								}
							}
							o = val;
						}
						m12.put(referencePath, o);
					}
				}
			}

			m.put("Value", m12);
		}

		/*
		 * if(secondaryCollnName != null&&secondaryFieldName != null) { for(Map m :
		 * queryMap1) { if(m.get(secondaryFieldName) != null)
		 * inObjects.add(m.get(secondaryFieldName)); }
		 * c.and(secondaryFieldName).in(inObjects); query2.addCriteria(c);
		 * System.out.println("query2 : : : : " + query2);
		 * query2.fields().include(secondaryFieldName); queryMap2 =
		 * mongoTemplate.find(query2, Map.class, secondaryCollnName); for(Map m1 :
		 * queryMap1) { for(Map m2 : queryMap2) { //obj =
		 * m2.get(m2.get(secondaryFieldName)); //
		 * System.out.println(m2.get(secondaryFieldName).getClass() +
		 * " : : : : : "+m1.get(primaryFieldName).getClass());
		 * if(m2.get(secondaryFieldName).equals(m1.get(primaryFieldName)) ) { for(int
		 * i=0;i<secondaryList.size();i++) { String s1 = secondaryList.get(i);
		 * if(m1.get(s1) != null) { val = m2.get(s1); m1.put(s1, m2.get(s1) +"; "+val);
		 * } else { val = m2.get(s1); if(m2.get(s1) != null) m1.put(s1, m2.get(s1));
		 * else m1.put(s1, ""); }
		 * 
		 * } } else { break;
		 * 
		 * } } m1.remove(secondaryFieldName); } }
		 * 
		 */

		returnMap.put("data", queryMap1);
		returnMap.put("count", count1);
		returnMap.put("headerList", headerList);
		returnMap.put("recordCount", ((skip + limit) >= count1) ? count1 : (skip + limit));
		returnMap.put("prevRecordCount", ((count1) <= 0) ? 0 : (pageNo * count - count + 1));
		// returnMap.put("sortFieldList", canSortFields);
		// returnMap.put("fieldList", fieldList);
		return returnMap;

		/*
		 * String tableName = entity.getLookupName(); String supportTableConfigQuery =
		 * "{tableName:#, isDeleted : false, isActive:true}"; String gridQuery = "";
		 * String countQuery = ""; MongoCursor<SupportTableConfig> configs = null;
		 * Object objectFromMap = null; Object objectNinFieldsMap = null;
		 * SupportTableConfig supportTableConfigTemp = null; Map<String,Object>
		 * filterByFields = entity.getFilterByFields(); Map<String,Object>
		 * filterByNinFields = entity.getNinFilterByFields(); StringBuilder queryString
		 * = new StringBuilder(1024); List<String> headerList = new
		 * LinkedList<String>(); List<String> fieldList = new LinkedList<String>();
		 * Map<String,Object> returnMap = new LinkedHashMap<>(); String query = ""; int
		 * parametersSize = 0; Object[] parameters = null; Object[] countparameters =
		 * null; List<Map> resultMap = null; Integer skip = entity.getSkip(); Integer
		 * limit = entity.getLimit(); Count count = null; SupportTableQueryConfig
		 * queryConfig = null; queryConfig =
		 * jongo.getCollection(supportTableQueries).findOne(supportTableConfigQuery,
		 * tableName) .as(SupportTableQueryConfig.class); configs =
		 * jongo.getCollection(supportTableConfig).find(supportTableConfigQuery,
		 * tableName) .sort("{displayOrderNo : 1}") .as(SupportTableConfig.class);
		 * 
		 * try { gridQuery = queryConfig.getDbGridQuery(); countQuery =
		 * queryConfig.getDbCountQuery(); int index = 0; while(configs.hasNext()) {
		 * supportTableConfigTemp= configs.next(); String referencePath =
		 * supportTableConfigTemp.getReferencePath(); String dataType =
		 * supportTableConfigTemp.getDataType(); fieldList.add(referencePath);
		 * if(filterByNinFields != null && filterByNinFields.size() > 0) {
		 * objectNinFieldsMap = filterByNinFields.get(referencePath); parametersSize =
		 * filterByNinFields.size();
		 * 
		 * 
		 * } if(filterByFields != null && filterByFields.size() > 0) { objectFromMap =
		 * filterByFields.get(referencePath); parametersSize += filterByFields.size(); }
		 * if((filterByFields != null && filterByFields.size() >0) || (filterByNinFields
		 * != null && filterByNinFields.size() >0) ) { objectFromMap =
		 * filterByFields.get(referencePath); if(parameters == null) { parameters = new
		 * Object[parametersSize + 1 + 1 +1]; countparameters = new
		 * Object[parametersSize ]; } if(objectNinFieldsMap !=
		 * null&&objectNinFieldsMap!=null) {
		 * queryString.append("\'"+referencePath+"\'");
		 * queryString.append(": {$nin : "); queryString.append("#");
		 * queryString.append("},"); parameters[index]= objectNinFieldsMap;
		 * countparameters[index] = objectNinFieldsMap; index++; } if(referencePath !=
		 * null && objectFromMap != null) { queryString.append("\'"+referencePath+"\'");
		 * queryString.append(":{$in :"); queryString.append("#");
		 * queryString.append("},"); parameters[index]= objectFromMap;
		 * countparameters[index] = objectFromMap; index++;
		 * 
		 * } } headerList.add(supportTableConfigTemp.getDisplayName()); }
		 * if(queryString.lastIndexOf(",") >=0) { query
		 * =queryString.toString().substring(0, queryString.toString().length()-1); }
		 * 
		 * if(query.length() == 0) { gridQuery = gridQuery.replace(",$filters", query);
		 * countQuery = countQuery.replace(",$filters", query); } else { gridQuery =
		 * gridQuery.replace("$filters", query); countQuery =
		 * countQuery.replace("$filters", query); }
		 * 
		 * //if(filterByFields != null && filterByFields.size() > 0 ) if(parametersSize
		 * >0) { Command c = jongo.runCommand2(countQuery,
		 * countparameters).throwOnError(); Boolean isCursorFlag =
		 * queryConfig.getIsCursorFlag(); if(isCursorFlag) { count =
		 * c.field("cursor").as(Count.class).get(0); } else { count= c.as(Count.class);
		 * } parameters[index++] = skip; parameters[index++] = limit;
		 * parameters[index++] = limit; resultMap = jongo.runCommand2(gridQuery,
		 * parameters )
		 * 
		 * .throwOnError() .field("cursor") .as(Map.class); } else { resultMap =
		 * jongo.runCommand(gridQuery,skip ,limit,limit) .throwOnError()
		 * .field("cursor") .as(Map.class); Command c =
		 * jongo.runCommand(countQuery).throwOnError(); Boolean isCursorFlag =
		 * queryConfig.getIsCursorFlag(); if(isCursorFlag) { count =
		 * c.field("cursor").as(Count.class).get(0); } else { count= c.as(Count.class);
		 * } } returnMap.put("data", resultMap); returnMap.put("count", count.getN());
		 * returnMap.put("headerList", headerList); returnMap.put("fieldList",
		 * fieldList); configs.close();
		 * 
		 * } catch(Exception e) { System.out.println("Exception message : " +
		 * e.getMessage()); e.printStackTrace(); } return returnMap;
		 */
	}

	/**
	 * counts the number of products at PH0003 hierarchy with supplement value "yes"
	 * under same parentId
	 * 
	 * returns nextOrderId for current new format (if and only if supplement value
	 * is "yes")
	 * 
	 * @author Rupesh Maharjan
	 **/

	@SuppressWarnings("unchecked")
	@Override
	public APIResponse<Object> getOrderId(String reqFields) {
		int nextOrderId = -1;
		APIResponse<Object> response = new APIResponse<>();
		java.util.logging.Logger log = java.util.logging.Logger.getLogger("showOrderID");
		String reqJson = reqFields;
		Map<String, Object> prod = new Gson().fromJson(reqJson, new TypeToken<HashMap<String, Object>>() {
		}.getType());
		if (prod.size() != 3) {
			response.setCode(APIResponse.ResponseCode.ERROR.toString());
			response.setData(null);
			response.setStatusMessage("All required fields are not present.");
			return response;
		}

		log.info("Request JSON: " + prod.toString());

		Query q = new Query();
		q.addCriteria(Criteria.where("metaDataFieldName").is("OrderID"));
		CMetaFieldLogicEntity logicEntity = (CMetaFieldLogicEntity) mongoTemplate.findOne(q,
				CMetaFieldLogicEntity.class, "cMetaFieldLogics");
		List<CMetaFieldLogicCondition> conditions = logicEntity.getCondition();
		log.info("conditions size: " + conditions.size());
		boolean checkCondition = true;

		List<Criteria> criterias = new ArrayList<>();

		for (CMetaFieldLogicCondition condition : conditions) {
			log.info("checking condition");
			if (!checkCondition) {
				log.info("failed");
				break;
			}
			String fieldname = condition.getMetaDataFieldName();
			String operator = condition.getOperator();
			switch (operator) {
			case "==":
				try {
					Object val = prod.get(fieldname);
					if (val.equals(condition.getValue().toString())) {
						continue;
					} else {
						checkCondition = false;
					}
				} catch (Exception e) {
				}

				break;
			case "!=":
				try {
					Object val1 = prod.get(fieldname);
					if (!val1.equals(condition.getValue())) {
						continue;
					} else {
						checkCondition = false;
					}
				} catch (Exception e) {
				}

				break;
			}
		}
		if (checkCondition) {

			String parentId = prod.get("parentID").toString();
			Query q1 = new Query();
			q1.addCriteria(Criteria.where("metadataId").is(parentId));
			Map<String, Object> p = mongoTemplate.findOne(q1, Map.class, "products");

			String pJson = new Gson().toJson(p);
			DocumentContext docCtxPJson = JsonPath.parse(pJson);
			String jpath = "$.Product.ProductIdentifier[?(@.ProductIDType == '01' && @.IDTypeName == 'Language Key')].IDValue";
			try {
				Object val = docCtxPJson.read(jpath);
				String langKey = val.toString().replace("]", "").replace("[", "").replace("\"", "");
				prod.put("LanguageKey", langKey);
			} catch (Exception e) {
				logger.warn("Language Key - getOrderID ->" + e.getMessage());
				response.setCode(APIResponse.ResponseCode.ERROR.toString());
				response.setData(null);
				response.setStatusMessage("doesn't contain Language key");
				return response;
			}
			q1 = new Query();
			List<Criteria> cs = getSubCriterias(logicEntity, prod);
			criterias.addAll(cs);
			Criteria c = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			q1.addCriteria(c);

			log.info("query {" + q1 + "}");

			int count = (int) mongoTemplate.count(q1, Object.class, "products");
			nextOrderId = count + 1;
			log.info("Next order Id will be: " + nextOrderId);
		} else {
			log.info("checkCondtion: " + checkCondition);
		}
		if (nextOrderId == -1) {
			response.setCode(APIResponse.ResponseCode.ERROR.toString());
			response.setData(null);
			response.setStatusMessage("error");
		} else {
			Map<String, Object> data = new HashMap<>();
			data.put("nextOrderId", nextOrderId);
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setData(data);
			response.setStatusMessage("success");

		}
		return response;
	}

	private List<Criteria> getSubCriterias(CMetaFieldLogicEntity logicEntity, Map<String, Object> prod) {
		List<String> reqList = logicEntity.getFieldsRequired();
		List<Criteria> subCriteria = new ArrayList<>();
		for (String reqField : reqList) {
			try {
				String val = prod.get(reqField).toString();
				if (reqField.equals("productHierarchyId") || reqField.equals("parentID")) {
					subCriteria.add(Criteria.where("CustomFields." + reqField).is(val));
				} else {
					Query q = new Query();
					q.addCriteria(Criteria.where("metaDataFieldName").is(reqField));
					MetaDataFieldsEntity metaDataField = mongoTemplate.findOne(q, MetaDataFieldsEntity.class,
							"cMetadataFields");
					String refPath = metaDataField.getReferencePath();
					String tempRefPath = refPath.substring(0, refPath.lastIndexOf(".") + 1);
					List<JsonDbPathConditionEntity> qconditions = metaDataField.getJsonDbPathCondition();

					if (qconditions.size() >= 1 && qconditions != null) {
						for (JsonDbPathConditionEntity qcondition : qconditions) {
							String field = qcondition.getField();
							String finalref = tempRefPath + field;
							Criteria c = processCriteria(finalref, qcondition);
							subCriteria.add(c);
						}
					}
					subCriteria.add(Criteria.where(refPath).is(val));
				}
			} catch (Exception e) {
				System.out.println("error: " + e.getMessage());
			}
		}
		return subCriteria;
	}

	private Criteria processCriteria(String ref, JsonDbPathConditionEntity condition) {
		switch (condition.getCondition()) {
		case "==":
			return Criteria.where(ref).is(condition.getValue());
		case "!=":
			return Criteria.where(ref).ne(condition.getValue());
		default:
			break;
		}
		return null;
	}

	@Override
	public APIResponse<Object> productSync(Map<String, Object> reqFields) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			String referenceParentId = null;
			String parentId = null;
			if (reqFields.keySet().contains("parentId")) {
				parentId = reqFields.get("parentId").toString();
			}
			if (reqFields.keySet().contains("referenceParentId")) {
				referenceParentId = reqFields.get("referenceParentId").toString();
			}
			if (referenceParentId == null && parentId == null) {
				throw new Exception();
			}
			if (referenceParentId != null && parentId != null) {
				throw new Exception();
			}
			String productHierarchy = reqFields.get("productHierarchyId").toString();
			String syncFields = metaDataDao.getSyncFields(productHierarchy, parentId, referenceParentId);
			ObjectMapper mapper = new ObjectMapper();
			Map<Object, Object> syncProd = mapper.readValue(syncFields, new TypeReference<Map<Object, Object>>() {
			});
			Map<String, Object> res = new HashMap<>();
			res.put("Product", syncProd);
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setStatus("success");
			response.setData(res);
			response.setStatusMessage("success");
			return response;
		} catch (Exception e) {
			logger.warn(e.getMessage());
			response.setCode(APIResponse.ResponseCode.ERROR.toString());
			response.setStatus("error");
			response.setData(null);
			response.setStatusMessage("must provide productHierarchyId and parentId/referenceId");
			return response;
		}
	}

	@Override
	public Map<String, Object> getCascadingData(SupportRequestEntity entity) {
		GroupOperation groupOperation = null;
		ProjectionOperation projectionOperation = null;
		CascadingBean cascadingBean = null;
		String lookupName = entity.getLookupName();
		Aggregation agg = null;
		Criteria c = new Criteria();
		List<SupportTableConfigMain> configMainList = new LinkedList<>();
		c.and("lookupName").is(lookupName);
		c.and("isDeleted").is(false).and("isActive").is(true);
		// Map<String,Object> selectedFields = entity.getSelectedFields();
		List<FieldInfo> ninFields = entity.getNinFilterByFields();
		Map<String, Object> updateFields = new LinkedHashMap<>();
		List<String> result = null;
		String selectedFieldName = entity.getSelectedFieldName();
		SupportTableConfigMain defaultSortConfigObject = null;
		List<FieldInfo> filterByFields = entity.getFilterByFields();
		String fieldName = null;
		Query query = new Query();
		CascadingBean bean = null;
		LinkedHashSet uniqueSet = new LinkedHashSet();
		query.addCriteria(c);
		query.with(new Sort(Sort.Direction.ASC, "displayOrderNo"));
		List<SupportTableConfigMain> supportTableConfigObjects = mongoTemplate.find(query, SupportTableConfigMain.class,
				supportTableConfig);
		SupportTableConfigMain config = null;
		Object objectValue = null;
		Object objectNinValue = null;
		String referencePath = null;
		FieldInfo fieldInfo = null;
		Integer s = 0;
		c = new Criteria();
		Integer rowLimit = null;
		Integer limit = null;
		Map updatedFields = null;
		Object key = null;
		c.and("SupportKey").is(lookupName).and("isActive").is(true).and("isDeleted").is(false);
		/*
		 * List selectedTemp = null; if(selectedFields != null &&
		 * selectedFields.size()>0) { for(String o : selectedFields.keySet()) {
		 * selectedTemp = new ArrayList<>(); selectedTemp.add(selectedFields.get(o));
		 * updateFields.put(o, selectedTemp);
		 * 
		 * } }
		 */
		for (int i = 0; i < supportTableConfigObjects.size(); i++) {
			config = supportTableConfigObjects.get(i);
			fieldName = config.getFieldName();
			Map condition = config.getCondition();
			updatedFields = config.getUpdateFields();
			objectNinValue = null;
			fieldInfo = null;
			if (condition != null) {
				configMainList.add(config);
			}
			if (updatedFields != null) {
				Object criterias = updatedFields.get("criterias");
				if (criterias != null && criterias instanceof List) {
					List criteriaList = (List) criterias;
					for (Object o : criteriaList) {
						ObjectMapper mapper = new ObjectMapper();
						FieldInfo fieldInfoTemp = mapper.convertValue(o, FieldInfo.class);
						c.and(fieldInfoTemp.getFieldName()).is(fieldInfoTemp.getFieldValue());
					}
				}
				Map isbnMap = metaDataDao.getISBNValue();
				if (isbnMap != null) {
					Object isbn = isbnMap.get("Key");
					// c= c.and(config.getReferencePath()).is(isbn );
					if (filterByFields == null) {
						filterByFields = new LinkedList<>();
					}
					filterByFields.add(new FieldInfo("Key", isbn));
				}
			}
			rowLimit = supportTableConfigObjects.get(i).getLimit();
			if (rowLimit != null) {
				limit = rowLimit;
			}
			if (config.getDefaultSort() != null && config.getDefaultSort()) {
				defaultSortConfigObject = config;
			}
			/*
			 * if(selectedFields == null) { break; } objectValue =
			 * selectedFields.get(fieldName); referencePath = config.getReferencePath();
			 * if(selectedFields != null && objectValue != null) { if(objectValue instanceof
			 * List && ((List)objectValue).size() > 0 ) c.and(referencePath).in(((List)
			 * objectValue).toArray()); else c.and(referencePath).in( objectValue); s=i; }
			 */
			/*
			 * if (filterByFields == null || filterByFields.size() == 0) { break; }
			 */
			// objectValue = filterByFields.get(fieldName);
			try {
				fieldInfo = filterByFields.get(filterByFields.indexOf(new FieldInfo(fieldName)));
			} catch (Exception e) {
			}
			referencePath = config.getReferencePath();
			if (filterByFields != null && (fieldInfo != null && fieldInfo.getFieldName().equals(fieldName))) {
				objectValue = fieldInfo.getFieldValue();
				if (objectValue instanceof List && ((List) objectValue).size() > 0) {
					c = c.and(referencePath);
					if (config.getMappedFields() != null) {
						processMappedFields(config.getMappedFields(), c, objectValue);
					} else {
						c = c.in(((List) objectValue).toArray());
					}
				} else {
					c = c.and(referencePath);
					if (config.getMappedFields() != null) {
						processMappedFields(config.getMappedFields(), c, objectValue);
					} else {
						c = c.in(objectValue);
					}
				}
				s = i;
			}
			try {
				if (fieldInfo == null && ninFields != null && ninFields.size() > 0) {
					fieldInfo = ninFields.get(ninFields.indexOf(new FieldInfo(fieldName)));
					objectNinValue = fieldInfo.getFieldValue();
				}

			} catch (Exception e) {
			}
			if (ninFields != null && objectNinValue != null) {
				if (objectNinValue instanceof List && ((List) objectNinValue).size() > 0)
					c.and(referencePath).nin(((List) objectNinValue).toArray());
				else
					c.and(referencePath).nin(objectNinValue);
				s = i;
			}
		}
		if (s == supportTableConfigObjects.size() - 1
				|| ((s == 0 && (filterByFields == null)) || (filterByFields != null && filterByFields.size() == 0))) {

			config = supportTableConfigObjects.get(s);
		} else
			config = supportTableConfigObjects.get(s + 1);
		referencePath = config.getReferencePath();

		fieldName = config.getFieldName();
		Sort sortBy = null;
		if (defaultSortConfigObject != null) /* Default sort by configuration */
		{
			sortBy = new Sort(Sort.Direction.ASC, defaultSortConfigObject.getReferencePath());
		} else {
			sortBy = new Sort(Sort.Direction.ASC, referencePath);
		}

		List defaultList = new ArrayList<>();
		query = new Query(c);
		query.with(sortBy);
		logger.info(": : : :: query : : : : " + query + " : : :s = : " + s);
		if (configMainList.size() == 0 || (filterByFields != null && filterByFields.size() > 0)) {
			result = mongoTemplate.getCollection(supportTableCollection).distinct(referencePath,
					query.getQueryObject());
			if (result.size() > 1) {
				Collections.sort(result);
			}
			cascadingBean = new CascadingBean(fieldName, config.getDisplayOrderNo(), result);
			updateFields.put(config.getFieldName(), cascadingBean);
		} else if ((filterByFields == null) || (filterByFields != null && filterByFields.size() == 0)) {

			Set<Map> results = new LinkedHashSet<>();
			Map m2 = null;
			fieldName = "";
			if (limit != null)
				query.limit(limit);
			else
				query.limit(defaultPageSize);// Hard coded
			List<Map> resultTemp = mongoTemplate.find(query, Map.class, supportTableCollection);
			for (Map m1 : resultTemp) {
				Object temp = "";
				Object obj1 = null;
				m2 = new HashMap();
				for (SupportTableConfigMain m : configMainList) {
					Map condition = m.getCondition();
					if (key == null) {
						key = condition.get("key");
					}
					if (condition != null) {
						temp = "";
						if (condition instanceof List) {
							List<Map> conditionList = (List) condition;
							for (Map cond : conditionList) {
								fieldName = (String) cond.get("fieldName");
								String operation = (String) cond.get("operation");
								List fields = (List) cond.get("fields");
								if (operation.equalsIgnoreCase("concat")) {
									for (Object o : fields) {
										obj1 = m1.get(o);
										if (obj1 != null && obj1 instanceof String) {
											temp = temp + obj1.toString();
										} else if (m1.get("Value") != null) {
											Map m10 = (Map) m1.get("Value");// Fix for getting proper Key/Value pairs
											obj1 = m10.get(o);
											if (obj1 != null && obj1 instanceof String)
												temp = temp + obj1.toString();
											else if (obj1 != null) {
												temp = obj1;
											} else
												temp = temp + "";
										} else {
											temp = temp + o.toString();
										}

									}
									m2.put(fieldName, temp);
								}
							}
						} else {
							fieldName = (String) condition.get("fieldName");
							String operation = (String) condition.get("operation");
							List fields = (List) condition.get("fields");
							if (operation.equalsIgnoreCase("concat")) {
								for (Object o : fields) {
									obj1 = m1.get(o);
									if (obj1 != null) {
										if (obj1 instanceof String)
											temp = temp + obj1.toString();
										else
											temp = obj1;
									} else if (m1.get("Value") != null) {
										Map m10 = (Map) m1.get("Value");// Fix for getting proper Key/Value pairs
										obj1 = m10.get(o);
										if (obj1 != null && obj1 instanceof String)
											temp = temp + obj1.toString();
										else if (obj1 != null) {
											temp = obj1;
										} else
											temp = temp + o.toString();
									} else {
										temp = temp + o.toString();
									}

								}
								m2.put(fieldName, temp);
							}
						}

					}

				}
				results.add(m2);
			}
			updateFields.put("CodeDescription", results);
			updateFields.put("KeyField", new SupportTableConfigMain((key == null) ? "Key" : key.toString()));
			return updateFields;

		}

		// updateFields.put(config.getFieldName(),result);

		Map<String, SupportTableConfigMain> groupMap = new LinkedHashMap<>();
		Boolean isCascade = false;
		Map condition = null;

		for (int s1 = s + 1; s1 < supportTableConfigObjects.size(); s1++) {
			config = supportTableConfigObjects.get(s1);
			referencePath = config.getReferencePath();
			isCascade = config.getIsCascade();
			fieldName = config.getFieldName();
			condition = config.getCondition();
			// System.out.println(fieldName +": : "+config.getReferencePath()+": : : " +
			// query.getQueryObject());
			if ((result.size() > 1 // || result.size() == 0
			) && ((isCascade == null) || (isCascade))) {
				if (updateFields.get(fieldName) == null) {
					cascadingBean = new CascadingBean(fieldName, config.getDisplayOrderNo(), defaultList);
					updateFields.put(fieldName, cascadingBean);
				}
			} else {
				// query.with(sortBy);
				// System.out.println("query in distinct loop : " + query + " : : : : :
				// fieldName : : : : " + fieldName);

				result = mongoTemplate.getCollection(supportTableCollection).distinct(referencePath,
						query.getQueryObject());

				if (config.getFirstFormatDefaultValue() != null) {
					boolean formatExists = true;
					if (entity.getMetadataId() != null) {
						if (!config.getProductHierarchyId().equals(entity.getpHid()))
							formatExists = metaDataDao.checkSFormatsExistForLanguage(entity.getMetadataId(),
									entity.getpHid());
					} else
						formatExists = false;
					if (formatExists)
						cascadingBean = new CascadingBean(fieldName, config.getDisplayOrderNo(), result);
					else {
						result.remove(config.getFirstFormatDefaultValue());
						cascadingBean = new CascadingBean(fieldName, config.getDisplayOrderNo(), result);

					}
					updateFields.put(config.getFieldName(), cascadingBean);

				} else {
					cascadingBean = new CascadingBean(fieldName, config.getDisplayOrderNo(), result);

					updateFields.put(config.getFieldName(), cascadingBean);
				}
				if (result.size() > 0)
					c = c.and(config.getReferencePath()).in(result.toArray());

				if (result.size() > 1) {
					Collections.sort(result);
				} else {
					// c= c.and(config.getReferencePath()).in(result.toArray());
				}
			}
			if (condition != null) {
				String operation = (String) condition.get("operation");
				if (operation != null && operation.equalsIgnoreCase("split")) {
					updateFields.remove(config.getFieldName());// Remove the irrelevant fields which is not configured
																// in cMetaDataFields
					String splitString = (String) condition.get("separator");
					List<String> fieldNames = (List) condition.get("fields");
					int displayOrderNo = config.getDisplayOrderNo();
					String fName = null;
					if (splitString != null) {
						for (int record = 0; record < result.size(); record++) {
							for (int i = 0; i < fieldNames.size(); i++) {
								fName = fieldNames.get(i);
								Object o = result.get(record);
								String tempStr = o.toString();
								String[] splittedStrings = tempStr.split(splitString);

								if (splittedStrings.length == 1)/* Split did not work */
								{
									splitString = splitString.toLowerCase();
									splittedStrings = tempStr.split(splitString);
									if (splittedStrings.length == 1) {
										splitString = splitString.toUpperCase();
										splittedStrings = tempStr.split(splitString);
									}
								}
								for (int j = 0; j < splittedStrings.length; j++) {
									if (i == j) {
										String splittedString = splittedStrings[j];
										LinkedList<Object> tempList = new LinkedList<>();
										tempList.add(splittedString);
										cascadingBean = new CascadingBean(fName, displayOrderNo++ * displayOrderNo++,
												tempList);
										updateFields.put(fName, cascadingBean);
										break;
									}
								}

							}
						}

					}

				}
				/*
				 * if(operation != null && operation.equalsIgnoreCase("concat")) {
				 * //updateFields.remove(config.getFieldName()); List<String> fieldNames =
				 * (List)condition.get("fields"); int displayOrderNo =
				 * config.getDisplayOrderNo(); String fName = null; String tempStr = ""; {
				 * 
				 * { for(int i = 0;i<fieldNames.size();i++) { fName = fieldNames.get(i); bean =
				 * (CascadingBean)updateFields.get(fName); if(bean != null) { Object objVal =
				 * bean.getFieldValue(); if(objVal instanceof List) { List<String> values =
				 * (List)objVal; for(int val = 0;val<values.size();val++) { if(val == i) {
				 * tempStr = tempStr+ values.get(i); break; } } } else {
				 * 
				 * } } else tempStr = tempStr+ fName;
				 * 
				 * 
				 * } LinkedList values = new LinkedList<Object>(); values.add(tempStr);
				 * bean.setFieldValue(values); updateFields.put(fName, cascadingBean); }
				 * 
				 * }
				 * 
				 * }
				 */
			}

		}
		return updateFields;

	}

	public void processMappedFields(Map mappedFields, Criteria c, Object value) {
		String operation = (String) mappedFields.get("operation");
		boolean isFound = false;
		Object object = null;
		if (operation != null) {
			switch (operation) {
			case "String_Substitute":
				object = mappedFields.get("substitutionArr");
				if (object instanceof List) {
					List<Map> objects = (List<Map>) object;
					for (Map m : objects) {
						if (m.get("oldValue").equals(value)) {
							c.in(m.get("newValue"));
							isFound = true;
							break;
						}
					}
					if (!isFound) {
						c.in(value);
					}
				} else {
					if (mappedFields.get("oldValue").equals(value)) {
						c.in((mappedFields.get("newValue") != null) ? mappedFields.get("newValue") : value);

					}
				}
				break;
			}
		}
	}

	@Override
	public APIResponse<Object> getISBNValue() {

		throw new IllegalStateException("Unimplemented yet");
		// return metaDataDao.getISBNValue();

	}

	@Override
	public APIResponse<Object> getSupportTablesList() {

		APIResponse<Object> res = new APIResponse<>();
		List<String> supportTables = this.metaDataDao.getSupportTablesList();
		if (supportTables == null) {
			res.setCode(APIResponse.ResponseCode.FAILURE.toString());
			res.setStatusMessage(failureRetrieval);
			res.setData(null);
		} else {
			res.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			res.setStatus(this.successRetrieval);
			res.setData(supportTables);
		}

		return res;
	}

	@Override
	public APIResponse<Object> getSupportTableMapping() {
		APIResponse<Object> response = new APIResponse<>();
		List<Map> supportMappings = metaDataDao.getSupportTableMapping();
		if (null != supportMappings && supportMappings.size() > 0) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Successfully retrieved fields");
			response.setData(supportMappings);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("No Records Found for the support table mapping");
		}
		return response;
	}

	@Override
	public APIResponse<Object> downloadExcelForSupportTable(SupportRequestEntity lookupName) {
		APIResponse<Object> response = new APIResponse<>();
		String path = metaDataDao.generateExcelForSupportTable(lookupName.getLookupName(), lookupName.getExportFormat(),
				lookupName.getUserId());
		if (null != path) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Successfully retrieved fields");
			response.setData(path);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("No Records Found for the support table mapping");
		}
		return response;
	}

	@Override
	public APIResponse<Object> uploadExcelForSupportTable(FileObj fileObj) {

		APIResponse<Object> response = new APIResponse<>();
		List<Map> search = metaDataDao.uploadExcelForSupportTable(fileObj);
		Boolean isErrorInExcel = false;
		for (Map m : search) {
			if (m.get("errors") != null) {
				isErrorInExcel = true;
				break;
			}
		}
		if (!isErrorInExcel) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Uploaded successfully, proceed to upload");
			response.setData(search);
		} else {
			response.setCode("200");
			response.setStatus(failure);
			response.setStatusMessage("Excel Upload having some errors ");
			response.setData(search);

		}
		return response;
	}

	@Override
	public APIResponse<Object> exportSupportTable(SupportRequestEntity supportEntity) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> search = metaDataDao.exportSupportTable(supportEntity);
		if (null != search) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Successfully retrieved fields");
			response.setData(search);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("No Records Found for the support table mapping");
		}
		return response;
	}

	@Override
	public APIResponse<Object> getConfigurationForSupportTable(SupportRequestEntity entity) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> search = metaDataDao.getConfigurationForSupportTable(entity);
		if (null != search) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Successfully retrieved fields");
			response.setData(search);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("No Records Found for the support table mapping");
		}
		return response;
	}

	@Override
	public APIResponse<Object> viewSearchForSupportTable(SupportRequestEntity lookupName) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> search = metaDataDao.viewSearchForSupportTable(lookupName);
		if (null != search) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Successfully retrieved fields");
			response.setData(search);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("No Records Found for the support table mapping");
		}
		return response;
	}

	public APIResponse<Object> lockProduct(String userId, String pHId, String productId) {
		APIResponse<Object> response = new APIResponse<>();
		LockType lockType = metaDataDao.isProductLocked(productId, pHId, userId);
		if (lockType == LockType.LOCKED) {
			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatus(LockType.LOCKED.value());
			response.setStatusMessage("Product is already locked by other user. Operation failed.");
		} else if (lockType == LockType.SELF_LOCK) {
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setStatus(LockType.SELF_LOCK.value());
			response.setStatusMessage("Product is already locked.");
		} else if (lockType == lockType.UNLOCKED) {

			boolean locked = metaDataDao.lockProduct(productId, pHId, userId);
			if (locked) {
				response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
				response.setStatus(LockType.UNLOCKED.value());
				response.setStatusMessage("Product is already locked.");
			} else {
				response.setCode(APIResponse.ResponseCode.FAILURE.toString());
				response.setStatus(failure);
				response.setStatusMessage("Error occured while locking product.");
			}
		}
		return response;
	}

	public APIResponse<Object> checkLockOnProduct(String userId, String pHId, String productId) {
		APIResponse<Object> response = new APIResponse<>();
		LockType lockType = metaDataDao.isProductLocked(productId, pHId, userId);
		if (lockType == LockType.LOCKED) {
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setData(true);
			response.setStatusMessage("Product is locked.");
		} else {
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setData(false);
			response.setStatusMessage("Product is not locked");
		}
		return response;
	}

	public APIResponse<Object> removeLockOnProduct(String userId) {
		APIResponse<Object> response = new APIResponse<>();

		boolean isLockRemoved = metaDataDao.removeLockOnProduct(userId);
		if (isLockRemoved) {
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setStatusMessage("Product has been unlocked successfully.");
			response.setData(true);
		} else {
			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatusMessage("Could not unlock product..! Operation failed.");
			response.setData(false);
		}
		return response;
	}

	@Override
	public APIResponse<Object> addOneSupportTableRecord(SupportRequestEntity entity) {
		APIResponse<Object> response = new APIResponse<>();
		Map search = null;
		if (entity.getMultiUpdateFields() != null && entity.getMultiUpdateFields().size() > 0) {
			search = metaDataDao.addMultiSupportTableRecords(entity);
		} else {
			search = metaDataDao.addOneSupportTableRecord(entity);
		}
		if (null == search.get("errors")) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Added record successfully");
			response.setData(search);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("Error in record addition");
			response.setData(search);
		}
		return response;
	}

	@Override
	public APIResponse<Object> editOneSupportTableRecord(SupportRequestEntity entity) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> search = metaDataDao.editOneSupportTableRecord(entity);
		if (null == search.get("errors")) {
			response.setCode("200");
			response.setStatus("success");
			response.setStatusMessage("Edit record successful");
			response.setData(search);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("Edit record unsuccessful");
			response.setData(search);
		}
		return response;
	}

	@Override
	public APIResponse<Object> actiDeactivateSupportConfig(SupportRequestEntity entity) {
		APIResponse<Object> response = new APIResponse<>();
		Boolean isActivate = metaDataDao.actiDeactivateSupportConfig(entity);
		if (null != isActivate) {
			response.setCode("200");
			response.setStatus("success");
			if (entity.getIsActive() != null && entity.getIsActive())
				response.setStatusMessage("Activated Successfully");
			else if (entity.getIsActive() != null) {
				response.setStatusMessage("Deactivated Successfully");
			}
			response.setData(isActivate);
		} else {
			response.setCode(ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("Edit record unsuccessful");
			response.setData(isActivate);
		}
		return response;
	}

	@Override
	public APIResponse<Object> getSiblingsData(Map<String, Object> obj) {

		APIResponse<Object> response = new APIResponse<>();
		Query query = new Query().addCriteria(Criteria.where("_id").is(obj.get("pHid")));
		ProductHierarchyEntity phEntity = mongoTemplate.findOne(query, ProductHierarchyEntity.class);
		List<Map<Object, Object>> finalData = null;
		if (phEntity.getSyncField() != null) {
			String syncField = phEntity.getSyncField().substring(phEntity.getSyncField().lastIndexOf(".") + 1);
			List<ProductMapEntity> productSiblings = metaDataDao.getSiblingsData(obj.get("referenceParentID"),
					obj.get("pHid"));

			finalData = new ArrayList<>();
			Map<Object, Object> productSiblingsData = null;

			for (ProductMapEntity productSibling : productSiblings) {
				productSiblingsData = new HashMap<Object, Object>();

				String title = "";
				String syncvalue = "";
				String autoGenFieldValue = "";

				String newproJson = new Gson().toJson(productSibling);
				DocumentContext newDoc = JsonPath.parse(newproJson);
				try {
					autoGenFieldValue = newDoc.read(phEntity.getAutoGenJsonPath()).toString();
					autoGenFieldValue = autoGenFieldValue.replace("[", "").replace("]", "").replace("\"", "")
							.replace("\\", "");
					productSiblingsData.put(phEntity.getAutoGenField(), autoGenFieldValue);
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
				try {
					title = newDoc.read(phEntity.getJsonPath()).toString();
					title = title.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
					productSiblingsData.put(phEntity.getProductHierarchyName(), title);
				} catch (Exception e) {

				}
				try {
					syncvalue = newDoc.read("$." + phEntity.getSyncField()).toString();
					syncvalue = syncvalue.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
					productSiblingsData.put(syncField, syncvalue);
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
				if (syncvalue.equals("Yes")) {
					if (title.equals(obj.get("fieldName")) && !syncvalue.equals(obj.get("fieldValue"))) {
						response.setCode(APIResponse.ResponseCode.NOT_MODIFIED.toString());
						response.setStatusMessage(title + " Can't change ");
						response.setData(finalData);
						return response;
					}
				}
				finalData.add(productSiblingsData);
			}

			if (finalData.size() > 0) {
				response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
				response.setStatusMessage("Successfully retrieved  Siblings data");
				response.setData(finalData);
			} else {
				response.setCode(APIResponse.ResponseCode.FAILURE.toString());
				response.setStatusMessage("Failure while retrieving Siblings data");
				response.setData(finalData);
			}

		} else {
			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatusMessage(phEntity.getProductHierarchyName() + "Doen't have siblings sync field");
			response.setData(finalData);
		}

		return response;
	}

	@Override
	public APIResponse<Object> getSupplementStatus(Map<String, Object> obj) {
		APIResponse<Object> response = new APIResponse<>();

		if (obj.get("createType").equals("publication")) {
			response.setCode(APIResponse.ResponseCode.NO_RECORDS_FOUND.toString());
			response.setStatus(failure);
			response.setStatusMessage("Publications first format cannot be supplement record");
		} else {
			boolean isSupplementExist = metaDataDao.checkSFormatsExistForLanguage(obj.get("metadataId"),
					obj.get("pHid"));
			if (isSupplementExist) {
				response.setCode(APIResponse.ResponseCode.RECORDS_FOUND.toString());
				response.setStatus(success);
				response.setStatusMessage("Publications exists supplement record");
			} else {
				response.setCode(APIResponse.ResponseCode.NO_RECORDS_FOUND.toString());
				response.setStatus(failure);
				response.setStatusMessage("Publications first format cannot be supplement record");
			}
		}
		return response;
	}

	public APIResponse<Object> retrieveFieldsBasedOnAvailCode(String availCode, String metadataId) {
		APIResponse<Object> response = new APIResponse<>();

		Map<String, Object> acConfig = metaDataDao.getAvailabilityCodeConfig();
		String phId = acConfig.get("productHierarchyId").toString();
		List<String> reqFields = (List<String>) ((Map<String, Object>) acConfig.get("config")).get(availCode);

		List<MetaDataFieldsEntity> metaDataFieldList = null;
		Query q1 = new Query();
		q1.addCriteria(
				Criteria.where("isActive").is(true).and("isDeleted").is(false).and("metaDataFieldName").in(reqFields));
		metaDataFieldList = mongoTemplate.find(q1, MetaDataFieldsEntity.class, "metaDataFieldConfig");

		if (metadataId != null) {

			List<String> unfilledFields = new ArrayList<>();
			Criteria criteria = Criteria.where("isActive").is(true).and("isDeleted").is(false).and("metaDataFieldName")
					.in(reqFields);
			Aggregation aggregation = newAggregation(match(criteria),
					group("productHierarchyId").addToSet("metaDataFieldName").as("fields"),
					project(Fields.fields().and("key", "_id").and("value", "fields")));

			AggregationResults<KeyValue> aggregationResults = mongoTemplate.aggregate(aggregation,
					"metaDataFieldConfig", KeyValue.class);
			Map<String, Object> phAndFields = aggregationResults.getMappedResults().stream()
					.collect(Collectors.toMap(KeyValue::getKey, KeyValue::getValue));
			// phAndFields.remove(phId);
			Map<String, ProductMapEntity> parentsAndChildern = getparentsAndChildernFromMetaDataId(metadataId, phId);

			for (Entry<String, Object> phAndField : phAndFields.entrySet()) {
				String prodHId = phAndField.getKey();
				List<String> fields = (List<String>) phAndField.getValue();

				if (prodHId.equals(phId)) {
					unfilledFields.addAll(fields);
					continue;
				}

				Map<String, String> mdFieldsAndJPs = metaDataFieldList.stream()
						.filter(p -> fields.contains(p.getMetaDataFieldName()))
						.collect(Collectors.toMap(MetaDataFieldsEntity::getMetaDataFieldName,
								MetaDataFieldsEntity::getJsonPath, (k, v) -> {
									return k;
								}));

				ProductMapEntity prod = parentsAndChildern.get(prodHId);
				DocumentContext docCtx = JsonPath.parse(new Gson().toJson(prod));

				for (Entry<String, String> mdAndJP : mdFieldsAndJPs.entrySet()) {
					try {
						Object val = docCtx.read(mdAndJP.getValue());
						if (val.equals("") || val.toString() == "[]")
							unfilledFields.add(mdAndJP.getKey());
					} catch (Exception e) {
						unfilledFields.add(mdAndJP.getKey());
					}
				}
			}

			Map<String, String> unfilledFieldsMap = metaDataFieldList.stream()
					.filter(entity -> unfilledFields.contains(entity.getMetaDataFieldName()))
					.collect(Collectors.toMap(MetaDataFieldsEntity::getMetaDataFieldName,
							MetaDataFieldsEntity::getFieldDisplayName, (k, v) -> {
								return k;
							}));
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setStatus(success);
			response.setData(unfilledFieldsMap);
			response.setStatusMessage("Required Fields");
			return response;
		}

		Map<String, String> fieldsMap = metaDataFieldList.stream()
				.filter(enitity -> enitity.getMetaDataFieldName() != null).collect(Collectors
						.toMap(book -> book.getMetaDataFieldName(), book -> book.getFieldDisplayName(), (k, v) -> {
							return k;
						}));

		response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
		response.setStatus(success);
		response.setData(fieldsMap);
		response.setStatusMessage("Required Fields");
		return response;
	}

	@Override
	public APIResponse<Object> updateManualStageApprove(Map<String, String> obj) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			Query query = new Query();
			/*
			 * query.addCriteria(Criteria.where("stageId").is(obj.get("stageId"))
			 * .and("approvalMethod").is("MANUALAPPROVE").and("isActive").is(true).and(
			 * "isDeleted") .is(false)); if(mongoTemplate.count(query, "cMetadataStages") >
			 * 0) {
			 */
			Query updateQry = new Query();
			updateQry.addCriteria(
					Criteria.where("metadataId").is(obj.get("metadataId")).and("productHierarchyId").is(obj.get("phId"))
							.and("remarks").is("Awaiting for approval").and("stageId").is(obj.get("stageId")));
			// ProductStagesApprovalEntity psaEntity = mongoTemplate.findOne(query,
			// ProductStagesApprovalEntity.class);//(query, "tProductStagesApproval");
			// if(mongoTemplate.count(updateQry, "tProductStagesApproval") > 0) {
			query = new Query();
			query.addCriteria(
					Criteria.where("stageId").is(obj.get("stageId")).and("productHierarchyId").is(obj.get("phId"))
							.and("isActive").is(true).and("isDeleted").is(false).and("isKeyField").is(true));
			List<MetadataFields> sMetaFields = mongoTemplate.find(query, MetadataFields.class);
			if (sMetaFields.size() > 0) {

				ProductMapEntity pMapEntity = metaDataDao.getProduct(obj.get("metadataId"), obj.get("phId"));
				boolean status = checkAllFieldsExistsInProduct(sMetaFields, pMapEntity);
				if (status) {
					Update update = new Update();
					update.set("remarks", "Completed").set("approvedBy", obj.get("loggedUserId")).set("approvedOn",
							new Date());
					mongoTemplate.updateFirst(updateQry, update, ProductStagesApprovalEntity.class);
					
         metaDataDao.mergeStageRecords(obj.get("loggedUserId"), obj.get("stageId"), 
        		 obj.get("phId"), obj.get("metadataId"), pMapEntity.getCustomFields().get("parentID").toString());
					response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
					response.setStatus(success);
					response.setData(null);
					response.setStatusMessage("Approved stage successfully");
				} else {
					response.setCode(APIResponse.ResponseCode.FAILURE.toString());
					response.setStatus(failure);
					response.setData(null);
					response.setStatusMessage("Please fill key fields!");
				}
			} else {
				response.setCode(APIResponse.ResponseCode.FAILURE.toString());
				response.setStatus(failure);
				response.setData(null);
				response.setStatusMessage("Fields are not configured!");
			}
			/*
			 * }else { response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			 * response.setStatus(failure); response.setData(null);
			 * response.setStatusMessage("Stage alreay approved!"); }
			 */

			/*
			 * }else { response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			 * response.setStatus(failure); response.setData(null);
			 * response.setStatusMessage("Stage not configured as manual approve!"); }
			 */

		} catch (Exception e) {
			logger.error("error in updateManualStageApprove :" + e.getMessage());
			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatus(failure);
			response.setData(null);
			response.setStatusMessage("error while saving stage approval");
		}
		return response;
	}

	/*
	 * if (cMFields.size() > 0) { String json = new Gson().toJson(pMapEntity);
	 * DocumentContext inDocCtx = JsonPath.parse(json); for (MetadataFields
	 * metaField : cMFields) { String jsonPath = metaField.getJsonPath(); try {
	 * 
	 * Object objVal = inDocCtx.read(jsonPath); if (objVal == null) { status =
	 * false; } } catch (Exception e) { status = false; }
	 * 
	 * } } else { status = true; } try { if (status) {
	 * 
	 * query = new Query();
	 * query.addCriteria(Criteria.where("metadataId").is(obj.get("metadataId")).and(
	 * "stageId") .is(obj.get("stageId")).and("remarks").is("InProgress"));
	 * ProductStagesApprovalEntity psEntity = mongoTemplate.findOne(query,
	 * ProductStagesApprovalEntity.class); if (psEntity != null) {
	 * psEntity.setApprovedOn(new Date());
	 * psEntity.setApprovedBy(obj.get("loggedUserId").toString());
	 * psEntity.setRemarks("Manual  Approved");
	 * 
	 * mongoTemplate.save(psEntity, "tProductStagesApproval");
	 * List<ProductStagesApprovalEntity> stageDatas =
	 * metaDataDao.getStagesdataWithStatus(obj.get("metadataId") ,obj.get("phId"));
	 * List<Map<Object, Object>> Stages = new ArrayList<Map<Object,Object>>();
	 * for(ProductStagesApprovalEntity stageData : stageDatas) { Map<Object, Object>
	 * stageStatus = new HashMap<Object, Object>(); stageStatus.put("stageId",
	 * stageData.getStageId()); stageStatus.put("stageDisplayName",
	 * stageData.getStageDisplayName()); stageStatus.put("status",
	 * stageData.getRemarks()); stageStatus.put("approvedDate",
	 * stageData.getApprovedOn()); stageStatus.put("approvalMethod",
	 * stageData.getApprovalMethod()); Stages.add(stageStatus); }
	 * response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
	 * response.setStatus(success); response.setData(Stages);
	 * response.setStatusMessage("Approved stage successfully"); } else {
	 * response.setCode(APIResponse.ResponseCode.FAILURE.toString());
	 * response.setStatus(failure); response.setData(null);
	 * response.setStatusMessage("Already stage approved"); }
	 * 
	 * } else { response.setCode(APIResponse.ResponseCode.FAILURE.toString());
	 * response.setStatus(failure); response.setData(null);
	 * response.setStatusMessage("Please fill the key fields in Stage"); }
	 */

	/*
	 * private boolean calculateKeyFields(ProductMapEntity pMap,List<MetadataFields>
	 * cMFields) { boolean status = true; String json = new Gson().toJson(pMap);
	 * List<MetadataFields> fieldsWithPhid = null; DocumentContext inDocCtx =
	 * JsonPath.parse(json); fieldsWithPhid =
	 * cMFields.stream().filter(p->p.getProductHierarchyId().
	 * equals(pMap.getCustomFields().get("productHierarchyId"))).collect(Collectors.
	 * toList()); status = statusOfKeyFieldswithHierarchy(inDocCtx,fieldsWithPhid);
	 * if(status) { List<ProductMapEntity> parents = getCustomfieldsParents(pMap);
	 * for(ProductMapEntity parent : parents) { json = new Gson().toJson(pMap);
	 * inDocCtx = JsonPath.parse(json); fieldsWithPhid =
	 * cMFields.stream().filter(p->p.getProductHierarchyId().
	 * equals(pMap.getCustomFields().get("productHierarchyId"))).collect(Collectors.
	 * toList()); status = statusOfKeyFieldswithHierarchy(inDocCtx,fieldsWithPhid);
	 * if(!status) break; } }else return status;
	 * 
	 * return status; } private boolean
	 * statusOfKeyFieldswithHierarchy(DocumentContext inDocCtx,List<MetadataFields>
	 * fieldsWithPhid) { boolean status = true; for (MetadataFields metaField :
	 * fieldsWithPhid) { String jsonPath = metaField.getJsonPath(); try {
	 * 
	 * Object objVal = inDocCtx.read(jsonPath); if (objVal == null) { status =
	 * false; } } catch (Exception e) { status = false; } if(!status) break; }
	 * return status; }
	 */
	private boolean checkUsrPermissionStageApprove(Object userId, Object stageId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId).is("isActive").is(true).and("isDeleted").is(false));
		query.fields().include("permissionGroupId").exclude("_id");
		UserEntity user = mongoTemplate.findOne(query, UserEntity.class);
		if (user.getPermissionGroupId() != null) {
			query = new Query();
			query.addCriteria(Criteria.where("permissionGroupId").is(user.getPermissionGroupId()).is("isActive")
					.is(true).and("isDeleted").is(false));
			query.fields().include("permissionGroupId").exclude("_id");
			PermissionGroupEntity pGroupEntity = mongoTemplate.findOne(query, PermissionGroupEntity.class);
			if (pGroupEntity.getRoleId() != null) {
				query = new Query();
				query.addCriteria(Criteria.where("roleId").is(pGroupEntity.getRoleId()).is("isActive").is(true)
						.and("isDeleted").is(false));
				RoleEntity roleEntity = mongoTemplate.findOne(query, RoleEntity.class);
			}
		}
		return true;
	}

	private Map<String, ProductMapEntity> getparentsAndChildernFromMetaDataId(String metadataId, String phId) {
		Map<String, ProductMapEntity> result = new HashMap<>();
		Query q = new Query();
		q.addCriteria(Criteria.where("metadataId").is(metadataId).and("CustomFields.productHierarchyId").is(phId));
		ProductMapEntity currProd = mongoTemplate.findOne(q, ProductMapEntity.class, "products");
		String currProdJson = new Gson().toJson(currProd);
		Map<String, Object> currProduct = new Gson().fromJson(currProdJson, Map.class);

		List<Map> tempparents = (List<Map>) ((Map) currProduct.get("CustomFields")).get("parents");
		Map<Object, Object> parentsMetaDataId = tempparents.stream().collect(Collectors
				.toMap(p -> ((Map) p.get("CustomFields")).get("productHierarchyId"), p -> p.get("metadataId")));

		List<ProductMapEntity> parents = new ArrayList<>();
		for (Entry<Object, Object> pm : parentsMetaDataId.entrySet()) {
			q = new Query();
			q.addCriteria(Criteria.where("metadataId").is(pm.getValue()).and("CustomFields.productHierarchyId")
					.is(pm.getKey()));
			ProductMapEntity prod = mongoTemplate.findOne(q, ProductMapEntity.class, "products");
			parents.add(prod);
		}

		q = new Query();
		List<Criteria> orCriteria = new ArrayList<>();
		orCriteria.add(Criteria.where("CustomFields.IsMaster").is("Yes"));
		orCriteria.add(Criteria.where("CustomFields.ParentChild").is("Yes"));

		List<Criteria> andCriteria = new ArrayList<>();
		andCriteria.add(Criteria.where("CustomFields.parents").elemMatch(
				Criteria.where("metadataId").is(metadataId).and("CustomFields.productHierarchyId").is(phId)));

		Criteria c = new Criteria().orOperator(orCriteria.toArray(new Criteria[orCriteria.size()]));
		andCriteria.add(c);

		Criteria cs = new Criteria().andOperator(andCriteria.toArray(new Criteria[andCriteria.size()]));
		q.addCriteria(cs);

		List<ProductMapEntity> children = mongoTemplate.find(q, ProductMapEntity.class, "products");

		for (ProductMapEntity child : children) {
			result.put(child.getCustomFields().get("productHierarchyId").toString(), child);
		}

		for (ProductMapEntity parent : parents) {
			result.put(parent.getCustomFields().get("productHierarchyId").toString(), parent);
		}

		return result;
	}

	@Override
	public APIResponse<Object> createSupportTable(SupportRequestEntity entity) {
		APIResponse<Object> response = new APIResponse<>();
		List<Map<String, Object>> mapList = metaDataDao.createSupportTable(entity);
		response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
		response.setStatus(success);
		response.setData(mapList);
		response.setStatusMessage("Support Table creation successful");
		return response;
	}

	@Override
	public APIResponse<Object> getSupportTableInfo(SupportRequestEntity entity) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> responseMap = metaDataDao.getSupportTableInfo(entity);
		response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
		response.setStatus(success);
		response.setData(responseMap);
		response.setStatusMessage("Support Table Info retrieved");
		return response;
	}

	@Override
	public APIResponse<Object> checkForDuplicate(Map<String, Object> reqFields) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			String fieldName = reqFields.get("field").toString();
			Object fieldValue = reqFields.get("value");
			Map<String, Boolean> doesExists = metaDataDao.doesValueExist(fieldName, fieldValue);
			if (doesExists == null) {
				throw new Exception("Field not found");
			}
			response.setCode(APIResponse.ResponseCode.SUCCESS.toString());
			response.setStatus(success);
			response.setData(doesExists);
			response.setStatusMessage("The value already present");
			return response;
		} catch (Exception e) {
			response.setCode(APIResponse.ResponseCode.FAILURE.toString());
			response.setStatus(failure);
			response.setData(null);
			response.setStatusMessage(e.getMessage());
			return response;
		}
	}

	@Override
	public APIResponse<Object> mergeStageRecords(String loggedUser, String stageId, String phId, String metadataIds,
			String parentID) {
		Map<String, Object> result = this.metaDataDao.mergeStageRecords(loggedUser, stageId, phId, metadataIds,
				parentID);
		APIResponse<Object> obj = new APIResponse<>();
		obj.setCode("200");
		obj.setData(result);
		obj.setStatus("Success");
		obj.setStatusMessage("Approval successful");
		return obj;

	}
}
