package com.codemantra.manage.metadata.serviceImpl;

import java.util.Arrays;
import java.util.List;

public enum ParsedCellType {
    BASIC("Basic"),
    
    TIME("Time"),
    
    ARRAY_STRING("Array<String>"),
    
    ARRAY_BOOLEAN("Array<Boolean>"),
    
    ARRAY_DOUBLE("Array<Double>"),
   
    REFERENCE("Reference"),
    
    OBJECT("Object");

    private String stringValue;

    ParsedCellType(String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    public static ParsedCellType fromString(String stringValue) {
        if (stringValue!=null) {
            for (ParsedCellType type : ParsedCellType.values()) {
                if (stringValue.equalsIgnoreCase(type.toString())) {
                    return type;
                }
            }
            if (isBasicType(stringValue))
                return BASIC;
        }

        throw new IllegalArgumentException("No constant with name " + stringValue + " found");
    }

    private static List<String> basicTypes = Arrays.asList(new String[]{"String", "Integer", "Float", "Double", "Boolean", "Basic"});

    public static boolean isBasicType(String typeName) {
        for (String basicType : basicTypes) {
            if (typeName.equalsIgnoreCase(basicType))
                return true;
        }
        return false;
    }

}
