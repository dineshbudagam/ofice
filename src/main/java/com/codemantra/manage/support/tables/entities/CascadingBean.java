/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.entities;

/**
 *
 * @author codemantra
 */
public class CascadingBean {
    public CascadingBean(){super();}
     public CascadingBean(String fieldName, Integer order, Object val )
     {
         super();
         this.fieldName = fieldName;
         this.fieldOrder = order;
         this.fieldValue = val;
     }
    private String fieldName;
    private Integer fieldOrder;
    private Object fieldValue;

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public Object getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(Object fieldValue) {
        this.fieldValue = fieldValue;
    }

    /**
     * @return the fieldOrder
     */
    public Integer getFieldOrder() {
        return fieldOrder;
    }

    /**
     * @param fieldOrder the fieldOrder to set
     */
    public void setFieldOrder(Integer fieldOrder) {
        this.fieldOrder = fieldOrder;
    }
    public int hashCode()
    {
        return (this.fieldName != null)? this.fieldName.hashCode():-1;
    }
    public boolean equals(Object obj)
    {
        if(obj != null && obj instanceof CascadingBean)
        {
            return ((CascadingBean)obj).fieldName.equals(this.fieldName);
        }
        return false;
    }
}
