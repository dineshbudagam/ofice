/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.entities;

/**
 *
 * @author codemantra
 */
public class CodeDescriptionBean {
    public CodeDescriptionBean(){}
    public CodeDescriptionBean(String code, String description)
    {
        this.code = code;
        this.description = description;
    }
    private String code;
    private String description;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
}
