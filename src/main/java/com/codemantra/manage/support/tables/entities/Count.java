/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.entities;

/**
 *
 * @author codemantra
 */
public class Count {
    private long n;
    public Count(){super();}

    /**
     * @return the n
     */
    public long getN() {
        return n;
    }

    /**
     * @param n the n to set
     */
    public void setN(long n) {
        this.n = n;
    }
}
