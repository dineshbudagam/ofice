/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.entities;

import com.codemantra.manage.support.tables.request.entities.FieldInfo;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author codemantra
 */
public class HeaderInfo implements java.io.Serializable, java.util.Comparator<HeaderInfo>{
    
    public HeaderInfo(){super();}
    public HeaderInfo(String fieldName, Boolean sortBy)
    {
    this.fieldName = fieldName;
    this.sortBy = sortBy;
    }
    
     public HeaderInfo(String headerName, Boolean sortBy, Boolean filter, 
             String dataType, String fieldName, Boolean isDisplay, Integer fieldOrder)
    {
    this.headerName = headerName;
    this.sortBy = sortBy;
    this.filter = filter;
    this.dataType = dataType;
    this.fieldName = fieldName;
    this.isDisplay = isDisplay;
    this.fieldOrder = fieldOrder;
    }
     public HeaderInfo(String headerName, Boolean sortBy, Boolean filter, 
             String dataType, String fieldName, Boolean isDisplay, Integer fieldOrder, String referencePath, 
             List<FieldInfo> defaultArray, Boolean required)
    {
    this(  headerName,   sortBy,   filter, 
               dataType,   fieldName,   isDisplay,   fieldOrder);
    this.referencePath = referencePath;
    this.fieldInfo = defaultArray;
    this.required = required;
    }
    private String headerName;
    private Boolean sortBy=false;
    private Boolean filter = true;
    private String dataType ="string";
    private String fieldName;
    private String referencePath;
    private Boolean defaultSort= false;
    private Boolean isDisplay = true;
    private Integer fieldOrder = 0;
    private Boolean isEditable = true;
    private String regex="";
    private List<FieldInfo> fieldInfo;
    private Boolean required = false;
    private Integer fieldLength;

    /**
     * @return the fieldName
     */
    public String getHeaderName() {
        return headerName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setHeaderName(String fieldName) {
        this.headerName = fieldName;
    }

    /**
     * @return the sortBy
     */
    public Boolean getSortBy() {
        return sortBy;
    }

    /**
     * @param sortBy the sortBy to set
     */
    public void setSortBy(Boolean sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * @return the filter
     */
    public Boolean getFilter() {
        return filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(Boolean filter) {
        this.filter = filter;
    }

    /**
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * @param dataType the dataType to set
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the referencePath
     */
    public String getReferencePath() {
        return referencePath;
    }

    /**
     * @param referencePath the referencePath to set
     */
    public void setReferencePath(String referencePath) {
        this.referencePath = referencePath;
    }

    /**
     * @return the defaultSort
     */
    public Boolean getDefaultSort() {
        return defaultSort;
    }

    /**
     * @param defaultSort the defaultSort to set
     */
    public void setDefaultSort(Boolean defaultSort) {
        this.defaultSort = defaultSort;
    }

    /**
     * @return the isDisplay
     */
    public Boolean getIsDisplay() {
        return isDisplay;
    }

    /**
     * @param isDisplay the isDisplay to set
     */
    public void setIsDisplay(Boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    /**
     * @return the fieldOrder
     */
    public Integer getFieldOrder() {
        return fieldOrder;
    }

    /**
     * @param fieldOrder the fieldOrder to set
     */
    public void setFieldOrder(Integer fieldOrder) {
        this.fieldOrder = fieldOrder;
    }

    /**
     * @return the isEditable
     */
    public Boolean getIsEditable() {
        return isEditable;
    }

    /**
     * @param isEditable the isEditable to set
     */
    public void setIsEditable(Boolean isEditable) {
        this.isEditable = isEditable;
    }

    public int compare(HeaderInfo h1, HeaderInfo h2)
    {
        if(h1.getFieldOrder() != null && h2.getFieldOrder() != null)
        {
            return h1.getFieldOrder() - h2.getFieldOrder();
        }
        if(h1.getFieldOrder() != null)
        {
            return h1.getFieldOrder();
        }
        if(h2.getFieldOrder() != null)
        {
            return h2.getFieldOrder();
        }
           return 0; 
    }

    /**
     * @return the fieldInfo
     */
    public List<FieldInfo> getFieldInfo() {
        return fieldInfo;
    }

    /**
     * @param fieldInfo the fieldInfo to set
     */
    public void setFieldInfo(List<FieldInfo> fieldInfo) {
        this.fieldInfo = fieldInfo;
    }

    /**
     * @return the isRequired
     */
    public Boolean getRequired() {
        return required;
    }

    /**
     * @param isRequired the isRequired to set
     */
    public void setRequired(Boolean isRequired) {
        this.required = isRequired;
    }

    /**
     * @return the regex
     */
    public String getRegex() {
        return regex;
    }

    /**
     * @param regex the regex to set
     */
    public void setRegex(String regex) {
        this.regex = regex;
    }

    /**
     * @return the fieldLength
     */
    public Integer getFieldLength() {
        return fieldLength;
    }

    /**
     * @param fieldLength the fieldLength to set
     */
    public void setFieldLength(Integer fieldLength) {
        this.fieldLength = fieldLength;
    }
}
