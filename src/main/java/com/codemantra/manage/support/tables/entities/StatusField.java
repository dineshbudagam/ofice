/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author codemantra
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatusField implements java.io.Serializable{
    public StatusField(){super();}
    public StatusField(Object key, Object value, String status){super();
    this.key = key;
    this.value = value;
    this.statusMessage = status;
    }
    private Object key;
    private Object value;
    private String statusMessage;

    /**
     * @return the key
     */
    public Object getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(Object key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * @return the statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage the statusMessage to set
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
    
}
