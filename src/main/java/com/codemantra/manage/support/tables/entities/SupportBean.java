/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.entities;

import java.util.Date;
import java.util.Map;
import org.bson.types.ObjectId;

/**
 *
 * @author codemantra
 */
public class SupportBean {
    private String _id;
    private Object Key;
    private Map<String, Object> Value;
    private Boolean isActive = true;
    private Boolean isDeleted = false;
    private Date createdOn = null;
    private Date modifiedOn = null;
    private String createdBy = null;
    private String modifiedBy = null;
    private String SupportKey = "";
    
    public SupportBean(){}
    public SupportBean(Object key){this.Key = key;}
    public SupportBean(Object key, Map<String,Object> value){
        this.Key = key;
        this.Value = value;
    }

    /**
     * @return the Key
     */
    public Object getKey() {
        return Key;
    }

    /**
     * @param Key the Key to set
     */
    public void setKey(Object Key) {
        this.Key = Key;
    }

    /**
     * @return the Value
     */
    public Map<String, Object> getValue() {
        return Value;
    }

    /**
     * @param Value the Value to set
     */
    public void setValue(Map<String, Object> Value) {
        this.Value = Value;
    }

    /**
     * @return the isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the isDeleted
     */
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted the isDeleted to set
     */
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the SupportKey
     */
    public String getSupportKey() {
        return SupportKey;
    }

    /**
     * @param SupportKey the SupportKey to set
     */
    public void setSupportKey(String SupportKey) {
        this.SupportKey = SupportKey;
    }

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(String _id) {
        this._id = _id;
    }
}
