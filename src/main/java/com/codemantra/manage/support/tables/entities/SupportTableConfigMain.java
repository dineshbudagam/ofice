/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.entities;

import com.codemantra.manage.support.tables.request.entities.FieldInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author codemantra
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SupportTableConfigMain {
    public SupportTableConfigMain(){super();}
        public SupportTableConfigMain(String fieldName){
            super();
            this.fieldName = fieldName;
        }

    private String _id;
    //private String primaryCollectionName;
    //private String secondaryCollectionName;
    //private String primaryFieldName;
    //private String secondaryFieldName;
    private String lookupName;
    private String lookupDisplayName;
    private String headerFieldName;
    private String metadataFieldName;
    private String referencePath;
    private String fieldName;
    private String displayName;
    //private String sortByFieldName;
    private Boolean isActive = null;
    private Boolean isDeleted = null;
    private Boolean canSort = null;
    private Boolean defaultSort = null;
    private Boolean isDisplay = null;
    private Date createdOn = null;
    private String createdBy = null;
    private Date modifiedOn = null;
    private String modifiedBy = null;
    //private Boolean unwind = false;
    private Boolean filter = null;
    private String dataType = null;
    private Integer displayOrderNo = null;
    private Integer excelDisplayOrder = null;
    private Boolean isCascade = null;
    
    private List<HeaderInfo>  fieldArray;
    private Map condition;
    private Map updateFields;
    private Boolean group;
    private Integer limit ;
    private Integer fontcolor=0;
    private Integer backgroundcolor=0;
    private String regex="";
//    private String excelDisplayName;
//    private String excelName;
    private String fieldDataType;
    private Boolean isEditable=true;
    private String productHierarchyId;
    private String productHierarchyName;
    private Boolean required= false;
    private Boolean isKey = false;
    private String keyFieldDataType;
    private List<FieldInfo> fieldInfo;
    private Map mappedFields;
    private String firstFormatDefaultValue;
    private String collectionNameRef;
    private Integer fieldLength;

    /**
     * @return the primaryCollectionName
     */
    /*
    public String getPrimaryCollectionName() {
        return primaryCollectionName;
    }
    */

    /**
     * @param primaryCollectionName the primaryCollectionName to set
     */
    /*
    public void setPrimaryCollectionName(String primaryCollectionName) {
        this.primaryCollectionName = primaryCollectionName;
    }
    */

    /**
     * @return the secondaryCollectionName
     */
    /*
    public String getSecondaryCollectionName() {
        return secondaryCollectionName;
    }
*/
    /**
     * @param secondaryCollectionName the secondaryCollectionName to set
     */
    /*
    public void setSecondaryCollectionName(String secondaryCollectionName) {
        this.secondaryCollectionName = secondaryCollectionName;
    }
*/
    /**
     * @return the primaryFieldName
     */
    /*
    public String getPrimaryFieldName() {
        return primaryFieldName;
    }
    */

    /**
     * @param primaryFieldName the primaryFieldName to set
     */
    /*
    public void setPrimaryFieldName(String primaryFieldName) {
        this.primaryFieldName = primaryFieldName;
    }
    */

    /**
     * @return the secondaryFieldName
     */
    /*
    public String getSecondaryFieldName() {
        return secondaryFieldName;
    }
*/
    /**
     * @param secondaryFieldName the secondaryFieldName to set
     */
    /*
    public void setSecondaryFieldName(String secondaryFieldName) {
        this.secondaryFieldName = secondaryFieldName;
    }
    */

    /**
     * @return the lookupName
     */
    public String getLookupName() {
        return lookupName;
    }

    /**
     * @param lookupName the lookupName to set
     */
    public void setLookupName(String lookupName) {
        this.lookupName = lookupName;
    }

    /**
     * @return the headerFieldName
     */
    public String getHeaderFieldName() {
        return headerFieldName;
    }

    /**
     * @param headerFieldName the headerFieldName to set
     */
    public void setHeaderFieldName(String headerFieldName) {
        this.headerFieldName = headerFieldName;
    }

    /**
     * @return the metadataFieldName
     */
    public String getMetadataFieldName() {
        return metadataFieldName;
    }

    /**
     * @param metadataFieldName the metadataFieldName to set
     */
    public void setMetadataFieldName(String metadataFieldName) {
        this.metadataFieldName = metadataFieldName;
    }

    /**
     * @return the referencePath
     */
    public String getReferencePath() {
        return referencePath;
    }

    /**
     * @param referencePath the referencePath to set
     */
    public void setReferencePath(String referencePath) {
        this.referencePath = referencePath;
    }

    /**
     * @return the isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the isDeleted
     */
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted the isDeleted to set
     */
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(String _id) {
        this._id = _id;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the sortByFieldName
     */
//    public String getSortByFieldName() {
//        return sortByFieldName;
//    }

    /**
     * @param sortByFieldName the sortByFieldName to set
     */
//    public void setSortByFieldName(String sortByFieldName) {
//        this.sortByFieldName = sortByFieldName;
//    }

    /**
     * @return the canSort
     */
    public Boolean getCanSort() {
        return canSort;
    }

    /**
     * @param canSort the canSort to set
     */
    public void setCanSort(Boolean canSort) {
        this.canSort = canSort;
    }

    /**
     * @return the defaultSort
     */
    public Boolean getDefaultSort() {
        return defaultSort;
    }

    /**
     * @param defaultSort the defaultSort to set
     */
    public void setDefaultSort(Boolean defaultSort) {
        this.defaultSort = defaultSort;
    }

    /**
     * @return the isDisplay
     */
    public Boolean getIsDisplay() {
        return isDisplay;
    }

    /**
     * @param isDisplay the isDisplay to set
     */
    public void setIsDisplay(Boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the unwind
     */
  /*  public Boolean getUnwind() {
        return unwind;
    }
*/
    /**
     * @param unwind the unwind to set
     */
    /*
    public void setUnwind(Boolean unwind) {
        this.unwind = unwind;
    }
*/
    /**
     * @return the filter
     */
    public Boolean getFilter() {
        return filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(Boolean filter) {
        this.filter = filter;
    }

    /**
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * @param dataType the dataType to set
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * @return the displayOrderNo
     */
    public Integer getDisplayOrderNo() {
        return displayOrderNo;
    }

    /**
     * @param displayOrderNo the displayOrderNo to set
     */
    public void setDisplayOrderNo(Integer displayOrderNo) {
        this.displayOrderNo = displayOrderNo;
    }

    /**
     * @return the isCascade
     */
    public Boolean getIsCascade() {
        return isCascade;
    }

    /**
     * @param isCascade the isCascade to set
     */
    public void setIsCascade(Boolean isCascade) {
        this.isCascade = isCascade;
    }

    /**
     * @return the fieldArray
     */
    public List<HeaderInfo> getFieldArray() {
        return fieldArray;
    }

    /**
     * @param fieldArray the fieldArray to set
     */
    public void setFieldArray(List<HeaderInfo> fieldArray) {
        this.fieldArray = fieldArray;
    }
    
    public int hashCode()
    {
        return (fieldName != null)? fieldName.hashCode(): 1000;
    }
    public boolean equals(Object o )
    {
        if(o instanceof SupportTableConfigMain)
        {
            if(o != null && fieldName != null)
                return ((SupportTableConfigMain) o).fieldName.equals(fieldName);
        }
        return false;
    }

    /**
     * @return the condition
     */
    public Map getCondition() {
        return condition;
    }

    /**
     * @param condition the condition to set
     */
    public void setCondition(Map condition) {
        this.condition = condition;
    }

    /**
     * @return the groupId
     */
    public Boolean getGroup() {
        return group;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroup(Boolean groupId) {
        this.group = groupId;
    }

    /**
     * @return the limit
     */
    public Integer getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * @return the updateFields
     */
    public Map getUpdateFields() {
        return updateFields;
    }

    /**
     * @param updateFields the updateFields to set
     */
    public void setUpdateFields(Map updateFields) {
        this.updateFields = updateFields;
    }

    /**
     * @return the fontcolor
     */
    public int getFontcolor() {
        return fontcolor;
    }

    /**
     * @param fontcolor the fontcolor to set
     */
    public void setFontcolor(Integer fontcolor) {
        this.fontcolor = fontcolor;
    }

    /**
     * @return the backgroundcolor
     */
    public int getBackgroundcolor() {
        return backgroundcolor;
    }

    /**
     * @param backgroundcolor the backgroundcolor to set
     */
    public void setBackgroundcolor(Integer backgroundcolor) {
        this.backgroundcolor = backgroundcolor;
    }

    /**
     * @return the regex
     */
    public String getRegex() {
        return regex;
    }

    /**
     * @param regex the regex to set
     */
    public void setRegex(String regex) {
        this.regex = regex;
    }

    /**
     * @return the excelDisplayName
     */
   /* public String getExcelDisplayName() {
        return excelDisplayName;
    }*/

    /**
     * @param excelDisplayName the excelDisplayName to set
     */
   /* public void setExcelDisplayName(String excelDisplayName) {
        this.excelDisplayName = excelDisplayName;
    }*/

    /**
     * @return the excelName
     */
    /*public String getExcelName() {
        return excelName;
    }*/

    /**
     * @param excelName the excelName to set
     */
   /* public void setExcelName(String excelName) {
        this.excelName = excelName;
    }*/

    /**
     * @return the fieldDataType
     */
    public String getFieldDataType() {
        return fieldDataType;
    }

    /**
     * @param fieldDataType the fieldDataType to set
     */
    public void setFieldDataType(String fieldDataType) {
        this.fieldDataType = fieldDataType;
    }

    /**
     * @return the isEditable
     */
    public Boolean getIsEditable() {
        return isEditable;
    }

    /**
     * @param isEditable the isEditable to set
     */
    public void setIsEditable(Boolean isEditable) {
        this.isEditable = isEditable;
    }

    /**
     * @return the productHierarchyId
     */
    public String getProductHierarchyId() {
        return productHierarchyId;
    }

    /**
     * @param productHierarchyId the productHierarchyId to set
     */
    public void setProductHierarchyId(String productHierarchyId) {
        this.productHierarchyId = productHierarchyId;
    }

    /**
     * @return the productHierarchyName
     */
    public String getProductHierarchyName() {
        return productHierarchyName;
    }

    /**
     * @param productHierarchyName the productHierarchyName to set
     */
    public void setProductHierarchyName(String productHierarchyName) {
        this.productHierarchyName = productHierarchyName;
    }

    /**
     * @return the required
     */
    public Boolean getRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(Boolean required) {
        this.required = required;
    }

    /**
     * @return the isKey
     */
    public Boolean getIsKey() {
        return isKey;
    }

    /**
     * @param isKey the isKey to set
     */
    public void setIsKey(Boolean isKey) {
        this.isKey = isKey;
    }

    /**
     * @return the keyFieldDataType
     */
    public String getKeyFieldDataType() {
        return keyFieldDataType;
    }

    /**
     * @param keyFieldDataType the keyFieldDataType to set
     */
    public void setKeyFieldDataType(String keyFieldDataType) {
        this.keyFieldDataType = keyFieldDataType;
    }
	public String getLookupDisplayName() {
		return lookupDisplayName;
	}
	public void setLookupDisplayName(String lookupDisplayName) {
		this.lookupDisplayName = lookupDisplayName;
	}

    /**
     * @return the fieldInfo
     */
    public List<FieldInfo> getFieldInfo() {
        return fieldInfo;
    }

    /**
     * @param fieldInfo the fieldInfo to set
     */
    public void setFieldInfo(List<FieldInfo> fieldInfo) {
        this.fieldInfo = fieldInfo;
    }

    /**
     * @return the mappedFields
     */
    public Map getMappedFields() {
        return mappedFields;
    }

    /**
     * @param mappedFields the mappedFields to set
     */
    public void setMappedFields(Map mappedFields) {
        this.mappedFields = mappedFields;
    }
	public String getFirstFormatDefaultValue() {
		return firstFormatDefaultValue;
	}
	public void setFirstFormatDefaultValue(String firstFormatDefaultValue) {
		this.firstFormatDefaultValue = firstFormatDefaultValue;
	}

    /**
     * @return the excelDisplayOrder
     */
    public Integer getExcelDisplayOrder() {
        return excelDisplayOrder;
    }

    /**
     * @param excelDisplayOrder the excelDisplayOrder to set
     */
    public void setExcelDisplayOrder(Integer excelDisplayOrder) {
        this.excelDisplayOrder = excelDisplayOrder;
    }

    /**
     * @return the collectionNameRef
     */
    public String getCollectionNameRef() {
        return collectionNameRef;
    }

    /**
     * @param collectionNameRef the collectionNameRef to set
     */
    public void setCollectionNameRef(String collectionNameRef) {
        this.collectionNameRef = collectionNameRef;
    }

    /**
     * @return the fieldLength
     */
    public Integer getFieldLength() {
        return fieldLength;
    }

    /**
     * @param fieldLength the fieldLength to set
     */
    public void setFieldLength(Integer fieldLength) {
        this.fieldLength = fieldLength;
    }
    
    
}
