/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.request.entities;

import java.io.Serializable;

/**
 *
 * @author codemantra
 */
public class FieldInfo implements Serializable{
    private String fieldName;
    private Object fieldValue;
    private Boolean isDefault;
    public FieldInfo(){super();}
    public FieldInfo(String fieldName)
    {
        this.fieldName = fieldName;
    }
    public FieldInfo(String fieldName, Object fieldValue)
    {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public int hashCode()
    {
        return (fieldName != null)? fieldName.hashCode():1000;
    }
    public boolean equals(Object obj)
    {
        if(obj != null && obj instanceof FieldInfo)
        {
            FieldInfo fi = (FieldInfo)obj;
            if(fi.fieldName != null &&  this.fieldName != null && fi.fieldName.equals(this.fieldName))
                return true;
        }
        return false;
    }
    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public Object getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(Object fieldValue) {
        this.fieldValue = fieldValue;
    }

    /**
     * @return the isDefault
     */
    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     * @param isDefault the isDefault to set
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

}
