/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.support.tables.request.entities;

import com.codemantra.manage.support.tables.entities.SupportTableConfigMain;
import java.util.List;
import java.util.Map;
import org.springframework.data.annotation.Transient;

/**
 *
 * @author codemantra
 */
public class SupportRequestEntity 
{
    public SupportRequestEntity(){super();}
    private String sortByField;
    private String sortByDirection;//ASC / DESC
    private String searchText;
    //private Map<String,Object> filterByFields;
    private List<FieldInfo> filterByFields;
    private String lookupName;
    private String lookupDisplayName;
     private PageInfo pageInfo;
    private String selectedFieldName;
    //private Integer skip=0;
    //private Integer limit =500; //Set as default limit
    private List<FieldInfo> ninFilterByFields;
    private Map<String,Object> selectedFields;
    private Map<String,Object> updateFields;
    private List<Map<String,Object>> multiUpdateFields;
    private List<Map<String,Object>> customUpdateFields;
    
    private Boolean includeInActive;
    private List<String> selectedFieldNames;
    private Boolean exportCheck=false;
    private String exportFormat = "XLSX";
    private String userId;
    @Transient
    private String operation;
    
    private String metadataId;
    
    private String title;
    
    private String pHid;
    
    private Boolean isActive = true;
    private Boolean isDeleted = false;
    
    @Transient
    private List<SupportTableConfigMain> configList;
    
    @Transient
    private String stageId;

    @Transient
    private String metadataIds;    
    
    @Transient
    private String parentID;
    /**
     * @return the sortByFields
     */
    public String getSortByField() {
        return sortByField;
    }

    /**
     * @param sortByField the sortByField to set
     */
    public void setSortByField(String sortByField) {
        this.sortByField = sortByField;
    }

    /**
     * @return the filterByFields
     */
    public List<FieldInfo> getFilterByFields() {
        return filterByFields;
    }

    /**
     * @param filterByFields the filterByFields to set
     */
    public void setFilterByFields(List<FieldInfo> filterByFields) {
        this.filterByFields = filterByFields;
    }


    /**
     * @return the skip
     */
  /*  public Integer getSkip() {
        return skip;
    }
*/
    /**
     * @param skip the skip to set
     */
    /*public void setSkip(Integer skip) {
        this.skip = skip;
    }
*/
    /**
     * @return the limit
     */
  /*  public Integer getLimit() {
        return limit;
    }
*/
    /**
     * @param limit the limit to set
     */
  /*  public void setLimit(Integer limit) {
        this.limit = limit;
    }
*/
    /**
     * @return the lookupName
     */
    public String getLookupName() {
        return lookupName;
    }

    /**
     * @param lookupName the lookupName to set
     */
    public void setLookupName(String lookupName) {
        this.lookupName = lookupName;
    }

    /**
     * @return the searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText the searchText to set
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    /**
     * @return the ninFilterByFields
     */
    public List<FieldInfo> getNinFilterByFields() {
        return ninFilterByFields;
    }

    /**
     * @param ninFilterByFields the ninFilterByFields to set
     */
    public void setNinFilterByFields(List<FieldInfo> ninFilterByFields) {
        this.ninFilterByFields = ninFilterByFields;
    }

    /**
     * @return the selectedFields
     */
    public Map<String,Object> getSelectedFields() {
        return selectedFields;
    }

    /**
     * @param selectedFields the selectedFields to set
     */
    public void setSelectedFields(Map<String,Object> selectedFields) {
        this.selectedFields = selectedFields;
    }

    /**
     * @return the updateFields
     */
    public Map<String,Object> getUpdateFields() {
        return updateFields;
    }

    /**
     * @param updateFields the updateFields to set
     */
    public void setUpdateFields(Map<String,Object> updateFields) {
        this.updateFields = updateFields;
    }

    /**
     * @return the pageInfo
     */
    public PageInfo getPageInfo() {
        return pageInfo;
    }

    /**
     * @param pageInfo the pageInfo to set
     */
    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    /**
     * @return the includeInActive
     */
    public Boolean getIncludeInActive() {
        return includeInActive;
    }

    /**
     * @param includeInActive the includeInActive to set
     */
    public void setIncludeInActive(Boolean includeInActive) {
        this.includeInActive = includeInActive;
    }

    /**
     * @return the sortByDirection
     */
    public String getSortByDirection() {
        return sortByDirection;
    }

    /**
     * @param sortByDirection the sortByDirection to set
     */
    public void setSortByDirection(String sortByDirection) {
        this.sortByDirection = sortByDirection;
    }

    /**
     * @return the selectedFieldName
     */
    public String getSelectedFieldName() {
        return selectedFieldName;
    }

    /**
     * @param selectedFieldName the selectedFieldName to set
     */
    public void setSelectedFieldName(String selectedFieldName) {
        this.selectedFieldName = selectedFieldName;
    }

    /**
     * @return the selectedFieldNames
     */
    public List<String> getSelectedFieldNames() {
        return selectedFieldNames;
    }

    /**
     * @param selectedFieldNames the selectedFieldNames to set
     */
    public void setSelectedFieldNames(List<String> selectedFieldNames) {
        this.selectedFieldNames = selectedFieldNames;
    }

    /**
     * @return the exportCheck
     */
    public Boolean getExportCheck() {
        return exportCheck;
    }

    /**
     * @param exportCheck the exportCheck to set
     */
    public void setExportCheck(Boolean exportCheck) {
        this.exportCheck = exportCheck;
    }

    /**
     * @return the exportFormat
     */
    public String getExportFormat() {
        return exportFormat;
    }

    /**
     * @param exportFormat the exportFormat to set
     */
    public void setExportFormat(String exportFormat) {
        this.exportFormat = exportFormat;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * @return the isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the isDeleted
     */
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted the isDeleted to set
     */
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return the multiUpdateFields
     */
    public List<Map<String,Object>> getMultiUpdateFields() {
        return multiUpdateFields;
    }

    /**
     * @param multiUpdateFields the multiUpdateFields to set
     */
    public void setMultiUpdateFields(List<Map<String,Object>> multiUpdateFields) {
        this.multiUpdateFields = multiUpdateFields;
    }

    /**
     * @return the configList
     */
    public List<SupportTableConfigMain> getConfigList() {
        return configList;
    }

    /**
     * @param configList the configList to set
     */
    public void setConfigList(List<SupportTableConfigMain> configList) {
        this.configList = configList;
    }

    /**
     * @return the lookupDisplayName
     */
    public String getLookupDisplayName() {
        return lookupDisplayName;
    }

    /**
     * @param lookupDisplayName the lookupDisplayName to set
     */
    public void setLookupDisplayName(String lookupDisplayName) {
        this.lookupDisplayName = lookupDisplayName;
    }

	public String getMetadataId() {
		return metadataId;
	}

	public void setMetadataId(String metadataId) {
		this.metadataId = metadataId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getpHid() {
		return pHid;
	}

	public void setpHid(String pHid) {
		this.pHid = pHid;
	}

    /**
     * @return the customUpdateFields
     */
    public List<Map<String,Object>> getCustomUpdateFields() {
        return customUpdateFields;
    }

    /**
     * @param customUpdateFields the customUpdateFields to set
     */
    public void setCustomUpdateFields(List<Map<String,Object>> customUpdateFields) {
        this.customUpdateFields = customUpdateFields;
    }

    /**
     * @return the metadataIds
     */
    public String getMetadataIds() {
        return metadataIds;
    }

    /**
     * @param metadataIds the metadataIds to set
     */
    public void setMetadataIds(String metadataIds) {
        this.metadataIds = metadataIds;
    }

    /**
     * @return the stageId
     */
    public String getStageId() {
        return stageId;
    }

    /**
     * @param stageId the stageId to set
     */
    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    /**
     * @return the parentID
     */
    public String getParentID() {
        return parentID;
    }

    /**
     * @param parentID the parentID to set
     */
    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

	

}
