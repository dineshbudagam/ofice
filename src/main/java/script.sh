#!/bin/bash 

export CSVXLS="$CSVORXLS";
export tailableFlag="$isTailableFlag";
export IPAddress="$IPAddress"
/home/codemantra/Programs/mongodb3.4/bin/mongo "$IPAddress"/manage -p testdev -u devtest /home/codemantra/testms/.mongorc.js "$1" --quiet |tail -n +3 > "$2" ;
noOfLines=`wc -l <"$2"`;
noOfChars=`wc -c <"$2"`;
noOfLines=`echo -n "$noOfLines"`;
noOfChars=`echo -n "$noOfChars"`;
if [ "$tailableFlag" == "true" ]; then
str=`tail -1 "$2"`; #get the last line of file
echo "$str"> "$2".log;
tail -n 1 "$2" | wc -c | xargs -I {} truncate "$2" -s -{}
cat "$2">>"$2".log
mv "$2".log "$2"
fi
if [ "$CSVXLS" == "XLSX" -a "$noOfChars" -gt 10  ]; then
temp="$2";
temp=`echo -n "${temp/\.csv/\.xlsx}"`; 
data1=`echo -n "${temp/\.xlsx/}"` ;
data1=`echo -n "${data1}" | sed -r 's/_//g'`;
mkdir "$data1";
mv "$2" "${data1}"/data1;
chmod 777 "${data1}"/data1;
"/usr/bin/ssconvert" --export-type=Gnumeric_Excel:xlsx2 "${data1}"/data1  "$temp" ;
rm -rf "${data1}";
chmod 777 "$temp";
fi

