var stageId="$stageId";
var productHierarchyId="$productHierarchyId";
var metadataId="$metadataId";
var tempMetadataId = ["$metadataId"]
var loggedUser="$loggedUser";
var parentId="$parentID";
function merge(src, dest)
{
var flag = false;

var keys1 = Object.keys(src);
var keys2 = Object.keys(dest);
var keys1Arr = [];
var keys2Arr = [];
for(var i1 = 0 ; i1<keys1.length;i1++)
{
keys1Arr[keys1[i1]]= keys1[i1];
}
for(var i1 = 0 ; i1<keys2.length;i1++)
{
keys2Arr[keys2[i1]]= keys2[i1];
}
for(var a in keys1Arr)
{
if(dest[a] == undefined)
{
dest[a] = src[a];
if(Array.isArray(dest[a]))
flag=true;
}
if(a == 'metadataId' || a == 'productHierarchyId')
{
var temp = dest[a];
if(!Array.isArray(temp))
{
   dest[a]=[];
   dest[a].push(temp);
}
dest[a].push(src[a]);

}
if(a == 'parents' && Array.isArray(src[a]))
{
   return; 
}
	if((a == 'Preflight' || a== 'Distribution') && Array.isArray(src[a]))
{
   return; 
}
if(Array.isArray(src[a]))
{
if(flag == false)
{
dest[a] = dest[a].concat(src[a]); /*Commented for the time being by M6S*/
}
flag=true;
 merge(src[a],dest[a]);
}
else if(typeof(src[a]) === 'object')
{
merge(src[a],dest[a]);
}
}
if(dest.CustomFields != undefined && dest.CustomFields.SalesRights != undefined)
{
dest.Product.SalesRights = dest.CustomFields.SalesRights;
dest.CustomFields.SalesRights = null;
}
 
return dest;
}
var Product1={};
function constructProduct(key, value, currentField) {

if(value == undefined) return false;
var refPath = currentField.referencePath;
var jsonDbPath = currentField.jsonDbPathCondition;
var fieldDataType = currentField.fieldDataType;
var res = refPath.split("."); 
var islast = false; 
 for (var i = 0; i < res.length; i++) {
var currentList = res[i];
var islast = (i == res.length - 1) ? true : false; 
if (i == 0) {
if (!Product1.hasOwnProperty(currentList)) {
Product1[currentList] = [];
}
} 
else if(i == 1) { 
if(currentField.jsonType == "List"){ 
var vaalue =[];
vaalue.push(value)
}else{ 
vaalue = value;
}
if (Product1[res[i - 1]].length == 0) { 
var setvalue = (islast) ? vaalue : [];
Product1[res[i - 1]].push({
[currentList]: setvalue
});
}
else if (Product1[res[i - 1]][0].hasOwnProperty(currentList)) {
if (islast && value) { 
Product1[res[i - 1]][0][currentList] = vaalue;
} 
} 
else if (!Product1[res[i - 1]][0].hasOwnProperty(currentList)) {
if (islast) { 
if (value) { 
Product1[res[i - 1]][0][currentList] = vaalue;
}
} else { 
Product1[res[i - 1]][0][currentList] = [];
}
} else {
} 
}else if (i == 2) {
if ((fieldDataType != undefined && fieldDataType.fieldType == "popup") || 
(currentField != undefined && currentField.isSeries == true)) {
if(currentField.jsonType == "MultiList"){ 
Product1[res[i - 2]][0][res[i - 1]].push(value[0]); 
}else{ 
var setvalue = (islast) ? value : []; 
	/* converted by m6s
angular.forEach(setvalue, function(kvalue, kkey) {
Product1[res[i - 2]][0][res[i - 1]].push(kvalue);
});
*/
var aaa = {}
for(var i1 = 0;i1<jsonDbPath.length;i1++)
{
aaa[jsonDbPath[i1].field] = jsonDbPath[i1].value;	
}
/* Converted by m6s
angular.forEach(jsonDbPath, function(jvalue, jkey) {
aaa[jvalue.field] = jvalue.value;
});
*/
aaa[currentList] = value; 
Product1[res[i - 2]][0][res[i - 1]].push(aaa);
 /*
for(var i1=0;i1<setvalue.length;i1++)
{
	Product1[res[i - 2]][0][res[i - 1]].push(setvalue[i1]);
}
 */
}
} else {
if (!jsonDbPath || jsonDbPath.length == 0) {
var setvalue = (islast) ? value : []; 
if (Product1[res[i - 2]][0][res[i - 1]].length == 0) { 
Product1[res[i - 2]][0][res[i - 1]].push({
[currentList]: setvalue
}); 
} else { 
Product1[res[i - 2]][0][res[i - 1]][currentList] = setvalue;  

}
} else {
if (islast) { 
if(currentField.metaDataFieldName == 'MainBISACCode'){ 
	/* Commented by M6S
angular.forEach(value, function(jvalue1, jkey1) { 
var aaa = {}
angular.forEach(jsonDbPath, function(jvalue, jkey) {
aaa[jvalue.field] = jvalue.value;
});
aaa[currentList] = jvalue1; 
Product1[res[i - 2]][0][res[i - 1]].push(aaa); 
});
*/
}else{ 
var aaa = {}
for(var i1 = 0;i1<jsonDbPath.length;i1++)
{
aaa[jsonDbPath[i1].field] = jsonDbPath[i1].value;	
}
/* Converted by m6s
angular.forEach(jsonDbPath, function(jvalue, jkey) {
aaa[jvalue.field] = jvalue.value;
});
*/
aaa[currentList] = value; 
Product1[res[i - 2]][0][res[i - 1]].push(aaa); 
if(currentField.metaDataFieldName == 'TitleTextValue' /*&& $scope.Subtitle != undefined */
) 
{ 
Product1[res[i - 2]][0][res[i - 1]][0]['Subtitle'] = "$scope.Subtitle" ;/*to uncomment by m6s */
}
}

} else { 
if (Product1[res[i - 2]][0][res[i - 1]].length == 0) {
Product1[res[i - 2]][0][res[i - 1]].push({
[currentList]: []
});
}
}

}
}
} else if (i == 3) {
if (fieldDataType.fieldType == "popup" || currentField.isSeries == true) { 
if(currentField.jsonType == "MultiList"){ 
Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]]=(value[0]); 
}else{ 
var setvalue = (islast) ? value : []; 
if (Product1[res[i - 3]][0][res[i - 2]].length == 0) {
Product1[res[i - 3]][0][res[i - 2]].push({
[res[i - 1]] : []
}); 
} 
/* Converted by m6s
angular.forEach(setvalue, function(kvalue, kkey) {
Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]].push(kvalue);
});
*/
for(var i1 = 0;i1<setvalue.length;i1++)
{
Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]].push(setvalue[i1]);
}
}

} else {
if (!jsonDbPath || jsonDbPath.length == 0) {
var setvalue = (islast) ? value : [];
if (Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]].length == 0) {
Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]].push({
[currentList]: setvalue
}); 
} else {
Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]][currentList] = setvalue; 
}
} else {
if (islast) { 
if(currentField.metaDataFieldName == 'MainBISACCode'){
/* To be converted by m6s 
angular.forEach(value, function(jvalue1, jkey1) {

var aaa = {}
angular.forEach(jsonDbPath, function(jvalue, jkey) {
aaa[jvalue.field] = jvalue.value;
});
aaa[currentList] = jvalue1; 
Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]].push(aaa); 
});
*/
}else{
var aaa = {}
/* Converted by m6s
angular.forEach(jsonDbPath, function(jvalue, jkey) {
aaa[jvalue.field] = jvalue.value;
});
*/
for(var i1 = 0;i1<jsonDbPath.length;i1++)
{
aaa[jsonDbPath[i1].field] = jsonDbPath[i1].value;	
}
aaa[currentList] = value; 
Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]].push(aaa);
}

} else { 
if (Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]].length == 0) {
Product1[res[i - 3]][0][res[i - 2]][0][res[i - 1]].push({
[currentList]: []
});
}
}

}
}
} else if (i == 4) {
if (fieldDataType.fieldType == "popup") {
var setvalue = (islast) ? value : [];
if (Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].length == 0) {
Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].push(setvalue);
} else {
	/* Converted by m6s 
angular.forEach(setvalue, function(kvalue, kkey) {
Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].push(kvalue);
});
*/
for(var i1 = 0;i1<setvalue.length;i1++)
{
var kvalue = setvalue[i1];
Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].push(kvalue);

}
}
} else {
if (!jsonDbPath || jsonDbPath.length == 0) {
var setvalue = (islast) ? value : [];
return false;
if (Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].length == 0) {
Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].push({
[currentList]: setvalue
});
} else {
 
Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]][currentList] = setvalue; 
}
} else {
if (islast) {
var aaa = {}
for(var i1 = 0;i1<jsonDbPath.length;i1++)
{
aaa[jsonDbPath[i1].field] = jsonDbPath[i1].value;	
}
/* Converted by m6s 
angular.forEach(jsonDbPath, function(jvalue, jkey) {
aaa[jvalue.field] = jvalue.value;
});
*/
aaa[currentList] = value;
Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].push(aaa);
} else {
/*
print(Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]]);
print(Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].length);
*/

if (Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].length == 0) {
Product1[res[i - 4]][0][res[i - 3]][0][res[i - 2]][0][res[i - 1]].push({
[currentList]: []
});
}
}

}
}
} else {

}
} 
}

var metaDataFields = [];
db.cMetadataFields.find({$and : [
{isActive : true}, {isDeleted : false},{stageId : stageId},
{productHierarchyId :  productHierarchyId} 
]}).forEach(function(doc){
metaDataFields[doc.metaDataFieldName]=doc;
});
/*
metadataId has value :! "", parentID L: "" metadataId in final_products metadataId 
metadataId has value :! "", parentID L: "VALUE" (metadataId,parentID) in final_products metadataId, if two or 
more records forEach to be done
no productHierarchyId
Logic : if metadataId 
*/ 
var mergeRecordsConfig = db.manageConfig.findOne({_id:'MergeRecordsFlag'});
var mergeFlag = mergeRecordsConfig.merge;/* This flag is for creating individual records for each hierarchy level or 
merging all levels into one - TBD*/
var levels = mergeRecordsConfig.hierarchyLevels;
 db.products.find({$and : [{metadataId : metadataId},{'CustomFields.productHierarchyId' : productHierarchyId},
{'CustomFields.parentID':parentId}]})
.forEach(function(doc){
var parents = doc.CustomFields.parents;
if(parents != null && parents.length > 0)
{
for(var i1=0;i1<parents.length;i1++)
{
tempMetadataId.push(parents[i1].metadataId);
}
}
for(var metadata in metaDataFields)
{
var prd = metaDataFields[metadata];
var value = jsonpath(doc,""+prd.jsonPath+"");
if(value != undefined && Array.isArray(value))
{
if(value[0] != undefined && value[0] instanceof Date )
 {
		value = value[0].getTime();
 value=new Date(value);
	}
else if(value != null)
{
value = value.toString();
}
else if(value == null)
{
value = value[0];
}

 if(prd.metaDataFieldName == 'isActive'||prd.metaDataFieldName == 'isDeleted')
  {
  value = (value == 'true');
}
 }
constructProduct(prd.metaDataFieldName,value,prd);

}
 
var finalProduct={"Product":(Product1.Product!= undefined)?Product1.Product[0]:{},
  "CustomFields":(Product1.CustomFields != undefined )?Product1.CustomFields[0]:{}};
var final_meta = db.final_products.findOne({'metadataId.metadataId' : {$in : tempMetadataId}} );
var metadataIds = [];
var phId = "";
var record = {};
if(final_meta != null)
{
if(final_meta.metadataId.length== levels)
{
/* Insert a new record and merge the products */
for(var i = 0;i<parents.length;i++)
{
/*
if(i == 0)
{
final_meta = parents[i];
metadataIds.push( {'productHierarchyId':parents[i].CustomFields.productHierarchyId,
'metadataId':parents[i].metadataId});
continue;
}
*/
metadataIds.push( {'productHierarchyId':parents[i].CustomFields.productHierarchyId,
'metadataId':parents[i].metadataId});
finalProduct=merge(parents[i], finalProduct);
final_meta=finalProduct;
}/*end for*/
final_meta.metadataId=(metadataIds);
final_meta._id = new ObjectId();
 final_meta.Product.LastModifiedOn = new Date();
db.final_products.save(final_meta);
}
else
{
   final_meta = merge(finalProduct , final_meta);
metadataIds=final_meta.metadataId;
if(final_meta.metadataId.findIndex(x => x.metadataId == metadataId)<0)
{
metadataIds.push( {'productHierarchyId':productHierarchyId,'metadataId':metadataId});
}
else
{
metadataIds=final_meta.metadataId;
}
final_meta.metadataId=(metadataIds);
db.final_products.save(final_meta);
}
 
}
else
{
finalProduct.metadataId=[{'metadataId' : metadataId, 'productHierarchyId' : productHierarchyId}];
 finalProduct.Product.LastModifiedOn = new Date();
db.final_products.save(finalProduct);
}
Product1={};
  })